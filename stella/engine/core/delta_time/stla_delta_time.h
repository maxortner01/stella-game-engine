#pragma once

namespace stla
{

class delta_time
{
public:

    delta_time();

    void tick();

    operator float() const;

private:

    float m_dt; // ms
    float m_last_frame;

};

} // namespace stla
