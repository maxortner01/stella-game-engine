#include "stla_delta_time.h"

#include <SDL3/SDL.h>

namespace stla
{

delta_time::delta_time() :
    m_dt(0.0f), m_last_frame(0.0f) { }

void delta_time::tick()
{
    float curr_frame = (float)SDL_GetTicks(); //(float)glfwGetTime() * 1000.0f;
    m_dt = curr_frame - m_last_frame;
    m_last_frame = curr_frame;
}

delta_time::operator float() const 
{ 
    return m_dt; 
}

} // namespace stla
