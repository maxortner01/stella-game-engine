#pragma once

#include "stla_resource_handle.h"

namespace stla
{

// source: https://godbolt.org/
template<typename T, typename RscMgr>
class resource_allocator {
public:

    using value_type     = T;
    using allocator_type = resource_allocator<T, RscMgr>;
    using pointer        = resource_handle<T, RscMgr>;
    using const_pointer  = resource_handle<T, RscMgr>;
    
    resource_allocator() = default;
    resource_allocator(const allocator_type& alloc) = default;
    template<typename U>
    resource_allocator(const resource_allocator<U, RscMgr>&) { }

    pointer allocate(size_t n) { return RscMgr::template allocate<T>(n); }
    void deallocate(pointer ptr, size_t n) { RscMgr::deallocate(ptr, n); }

    bool operator==(const allocator_type&) const { return true; }
    bool operator!=(const allocator_type&) const { return false; }
};

} // namespace name
