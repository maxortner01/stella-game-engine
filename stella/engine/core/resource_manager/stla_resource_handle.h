#pragma once

#include <cstddef>
#include <iterator>
#include <memory>
#include <functional>
#include <utility>
#include <type_traits>

namespace stla
{

template<typename T, typename RscMgr>
class resource_handle {
    T* ptr = nullptr;
    resource_handle(T* ptr, bool) : ptr(ptr) {}
public:
    using element_type      = T;
    using difference_type   = std::ptrdiff_t;
    using value_type        = element_type;
    using pointer           = element_type*;
    using reference         = element_type&;
    using iterator_category = std::random_access_iterator_tag;

    resource_handle() = default;
    resource_handle(pointer ptr) : ptr(ptr) { }
    resource_handle(const resource_handle&) = default;

    //todo make this protected/private
    pointer get_raw_pointer() { return ptr; }

    resource_handle& operator=(const resource_handle&) = default;

    static resource_handle pointer_to(element_type& r) noexcept {
        return resource_handle(std::addressof(r), true);
    }

    // allocator::pointer is convertible to allocator::const_pointer
    template<typename U = T, typename std::enable_if<std::is_const<U>::value, int>::type = 0>
    resource_handle(const resource_handle<typename std::remove_const<T>::type, RscMgr>& p) : 
        ptr(p.operator->()) { } // std::to_address(p) in C++20 

    // NullablePointer
    resource_handle(std::nullptr_t) : resource_handle() {}
    resource_handle& operator=(std::nullptr_t) {
        ptr = nullptr;
        return *this;
    }
    explicit operator bool() const { return *this != nullptr; }

    // to_address, InputIterator
    pointer operator->() const {
        return ptr;
    }

    // Iterator
    element_type& operator*() const {
        return *ptr;
    }
    resource_handle& operator++() {
        ++ptr;
        return *this;
    }
    // InputIterator
    friend bool operator==(resource_handle l, resource_handle r) {
        return l.ptr == r.ptr;
    }
    friend bool operator!=(resource_handle l, resource_handle r) {
        return !(l == r);
    }
    resource_handle operator++(int) {
        return resource_handle(ptr++, true);
    }
    // BidirectionalIterator
    resource_handle& operator--() {
        --ptr;
        return *this;
    }
    resource_handle operator--(int) {
        return resource_handle(ptr--, true);
    }
    // RandomAccessIterator
    resource_handle& operator+=(difference_type n) {
        ptr += n;
        return *this;
    }
    friend resource_handle operator+(resource_handle p, difference_type n) {
        return p += n;
    }
    friend resource_handle operator+(difference_type n, resource_handle p) {
        return p += n;
    }
    resource_handle& operator-=(difference_type n) {
        ptr -= n;
        return *this;
    }
    friend resource_handle operator-(resource_handle p, difference_type n) {
        return p -= n;
    }
    friend difference_type operator-(resource_handle a, resource_handle b) {
        return a.ptr - b.ptr;
    }
    reference operator[](difference_type n) const {
        return ptr[n];
    }
    friend bool operator<(resource_handle a, resource_handle b) {
        return std::less<pointer>(a.ptr, b.ptr);
    }
    friend bool operator> (resource_handle a, resource_handle b) { return b < a; }
    friend bool operator>=(resource_handle a, resource_handle b) { return !(a < b); }
    friend bool operator<=(resource_handle a, resource_handle b) { return !(b < a); }

    template<typename U> resource_handle(resource_handle<U, RscMgr> p) : ptr(static_cast<T*>(p.operator->())) {}
    
#if defined(_GLIBCXX_MEMORY)
    // Extra libstdc++ requirement (Since libstdc++ uses raw pointers internally and tries to implicitly cast back
    // and also casts from pointers to different types)
    resource_handle(T* ptr) : resource_handle(ptr, true) {}
    operator T* () { return ptr; }
#endif
};

// NullablePointer (Not strictly necessary because of implicit conversion from nullptr to resource_handle)
template<typename T, typename RscMgr>
bool operator==(resource_handle<T, RscMgr> p, std::nullptr_t) {
    return p == resource_handle<T, RscMgr>();
}
template<typename T, typename RscMgr>
bool operator==(std::nullptr_t, resource_handle<T, RscMgr> p) {
    return resource_handle<T, RscMgr>() == p;
}
template<typename T, typename RscMgr>
bool operator!=(resource_handle<T, RscMgr> p, std::nullptr_t) {
    return p != resource_handle<T, RscMgr>();
}
template<typename T, typename RscMgr>
bool operator!=(std::nullptr_t, resource_handle<T, RscMgr> p) {
    return resource_handle<T, RscMgr>() != p;
}

template<typename RscMgr>
class resource_handle<void, RscMgr> {
    void* ptr = nullptr;
    resource_handle(void* ptr, bool) : ptr(ptr) {}
public:
    using element_type = void;
    using pointer      = void*;

    resource_handle() = default;
    resource_handle(const resource_handle&) = default;
    template<typename T, typename std::enable_if<!std::is_const<T>::value, int>::type = 0>
    resource_handle(resource_handle<T, RscMgr> p) : ptr(static_cast<void*>(p.operator->())) {}

    resource_handle& operator=(const resource_handle&) = default;
    resource_handle& operator=(std::nullptr_t) { ptr = nullptr; return *this; }

    pointer operator->() const {
        return ptr;
    }

    // static_cast<A::pointer>(vp) == p
    template<typename T>
    explicit operator resource_handle<T, RscMgr>() {
        if (ptr == nullptr) return nullptr;
        return std::pointer_traits<resource_handle<T, RscMgr>>::pointer_to(*static_cast<T*>(ptr));
    }
};

template<typename RscMgr>
class resource_handle<const void, RscMgr> {
    const void* ptr = nullptr;
    resource_handle(const void* ptr, bool) : ptr(ptr) {}
public:
    using element_type = const void;
    using pointer      = const void*;

    resource_handle() = default;
    resource_handle(const resource_handle&) = default;
    template<typename T>
    resource_handle(resource_handle<T, RscMgr> p) : ptr(static_cast<const void*>(p.operator->())) {}

    resource_handle& operator=(const resource_handle&) = default;
    resource_handle& operator=(std::nullptr_t) { ptr = nullptr; return *this; }

    pointer operator->() const {
        return ptr;
    }

    // static_cast<A::const_pointer>(cvp) == cp
    template<typename T>
    explicit operator resource_handle<const T, RscMgr>() {
        if (ptr == nullptr) return nullptr;
        return std::pointer_traits<resource_handle<const T, RscMgr>>::pointer_to(*static_cast<const T*>(ptr));
    }
};

} // namespace stla
