#pragma once


#include "stla_resource_handle.h"

namespace stla
{

class default_resource_manager
{
public:

    template<typename T>
    using handle = resource_handle<T, default_resource_manager>;

    template<typename T>
    static handle<T> allocate(size_t n)
    {
        return handle<T>(reinterpret_cast<T*>(new std::byte[sizeof(T) * n]));
    }

    template<typename T>
    static void deallocate(handle<T>& handle, size_t n)
    {
        delete[] reinterpret_cast<std::byte*>(handle.get_raw_pointer());
    }

    template<typename T>
    static handle<T> pointer_to(T& val)
    {
        return handle<T>(&val);
    }
};
    
} // namespace stla
