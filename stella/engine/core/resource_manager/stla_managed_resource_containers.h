#pragma once

#include "stla_resource_manager.h"
#include "stla_resource_allocator.h"

#include <string>

namespace stla
{
namespace mrsc
{

template<typename RscMgr=default_resource_manager>
using string = std::basic_string<char, std::char_traits<char>, resource_allocator<char, RscMgr>>;

} // namespace mrsc
} // namespace stla
