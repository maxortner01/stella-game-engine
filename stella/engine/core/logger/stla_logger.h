#pragma once

#include <spdlog/spdlog.h>
#include "spdlog/sinks/stdout_color_sinks.h"

#include <memory>

namespace stla
{
    
class logger
{
public:

    static void init()
    {
        //thread:function:line
        //spdlog::set_pattern("[%H:%M:%S] [%t:%!():%#] [%n] %^[%l] %v%$");
        spdlog::set_pattern("%^%v%$");
        //spdlog::set_pattern("%v%");
     
        s_core_logger = spdlog::stdout_color_mt("Stella");
        s_core_logger->set_level(spdlog::level::trace);
    }

    static std::shared_ptr<spdlog::logger> get_core_logger()
    {
        return s_core_logger;
    }

    
private:

    static inline std::shared_ptr<spdlog::logger> s_core_logger;

};

} // namespace stla

#define STLA_CORE_LOG_TRACE(...)    SPDLOG_LOGGER_TRACE(stla::logger::get_core_logger(), ##__VA_ARGS__) 
#define STLA_CORE_LOG_DEBUG(...)    SPDLOG_LOGGER_DEBUG(stla::logger::get_core_logger(), ##__VA_ARGS__)
#define STLA_CORE_LOG_INFO(...)     SPDLOG_LOGGER_INFO(stla::logger::get_core_logger(), ##__VA_ARGS__)
#define STLA_CORE_LOG_WARN(...)     SPDLOG_LOGGER_WARN(stla::logger::get_core_logger(), ##__VA_ARGS__)
#define STLA_CORE_LOG_ERROR(...)    SPDLOG_LOGGER_ERROR(stla::logger::get_core_logger(), ##__VA_ARGS__)
#define STLA_CORE_LOG_CRITICAL(...) SPDLOG_LOGGER_CRITICAL(stla::logger::get_core_logger(), ##__VA_ARGS__)


