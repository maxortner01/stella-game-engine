#pragma once

#include <etl/queue_mpmc_mutex.h>

namespace stla
{

template<typename T, const size_t MAX_SIZE>
using thread_safe_queue = etl::queue_mpmc_mutex<T, MAX_SIZE>;

template<typename T>
using thread_safe_queue_4 = thread_safe_queue<T, 4>;

template<typename T>
using thread_safe_queue_8 = thread_safe_queue<T, 8>;

template<typename T>
using thread_safe_queue_16 = thread_safe_queue<T, 16>;

template<typename T>
using thread_safe_queue_32 = thread_safe_queue<T, 32>;

template<typename T>
using thread_safe_queue_64 = thread_safe_queue<T, 64>;

template<typename T>
using thread_safe_queue_2048 = thread_safe_queue<T, 2048>;

template<typename T>
using thread_safe_queue_4096 = thread_safe_queue<T, 4096>;

} // namespace stla
