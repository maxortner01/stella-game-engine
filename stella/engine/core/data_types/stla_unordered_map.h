#pragma once

#include <etl/unordered_map.h>

namespace stla
{

template<typename TKey, typename TValue>
using unordered_map_4 = etl::unordered_map<TKey, TValue, 4>;

template<typename TKey, typename TValue>
using unordered_map_8 = etl::unordered_map<TKey, TValue, 8>;

template<typename TKey, typename TValue>
using unordered_map_16 = etl::unordered_map<TKey, TValue, 16>;

template<typename TKey, typename TValue>
using unordered_map_32 = etl::unordered_map<TKey, TValue, 32>;

template<typename TKey, typename TValue>
using unordered_map_64 = etl::unordered_map<TKey, TValue, 64>;

template<typename TKey, typename TValue>
using unordered_map_2048 = etl::unordered_map<TKey, TValue, 2048>;

template<typename TKey, typename TValue>
using unordered_map_4096 = etl::unordered_map<TKey, TValue, 4096>;

template<typename TKey, typename TValue, const size_t MAX_SIZE>
using unordered_map = etl::unordered_map<TKey, TValue, MAX_SIZE>;

} // namespace stla
