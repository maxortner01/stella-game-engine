#pragma once

#include <etl/vector.h>

#include <stdint.h>

namespace stla
{

template<typename T>
using vector_4 = etl::vector<T, 4>;

template<typename T>
using vector_8 = etl::vector<T, 8>;

template<typename T>
using vector_16 = etl::vector<T, 16>;

template<typename T>
using vector_32 = etl::vector<T, 32>;

template<typename T>
using vector_64 = etl::vector<T, 64>;

template<typename T>
using vector_2048 = etl::vector<T, 2048>;

template<typename T>
using vector_4096 = etl::vector<T, 4096>;

template<typename T, const size_t MAX_SIZE>
using vector = etl::vector<T, MAX_SIZE>;

} // namespace stla
