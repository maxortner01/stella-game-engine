#pragma once

#include <etl/list.h>

#include <stdint.h>

namespace stla
{

template<typename T>
using list_4 = etl::list<T, 4>;

template<typename T>
using list_8 = etl::list<T, 8>;

template<typename T>
using list_16 = etl::list<T, 16>;

template<typename T>
using list_32 = etl::list<T, 32>;

template<typename T>
using list_64 = etl::list<T, 64>;

template<typename T>
using list_2048 = etl::list<T, 2048>;

template<typename T>
using list_4096 = etl::list<T, 4096>;

template<typename T, const size_t MAX_SIZE>
using list = etl::list<T, MAX_SIZE>;

} // namespace stla
