#pragma once

#include <etl/string.h>

#include <stdint.h>

namespace stla
{

using string_4 = etl::string<4>;

using string_8 = etl::string<8>;

using string_16 = etl::string<16>;

using string_32 = etl::string<32>;

using string_64 = etl::string<64>;

using string_2048 = etl::string<2048>;

using string_4096 = etl::string<4096>;

template<const size_t MAX_SIZE>
using string = etl::string<MAX_SIZE>;

} // namespace stla
