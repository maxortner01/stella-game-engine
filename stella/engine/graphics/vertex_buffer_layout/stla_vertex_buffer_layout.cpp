#include "stla_vertex_buffer_layout.h"
#include "stla_assert.h"

#include <glm/glm.hpp>

#include <algorithm>

namespace stla
{

const char *get_vertex_buffer_attribute_type_string(vertex_buffer_attribute_type type)
{
    switch (type)
    {
        case vertex_buffer_attribute_type::f:  return "float";
        case vertex_buffer_attribute_type::v2: return "vec2";
        case vertex_buffer_attribute_type::v3: return "vec3";
        case vertex_buffer_attribute_type::v4: return "vec4";
        //case vertex_buffer_attribute_type::m4: return "mat4";
        default: return "unknown";
    }
}

static uint32_t get_attribute_type_size(vertex_buffer_attribute_type type)
{
    switch (type)
    {
        case vertex_buffer_attribute_type::f:  return sizeof(float);
        case vertex_buffer_attribute_type::v2: return sizeof(glm::vec2);
        case vertex_buffer_attribute_type::v3: return sizeof(glm::vec3);
        case vertex_buffer_attribute_type::v4: return sizeof(glm::vec4);
        //case vertex_buffer_attribute_type::m4: return sizeof(glm::mat4);
        default: return 0;
    }
}

uint32_t get_attribute_type_count(vertex_buffer_attribute_type type)
{
    switch (type)
    {
        case vertex_buffer_attribute_type::f:  return 1;
        case vertex_buffer_attribute_type::v2: return 2;
        case vertex_buffer_attribute_type::v3: return 3;
        case vertex_buffer_attribute_type::v4: return 4;
        //case vertex_buffer_attribute_type::m4: return 16;
        default: return 0;
    }
}

uint32_t vertex_buffer_attribute::get_size() const
{
    switch (m_type)
    {
        case vertex_buffer_attribute_type::f:  return sizeof(float);
        case vertex_buffer_attribute_type::v2: return sizeof(glm::vec2);
        case vertex_buffer_attribute_type::v3: return sizeof(glm::vec3);
        case vertex_buffer_attribute_type::v4: return sizeof(glm::vec4);
        //case vertex_buffer_attribute_type::m4: return sizeof(glm::mat4);
        default: return 0;
    }
}
 
uint32_t vertex_buffer_attribute::get_count() const
{
    switch (m_type)
    {
        case vertex_buffer_attribute_type::f:  return 1;
        case vertex_buffer_attribute_type::v2: return 2;
        case vertex_buffer_attribute_type::v3: return 3;
        case vertex_buffer_attribute_type::v4: return 4;
        //case vertex_buffer_attribute_type::m4: return 16;
        default: return 0;
    }
}

vertex_buffer_layout &vertex_buffer_layout::add_attribute(vertex_buffer_attribute_type type, uint32_t index, bool instanced)
{
    if (m_layout.find(index) != m_layout.end()) {
        STLA_ASSERT(0, "vertex buffer layout already contains attribute at index %d", index);
        return *this;
    }

    m_layout[index] = vertex_buffer_attribute(type, index, instanced);
    m_element_count = 0; // reset element count
    m_stride        = 0; // reset stride

    for (auto& [key, value] : m_layout) {
        m_element_count += value.get_count();
    }

    for (auto& [key, value] : m_layout) {
        m_stride += value.get_size();
    }

    return *this;
}

vertex_buffer_attribute_array vertex_buffer_layout::get_attribute_array() const
{
    vertex_buffer_attribute_array attributes;

    for (auto &[key, attrib] : m_layout) {
        attributes.push_back(attrib);
    }

    std::sort(attributes.begin(), attributes.end(), [](vertex_buffer_attribute a1, vertex_buffer_attribute a2){
        return a1.m_index < a2.m_index;
    });

    return attributes;
}

uint32_t vertex_buffer_layout::get_stride() const
{
    return m_stride;
}

uint32_t vertex_buffer_layout::get_element_count() const
{
    return m_element_count;
}

uint32_t vertex_buffer_layout::get_attribute_count() const
{
    return m_layout.size();
}

bool vertex_buffer_layout::operator==(const vertex_buffer_layout &obj) const
{
    if (m_layout.size() != obj.m_layout.size()) {
        return false;
    }

    for (const auto &[binding_loc, binding] : m_layout) {
        if (obj.m_layout.find(binding_loc) != obj.m_layout.end()) {
            if (binding != obj.m_layout.at(binding_loc)) {
                return false;
            }
        }
        else {
            return false;
        }
    }

    return true;
}

} // namespace stla
