#pragma once

#include "stla_vector.h"
#include "stla_unordered_map.h"

#include <vulkan/vulkan.h>

#include <stdint.h>

namespace stla
{

enum class vertex_buffer_attribute_type
{
    f, v2, v3, v4, m4
};

const char *get_vertex_buffer_attribute_type_string(vertex_buffer_attribute_type type);

struct vertex_buffer_attribute
{
    
    uint32_t m_index{ 0 };
    bool     m_instanced{ false };
    vertex_buffer_attribute_type m_type{ vertex_buffer_attribute_type::f };

    vertex_buffer_attribute() = default;
    vertex_buffer_attribute(vertex_buffer_attribute_type type, uint32_t index, bool instanced) : m_type(type), m_index(index), m_instanced(instanced) { }
    uint32_t get_size() const;
    uint32_t get_count() const;

    bool operator==(const vertex_buffer_attribute &obj) const 
    {
        return m_index == obj.m_index && m_instanced == obj.m_instanced && m_type == obj.m_type;
    }
};

static inline const uint32_t MAX_VERTEX_BUFFER_ATTRIBUTES = 16;
using vertex_buffer_attribute_array = vector_16<vertex_buffer_attribute>;

class vertex_buffer_layout
{
public:

    vertex_buffer_layout() = default;
    vertex_buffer_layout &add_attribute(vertex_buffer_attribute_type type, uint32_t index, bool instanced);
    vertex_buffer_attribute_array get_attribute_array() const;
    uint32_t get_stride() const;
    uint32_t get_element_count() const;
    uint32_t get_attribute_count() const;
    void clear() { m_layout.clear(); }

    bool operator==(const vertex_buffer_layout &obj) const;

private:

    unordered_map_16<uint32_t, vertex_buffer_attribute> m_layout;
    uint32_t m_element_count{ 0 };
    uint32_t m_stride{ 0 };
};
    
} // namespace stla
