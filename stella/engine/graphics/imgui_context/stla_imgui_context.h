#pragma once

#include <SDL3/SDL.h>
#include <vulkan/vulkan.h>
#include <imgui_impl_vulkan.h>

#include <stdint.h>

namespace stla
{

namespace vk
{

class state;

} // namespace vk

class imgui_context
{
public:

    bool init(SDL_Window *p_window_handle, vk::state *p_state);
    void shutdown();
    bool process_event(SDL_Event &e);
    void start_frame();
    void end_frame();
    void on_update();

    bool set_font(const char *p_font_path, float size);

private:

    static inline uint32_t MIN_IMAGE_COUNT = 2;

    void init_theme();
    bool frame_render(ImDrawData *p_draw_data);
    bool frame_present();

    VkDescriptorPool         m_imgui_desc_pool{ nullptr };
    vk::state *           mp_state{ nullptr };
    SDL_Window *             mp_window{ nullptr };
    ImGui_ImplVulkanH_Window m_window_data{ };
    bool                     m_swap_chain_rebuild{ false };
};

} // namespace stla
