#include "stla_imgui_context.h"
#include "vk_state.h"
#include "vk_common.h"
#include "imgui.h"
#include "imgui_impl_sdl3.h"
#include "imgui_impl_vulkan.h"

#include <imgui.h>
#include <vulkan/vulkan.h>
#include <SDL3/SDL.h>

#include <stdint.h>

namespace stla
{

bool imgui_context::init(SDL_Window *p_window_handle, vk::state *p_state)
{
    if (mp_state != nullptr) {
        return false;
    }

    if (p_window_handle == nullptr || p_state == nullptr) {
        return false;
    }

    mp_state = p_state;
    mp_window   = p_window_handle;

    m_window_data.Surface = mp_state->m_surface;

    // Create descriptor pool
    {
        VkDescriptorPoolSize pool_sizes[] =
        {
            { VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
            { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
            { VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
            { VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
        };
        VkDescriptorPoolCreateInfo pool_info = {};
        pool_info.sType         = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        pool_info.flags         = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
        pool_info.maxSets       = 1000 * IM_ARRAYSIZE(pool_sizes);
        pool_info.poolSizeCount = (uint32_t)IM_ARRAYSIZE(pool_sizes);
        pool_info.pPoolSizes    = pool_sizes;
        VK_ASSERT(vkCreateDescriptorPool(
            mp_state->m_device, &pool_info, nullptr, &m_imgui_desc_pool));
    }

    // Setup Vulkan Window
    // Select Surface Format
    const VkFormat request_surface_image_format[] = { 
        VK_FORMAT_B8G8R8A8_UNORM, 
        VK_FORMAT_R8G8B8A8_UNORM, 
        VK_FORMAT_B8G8R8_UNORM, 
        VK_FORMAT_R8G8B8_UNORM 
    };
    
    const VkColorSpaceKHR request_surface_color_space = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
    
    m_window_data.SurfaceFormat = ImGui_ImplVulkanH_SelectSurfaceFormat(
        mp_state->m_physical_device, m_window_data.Surface, request_surface_image_format, (size_t)IM_ARRAYSIZE(request_surface_image_format), request_surface_color_space);

    VkPresentModeKHR present_modes[] = { VK_PRESENT_MODE_FIFO_KHR };

    m_window_data.PresentMode = ImGui_ImplVulkanH_SelectPresentMode(
        mp_state->m_physical_device, m_window_data.Surface, &present_modes[0], IM_ARRAYSIZE(present_modes));

    int width, height;
    SDL_GetWindowSizeInPixels(p_window_handle, &width, &height);

    ImGui_ImplVulkanH_CreateOrResizeWindow(
        mp_state->m_instance, 
        mp_state->m_physical_device, 
        mp_state->m_device, 
        &m_window_data, 
        mp_state->m_graphics_queue.queue_family, 
        nullptr, 
        width, height, 
        MIN_IMAGE_COUNT
    );

    // Setup ImGui 
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    ImGui::StyleColorsDark();

    ImGui_ImplSDL3_InitForVulkan(p_window_handle);
    ImGui_ImplVulkan_InitInfo init_info = {};
    init_info.Instance        = mp_state->m_instance;
    init_info.PhysicalDevice  = mp_state->m_physical_device;
    init_info.Device          = mp_state->m_device;
    init_info.QueueFamily     = mp_state->m_graphics_queue.queue_family;
    init_info.Queue           = mp_state->m_graphics_queue.queue;
    init_info.PipelineCache   = nullptr;
    init_info.DescriptorPool  = m_imgui_desc_pool;
    init_info.Subpass         = 0;
    init_info.MinImageCount   = MIN_IMAGE_COUNT;
    init_info.ImageCount      = m_window_data.ImageCount;
    init_info.MSAASamples     = VK_SAMPLE_COUNT_1_BIT;
    init_info.Allocator       = nullptr;
    init_info.CheckVkResultFn = nullptr;
    ImGui_ImplVulkan_Init(&init_info, m_window_data.RenderPass);

    // Upload Fonts
    {
        // Use any command queue
        VkCommandPool command_pool     = m_window_data.Frames[m_window_data.FrameIndex].CommandPool;
        VkCommandBuffer command_buffer = m_window_data.Frames[m_window_data.FrameIndex].CommandBuffer;

        VK_ASSERT(vkResetCommandPool(mp_state->m_device, command_pool, 0));

        VkCommandBufferBeginInfo begin_info = {};
        begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        begin_info.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        VK_ASSERT(vkBeginCommandBuffer(command_buffer, &begin_info));

        ImGui_ImplVulkan_CreateFontsTexture(command_buffer);

        VkSubmitInfo end_info = { };
        end_info.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        end_info.commandBufferCount = 1;
        end_info.pCommandBuffers    = &command_buffer;

        VK_ASSERT(vkEndCommandBuffer(command_buffer));
        VK_ASSERT(vkQueueSubmit(mp_state->m_graphics_queue.queue, 1, &end_info, VK_NULL_HANDLE));

        VK_ASSERT(vkDeviceWaitIdle(mp_state->m_device));
        ImGui_ImplVulkan_DestroyFontUploadObjects();
    }

    init_theme();

    return true;
}

void imgui_context::shutdown()
{
    if (mp_state == nullptr) {
        return;
    }

    VK_ASSERT(vkDeviceWaitIdle(mp_state->m_device));

    //todo validation error with custom font, image/imageview/memory not destroyed
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.Fonts->Clear();
    io.Fonts->ClearFonts();
    io.Fonts->ClearInputData();
    io.Fonts->ClearTexData();

    ImGui_ImplVulkan_Shutdown();
    ImGui_ImplSDL3_Shutdown();
    ImGui::DestroyContext();

    mp_state->m_surface = nullptr; //todo handle this better, gets deleted by imgui, set to null so state doesn't delete

    ImGui_ImplVulkanH_DestroyWindow(mp_state->m_instance, mp_state->m_device, &m_window_data, nullptr);

    mp_state->push_deletor([imgui_desc_pool = m_imgui_desc_pool](vk::state &state) {
        vkDestroyDescriptorPool(state.get_device_handle(), imgui_desc_pool, nullptr);
    });

    mp_state = nullptr;
}

bool imgui_context::process_event(SDL_Event &e)
{
    return ImGui_ImplSDL3_ProcessEvent(&e);
}

void imgui_context::init_theme()
{
    auto& colors = ImGui::GetStyle().Colors;
	colors[ImGuiCol_WindowBg] = ImVec4{ 0.1f, 0.105f, 0.11f, 1.0f };

	// Headers
	colors[ImGuiCol_Header]        = ImVec4{ 0.2f, 0.205f, 0.21f, 1.0f };
	colors[ImGuiCol_HeaderHovered] = ImVec4{ 0.3f, 0.305f, 0.31f, 1.0f };
	colors[ImGuiCol_HeaderActive]  = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };
	
	// Buttons
	colors[ImGuiCol_Button]        = ImVec4{ 0.2f, 0.205f, 0.21f, 1.0f };
	colors[ImGuiCol_ButtonHovered] = ImVec4{ 0.3f, 0.305f, 0.31f, 1.0f };
	colors[ImGuiCol_ButtonActive]  = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };

	// Frame BG
	colors[ImGuiCol_FrameBg]        = ImVec4{ 0.2f, 0.205f, 0.21f, 1.0f };
	colors[ImGuiCol_FrameBgHovered] = ImVec4{ 0.3f, 0.305f, 0.31f, 1.0f };
	colors[ImGuiCol_FrameBgActive]  = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };

	// Tabs
	colors[ImGuiCol_Tab]                = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };
	colors[ImGuiCol_TabHovered]         = ImVec4{ 0.38f, 0.3805f, 0.381f, 1.0f };
	colors[ImGuiCol_TabActive]          = ImVec4{ 0.28f, 0.2805f, 0.281f, 1.0f };
	colors[ImGuiCol_TabUnfocused]       = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };
	colors[ImGuiCol_TabUnfocusedActive] = ImVec4{ 0.2f, 0.205f, 0.21f, 1.0f };

	// Title
	colors[ImGuiCol_TitleBg]          = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };
	colors[ImGuiCol_TitleBgActive]    = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };
	colors[ImGuiCol_TitleBgCollapsed] = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };
}

void imgui_context::start_frame()
{
    // Start the Dear ImGui frame
    ImGui_ImplVulkan_NewFrame();
    ImGui_ImplSDL3_NewFrame();
    ImGui::NewFrame();
    ImGui::DockSpaceOverViewport(ImGui::GetMainViewport(), ImGuiDockNodeFlags_PassthruCentralNode);
    
    //ImGui::ShowDemoWindow();
}

void imgui_context::end_frame()
{
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    // Rendering
    ImGui::Render();
    ImDrawData *p_draw_data = ImGui::GetDrawData();
    const bool is_minimized = (p_draw_data->DisplaySize.x <= 0.0f || p_draw_data->DisplaySize.y <= 0.0f);
    if (!is_minimized)
    {
        m_window_data.ClearValue.color.float32[0] = clear_color.x * clear_color.w;
        m_window_data.ClearValue.color.float32[1] = clear_color.y * clear_color.w;
        m_window_data.ClearValue.color.float32[2] = clear_color.z * clear_color.w;
        m_window_data.ClearValue.color.float32[3] = clear_color.w;
        
        frame_render(p_draw_data);
        frame_present();
    }
}

bool imgui_context::frame_render(ImDrawData *p_draw_data)
{
    VkSemaphore image_acquired_semaphore  = m_window_data.FrameSemaphores[m_window_data.SemaphoreIndex].ImageAcquiredSemaphore;
    VkSemaphore render_complete_semaphore = m_window_data.FrameSemaphores[m_window_data.SemaphoreIndex].RenderCompleteSemaphore;
    VkResult res = vkAcquireNextImageKHR(mp_state->m_device, m_window_data.Swapchain, UINT64_MAX, image_acquired_semaphore, VK_NULL_HANDLE, &m_window_data.FrameIndex);
    if (res == VK_ERROR_OUT_OF_DATE_KHR || res == VK_SUBOPTIMAL_KHR) {
        m_swap_chain_rebuild = true;
        return false;
    }
    VK_ASSERT(res);

    ImGui_ImplVulkanH_Frame *p_frame_data = &m_window_data.Frames[m_window_data.FrameIndex];
    VK_ASSERT(vkWaitForFences(mp_state->m_device, 1, &p_frame_data->Fence, VK_TRUE, UINT64_MAX)); 
    VK_ASSERT(vkResetFences(mp_state->m_device, 1, &p_frame_data->Fence));

    
    VK_ASSERT(vkResetCommandPool(mp_state->m_device, p_frame_data->CommandPool, 0));
    VkCommandBufferBeginInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    info.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    VK_ASSERT(vkBeginCommandBuffer(p_frame_data->CommandBuffer, &info));
    
    VkRenderPassBeginInfo render_pass_info    = { };
    render_pass_info.sType                    = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    render_pass_info.renderPass               = m_window_data.RenderPass;
    render_pass_info.framebuffer              = p_frame_data->Framebuffer;
    render_pass_info.renderArea.extent.width  = m_window_data.Width;
    render_pass_info.renderArea.extent.height = m_window_data.Height;
    render_pass_info.clearValueCount          = 1;
    render_pass_info.pClearValues             = &m_window_data.ClearValue;
    vkCmdBeginRenderPass(p_frame_data->CommandBuffer, &render_pass_info, VK_SUBPASS_CONTENTS_INLINE);

    // Record dear imgui primitives into command buffer
    ImGui_ImplVulkan_RenderDrawData(p_draw_data, p_frame_data->CommandBuffer);

    // Submit command buffer
    vkCmdEndRenderPass(p_frame_data->CommandBuffer);
    VkPipelineStageFlags wait_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    
    VkSubmitInfo submit_info  = { };
    submit_info.sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.waitSemaphoreCount   = 1;
    submit_info.pWaitSemaphores      = &image_acquired_semaphore;
    submit_info.pWaitDstStageMask    = &wait_stage;
    submit_info.commandBufferCount   = 1;
    submit_info.pCommandBuffers      = &p_frame_data->CommandBuffer;
    submit_info.signalSemaphoreCount = 1;
    submit_info.pSignalSemaphores    = &render_complete_semaphore;

    VK_ASSERT(vkEndCommandBuffer(p_frame_data->CommandBuffer));
    VK_ASSERT(vkQueueSubmit(mp_state->m_graphics_queue.queue, 1, &submit_info, p_frame_data->Fence));

    return true;
}

bool imgui_context::frame_present()
{
    if (m_swap_chain_rebuild) {
        return false;
    }

    VkSemaphore render_complete_semaphore = m_window_data.FrameSemaphores[m_window_data.SemaphoreIndex].RenderCompleteSemaphore;
    VkPresentInfoKHR info   = { };
    info.sType              = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    info.waitSemaphoreCount = 1;
    info.pWaitSemaphores    = &render_complete_semaphore;
    info.swapchainCount     = 1;
    info.pSwapchains        = &m_window_data.Swapchain;
    info.pImageIndices      = &m_window_data.FrameIndex;

    VkResult res = vkQueuePresentKHR(mp_state->m_graphics_queue.queue, &info);
    if (res == VK_ERROR_OUT_OF_DATE_KHR || res == VK_SUBOPTIMAL_KHR) {
        m_swap_chain_rebuild = true;
        return false;
    }
    VK_ASSERT(res);
    m_window_data.SemaphoreIndex = (m_window_data.SemaphoreIndex + 1) % m_window_data.ImageCount; // Now we can use the next set of semaphores
    return true;
}

void imgui_context::on_update()
{
    if (m_swap_chain_rebuild)
    {
        int width, height;
        SDL_GetWindowSize(mp_window, &width, &height);
        if (width > 0 && height > 0)
        {
            ImGui_ImplVulkan_SetMinImageCount(MIN_IMAGE_COUNT);
            ImGui_ImplVulkanH_CreateOrResizeWindow(
                mp_state->m_instance, 
                mp_state->m_physical_device, 
                mp_state->m_device, 
                &m_window_data, 
                mp_state->m_graphics_queue.queue_family, 
                nullptr, 
                width, height, 
                MIN_IMAGE_COUNT
            );
            m_window_data.FrameIndex = 0;
            m_swap_chain_rebuild = false;
        }
    }
}

bool imgui_context::set_font(const char *p_font_path, float size)
{
    if (p_font_path == nullptr) {
        return false;
    }

    ImGuiIO& io = ImGui::GetIO(); (void)io;
   // io.Fonts->ClearFonts();
    io.Fonts->AddFontFromFileTTF(p_font_path, size);
    io.FontDefault = io.Fonts->AddFontFromFileTTF(p_font_path, size);

    // Use any command queue
    VkCommandPool command_pool     = m_window_data.Frames[m_window_data.FrameIndex].CommandPool;
    VkCommandBuffer command_buffer = m_window_data.Frames[m_window_data.FrameIndex].CommandBuffer;

    VK_ASSERT(vkResetCommandPool(mp_state->m_device, command_pool, 0));

    VkCommandBufferBeginInfo begin_info = {};
    begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin_info.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    VK_ASSERT(vkBeginCommandBuffer(command_buffer, &begin_info));

    ImGui_ImplVulkan_CreateFontsTexture(command_buffer);

    VkSubmitInfo end_info = { };
    end_info.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    end_info.commandBufferCount = 1;
    end_info.pCommandBuffers    = &command_buffer;

    VK_ASSERT(vkEndCommandBuffer(command_buffer));
    VK_ASSERT(vkQueueSubmit(mp_state->m_graphics_queue.queue, 1, &end_info, VK_NULL_HANDLE));

    VK_ASSERT(vkDeviceWaitIdle(mp_state->m_device));
    ImGui_ImplVulkan_DestroyFontUploadObjects();

    return true;
}

} // namespace stla
