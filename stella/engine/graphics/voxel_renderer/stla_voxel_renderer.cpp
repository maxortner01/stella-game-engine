#include "stla_voxel_renderer.h"
#include "stla_assert.h"
#include "stla_geometry.h"
#include "stla_utils.h"
#include "stla_logger.h"

#include "vk_state.h"

#include <stdlib.h>

#include <filesystem>

namespace stla
{

voxel_structure_grid_writer::voxel_structure_grid_writer(voxel_structure_grid_handle grid_handle, voxel_renderer &renderer) :
    m_renderer(renderer), m_handle(grid_handle)
{
    m_dims_with_padding = renderer.get_structure_grid_dimensions(m_handle) + voxel_renderer::VOXEL_DIM_PADDING;
    m_voxel_data.resize(m_dims_with_padding.x * m_dims_with_padding.y * m_dims_with_padding.z);
    m_voxel_material_data.resize(m_dims_with_padding.x * m_dims_with_padding.y * m_dims_with_padding.z);
    m_renderer.read_structure_grid_data(m_handle, m_voxel_data.size() * sizeof(int), m_voxel_data.data());
    m_renderer.read_material_grid_data(m_handle, m_voxel_material_data.size() * sizeof(voxel_material_data), m_voxel_material_data.data());
}

voxel_structure_grid_writer::~voxel_structure_grid_writer()
{
    m_voxel_data.clear();
}

bool voxel_structure_grid_writer::set_active(uint32_t x, uint32_t y, uint32_t z)
{
    glm::uvec3 index_with_padding = get_index_with_padding_applied(x, y, z);
    if (glm::any(glm::greaterThanEqual(index_with_padding, m_dims_with_padding))) {
        STLA_CORE_LOG_ERROR("Index ({0} {1} {2}) out of bounds", x, y, z);
        return false;
    }
    uint32_t linear_index = flatten(index_with_padding.x, index_with_padding.y, index_with_padding.z, m_dims_with_padding.x, m_dims_with_padding.z);
    STLA_ASSERT_RELEASE(linear_index < m_voxel_data.size(), "linear index (%d) out of bounds (expected<%d)", linear_index, m_voxel_data.size());

    m_voxel_data[linear_index] = 1;

    return true;
}

bool voxel_structure_grid_writer::set_all_active()
{
    const glm::uvec3 dims = m_renderer.get_structure_grid_dimensions(m_handle);   
    for (int i = 0; i < dims.x; i++) {
        for (int j = 0; j < dims.y; j++) {
            for (int k = 0; k < dims.z; k++) {
                if (!set_active(i, j, k)) {
                    return false;
                }
            }
        }
    }
    return true;
}

bool voxel_structure_grid_writer::set_inactive(uint32_t x, uint32_t y, uint32_t z)
{
    glm::uvec3 index_with_padding = get_index_with_padding_applied(x, y, z);
    if (glm::any(glm::greaterThanEqual(index_with_padding, m_dims_with_padding))) {
        STLA_CORE_LOG_INFO("Index ({0} {1} {2}) out of bounds", x, y, z);
        return false;
    }

    uint32_t linear_index = flatten(index_with_padding.x, index_with_padding.y, index_with_padding.z, m_dims_with_padding.x, m_dims_with_padding.z);
    STLA_ASSERT_RELEASE(linear_index < m_voxel_data.size(), "linear index (%d) out of bounds (expected<%d)", linear_index, m_voxel_data.size());

    m_voxel_data[linear_index] = 0;
    
    return true;
}

bool voxel_structure_grid_writer::set_all_inactive()
{
    const glm::uvec3 dims = m_renderer.get_structure_grid_dimensions(m_handle);   
    for (int i = 0; i < dims.x; i++) {
        for (int j = 0; j < dims.y; j++) {
            for (int k = 0; k < dims.z; k++) {
                if (!set_inactive(i, j, k)) {
                    return false;
                }
            }
        }
    }
    return true;
}

bool voxel_structure_grid_writer::set_color(uint32_t x, uint32_t y, uint32_t z, const glm::vec4 &color)
{
    glm::uvec3 index_with_padding = get_index_with_padding_applied(x, y, z);
    if (glm::any(glm::greaterThanEqual(index_with_padding, m_dims_with_padding))) {
        STLA_CORE_LOG_ERROR("Index ({0} {1} {2}) out of bounds", x, y, z);
        return false;
    }

    uint32_t linear_index = flatten(index_with_padding.x, index_with_padding.y, index_with_padding.z, m_dims_with_padding.x, m_dims_with_padding.z);
    STLA_ASSERT_RELEASE(linear_index < m_voxel_material_data.size(), "linear index (%d) out of bounds (expected<%d)", linear_index, m_voxel_material_data.size());

    m_voxel_material_data[linear_index].color = color;
}

bool voxel_structure_grid_writer::commit()
{
    if (!m_renderer.write_structure_grid_data(m_handle, m_voxel_data.size() * sizeof(int), m_voxel_data.data())) {
        return false;
    }

    return m_renderer.write_material_grid_data(m_handle, m_voxel_material_data.size() * sizeof(voxel_material_data), m_voxel_material_data.data());
}

glm::uvec3 voxel_structure_grid_writer::get_dims() const 
{ 
    return m_renderer.get_structure_grid_dimensions(m_handle); 
}

glm::uvec3 voxel_structure_grid_writer::get_index_with_padding_applied(uint32_t x, uint32_t y, uint32_t z) const
{
    return glm::uvec3(x, y, z) + voxel_renderer::VOXEL_DIM_PADDING / 2;
}

bool voxel_renderer::create(vk::state &state, const glm::uvec2 &resolution)
{
    mp_state = &state;

    srand((unsigned)time(NULL));

    //todo check validity
    m_scene_data.resolution  = resolution;
    m_render_fence           = state.create_fence();
    m_compute_command_pool   = state.create_compute_command_pool();
    m_compute_command_buffer = m_compute_command_pool.acquire_command_buffer();

    m_voxel_structure_grid_descriptors.resize(MAX_VOXEL_GRIDS);

    m_compute_pipeline_context = state.create_compute_pipeline_context_from_file(STLA_VOXEL_RENDERER_COMPUTE_SHADER_PATH);
    m_descriptor_set           = state.alloc_descriptor_set(
        m_compute_pipeline_context.get_descriptor_set_layout_container().get_layout(vk::descriptor_set_loc::zero));

    // Create buffers
    m_scene_data_device                       = state.create_buffer(get_binding(compute_shader_binding::scene_data));
    m_pixel_buffer_device                     = state.create_buffer(resolution.x * resolution.y * sizeof(glm::vec4), get_binding(compute_shader_binding::pixel_buffer));
    m_voxel_structure_grid_descriptors_device = state.create_buffer(MAX_VOXEL_GRIDS * sizeof(voxel_structure_grid_descriptor), get_binding(compute_shader_binding::structure_descriptors), m_voxel_structure_grid_descriptors.data());
    m_voxel_structure_data_device             = state.create_buffer(MAX_VOXEL_DATA, get_binding(compute_shader_binding::structure_data));//, m_voxel_structure_data.data());
    m_voxel_material_data_device              = state.create_buffer(MAX_VOXEL_DATA, get_binding(compute_shader_binding::material_data));
    m_aabb_buffer_device                      = state.create_buffer(MAX_BLAS_COUNT * sizeof(aabb_3d), get_binding(compute_shader_binding::aabb_buffer));
    
    m_tlas.build(state);

    // Bind buffers
    m_descriptor_set.bind(get_binding(compute_shader_binding::scene_data), m_scene_data_device);
    m_descriptor_set.bind(get_binding(compute_shader_binding::pixel_buffer), m_pixel_buffer_device);
    m_descriptor_set.bind(get_binding(compute_shader_binding::structure_descriptors), m_voxel_structure_grid_descriptors_device);
    m_descriptor_set.bind(get_binding(compute_shader_binding::structure_data), m_voxel_structure_data_device);
    m_descriptor_set.bind(get_binding(compute_shader_binding::material_data), m_voxel_material_data_device);
    m_descriptor_set.bind(get_binding(compute_shader_binding::tlas), m_tlas);
    m_descriptor_set.bind(get_binding(compute_shader_binding::aabb_buffer), m_aabb_buffer_device);

    // Init structure context
    return true;
}

void voxel_renderer::destroy()
{
    STLA_ASSERT(m_render_fence.wait(1000), "voxel_renderer render fence timeout");
    m_compute_command_pool.return_command_buffer(m_compute_command_buffer);
    m_compute_command_pool.destroy();
    m_render_fence.destroy();
    m_scene_data_device.destroy();
    m_pixel_buffer_device.destroy();
    m_voxel_structure_grid_descriptors_device.destroy();
    m_voxel_structure_data_device.destroy();
    m_voxel_material_data_device.destroy();
    m_aabb_buffer_device.destroy();
    m_compute_pipeline_context.destroy();
    m_compute_shader.destroy();
    for (auto &blas : m_blases) {
        blas.destroy();
    }
    m_tlas.destroy();
    mp_state = nullptr;
}

void voxel_renderer::resize(const glm::uvec2 &resolution)
{
    m_pixel_buffer_device.destroy();
    const vk::descriptor_set_layout_container& desc_set_layout_container = m_compute_pipeline_context.get_descriptor_set_layout_container();
    m_scene_data.resolution   = resolution;
    auto pixel_buffer_binding = desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 1);
    m_pixel_buffer_device     = mp_state->create_buffer(resolution.x * resolution.y * sizeof(glm::vec4), pixel_buffer_binding);
    m_descriptor_set.bind(pixel_buffer_binding, m_pixel_buffer_device);
}

void voxel_renderer::on_render(vk::semaphore *p_signal_semaphore)
{
    STLA_ASSERT(m_render_fence.wait(1000), "voxel_renderer render fence timeout");
    m_render_fence.reset();

    m_scene_data.random_seed = rand();
    STLA_ASSERT(m_scene_data_device.copy_from(sizeof(m_scene_data), &m_scene_data), 
        "voxel_renderer failed to update scene data device buffer");

    m_compute_command_buffer.begin();
    if (p_signal_semaphore) {
        m_compute_command_buffer.push_signal_semaphore(*p_signal_semaphore);
    }

    m_compute_command_buffer.bind_pipeline(m_compute_pipeline_context.get_pipeline());
    m_compute_command_buffer.bind_descriptor_set(m_compute_pipeline_context.get_pipeline(), m_descriptor_set);
    m_compute_command_buffer.dispatch(
        (m_scene_data.resolution.x + WORKGROUP_WIDTH - 1) / WORKGROUP_WIDTH,
        (m_scene_data.resolution.y + WORKGROUP_HEIGHT - 1) / WORKGROUP_HEIGHT, 1);

    m_compute_command_buffer.end(m_render_fence);

    mp_state->submit_command_buffer(m_compute_command_buffer);
}

voxel_structure_grid_handle voxel_renderer::create_structure_grid(const glm::uvec3 &dims, float voxel_size)
{
    voxel_structure_grid_descriptor *p_descriptor = acquire_structure_grid_descriptor();

    if (p_descriptor == nullptr) {
        return nullptr;
    }

    // Allocate memory
    glm::uvec3 dims_with_padding       = dims + glm::uvec3(VOXEL_DIM_PADDING);
    uint32_t allocation_start          = m_next_structure_allocation_offset;
    uint32_t allocation_end            = allocation_start + (dims_with_padding.x * dims_with_padding.y * dims_with_padding.z);// * sizeof(uint32_t));
    m_next_structure_allocation_offset = allocation_end;
    STLA_ASSERT_RELEASE((allocation_end * sizeof(uint32_t)) < MAX_VOXEL_DATA, "voxel_renderer out of structure data");

    uint32_t material_allocation_start = m_next_material_allocation_offset;
    uint32_t material_allocation_end   = material_allocation_start + (dims_with_padding.x * dims_with_padding.y * dims_with_padding.z);
    m_next_material_allocation_offset  = material_allocation_end;
    STLA_ASSERT_RELEASE((m_next_material_allocation_offset * sizeof(voxel_material_data)) < MAX_VOXEL_DATA, "voxel_renderer out of material data");

    // Create AABB
    const glm::vec3 aabb_max = (glm::vec3(dims_with_padding) * voxel_size) / 2.0f;
    const glm::vec3 aabb_min = -aabb_max;
    aabb_3d aabb(aabb_min.x, aabb_min.y, aabb_min.z, aabb_max.x, aabb_max.y, aabb_max.z);
    m_aabbs.push_back(aabb);
    m_aabb_buffer_device.copy_from(m_aabbs.size() * sizeof(aabb_3d), m_aabbs.data());

    // Create BLAS and update TLAS
    vk::blas blas;
    STLA_ASSERT_RELEASE(blas.create_from_aabbs(m_aabb_buffer_device, 1, *mp_state, m_aabb_buffer_device.get_size() - sizeof(aabb_3d)), 
        "voxel_renderer unable to create blas");
    m_blases.push_back(blas);

    // Update Descriptor
    p_descriptor->dimensions       = dims;
    p_descriptor->structure_offset = allocation_start;
    p_descriptor->material_offset  = material_allocation_start;
    p_descriptor->voxel_size       = voxel_size;
    p_descriptor->in_use           = true;
    p_descriptor->blas_id          = m_blases.size() - 1;

    m_voxel_structure_grid_descriptors_device.copy_from(m_voxel_structure_grid_descriptors.size() * sizeof(voxel_structure_grid_descriptor), m_voxel_structure_grid_descriptors.data());

    return (voxel_structure_grid_handle)p_descriptor;
}

glm::uvec3 voxel_renderer::get_structure_grid_dimensions(voxel_structure_grid_handle grid_handle) const
{
    auto p_descriptor = structure_grid_handle_to_descriptor_ptr(grid_handle);
    return p_descriptor->dimensions;
}

float voxel_renderer::get_structure_grid_voxel_size(voxel_structure_grid_handle grid_handle) const
{
    auto p_descriptor = structure_grid_handle_to_descriptor_ptr(grid_handle);
    return p_descriptor->voxel_size;        
}

bool voxel_renderer::write_structure_grid_data(voxel_structure_grid_handle grid_handle, uint32_t size, const void *p_src)
{
    if (p_src == nullptr) {
        return false;
    }
    auto p_descriptor = structure_grid_handle_to_descriptor_ptr(grid_handle);
    return m_voxel_structure_data_device.copy_from(size, p_src, p_descriptor->structure_offset * sizeof(uint32_t));
}

bool voxel_renderer::read_structure_grid_data(voxel_structure_grid_handle grid_handle, uint32_t size, void *p_dest)
{
    if (p_dest == nullptr) {
        return false;
    }

    auto p_descriptor = structure_grid_handle_to_descriptor_ptr(grid_handle);
    return m_voxel_structure_data_device.copy_to(size, p_dest, p_descriptor->structure_offset * sizeof(uint32_t));
}

bool voxel_renderer::write_material_grid_data(voxel_structure_grid_handle grid_handle, uint32_t size, const void *p_src)
{
    if (p_src == nullptr) {
        return false;
    }
    auto p_descriptor = structure_grid_handle_to_descriptor_ptr(grid_handle);
    return m_voxel_material_data_device.copy_from(size, p_src, p_descriptor->material_offset * sizeof(voxel_material_data));
}

bool voxel_renderer::read_material_grid_data(voxel_structure_grid_handle grid_handle, uint32_t size, void *p_dest)
{
    if (p_dest == nullptr) {
        return false;
    }

    auto p_descriptor = structure_grid_handle_to_descriptor_ptr(grid_handle);
    return m_voxel_material_data_device.copy_to(size, p_dest, p_descriptor->material_offset * sizeof(voxel_material_data));
}

voxel_renderer::voxel_structure_grid_descriptor *voxel_renderer::structure_grid_handle_to_descriptor_ptr(voxel_structure_grid_handle grid_handle) const
{
    STLA_ASSERT_RELEASE(grid_handle != nullptr, "grid_handle == nullptr");

    voxel_structure_grid_descriptor *p_descriptor = (voxel_structure_grid_descriptor *)grid_handle; 
    STLA_ASSERT_RELEASE(
        p_descriptor >= &m_voxel_structure_grid_descriptors.front() && p_descriptor <= &m_voxel_structure_grid_descriptors.back(),
        "invalid structure grid handle");

    return p_descriptor;
}

voxel_renderer::voxel_structure_grid_descriptor *voxel_renderer::acquire_structure_grid_descriptor() 
{
    // Acquire descriptor
    for (voxel_structure_grid_descriptor &descriptor : m_voxel_structure_grid_descriptors) {
        if (!descriptor.in_use) {
            return &descriptor;
        }
    }
    return nullptr;
}

vk::descriptor_set_binding voxel_renderer::get_binding(compute_shader_binding binding) const
{
    const vk::descriptor_set_layout_container &desc_set_layout_container = m_compute_pipeline_context.get_descriptor_set_layout_container();

    switch (binding)
    {
        case compute_shader_binding::scene_data:            return desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 0);
        case compute_shader_binding::pixel_buffer:          return desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 1);
        case compute_shader_binding::structure_descriptors: return desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 2);
        case compute_shader_binding::structure_data:        return desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 3);
        case compute_shader_binding::material_data:         return desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 4);
        case compute_shader_binding::tlas:                  return desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 5);
        case compute_shader_binding::aabb_buffer:           return desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 6);
    }
}

} // namespace stla
