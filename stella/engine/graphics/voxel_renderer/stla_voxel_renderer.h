#pragma once

#include "vk_shader.h"
#include "vk_pipeline_context.h"
#include "vk_buffer.h"
#include "vk_descriptors.h"
#include "vk_sync.h"
#include "vk_command_pool.h"
#include "vk_command_buffer.h"
#include "vk_raytrace.h"

#include "stla_geometry.h"
#include "stla_utils.h"

#include <glm/glm.hpp>

#include <vector>

namespace stla
{

class voxel_renderer_debug_panel;

namespace vk
{
class state;
} // namespace vk

using voxel_structure_grid_handle = void *;
using voxel_material_grid_handle  = void *;

class voxel_renderer;

struct voxel_material_data
{
    glm::vec4 color;
};

class voxel_structure_grid_writer
{
public:

    voxel_structure_grid_writer(voxel_structure_grid_handle grid_handle, voxel_renderer &renderer);
    ~voxel_structure_grid_writer();

    bool set_active(uint32_t x, uint32_t y, uint32_t z);
    bool set_all_active();
    bool set_inactive(uint32_t x, uint32_t y, uint32_t z);
    bool set_all_inactive();
    bool set_color(uint32_t x, uint32_t y, uint32_t z, const glm::vec4 &color);
    bool commit();
    glm::uvec3 get_dims() const;
    glm::uvec3 get_dims_with_padding() const { return m_dims_with_padding; }

    //tmp
    std::vector<int> &get_voxel_data() { return m_voxel_data; }

private:

    glm::uvec3 get_index_with_padding_applied(uint32_t x, uint32_t y, uint32_t z) const;

    voxel_renderer &                 m_renderer;
    voxel_structure_grid_handle      m_handle{ nullptr };
    glm::uvec3                       m_dims_with_padding;
    std::vector<int>                 m_voxel_data;
    std::vector<voxel_material_data> m_voxel_material_data;
};

class voxel_structure_volume
{
public:

    voxel_structure_volume() = default;
    voxel_structure_volume(const voxel_structure_volume&) = default;

private:

    std::vector<voxel_structure_grid_handle> m_sub_volumes;
};

class voxel_renderer
{
public:

    static inline const uint32_t WORKGROUP_WIDTH   = 16;
    static inline const uint32_t WORKGROUP_HEIGHT  = 8;
    static inline const uint32_t MAX_VOXEL_GRIDS   = 4096;
    static inline const uint32_t MAX_VOXEL_DATA    = ONE_GB * 1;
    static inline const uint32_t VOXEL_DIM_PADDING = 2;
    static inline const uint32_t MAX_BLAS_COUNT    = 1024;

    enum class render_type
    {
        standard = 0,
        pixel_gradient,
        solid_red,
        solid_green,
        solid_blue,
        debug_grid,
        debug_grid_bb
    };

    static inline const char * const s_render_type_string[] = {
        "standard",
        "pixel_gradient",
        "solid_red",
        "solid_green",
        "solid_blue",
        "debug_grid",
        "debug_grid_bb"
    };

    voxel_renderer() = default;
    ~voxel_renderer() = default;

    bool create(vk::state &state, const glm::uvec2 &resolution);
    void destroy();

    void set_render_type(render_type type) { m_scene_data.render_type = (int)type; }
    void set_samples(uint32_t samples) { m_scene_data.samples = samples; }
    void set_ray_depth(uint32_t depth) { m_scene_data.max_ray_depth = depth; }
    void set_t_min(float t) { m_scene_data.t_min = t; }
    void set_t_max(float t) { m_scene_data.t_max = t; }
    void set_debug_background_color(const glm::vec4 &color) { m_scene_data.debug_background_color = color; }
    void set_color_divisor(float divisor) { m_scene_data.color_divisor = divisor; }

    void resize(const glm::uvec2 &resolution);
    void on_render(vk::semaphore *p_signal_semaphore=nullptr);
    void set_camera_position(const glm::vec3& position) { m_scene_data.camera_position = glm::vec4(position, 0.0f); }
    const vk::buffer &get_pixel_buffer() const { return m_pixel_buffer_device; }
    glm::uvec2 get_resolution() const { return m_scene_data.resolution; }

    voxel_structure_grid_handle create_structure_grid(const glm::uvec3 &dims, float voxel_size);
    glm::uvec3 get_structure_grid_dimensions(voxel_structure_grid_handle grid_handle) const;
    float get_structure_grid_voxel_size(voxel_structure_grid_handle grid_handle) const;
    bool write_structure_grid_data(voxel_structure_grid_handle grid_handle, uint32_t size, const void *p_src);
    bool read_structure_grid_data(voxel_structure_grid_handle grid_handle, uint32_t size, void *p_dest);
    bool write_material_grid_data(voxel_structure_grid_handle grid_handle, uint32_t size, const void *p_src);
    bool read_material_grid_data(voxel_structure_grid_handle grid_handle, uint32_t size, void *p_dest);

private:

    enum class compute_shader_binding
    {
        scene_data,
        pixel_buffer,
        structure_descriptors,
        structure_data,
        material_data,
        tlas,
        aabb_buffer
    };

    struct scene_data
    {
        glm::vec4  camera_position = glm::vec4(-5.0f, 10.0f, 22.0f, 0.0f);
        glm::vec4  debug_background_color = glm::vec4(0.2, 0.2, 0.2, 1.0);
        glm::uvec2 resolution = glm::uvec2(0);
        int        random_seed{ 0 };
        int        render_type{ (int)render_type::debug_grid };
        int        samples{ 1 };
        int        max_ray_depth{ 1 };
        float      t_max{ 10000.0f };
        float      t_min{ 0.001f };
        int        grid_id{ 0 };
        float      color_divisor{ 1.0f };
    };

    struct voxel_structure_grid_descriptor
    {
        glm::uvec3 dimensions;
        bool       in_use;
        uint32_t   structure_offset;
        uint32_t   material_offset;
        float      voxel_size;
        uint32_t   blas_id;
    };

    vk::descriptor_set_binding get_binding(compute_shader_binding binding) const;
    voxel_structure_grid_descriptor *structure_grid_handle_to_descriptor_ptr(voxel_structure_grid_handle grid_handle) const;
    voxel_structure_grid_descriptor *acquire_structure_grid_descriptor();

    vk::state *                  mp_state{ nullptr };
    vk::fence                    m_render_fence;
    vk::compute_command_pool     m_compute_command_pool;
    vk::compute_command_buffer   m_compute_command_buffer;
    vk::shader                   m_compute_shader;
    vk::compute_pipeline_context m_compute_pipeline_context;
    vk::descriptor_set           m_descriptor_set;
    scene_data                   m_scene_data;
    vk::buffer                   m_scene_data_device;                       // set = 0, binding = 0, scene_data
    vk::buffer                   m_pixel_buffer_device;                     // set = 0, binding = 1, vec4[]
    vk::buffer                   m_voxel_structure_grid_descriptors_device; // set = 0, binding = 2, voxel_structure_grid_descriptor[]
    vk::buffer                   m_voxel_structure_data_device;             // set = 0, binding = 3, int[]
    vk::buffer                   m_voxel_material_data_device;              // set = 0, binding = 4, int[]
    vk::tlas                     m_tlas;                                    // set = 0, binding = 5, accelerationStructureEXT
    vk::buffer                   m_aabb_buffer_device;                      // set = 0, binding = 6, float[] 

    std::vector<voxel_structure_grid_descriptor> m_voxel_structure_grid_descriptors;
    uint32_t m_next_structure_allocation_offset{ 0 };
    uint32_t m_next_material_allocation_offset{ 0 };

    std::vector<aabb_3d>  m_aabbs;
    std::vector<vk::blas> m_blases;

    friend class voxel_renderer_debug_panel;
};
    
} // namespace stla
