[shader(compute)]
#version 460
#extension GL_EXT_ray_query : require
#extension GL_EXT_debug_printf : require

layout (local_size_x = 16, local_size_y = 8, local_size_z = 1) in;

#define RENDER_TYPE_STANDARD       (0)
#define RENDER_TYPE_PIXEL_GRADIENT (1)
#define RENDER_TYPE_SOLID_RED      (2)
#define RENDER_TYPE_SOLID_GREEN    (3)
#define RENDER_TYPE_SOLID_BLUE     (4)
#define RENDER_TYPE_DEBUG_GRID     (5)
#define RENDER_TYPE_DEBUG_GRID_BB  (6)

#define FLATTEN(_x, _y, _z, _w, _d) ((_d) * ((_y) * (_w) + (_x)) + (_z)) 

#define VOXEL_DIM_PADDING (2) 

//todo padding and alignment, portability

struct voxel_structure_grid_descriptor_t
{
    uvec3 dimensions;
    bool  in_use;
    int   structure_offset;
    int   material_offset;
    float voxel_size;
    int   blas_id; // aabb id
};

struct voxel_material_t
{
    vec4 color;
};

struct aabb_t
{
    vec3 min;
    vec3 max;
};

struct ray_t
{
    vec3 origin;
    vec3 direction;
};

struct dda_result_t
{
    bool   hit;
    ivec3  position;
    vec3   normal;
    aabb_t voxel_aabb;
};

struct voxel_ray_query_t
{
    bool  hit;
    float t;
    ivec3 voxel_position;
    vec3  normal;
    vec2  uv;
};

layout (set = 0, binding = 0) uniform scene_data_block 
{
    vec4  g_camera_position;
    vec4  g_debug_background_color;
    uvec2 g_resolution;
    int   g_random_seed;
    int   g_render_type;
    int   g_samples;
    int   g_max_ray_depth;
    float g_t_max;
    float g_t_min;
    int   g_grid_id;
    float g_color_divisor;
};

layout (set = 0, binding = 1) buffer pixel_buffer_block
{
    vec4 g_pixels[];
};

layout (set = 0, binding = 2) buffer voxel_structure_grid_descriptor_block
{
    voxel_structure_grid_descriptor_t g_voxel_structure_grid_descriptors[];
};

layout (set = 0, binding = 3) buffer voxel_structure_data_block
{
    int g_voxel_structure_data[];
};

layout (set = 0, binding = 4) buffer voxel_material_data_block
{
    voxel_material_t g_voxel_material_data[];
};

layout (set = 0, binding = 5) uniform accelerationStructureEXT tlas;

layout (set = 0, binding = 6) buffer aabb_block
{
    float g_aabb_buffer[];
};

void print(voxel_structure_grid_descriptor_t desc);
void print(aabb_t aabb);

bool voxel_is_active(uint desc_id, ivec3 voxel_index);
vec4 get_voxel_color(uint desc_id, ivec3 voxel_index);

aabb_t new_aabb(vec3 min, vec3 max);

aabb_t get_aabb(uint geometry_id);
voxel_structure_grid_descriptor_t get_structure_descriptor(uint id);

ray_t new_ray(vec3 o, vec3 d);
vec3  ray_at(ray_t r, float t);
float ray_aabb_intersection(ray_t ray, vec3 aabb_min, vec3 aabb_max, float max_t);
bool  ray_aabb_intersects(ray_t r, aabb_t aabb, float t_max);

dda_result_t new_dda_result();
dda_result_t new_dda_result(bool hit, ivec3 position, vec3 normal, aabb_t voxel_aabb);
dda_result_t dda_query(ray_t ray, float intersection, uint desc_id);

voxel_ray_query_t new_voxel_ray_query(float max_t);
voxel_ray_query_t voxel_ray_query_hit(ray_t ray, float min_t, float max_t);

ray_t camera_get_ray(vec3 camera_position);

vec3 render_scene(uint random_state);
vec3 render_debug_grid();
vec3 render_debug_grid_bb();

uint step_random(uint random_state);
float step_and_output_random_float(inout uint random_state);

void main()
{
    const uvec2 pixel      = gl_GlobalInvocationID.xy;
    const uint pixel_index = g_resolution.x * pixel.y + pixel.x;
    vec3 pixel_color       = vec3(1.0, 0.0, 1.0);

    if ((pixel.x >= g_resolution.x) || (pixel.y >= g_resolution.y)) {
        return;
    }

    uint random_state = g_random_seed * g_resolution.x * pixel.y + pixel.x;  // Initial seed

    switch (g_render_type) {
        case RENDER_TYPE_STANDARD: 
            pixel_color = render_scene(random_state);
            break;
        case RENDER_TYPE_PIXEL_GRADIENT: 
            pixel_color = vec3(float(pixel.x) / g_resolution.x, float(pixel.y) / g_resolution.y, 0.0);
            break;
        case RENDER_TYPE_SOLID_RED:
            pixel_color = vec3(1.0, 0.0, 0.0);
            break;
        case RENDER_TYPE_SOLID_GREEN:
            pixel_color = vec3(0.0, 1.0, 0.0);
            break;
        case RENDER_TYPE_SOLID_BLUE:
            pixel_color = vec3(0.0, 0.0, 1.0);
            break;
        case RENDER_TYPE_DEBUG_GRID:
            pixel_color = render_debug_grid();
            break;
        case RENDER_TYPE_DEBUG_GRID_BB:
            pixel_color = render_debug_grid_bb(); 
            break;
    }
    
    g_pixels[pixel_index] = vec4(pixel_color * 1.0 / g_color_divisor, 1.0);
}

void print(voxel_structure_grid_descriptor_t desc)
{
    debugPrintfEXT("voxel_structure_grid_descriptor_t: dims = %d, %d, %d  in_use = %d  structure_offset = %d  voxel_size = %f  blas_id = %d", 
        desc.dimensions.x, desc.dimensions.y, desc.dimensions.z, desc.in_use, desc.structure_offset, desc.voxel_size, desc.blas_id);
}

void print(aabb_t aabb)
{
    debugPrintfEXT("aabb: min = %f, %f, %f  max = %f, %f, %f", aabb.min.x, aabb.min.y, aabb.min.z, aabb.max.x, aabb.max.y, aabb.max.z);
}

aabb_t new_aabb(vec3 min, vec3 max)
{
    aabb_t aabb;
    aabb.min = min;
    aabb.max = max;
    return aabb;
}

aabb_t get_aabb(uint geometry_id)
{
    const uint aabb_struct_size = 6; // 6 floats
    const uint aabb_start       = geometry_id * aabb_struct_size;

    if (aabb_start >= g_aabb_buffer.length()) {
        debugPrintfEXT("[WARN] AABB index (%d) out of bounds (aabb_buffer_length=%d)", geometry_id, g_aabb_buffer.length());
        return new_aabb(vec3(0.0), vec3(0.0));
    }

    aabb_t aabb;
    aabb.min = vec3(g_aabb_buffer[aabb_start + 0], g_aabb_buffer[aabb_start + 1], g_aabb_buffer[aabb_start + 2]);
    aabb.max = vec3(g_aabb_buffer[aabb_start + 3], g_aabb_buffer[aabb_start + 4], g_aabb_buffer[aabb_start + 5]);
    
    if (aabb.min == aabb.max || any(greaterThan(aabb.min, aabb.max))) {
        debugPrintfEXT("[WARN] Invalid aabb at index (%d) (min=%f %f %f) (max=%f %f %f)", 
            aabb_start, aabb.min.x, aabb.min.y, aabb.min.z, aabb.max.x, aabb.max.y, aabb.max.z);
    }
    
    return aabb;
}

voxel_structure_grid_descriptor_t get_structure_descriptor(uint id)
{
    if (id >= g_voxel_structure_grid_descriptors.length()) {
        debugPrintfEXT("[ERROR] Structure Descriptor (%d) out of bounds", id);
        id = 0;
    }

    return g_voxel_structure_grid_descriptors[id];
}

ray_t new_ray(vec3 o, vec3 d)
{
    ray_t r;
    r.origin    = o;
    r.direction = normalize(d);
    return r;
}

vec3 ray_at(ray_t r, float t)
{
    return r.origin + (r.direction * t);
}

float ray_aabb_intersection(ray_t ray, aabb_t aabb, float max_t)
{
    // Inside aabb
    if (all(greaterThanEqual(ray.origin, aabb.min)) && all(lessThanEqual(ray.origin, aabb.max))) {
        return 0.0;
    }

    float tmin = -max_t;
    float tmax =  max_t;

    for (int i = 0; i < 3; i++) {
        float t1 = (aabb.max[i] - ray.origin[i]) / ray.direction[i];
        float t2 = (aabb.min[i] - ray.origin[i]) / ray.direction[i];

        tmin = max(tmin, min(t1, t2));
        tmax = min(tmax, max(t1, t2));
    }

    return tmin;
}

bool ray_aabb_intersects(ray_t r, aabb_t aabb, float t_max) {
    float tmin = -t_max;
    float tmax =  t_max;

    for (int i = 0; i < 3; ++i) {
        float t1 = (aabb.min[i] - r.origin[i]) / r.direction[i];
        float t2 = (aabb.max[i] - r.origin[i]) / r.direction[i];

        tmin = max(tmin, min(t1, t2));
        tmax = min(tmax, max(t1, t2));
    }

    return tmax > max(tmin, 0.0);
}

dda_result_t new_dda_result()
{
    dda_result_t result;
    result.hit        = false;
    result.position   = ivec3(-1.0);
    result.normal     = vec3(0.0);
    result.voxel_aabb = new_aabb(vec3(0.0), vec3(0.0));
    return result;
}

dda_result_t new_dda_result(bool hit, ivec3 position, vec3 normal, aabb_t voxel_aabb)
{
    dda_result_t result;
    result.hit        = hit;
    result.position   = position;
    result.normal     = normal;
    result.voxel_aabb = voxel_aabb;
    return result;
}

dda_result_t dda_query(ray_t ray, float intersection, uint desc_id)
{
    voxel_structure_grid_descriptor_t desc = get_structure_descriptor(desc_id);
    uvec3 dims  = desc.dimensions + uvec3(VOXEL_DIM_PADDING);
    aabb_t aabb = get_aabb(desc.blas_id);
    ivec3 voxel_position = ivec3(-1);

    const vec3 aabb_object_intersection = ray_at(ray, intersection);
    ray.origin = aabb_object_intersection;
    vec3 ray_start = ray.origin;

    // to -> [0.0, 1.0]
    ray_start = (ray_start - aabb.min) / (aabb.max - aabb.min);

    // [0.0, volume_size]
    ray_start *= vec3(dims);

    vec3 dda_ray_position  = ray_start;
    vec3 dda_ray_direction = ray.direction;
    ivec3 dda_map_position = ivec3(floor(dda_ray_position + 0.));

    vec3 dda_delta_distance = abs(vec3(length(dda_ray_direction)) / dda_ray_direction);        
    ivec3 dda_ray_step      = ivec3(sign(dda_ray_direction));
    vec3 dda_side_distance  = (sign(dda_ray_direction) * (vec3(dda_map_position) - dda_ray_position) + (sign(dda_ray_direction) * 0.5) + 0.5) * dda_delta_distance; 
    
    bvec3 dda_hit_mask = bvec3(false);

    for (int i = 0; i < max(max(dims.x, dims.y), dims.z); i++) {
        if (any(greaterThanEqual(dda_map_position, dims))) {
            return new_dda_result();
        }

        if (any(lessThan(dda_map_position, vec3(0.0)))) {
            return new_dda_result();
        }
                   
        if (voxel_is_active(desc_id, dda_map_position)) { 
            vec3 aabb_min = ((vec3(dda_map_position) / vec3(dims)) * (aabb.max - aabb.min)) + aabb.min;
            return new_dda_result(
                true, 
                dda_map_position, 
                clamp(vec3(dda_hit_mask), 0.0, 1.0), 
                new_aabb(aabb_min, aabb_min + desc.voxel_size)
            ); 
        }

        dda_hit_mask = lessThanEqual(dda_side_distance.xyz, min(dda_side_distance.yzx, dda_side_distance.zxy)); 
        dda_side_distance += vec3(dda_hit_mask) * dda_delta_distance;
        dda_map_position += ivec3(vec3(dda_hit_mask)) * dda_ray_step;
    }

    return new_dda_result();
}


voxel_ray_query_t new_voxel_ray_query(float max_t)
{
    voxel_ray_query_t query;
    query.hit            = false;
    query.t              = max_t;
    query.voxel_position = ivec3(-1);
    query.normal         = vec3(0.0);
    query.uv             = vec2(0.0);
    return query;
}

voxel_ray_query_t voxel_ray_query_hit(ray_t ray, float min_t, float max_t)
{
    //todo pull from buffers
    //const ivec3 voxel_dims = ivec3(DIM);
    //const vec3 voxel_size  = vec3(0.03125);

    voxel_ray_query_t voxel_ray_query = new_voxel_ray_query(max_t);
    /*
    rayQueryEXT ray_query;
    rayQueryInitializeEXT(ray_query, tlas, 0, 0xFF, ray.origin, min_t, ray.direction, max_t);

    while (rayQueryProceedEXT(ray_query)) { 
        ray_t object_ray = new_ray(
            rayQueryGetIntersectionObjectRayOriginEXT(ray_query, false),
            rayQueryGetIntersectionObjectRayDirectionEXT(ray_query, false)
        );

        // instance_descriptor_t instance_desc = get_instance_descriptor(rayQueryGetIntersectionInstanceIdEXT(ray_query, false));

        int geometry_index = rayQueryGetIntersectionGeometryIndexEXT(ray_query, false);
        aabb_t aabb        = get_aabb(geometry_index);

        float aabb_intersection = ray_aabb_intersection(object_ray, aabb, max_t) + 0.0001; // push into volume a bit

        if (aabb_intersection < voxel_ray_query.t) {
            dda_result_t dda_result = dda_query(object_ray, aabb_intersection, aabb, voxel_dims);
            if (dda_result.hit) {
                aabb_t voxel_aabb;
                voxel_aabb.min  = ((vec3(dda_result.position) / vec3(voxel_dims)) * (aabb.max - aabb.min)) + aabb.min;
                voxel_aabb.max  = voxel_aabb.min + voxel_size;
                float voxel_t   = ray_aabb_intersection(object_ray, voxel_aabb, max_t);

                if (voxel_t < voxel_ray_query.t) {    
                    // calculate uv
                    vec3 voxel_hit = ray_at(object_ray, voxel_t);
                    vec3 local     = (voxel_hit - voxel_aabb.min) / (voxel_aabb.max - voxel_aabb.min);
                    vec2 uv        = vec2(0.0);
                    dda_result.normal.x *= -sign(object_ray.direction.x);
                    dda_result.normal.y *= -sign(object_ray.direction.y);
                    dda_result.normal.z *= -sign(object_ray.direction.z);

                    if (dda_result.normal.z == 1.0) {
                        uv = vec2(local.x, 1.0 - local.y);
                    }
                    else if (dda_result.normal.z == -1.0) {
                        uv = vec2(1.0 - local.x, 1.0 - local.y);
                    }
                    else if (dda_result.normal.y == 1.0) {
                        uv = vec2(local.x, local.z);
                    }
                    else if (dda_result.normal.y == -1.0) {
                        uv = vec2(local.x, local.z);
                    }
                    else if (dda_result.normal.x == 1.0) {
                        uv = vec2(1.0 - local.z, 1.0 - local.y);
                    }
                    else if (dda_result.normal.x == -1.0) {
                        uv = vec2(local.z, 1.0 - local.y);
                    }

                    mat4x3 object_to_world  = rayQueryGetIntersectionObjectToWorldEXT(ray_query, false);
                    mat3x3 normal_matrix    = transpose(inverse(mat3x3(object_to_world)));
                    dda_result.normal = normalize(normal_matrix * dda_result.normal);
                    
                    voxel_ray_query.hit            = true;
                    voxel_ray_query.t              = voxel_t;
                    voxel_ray_query.voxel_position = dda_result.position;
                    voxel_ray_query.normal         = dda_result.normal;
                    voxel_ray_query.uv             = uv;
                } 
            }
        }
    }
    */
    return voxel_ray_query;
}

ray_t camera_get_ray(vec3 camera_position)
{
    float vfov          = 90;             // Vertical view angle (field of view)
    vec3 look_from      = g_camera_position.xyz; //vec3(0, 0, -1); // Point camera is looking from
    vec3 look_at        = vec3(0, 0,  0); // Point camera is looking at
    vec3 vup            = vec3(0, 1,  0); // Camera-relative "up" direction
    float defocus_angle = 0;              // Variation angle of rays through each pixel
    float focus_dist    = 10;             // Distance from camera lookfrom point to plane of perfect focus

    uint image_width  = g_resolution.x;
    uint image_height = g_resolution.y; 

    vec3 center = look_from;

    // Determine viewport dimensions.
    float theta = radians(vfov);
    float h = tan(theta / 2.0);
    float viewport_height = 2.0 * h * focus_dist;
    float viewport_width  = viewport_height * (float(image_width) / float(image_height));

    // Calculate the u,v,w unit basis vectors for the camera coordinate frame.
    vec3 w = normalize(look_from - look_at);
    vec3 u = normalize(cross(vup, w));
    vec3 v = cross(w, u);

    // Calculate the vectors across the horizontal and down the vertical viewport edges.
    vec3 viewport_u = viewport_width * u;    // Vector across viewport horizontal edge
    vec3 viewport_v = viewport_height * -v;  // Vector down viewport vertical edge

    // Calculate the horizontal and vertical delta vectors to the next pixel.
    vec3 pixel_delta_u = viewport_u / image_width;
    vec3 pixel_delta_v = viewport_v / image_height;

    // Calculate the location of the upper left pixel.
    vec3 viewport_upper_left = center - (focus_dist * w) - viewport_u/2 - viewport_v/2;
    vec3 pixel00_loc = viewport_upper_left + 0.5 * (pixel_delta_u + pixel_delta_v);

    // Calculate the camera defocus disk basis vectors.
    float defocus_radius = focus_dist * tan(radians(defocus_angle / 2.0));

    vec3 defocus_disk_u = u * defocus_radius;
    vec3 defocus_disk_v = v * defocus_radius;
    
    const uvec2 pixel = gl_GlobalInvocationID.xy;
    vec3 pixel_center = pixel00_loc + (pixel.x * pixel_delta_u) + (pixel.y * pixel_delta_v);
    vec3 pixel_sample = pixel_center + vec3((pixel_delta_u) + (pixel_delta_v)); // pixel_center + pixel_sample_square();

    //auto ray_origin = (defocus_angle <= 0) ? center : defocus_disk_sample();
    vec3 ray_origin    = center;
    vec3 ray_direction = pixel_sample - ray_origin;

    //return new_ray(camera_center, normalize(pixel_sample - camera_center));
    return new_ray(ray_origin, ray_direction);
}

vec3 render_scene(uint random_state)
{
    vec3 final_color = vec3(0.0);

/*
    for (int s = 0; s < g_samples; s++) {
        //float px = -0.5 + step_and_output_random_float(random_state);
        //float py = -0.5 + step_and_output_random_float(random_state);

        //vec3 pixel_sample = pixel_center + vec3((px * pixel_delta_u) + (py * pixel_delta_v));
        //ray_t ray = new_ray(camera_center, normalize(pixel_sample - camera_center));
        
        ray_t ray = camera_get_ray(g_camera_position.xyz);

        vec4 accumulated_color = vec3(1.0);
        for (int traced_segment = 0; traced_segment < max_depth; traced_segment++) {
            voxel_ray_query_t voxel_query = voxel_ray_query_hit(ray, g_t_min, g_t_max);

            if (voxel_query.hit) {
                accumulated_color *= 0.75 * voxel_query.normal;
                const float theta = 6.2831853 * step_and_output_random_float(random_state);   // Random in [0, 2pi]
                const float u     = 2.0 * step_and_output_random_float(random_state) - 1.0;  // Random in [-1, 1]
                const float r     = sqrt(1.0 - u * u);
                
                ray = new_ray(ray_at(ray, voxel_query.t), voxel_query.normal + vec3(r * cos(theta), r * sin(theta), u));
                // ray.origin = ray_at(ray, 0.25); //todo start - intersection issues when starting inside volume

                // /if (traced_segment == 1)
                    //break;
            }
            else {
                accumulated_color *= g_debug_background_color.xyz; //get_background_color(ray.origin, ray.direction);;
                final_color += accumulated_color;
                break;
            }
        }
        //final_color += accumulated_color;
    }
*/
    return final_color;
}

vec3 render_debug_grid()
{
    aabb_t aabb = get_aabb(g_grid_id);
    ray_t ray   = camera_get_ray(g_camera_position.xyz);

    if (!ray_aabb_intersects(ray, aabb, g_t_max)) {
        return g_debug_background_color.xyz;
    }

    float t = ray_aabb_intersection(ray, aabb, g_t_max) + 0.0001;

    dda_result_t dda_result = dda_query(ray, t, g_grid_id);

    if (!dda_result.hit) {
        return g_debug_background_color.xyz;
    }

    //return vec3(ray_aabb_intersection(ray, dda_result.voxel_aabb, g_t_max));
    //return dda_result.normal;
    return get_voxel_color(g_grid_id, dda_result.position).xyz;
}

vec3 render_debug_grid_bb()
{
    aabb_t aabb = get_aabb(g_grid_id);
    ray_t ray   = camera_get_ray(g_camera_position.xyz);

    if (!ray_aabb_intersects(ray, aabb, g_t_max)) {
        return g_debug_background_color.xyz;
    }

    return vec3(ray_aabb_intersection(ray, aabb, g_t_max));
}

bool voxel_is_active(uint desc_id, ivec3 voxel_index)
{
    voxel_structure_grid_descriptor_t desc = get_structure_descriptor(desc_id);

    if (!desc.in_use) {
        debugPrintfEXT("[ERROR] Structure Descriptor (%d) not in use", desc_id);
        return false;
    }

    uvec3 dims = desc.dimensions + uvec3(VOXEL_DIM_PADDING);

    if (voxel_index.x < 0 || voxel_index.x >= dims.x) {
        debugPrintfEXT("[ERROR] Index x (%d) out of bound (expected<%d)", voxel_index.x, dims.x);
        return false;
    }

    if (voxel_index.y < 0 || voxel_index.y >= dims.y) {
        debugPrintfEXT("[ERROR] Index y (%d) out of bound (expected<%d)", voxel_index.y, dims.y);
        return false;
    }

    if (voxel_index.z < 0 || voxel_index.z >= dims.z) {
        debugPrintfEXT("[ERROR] Index z (%d) out of bound (expected<%d)", voxel_index.z, dims.z);
        return false;
    }

    uint vwidth = dims.x;
    uint vdepth = dims.z;

    uint index = FLATTEN(voxel_index.x, voxel_index.y, voxel_index.z, vwidth, vdepth);

    index += desc.structure_offset; 
    return g_voxel_structure_data[index] == 1;
}

vec4 get_voxel_color(uint desc_id, ivec3 voxel_index)
{
    voxel_structure_grid_descriptor_t desc = get_structure_descriptor(desc_id);

    if (!desc.in_use) {
        debugPrintfEXT("[ERROR] Structure Descriptor (%d) not in use", desc_id);
        return vec4(1.0, 0.0, 1.0, 1.0);
    }

    uvec3 dims = desc.dimensions + uvec3(VOXEL_DIM_PADDING);

    if (voxel_index.x < 0 || voxel_index.x >= dims.x) {
        debugPrintfEXT("[ERROR] Index x (%d) out of bound (expected<%d)", voxel_index.x, dims.x);
        return vec4(1.0, 0.0, 1.0, 1.0);
    }

    if (voxel_index.y < 0 || voxel_index.y >= dims.y) {
        debugPrintfEXT("[ERROR] Index y (%d) out of bound (expected<%d)", voxel_index.y, dims.y);
        return vec4(1.0, 0.0, 1.0, 1.0);
    }

    if (voxel_index.z < 0 || voxel_index.z >= dims.z) {
        debugPrintfEXT("[ERROR] Index z (%d) out of bound (expected<%d)", voxel_index.z, dims.z);
        return vec4(1.0, 0.0, 1.0, 1.0);
    }

    uint vwidth = dims.x;
    uint vdepth = dims.z;

    uint index = FLATTEN(voxel_index.x, voxel_index.y, voxel_index.z, vwidth, vdepth);

    index += desc.material_offset; 
    return g_voxel_material_data[index].color;
}

uint step_random(uint random_state)
{
    return random_state * 747796405 + 1;
}

// Steps the RNG and returns a floating-point value between 0 and 1 inclusive.
float step_and_output_random_float(inout uint random_state)
{
    // Condensed version of pcg_output_rxs_m_xs_32_32, with simple conversion to floating-point [0,1].
    random_state = step_random(random_state);
    uint word    = ((random_state >> ((random_state >> 28) + 4)) ^ random_state) * 277803737;
    word         = (word >> 22) ^ word;
    return float(word) / 4294967295.0f;
}