#pragma once

#include "stla_geometry.h"
#include "stla_vertex_buffer_layout.h"

#include <glm/glm.hpp>

#include <stdint.h>

#include <vector>

namespace stla
{

class tri_mesh
{
public:

    bool     load(const char *p_file_name);
    uint32_t get_vertex_count() const;

    const std::vector<float> &get_vertices() const { return m_vertices; }
    const std::vector<uint32_t> &get_indices() const { return m_indices; }
    vertex_buffer_layout get_vertex_layout() const { return m_vertex_layout; }
    aabb_3d get_aabb() const;
    glm::vec3 get_position(uint32_t index) const;
    glm::vec3 get_normal(uint32_t index) const;
    void transform(const glm::mat4 &t);

private:
    std::vector<float>    m_vertices;
    std::vector<uint32_t> m_indices;
    vertex_buffer_layout  m_vertex_layout;
    bool                  m_has_normals{ false };
    bool                  m_has_uvs{ false };
};
    
} // namespace stla
