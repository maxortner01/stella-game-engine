#include "stla_tri_mesh.h"
#include "stla_logger.h"
#include "stla_assert.h"

#define TINYOBJLOADER_IMPLEMENTATION // define this in only *one* .cc
#define TINYOBJLOADER_USE_MAPBOX_EARCUT
#include <tiny_obj_loader.h>
#include <glm/glm.hpp>

#include <string.h>

#include <filesystem>
#include <vector>
#include <initializer_list>
#include <unordered_map>
#include <limits>

namespace stla
{

bool tri_mesh::load(const char *p_file_name)
{
    std::filesystem::path in_file_path(p_file_name);
    
    // Check file extension
    if (in_file_path.extension() != ".obj") {
        return false;
    }

    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string warn, err;

    std::string p = in_file_path.string();
    if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, p.c_str(), nullptr, true)) {
        STLA_CORE_LOG_ERROR("Unable to load obj file {0}: warn: {1} err: {2}\n", 
            p_file_name, (const char *)warn.c_str(), (const char *)err.c_str());
        return false;
    }

    // Determine vertex layout
    // todo handle other sizes of positions

    uint32_t curr_index = 0;
    if (attrib.vertices.size()) {
        m_vertex_layout.add_attribute(vertex_buffer_attribute_type::v3, curr_index, false);
        curr_index++;
    }
    
    if (attrib.normals.size()) {
        m_vertex_layout.add_attribute(vertex_buffer_attribute_type::v3, curr_index, false);
        curr_index++;
    }
    
    if (attrib.texcoords.size()) {
        m_vertex_layout.add_attribute(vertex_buffer_attribute_type::v2, curr_index, false);
        curr_index++;
    }

    std::unordered_map<std::string, uint32_t> unique_vertices;

    m_vertices.reserve(attrib.vertices.size() + attrib.normals.size() + attrib.texcoords.size());
    m_indices.reserve(attrib.vertices.size() / 3);

    STLA_CORE_LOG_INFO("Loading obj model from {0}", p_file_name);
    STLA_CORE_LOG_INFO(" Vertices        : {0}", attrib.vertices.size() / 3);
    STLA_CORE_LOG_INFO(" Normals         : {0}", attrib.normals.size() / 3);
    STLA_CORE_LOG_INFO(" Texcoords       : {0}", attrib.texcoords.size() / 2);
    STLA_CORE_LOG_INFO(" Shapes          : {0}", shapes.size());
    STLA_CORE_LOG_INFO(" Materials       : {0}", materials.size());

    STLA_CORE_LOG_INFO(" Vertex Input (stride={0}) (attrib_count={1}) (element_count={2})", 
        m_vertex_layout.get_stride(), m_vertex_layout.get_attribute_count(), m_vertex_layout.get_element_count());
    
    for (const vertex_buffer_attribute &attrib : m_vertex_layout.get_attribute_array()) {
        uint32_t index = attrib.m_index;
        vertex_buffer_attribute_type type = attrib.m_type;
        STLA_CORE_LOG_INFO("  (loc={0}) (type={1}) (size={2})", index, (int)type, attrib.get_size());
    }

    /*
    
    uint32_t shape_num = 0;
    for (const tinyobj::shape_t &shape : shapes) {
        STLA_CORE_LOG_INFO("   Shape ({0}) (name={1})", shape_num, shape.name.c_str());
        shape_num++;
        
        for (const auto &index : shape.mesh.indices) {
            std::vector<float> vertex;
            vertex.reserve(m_vertex_layout.get_element_count());

            // Position
            if (attrib.vertices.size() != 0) {
                vertex.push_back(attrib.vertices[3 * index.vertex_index + 0]);
                vertex.push_back(attrib.vertices[3 * index.vertex_index + 1]);
                vertex.push_back(attrib.vertices[3 * index.vertex_index + 2]);
            }

            // Normals
            if (attrib.normals.size() != 0) {
                vertex.push_back(attrib.normals[3 * index.normal_index + 0]);
                vertex.push_back(attrib.normals[3 * index.normal_index + 1]);
                vertex.push_back(attrib.normals[3 * index.normal_index + 2]);
            }

            // UVs
            if (attrib.texcoords.size() != 0) {
                vertex.push_back(attrib.texcoords[2 * index.texcoord_index + 0]);
                vertex.push_back(attrib.texcoords[2 * index.texcoord_index + 1]);
            }

            // TODO a more efficient way to do this
            std::string vertex_string((const char *)vertex.data(), sizeof(float) * vertex.size());
            printf("vertex string: %s\n", vertex_string.c_str());

            if (unique_vertices.find(vertex_string) == unique_vertices.end()) {
                unique_vertices[vertex_string] = (uint32_t)(m_vertices.size() / vertex.size()); // store vertex position in vertices vector
                for (float value : vertex) {
                    m_vertices.push_back(value);
                }
            }

            m_indices.push_back(unique_vertices[vertex_string]);    
        }
    }
    
    */
    
    //todo detect duplicate vertices

    uint32_t index = 0;
    for (size_t s = 0; s < shapes.size(); s++) {
        STLA_CORE_LOG_INFO("   Shape ({0}) (name={1})", s, shapes[s].name.c_str());

        // Loop over faces(polygon)
        size_t index_offset = 0;
        for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
            size_t fv = size_t(shapes[s].mesh.num_face_vertices[f]);

            // Loop over vertices in the face.
            for (size_t v = 0; v < fv; v++) {
                // access to vertex
                tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
                tinyobj::real_t vx = attrib.vertices[3*size_t(idx.vertex_index)+0];
                tinyobj::real_t vy = attrib.vertices[3*size_t(idx.vertex_index)+1];
                tinyobj::real_t vz = attrib.vertices[3*size_t(idx.vertex_index)+2];

                m_vertices.push_back(vx);
                m_vertices.push_back(vy);
                m_vertices.push_back(vz);

                // Check if `normal_index` is zero or positive. negative = no normal data
                if (idx.normal_index >= 0) {
                    tinyobj::real_t nx = attrib.normals[3*size_t(idx.normal_index)+0];
                    tinyobj::real_t ny = attrib.normals[3*size_t(idx.normal_index)+1];
                    tinyobj::real_t nz = attrib.normals[3*size_t(idx.normal_index)+2];
                
                    m_vertices.push_back(nx);
                    m_vertices.push_back(ny);
                    m_vertices.push_back(nz);
                    
                    m_has_normals = true;
                }

                // Check if `texcoord_index` is zero or positive. negative = no texcoord data
                if (idx.texcoord_index >= 0) {
                    tinyobj::real_t tx = attrib.texcoords[2*size_t(idx.texcoord_index)+0];
                    tinyobj::real_t ty = attrib.texcoords[2*size_t(idx.texcoord_index)+1];

                    m_vertices.push_back(tx);
                    m_vertices.push_back(ty);
                
                    m_has_uvs = true;
                }

                // Optional: vertex colors
                // tinyobj::real_t red   = attrib.colors[3*size_t(idx.vertex_index)+0];
                // tinyobj::real_t green = attrib.colors[3*size_t(idx.vertex_index)+1];
                // tinyobj::real_t blue  = attrib.colors[3*size_t(idx.vertex_index)+2];
            
                m_indices.push_back(index);
                index++;
            }
            index_offset += fv;

            // per-face material
            //shapes[s].mesh.material_ids[f];
        }
    }

    STLA_CORE_LOG_INFO(" Indices         : {0}", m_indices.size());
    STLA_CORE_LOG_INFO(" Packed Vertices : {0}", m_vertices.size() / m_vertex_layout.get_element_count());

    return true;
}

uint32_t tri_mesh::get_vertex_count() const
{
    return (uint32_t)(m_vertices.size() / m_vertex_layout.get_element_count());
}

aabb_3d tri_mesh::get_aabb() const
{
    aabb_3d aabb(std::numeric_limits<float>::max(), std::numeric_limits<float>::min());

    // Iterate all vertex positions
    for (int i = 0; i < m_indices.size(); i++) {
        uint32_t index = m_indices[i];
        float x = m_vertices[(index * m_vertex_layout.get_element_count()) + 0];
        float y = m_vertices[(index * m_vertex_layout.get_element_count()) + 1];
        float z = m_vertices[(index * m_vertex_layout.get_element_count()) + 2];

        if (x < aabb.min_x) { aabb.min_x = x; }
        if (y < aabb.min_y) { aabb.min_y = y; }
        if (z < aabb.min_z) { aabb.min_z = z; }

        if (x > aabb.max_x) { aabb.max_x = x; }
        if (y > aabb.max_y) { aabb.max_y = y; }
        if (z > aabb.max_z) { aabb.max_z = z; }
    }

    return aabb;  
}

glm::vec3 tri_mesh::get_position(uint32_t index) const
{
    float x = m_vertices[(index * m_vertex_layout.get_element_count()) + 0];
    float y = m_vertices[(index * m_vertex_layout.get_element_count()) + 1];
    float z = m_vertices[(index * m_vertex_layout.get_element_count()) + 2];
    return glm::vec3(x, y, z);
}

glm::vec3 tri_mesh::get_normal(uint32_t index) const
{
    STLA_ASSERT(m_has_normals, "tri_mesh doesn't contain normals");

    return glm::vec3(
        m_vertices[(index * m_vertex_layout.get_element_count()) + 3],
        m_vertices[(index * m_vertex_layout.get_element_count()) + 4],
        m_vertices[(index * m_vertex_layout.get_element_count()) + 5]
    );
}

void tri_mesh::transform(const glm::mat4 &t)
{
    for (int i = 0; i < m_indices.size(); i++) {
        uint32_t index = m_indices[i];
        uint32_t x_offset = (index * m_vertex_layout.get_element_count()) + 0;
        uint32_t y_offset = (index * m_vertex_layout.get_element_count()) + 1;
        uint32_t z_offset = (index * m_vertex_layout.get_element_count()) + 2;
        
        glm::vec4 transformed_point = t * glm::vec4(m_vertices[x_offset], m_vertices[y_offset], m_vertices[z_offset], 1.0f);

        m_vertices[x_offset] = transformed_point.x;
        m_vertices[y_offset] = transformed_point.y;
        m_vertices[z_offset] = transformed_point.z;
    }
}

} // namespace geri
