#pragma once

#include <stdio.h>
#include <stdlib.h>

#include <string>

namespace stla
{

enum class shader_type
{
    vertex, fragment, raygen, anyhit, closesthit, miss, intersection, callable, compute
};

//todo deal with this better in parser, potentially a log of unused space
using shader_source_code = std::string;
    
class shader_parser
{
public:
    //[shader("<type>")]
    static inline const char * const SHADER_TYPE_DESIGNATOR  = "[shader(";
    static inline const char * const VERTEX_IDENTIFIER       = "vertex";
    static inline const char * const FRAGMENT_IDENTIFIER     = "fragment";
    static inline const char * const RAYGEN_IDENTIFIER       = "raygen";
    static inline const char * const ANY_HIT_IDENTIFIER      = "anyhit";
    static inline const char * const CLOSEST_HIT_IDENTIFIER  = "closesthit";
    static inline const char * const MISS_IDENTIFIER         = "miss";
    static inline const char * const INTERSECTION_IDENTIFIER = "intersection";
    static inline const char * const CALLABLE_IDENTIFIER     = "callable";
    static inline const char * const COMPUTE_IDENTIFIER      = "compute";

    bool parse(const char *p_path);
    void clear();

    const shader_source_code &get_source(shader_type type) const;

private:
    
    shader_source_code *get_shader_dest_ptr(const char *p_type_designator);

    // Rasterization
    shader_source_code m_vertex_source;
    shader_source_code m_fragment_source;

    // RT Shaders
    shader_source_code m_ray_gen_source;
    shader_source_code m_any_hit_source;
    shader_source_code m_closest_hit_source;
    shader_source_code m_miss_source;
    shader_source_code m_intersection_source;
    shader_source_code m_callable_source;

    // Compute
    shader_source_code m_compute_source;
};

} // namespace stla
