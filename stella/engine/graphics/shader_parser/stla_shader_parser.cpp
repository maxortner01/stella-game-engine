#include "stla_shader_parser.h"
#include "stla_assert.h"

namespace stla
{

bool shader_parser::parse(const char *p_path)
{
    FILE *fp = nullptr;
    char buf[256] = { 0 };
    shader_source_code *p_current_dest = nullptr;

    fp = fopen(p_path, "r");
    if (fp == nullptr) {
        STLA_ASSERT(0, "invalid shader path: %s", p_path);
        return false;
    }

    clear();

    while (fgets(&buf[0], sizeof(buf), fp)) {
        // if identifier
        if (strstr(&buf[0], SHADER_TYPE_DESIGNATOR) != nullptr) {
            p_current_dest = get_shader_dest_ptr(&buf[0]);
            if (p_current_dest == nullptr) {
                return false;
            }
        }
        else {
            if (p_current_dest != nullptr) {
                p_current_dest->append(&buf[0]);
                
                //if (!p_current_dest->available()) {
                //    STLA_ASSERT(0, "shader source too long");
                //    return false;
                //}
            }
        }

        memset(&buf[0], 0, sizeof(buf));
    }

    fclose(fp);
    return true;
}

void shader_parser::clear()
{
    m_vertex_source.clear();
    m_fragment_source.clear();
    m_ray_gen_source.clear();
    m_any_hit_source.clear();
    m_closest_hit_source.clear();
    m_miss_source.clear();
    m_intersection_source.clear();
    m_callable_source.clear();
    m_compute_source.clear();
}

const shader_source_code &shader_parser::get_source(shader_type type) const
{
    switch (type)
    {
        case shader_type::vertex: return m_vertex_source;
        case shader_type::fragment: return m_fragment_source;
        case shader_type::raygen: return m_ray_gen_source;
        case shader_type::anyhit: return m_any_hit_source;
        case shader_type::closesthit: return m_closest_hit_source;
        case shader_type::miss: return m_miss_source;
        case shader_type::intersection: return m_intersection_source;
        case shader_type::callable: return m_callable_source;
        case shader_type::compute: return m_compute_source;
    }
}

shader_source_code *shader_parser::get_shader_dest_ptr(const char *p_type_designator)
{
    if (p_type_designator == nullptr) {
        STLA_ASSERT(0, "null shader type designator");
        return nullptr;
    }

    if (strstr(p_type_designator, VERTEX_IDENTIFIER) != nullptr) {
        return &m_vertex_source;
    }
    else if (strstr(p_type_designator, FRAGMENT_IDENTIFIER) != nullptr) {
        return &m_fragment_source;
    }
    else if (strstr(p_type_designator, RAYGEN_IDENTIFIER) != nullptr) {
        return &m_ray_gen_source;
    }
    else if (strstr(p_type_designator, ANY_HIT_IDENTIFIER) != nullptr) {
        return &m_any_hit_source;
    }
    else if (strstr(p_type_designator, CLOSEST_HIT_IDENTIFIER) != nullptr) {
        return &m_closest_hit_source;
    }
    else if (strstr(p_type_designator, MISS_IDENTIFIER) != nullptr) {
        return &m_miss_source;
    }
    else if (strstr(p_type_designator, INTERSECTION_IDENTIFIER) != nullptr) {
        return &m_intersection_source;
    }
    else if (strstr(p_type_designator, CALLABLE_IDENTIFIER) != nullptr) {
        return &m_callable_source;
    }
    else if (strstr(p_type_designator, COMPUTE_IDENTIFIER) != nullptr) {
        return &m_compute_source;
    }

    STLA_ASSERT(0, "invalid shader type designator: %s", p_type_designator);
    return nullptr;
}

    
} // namespace stla
