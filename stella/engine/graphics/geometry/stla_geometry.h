#pragma once

#include "stla_utils.h"

#include <glm/glm.hpp>

namespace stla
{

/*
Vulkan Spec:
The AABB data in memory is six 32-bit floats consisting of the minimum x, y, and z values followed by the maximum x, y, and z values.
 - offset must be less than the size of aabbData
 - offset must be a multiple of 8
 - stride must be a multiple of 8

typedef struct VkAabbPositionsKHR {
    float    minX;
    float    minY;
    float    minZ;
    float    maxX;
    float    maxY;
    float    maxZ;
} VkAabbPositionsKHR;
*/

STLA_PACK(struct aabb_3d
{
    float min_x{ 0.0f };
    float min_y{ 0.0f };
    float min_z{ 0.0f };
    float max_x{ 0.0f };
    float max_y{ 0.0f };
    float max_z{ 0.0f };

    aabb_3d() = default;
    aabb_3d(float min, float max);
    aabb_3d(float min_x, float min_y, float min_z, float max_x, float max_y, float max_z);
    aabb_3d(const glm::vec3 &min, const glm::vec3 &max);

    bool operator == (const aabb_3d &rhs);
});

struct triangle
{
    glm::vec3 p0;
    glm::vec3 p1;
    glm::vec3 p2;

    triangle(const glm::vec3 &p0, const glm::vec3 &p1, const glm::vec3 &p2) :
        p0(p0), p1(p1), p2(p2) { }
};

bool intersects(const triangle &tri, const aabb_3d &aabb);

} // namespace stla
