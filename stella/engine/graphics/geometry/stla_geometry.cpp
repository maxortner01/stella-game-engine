#include "stla_geometry.h"

namespace stla
{
    
aabb_3d::aabb_3d(float min, float max) :
    min_x(min), min_y(min), min_z(min), max_x(max), max_y(max), max_z(max) { }

aabb_3d::aabb_3d(float min_x, float min_y, float min_z, float max_x, float max_y, float max_z) :
    min_x(min_x), min_y(min_y), min_z(min_z), max_x(max_x), max_y(max_y), max_z(max_z) { }

aabb_3d::aabb_3d(const glm::vec3 &min, const glm::vec3 &max) :
    min_x(min.x), min_y(min.y), min_z(min.z), max_x(max.x), max_y(max.y), max_z(max.x) { }


bool aabb_3d::operator == (const aabb_3d &rhs)
{
    return min_x == rhs.min_x && min_y == rhs.min_y && min_z == rhs.min_z && max_x == rhs.max_x && max_y == rhs.max_y && max_z == rhs.max_z;
}

// Testing axis: axis_u0_f0
// Project all 3 vertices of the triangle onto the Seperating axis
bool sep_axis_test(const glm::vec3 &axis, const glm::vec3 &v0, const glm::vec3 &v1, const glm::vec3 &v2, const glm::vec3 &extents) 
{
    glm::vec3 u0(1.0f, 0.0f, 0.0f);
    glm::vec3 u1(0.0f, 1.0f, 0.0f);
    glm::vec3 u2(0.0f, 0.0f, 1.0f);

    float p0 = glm::dot(v0, axis);
    float p1 = glm::dot(v1, axis);
    float p2 = glm::dot(v2, axis);
    // Project the AABB onto the seperating axis
    // We don't care about the end points of the prjection
    // just the length of the half-size of the AABB
    // That is, we're only casting the extents onto the 
    // seperating axis, not the AABB center. We don't
    // need to cast the center, because we know that the
    // aabb is at origin compared to the triangle!
    
    float r = extents.x * glm::abs(glm::dot(u0, axis)) +
              extents.y * glm::abs(glm::dot(u1, axis)) + 
              extents.z * glm::abs(glm::dot(u2, axis));

    //float r = e.x * glm::abs(vec3_dot(u0, axis)) +
    //        e.y * glm::abs(vec3_dot(u1, axis)) +
      //      e.z * glm::abs(vec3_dot(u2, axis));
    
    // Now do the actual test, basically see if either of
    // the most extreme of the triangle points intersects r
    // You might need to write Min & Max functions that take 3 arguments
    
    if (glm::max(-glm::max(glm::max(p0, p1), p2), glm::min(glm::min(p0, p1), p2)) > r) {
        // This means BOTH of the points of the projected triangle
        // are outside the projected half-length of the AABB
        // Therefore the axis is seperating and we can exit
        return false;
    }
    return true;
};

/*
vec3 box_center(AABB box)
{
    vec3 result = vec3_add(box.max, box.min);
    result.x *= 0.5f; 
    result.y *= 0.5f; 
    result.z *= 0.5f; 
    return result;
}

vec3 box_extents(AABB box)
{
    return vec3_new(
        (box.max.x - box.min.x) / 2.0f,
        (box.max.y - box.min.y) / 2.0f,
        (box.max.z - box.min.z) / 2.0f
    );
}
*/

bool intersects(const triangle &tri, const aabb_3d& aabb)
{
//bool intersect_aabb_triangle(AABB box, vec3 p1, vec3 p2, vec3 p3) 
    glm::vec3 v0 = tri.p0; 
    glm::vec3 v1 = tri.p1;
    glm::vec3 v2 = tri.p2;

    // Convert AABB to center-extents form
    //vec3 c = box_center(box); 
    //vec3 e = box_extents(box);
    glm::vec3 aabb_min = glm::vec3(aabb.min_x, aabb.min_y, aabb.min_z);
    glm::vec3 aabb_max = glm::vec3(aabb.max_x, aabb.max_y, aabb.max_z);
    glm::vec3 c = aabb_min + (0.5f * (aabb_max - aabb_min)); 
    glm::vec3 e = (aabb_max - aabb_min) * 0.5f;

    // Translate the triangle as conceptually moving the AABB to origin
    // This is the same as we did with the point in triangle test
    v0 = v0 - c;
    v1 = v1 - c;
    v2 = v2 - c;

    // Compute the edge vectors of the triangle  (ABC)
    // That is, get the lines between the points as vectors
    glm::vec3 f0 = v1 - v0; // B - A
    glm::vec3 f1 = v2 - v1; // C - B
    glm::vec3 f2 = v0 - v2; // A - C

    // Compute the face normals of the AABB, because the AABB
    // is at center, and of course axis aligned, we know that 
    // it's normals are the X, Y and Z axis.
    glm::vec3 u0(1.0f, 0.0f, 0.0f);
    glm::vec3 u1(0.0f, 1.0f, 0.0f);
    glm::vec3 u2(0.0f, 0.0f, 1.0f);

    // There are a total of 13 axis to test!

    // We first test against 9 axis, these axis are given by
    // cross product combinations of the edges of the triangle
    // and the edges of the AABB. You need to get an axis testing
    // each of the 3 sides of the AABB against each of the 3 sides
    // of the triangle. The result is 9 axis of seperation
    // https://awwapp.com/b/umzoc8tiv/

    // Compute the 9 axis
    glm::vec3 axis_u0_f0 = glm::cross(u0, f0);
    glm::vec3 axis_u0_f1 = glm::cross(u0, f1);
    glm::vec3 axis_u0_f2 = glm::cross(u0, f2);

    glm::vec3 axis_u1_f0 = glm::cross(u1, f0);
    glm::vec3 axis_u1_f1 = glm::cross(u1, f1);
    glm::vec3 axis_u1_f2 = glm::cross(u2, f2);

    glm::vec3 axis_u2_f0 = glm::cross(u2, f0);
    glm::vec3 axis_u2_f1 = glm::cross(u2, f1);
    glm::vec3 axis_u2_f2 = glm::cross(u2, f2);

    // Repeat this test for the other 8 seperating axis
    if (!sep_axis_test(axis_u0_f0, v0, v1, v2, e) || 
        !sep_axis_test(axis_u0_f1, v0, v1, v2, e) || 
        !sep_axis_test(axis_u0_f2, v0, v1, v2, e) ||
        !sep_axis_test(axis_u1_f0, v0, v1, v2, e) || 
        !sep_axis_test(axis_u1_f1, v0, v1, v2, e) || 
        !sep_axis_test(axis_u1_f2, v0, v1, v2, e) ||
        !sep_axis_test(axis_u2_f0, v0, v1, v2, e) || 
        !sep_axis_test(axis_u2_f1, v0, v1, v2, e) || 
        !sep_axis_test(axis_u2_f2, v0, v1, v2, e)) {
        return false;
    }

    // Next, we have 3 face normals from the AABB
    // for these tests we are conceptually checking if the bounding box
    // of the triangle intersects the bounding box of the AABB
    // that is to say, the seperating axis for all tests are axis aligned:
    // axis1: (1, 0, 0), axis2: (0, 1, 0), axis3 (0, 0, 1)
    // Do the SAT given the 3 primary axis of the AABB
    // You already have vectors for this: u0, u1 & u2
    if (!sep_axis_test(u0, v0, v1, v2, e) || 
        !sep_axis_test(u1, v0, v1, v2, e) || 
        !sep_axis_test(u2, v0, v1, v2, e)) {
        return false;
    }

    // Finally, we have one last axis to test, the face normal of the triangle
    // We can get the normal of the triangle by crossing the first two line segments
    glm::vec3 tri_normal = glm::cross(f0, f1);
    if (!sep_axis_test(tri_normal, v0, v1, v2, e)) {
        return false;
    }

    // Passed testing for all 13 seperating axis that exist!
    return true;
}

} // namespace stla
