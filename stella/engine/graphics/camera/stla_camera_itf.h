#pragma once

#include <glm/glm.hpp>

namespace stla
{
    
class camera_itf
{
public:
    virtual glm::mat4 get_view() const = 0;
    virtual glm::mat4 get_projection() const = 0;
    virtual glm::mat4 get_view_projection() const = 0;
    virtual glm::vec3 get_position() const = 0;
    virtual glm::vec3 get_direction() const = 0;
};

} // namespace stla
