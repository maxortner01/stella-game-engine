#include "stla_perspective_camera.h"

namespace stla
{
    
perspective_camera::perspective_camera(float fov, float aspect, float near, float far, glm::vec3 position, glm::vec3 front) :
    m_projection(glm::perspective(glm::radians(fov), aspect, near, far)), m_position(position), m_front(front), m_up(glm::vec3(0.0f, 1.0f, 0.0f)),
    m_fov(fov), m_aspect(aspect), m_near(near), m_far(far)
{ }

glm::mat4 perspective_camera::get_view() const
{
    return glm::lookAt(m_position, m_position + m_front, m_up);
}

glm::mat4 perspective_camera::get_projection() const
{
    return m_projection;
}

glm::mat4 perspective_camera::get_view_projection() const
{
    return m_projection * glm::lookAt(m_position, m_position + m_front, m_up);
}

glm::vec3 perspective_camera::get_position() const
{
    return m_position;
}

void perspective_camera::set_position(glm::vec3 position)
{
    m_position = position;
}

glm::vec3 perspective_camera::get_direction() const
{
    return m_front;
}

void perspective_camera::set_direction(glm::vec3 direction)
{
    m_front = direction;
}

glm::vec3 perspective_camera::get_up() const
{
    return m_up;
}

void perspective_camera::set_up(glm::vec3 up)
{
    m_up = up;
}

float perspective_camera::get_fov() const
{
    return m_fov;
}

void perspective_camera::set_fov(float fov)
{
    m_fov = fov;
    update_projection();
}

float perspective_camera::aspect_ratio() const
{
    return m_aspect;
}

void perspective_camera::set_aspect_ratio(float ar)
{
    m_aspect = ar;
    update_projection();
}

float perspective_camera::get_near() const
{
    return m_near;
}

void perspective_camera::set_near(float near)
{
    m_near = near;
    update_projection();
}

float perspective_camera::get_far() const
{
    return m_far;
}

void perspective_camera::set_far(float far)
{
    m_far = far;
    update_projection();
}

void perspective_camera::update_projection() 
{ 
    m_projection = glm::perspective(glm::radians(m_fov), m_aspect, m_near, m_far);
}

} // namespace stla
