#include "vk_render_target.h"
#include "vk_state.h"
#include "vk_common.h"
#include "vk_renderpass.h"

#include "stla_assert.h"

namespace stla
{
namespace vk
{

render_target::render_target(uint32_t width, uint32_t height, VkFramebuffer framebuffer, uint32_t attachment_count, image *p_attachments, state *p_state) :
    m_framebuffer(framebuffer), m_width(width), m_height(height), mp_state(p_state) 
{
    STLA_ASSERT_RELEASE(p_attachments != nullptr, "p_attachments is null");
    STLA_ASSERT_RELEASE(attachment_count != 0, "attachment_count = 0");

    for (int i = 0; i < attachment_count; i++) {
        m_attachments.push_back(p_attachments[i]);
    }
}


void render_target::destroy()
{
    if (mp_state == nullptr) {
        return;
    }

    mp_state->push_deletor([framebuffer = m_framebuffer](state &state) {
        vkDestroyFramebuffer(state.get_device_handle(), framebuffer, nullptr);
    });

    for (auto &im : m_attachments) {
        im.destroy();
    }
    
    m_width       = 0;
    m_height      = 0;
    m_framebuffer = nullptr;
    mp_state      = nullptr;
}

const image &render_target::get_attachment_by_index(uint8_t index)
{
    STLA_ASSERT_RELEASE(index < m_attachments.size(), "invalid attachment index (index={%d}) (expected<%d)", index, m_attachments.size());
    return m_attachments[index];
}

render_target_builder &render_target_builder::add_attachment(
    VkFormat format, 
    VkImageTiling tiling, 
    VkImageUsageFlags usage, 
    VkMemoryPropertyFlags props, 
    VkImageAspectFlags aspect_flags, 
    VkFormatFeatureFlags features)
{
    image::create_info depth_attach_info = {
        .width        = m_width,
        .height       = m_height,
        .format       = format,
        .tiling       = tiling,
        .usage        = usage,
        .properties   = props,
        .aspect_flags = aspect_flags,
        .features     = features
    };

    STLA_ASSERT(m_attachment_create_infos.available(), "Vector full");

    m_attachment_create_infos.push_back(depth_attach_info);

    return *this;
}

bool render_target_builder::build(render_target &target)
{
    VkFramebuffer framebuffer;
    vector_8<image> attachment_images;

    for (int i = 0; i < m_attachment_create_infos.size(); i++) {
        image new_image;
        if (!new_image.create(m_attachment_create_infos[i], m_state)) {
            STLA_ASSERT(0, "Error creating image");
        }
        else {
            attachment_images.push_back(new_image);
        }
    }

    vector_8<VkImageView> attachments;
    for (image &im : attachment_images) {
        attachments.push_back(im.get_view_handle());
    }

    VkFramebufferCreateInfo framebuffer_info = { };
    framebuffer_info.sType           = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebuffer_info.renderPass      = m_renderpass.get_handle();
    framebuffer_info.attachmentCount = attachments.size();
    framebuffer_info.pAttachments    = attachments.data();
    framebuffer_info.width           = m_width;
    framebuffer_info.height          = m_height;
    framebuffer_info.layers          = 1;

    if ((framebuffer = m_state.vk_create_framebuffer(framebuffer_info)) == nullptr) {
        return false;
    }

    target.destroy();
    target = render_target(
        m_width, m_height, framebuffer, attachment_images.size(), attachment_images.data(), &m_state);
    return target.is_valid();
}
    
} // namespace vk
} // namespace stla
