#include "vk_command_pool.h"
#include "vk_common.h"
#include "vk_state.h"

#include <vulkan/vulkan.h>

namespace stla
{
namespace vk
{

template<typename T>
bool command_pool<T>::create(uint32_t queue_family_index, state &state)
{
    VkCommandPool new_pool = nullptr;

    VkCommandPoolCreateInfo command_pool_info = { };
    command_pool_info.sType            = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    command_pool_info.pNext            = nullptr;
    command_pool_info.queueFamilyIndex = queue_family_index;
    command_pool_info.flags            = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    if ((new_pool = state.vk_create_command_pool(command_pool_info)) == nullptr) {
        return false;
    }

    if (m_command_pool) {
        destroy();
    }

    m_command_pool = new_pool;
    mp_state       = &state;
    return true;
}

template<typename T>
void command_pool<T>::destroy()
{
    if (mp_state == nullptr) {
        return;
    }

    while (m_command_buffers.size() != 0) {
        T cmd_buffer = m_command_buffers.front();
        m_command_buffers.pop_front();
        cmd_buffer.destroy(); // invalidate command buffers
    }

    if (m_command_pool) {
        mp_state->push_deletor([command_pool = m_command_pool](state &state) {        
            vkDestroyCommandPool(state.get_device_handle(), command_pool, nullptr);
        });  
    }

    m_command_buffers.clear();
    mp_state       = nullptr;
    m_command_pool = nullptr;
}

template<typename T>
T command_pool<T>::acquire_command_buffer()
{
    STLA_ASSERT_RELEASE(is_valid(), "command_pool invalid");

    if (m_command_buffers.size() != 0) {
        T cmd_buffer = m_command_buffers.front();
        m_command_buffers.pop_front();
        return cmd_buffer;
    }

    VkCommandBuffer vk_cmd_buffer = nullptr;

    VkCommandBufferAllocateInfo info = { };
    info.sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    info.pNext              = nullptr;
    info.commandPool        = m_command_pool;
    info.commandBufferCount = 1;
    info.level              = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

    vk_cmd_buffer = mp_state->vk_alloc_command_buffer(info);
    STLA_ASSERT_RELEASE(vk_cmd_buffer != nullptr, "unable to allocate command buffer");

    T cmd_buffer = T(vk_cmd_buffer);
    cmd_buffer.set_pool_handle(this);
    return cmd_buffer;
}

template<typename T>
bool command_pool<T>::return_command_buffer(const T &cmd_buffer)
{
    STLA_ASSERT_RELEASE(is_valid(), "command_pool invalid");
    STLA_ASSERT_RELEASE(m_command_buffers.available(), "m_command_buffers full");
    STLA_ASSERT(cmd_buffer.get_pool_handle() == this, "returned a command buffer to the wrong pool");

    if (cmd_buffer.get_pool_handle() != this) {
        return false;
    }

    m_command_buffers.push_back(cmd_buffer);
    return true;
}

template bool command_pool<graphics_command_buffer>::create(uint32_t, vk::state &);
template void command_pool<graphics_command_buffer>::destroy();
template graphics_command_buffer command_pool<graphics_command_buffer>::acquire_command_buffer();
template bool command_pool<graphics_command_buffer>::return_command_buffer(const graphics_command_buffer &);

template bool command_pool<compute_command_buffer>::create(uint32_t, vk::state &);
template void command_pool<compute_command_buffer>::destroy();
template compute_command_buffer command_pool<compute_command_buffer>::acquire_command_buffer();
template bool command_pool<compute_command_buffer>::return_command_buffer(const compute_command_buffer &);

} // namespace vk
} // namespace stla
