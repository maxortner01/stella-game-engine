#pragma once

#include "stla_assert.h"

#include <vulkan/vulkan.h>
#include <vma/vk_mem_alloc.h>

#include <stdint.h>

namespace stla
{
namespace vk
{

class state;

class buffer
{
public:

    bool alloc(uint32_t size, VkBufferUsageFlags buffer_usage, VkMemoryPropertyFlags memory_properties, state &state);
    void destroy();
    bool copy_to(uint32_t size, void *p_dest, uint32_t offset = 0) const;
    bool copy_from(uint32_t size, const void *p_src, uint32_t offset = 0);
    bool is_valid() const { return m_buffer != nullptr && m_allocation != nullptr && mp_state != nullptr && m_size != 0; }
    bool map(void **pp_ptr);
    void unmap();
    bool memset(uint8_t val);

    const VkBuffer &get_buffer_handle() const { return m_buffer; }
    const VmaAllocation &get_allocation_handle() const { return m_allocation; }
    uint32_t get_size() const { return m_size; }

private:

    VkBuffer              m_buffer{ nullptr };
    VmaAllocation         m_allocation{ nullptr };
    uint32_t              m_size{ 0 };
    VkBufferUsageFlags    m_buffer_usage; 
    VkMemoryPropertyFlags m_memory_properties;
    state *               mp_state{ nullptr };

};

} // namespace vk
} // namespace stla
