#include "vk_descriptors.h"
#include "vk_common.h"
#include "vk_state.h"
#include "vk_buffer.h"
#include "vk_raytrace.h"

#include "stla_logger.h"
#include "stla_vector.h"

#include <algorithm>

namespace stla
{
namespace vk
{

descriptor_set_binding::descriptor_set_binding(uint32_t size, uint32_t binding, VkDescriptorType type, VkShaderStageFlags shader_stage_flags) :
        size(size), binding(binding), type(type), shader_stage_flags(shader_stage_flags) { }

bool descriptor_set_layout::create(const descriptor_set_binding_array &bindings, state &state)
{
    return create(bindings.size(), bindings.data(), state);
}

bool descriptor_set_layout::create(uint32_t num_bindings, const descriptor_set_binding *p_bindings, state &state)
{
    if (p_bindings == nullptr || num_bindings == 0) {
        STLA_CORE_LOG_ERROR("invalid argument: p_bindings = {0}, num_bindings = {1}", fmt::ptr(p_bindings), num_bindings);
        return false;
    }

    if (num_bindings > MAX_DESCRIPTOR_SET_BINDINGS) {
        STLA_CORE_LOG_WARN("truncating descriptor set bindings (num_bindings={0}) (expected<={1})", num_bindings, MAX_DESCRIPTOR_SET_BINDINGS);
        num_bindings = MAX_DESCRIPTOR_SET_BINDINGS;
    }

    vector<VkDescriptorSetLayoutBinding, MAX_DESCRIPTOR_SET_BINDINGS> layout_bindings;
    
    for (int i = 0; i < num_bindings; i++) {
        VkDescriptorSetLayoutBinding vk_binding = { };
        vk_binding.binding         = p_bindings[i].binding; 
        vk_binding.descriptorType  = p_bindings[i].type;
        vk_binding.stageFlags      = p_bindings[i].shader_stage_flags;
        vk_binding.descriptorCount = 1;
        layout_bindings.push_back(vk_binding);

        m_bindings.push_back(p_bindings[i]);
    }

    VkDescriptorSetLayoutCreateInfo layout_create_info = { };
	layout_create_info.sType        = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layout_create_info.bindingCount = layout_bindings.size();
	layout_create_info.flags        = 0;
	layout_create_info.pBindings    = layout_bindings.data();

    VkDescriptorSetLayout layout = nullptr;
    if ((layout = state.vk_create_descriptor_set_layout(layout_create_info)) == nullptr) {
        return false;
    }

    if (is_valid()) {
        destroy();
    }

    std::sort(m_bindings.begin(), m_bindings.end(), [](descriptor_set_binding lhs, descriptor_set_binding rhs){
        return lhs.binding < rhs.binding;
    });

    m_layout = layout;
    mp_state = &state;
    return true;
}   

void descriptor_set_layout::destroy()
{
    if (mp_state == nullptr) {
        return;
    }

    if (m_layout != nullptr) {
        mp_state->push_deletor([layout = m_layout](state &state) {
            vkDestroyDescriptorSetLayout(state.get_device_handle(), layout, nullptr);
        });
    }

    m_bindings.clear();
    m_layout = nullptr;
    mp_state = nullptr;
}

descriptor_set_binding descriptor_set_layout::get_binding(uint32_t binding) const
{
    for (const descriptor_set_binding &desc_set : m_bindings) {
        if (desc_set.binding == binding) {
            return desc_set;
        }
    }
    STLA_ASSERT_RELEASE(0, "descriptor_set_layout doesn't have binding at %d", binding);
    return descriptor_set_binding();   
}

bool descriptor_set::bind(const descriptor_set_binding &binding, const buffer &buffer)
{
    if (!is_valid()) {
        return false;
    }

    VkDescriptorBufferInfo binfo;
    binfo.buffer = buffer.get_buffer_handle();
    binfo.offset = 0;
    binfo.range  = buffer.get_size();

    VkWriteDescriptorSet set_write = { };
    set_write.sType           = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    set_write.pNext           = nullptr;
    set_write.dstBinding      = binding.binding;
    set_write.dstSet          = m_descriptor_set;
    set_write.descriptorCount = 1;
    set_write.descriptorType  = binding.type;;
    set_write.pBufferInfo     = &binfo;

    mp_state->update_descriptor_set(set_write);

    return true;
}

bool descriptor_set::bind(const descriptor_set_binding &binding, const tlas &tlas)
{
    if (!is_valid()) {
        return false;
    }

    VkWriteDescriptorSetAccelerationStructureKHR tlas_set_write = { };
    tlas_set_write.sType                      = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
    tlas_set_write.accelerationStructureCount = 1;
    tlas_set_write.pAccelerationStructures    = &tlas.get_handle();

    VkWriteDescriptorSet set_write = { };
    set_write.sType           = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    set_write.pNext           = &tlas_set_write;
    set_write.dstBinding      = binding.binding;
    set_write.dstSet          = m_descriptor_set;
    set_write.descriptorCount = 1;
    set_write.descriptorType  = binding.type;

    mp_state->update_descriptor_set(set_write);

    return true;
}

bool descriptor_pool::create(uint32_t pool_sizes_length, const VkDescriptorPoolSize *p_pool_sizes, uint32_t max_sets, state &state)
{
    if (p_pool_sizes == nullptr) {
        return false;
    }

    VkDescriptorPoolCreateInfo pool_info = {};
	pool_info.sType         = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	pool_info.flags         = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
	pool_info.maxSets       = max_sets;
	pool_info.poolSizeCount = pool_sizes_length;
	pool_info.pPoolSizes    = &p_pool_sizes[0];

    VkDescriptorPool pool = nullptr;
    if ((pool = state.vk_create_descriptor_pool(pool_info)) == nullptr) {
        return false;
    }

    if (mp_state != nullptr) {
        destroy();
    }

    m_pool      = pool;
    mp_state = &state;

    return true;
}

bool descriptor_pool::destroy()
{
    if (mp_state == nullptr) {
        return false;
    }

    if (m_pool != nullptr) {
        mp_state->push_deletor([pool = m_pool](state &state) {
            vkDestroyDescriptorPool(state.get_device_handle(), pool, nullptr);
        });
    }

    m_pool   = nullptr;
    mp_state = nullptr;
    return true;
}

descriptor_set descriptor_pool::alloc_descriptor_set(const descriptor_set_layout &layout) const
{
    STLA_ASSERT_RELEASE(is_valid(), "descriptor_pool is invalid");

    VkDescriptorSetAllocateInfo alloc_info = {};
    alloc_info.pNext              = nullptr;
    alloc_info.sType              = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    alloc_info.descriptorPool     = m_pool;
    alloc_info.descriptorSetCount = 1;
    alloc_info.pSetLayouts        = &layout.get_handle();

    VkDescriptorSet set = mp_state->vk_create_descriptor_set(alloc_info);
    STLA_ASSERT_RELEASE(set != nullptr, "unable to allocate descriptor set");
    return descriptor_set(set, *mp_state);
}

bool descriptor_set_layout_container::create(state &state)
{
    if (mp_state != nullptr) {
        destroy();
    }

    mp_state = &state;
    return true;
}

void descriptor_set_layout_container::destroy()
{
    if (mp_state == nullptr) {
        return;
    }

    for (auto &[key, layout] : m_layouts) {
        layout.destroy();
    }

    mp_state = nullptr;
    m_layouts.clear();
}

bool descriptor_set_layout_container::create_layout(descriptor_set_loc loc, const descriptor_set_binding_array &bindings)
{
    STLA_ASSERT_RELEASE(mp_state != nullptr, "descriptor_set_layout_container is invalid");
    STLA_ASSERT_RELEASE(!has_layout(loc), "descriptor set layout already exists at location %d", (int)loc);
    STLA_ASSERT_RELEASE(bindings.size() != 0, "can't create descriptor set layout with zero bindings");

    descriptor_set_layout new_layout = mp_state->create_descriptor_set_layout(bindings);
    if (!new_layout.is_valid()) {
        return false;
    }
    m_layouts[loc] = new_layout;
    return true;
}

const descriptor_set_layout &descriptor_set_layout_container::get_layout(descriptor_set_loc loc) const
{
    STLA_ASSERT_RELEASE(mp_state != nullptr, "descriptor_set_layout_container is invalid");
    STLA_ASSERT_RELEASE(has_layout(loc), "no descriptor set layout at location %d", (int)loc);
    return m_layouts.at(loc);
}

bool descriptor_set_layout_container::has_layout(descriptor_set_loc loc) const
{
    STLA_ASSERT_RELEASE(mp_state != nullptr, "descriptor_set_layout_container is invalid");
    return m_layouts.find(loc) != m_layouts.end();
}

} // namespace vk
} // namespace stla
