#pragma once

#include "vk_command_pool.h"

#include <etl/queue.h>
#include <etl/vector.h>
#include <vulkan/vulkan.h>

#include <stdint.h>

namespace stla
{
namespace vk
{

class state;

class renderpass
{
public:

    renderpass() = default;
    renderpass(renderpass &) = default;
    renderpass(VkRenderPass pass, state &state) : m_renderpass(pass), mp_state(&state) { }
    void destroy();
    bool is_valid() const { return m_renderpass != nullptr && mp_state != nullptr; }
    const VkRenderPass &get_handle() const { return m_renderpass; }

private:

    VkRenderPass m_renderpass{ nullptr };
    state *      mp_state{ nullptr };
};

class renderpass_builder
{
public:

    enum class attachment_type
    {
        color, depth
    };

    renderpass_builder(state &state);
    
    renderpass_builder &add_attachment(
        attachment_type type,
        VkFormat format,
        VkSampleCountFlagBits samples,
        VkAttachmentLoadOp load_op,
        VkAttachmentStoreOp store_op,
        VkAttachmentLoadOp stencil_load_op,
        VkAttachmentStoreOp stencil_store_op,
        VkImageLayout initial_layout,
        VkImageLayout final_layout
    );

	bool build(renderpass &pass);

private:

    static inline const uint32_t MAX_COLOR_ATTACHMENTS = 12;

    state &m_state;
    etl::vector<VkAttachmentDescription, MAX_COLOR_ATTACHMENTS> m_color_attachments;
    bool m_has_depth_attachment{ false };
    VkAttachmentDescription m_depth_attachment{ };
};

} // namespace vk
} // namespace stla
