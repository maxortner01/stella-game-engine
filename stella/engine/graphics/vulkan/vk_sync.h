#pragma once

#include <vulkan/vulkan.h>

namespace stla
{
namespace vk
{
    
class state;

class semaphore
{
public:

    VkSemaphore m_semaphore{ nullptr };

    bool create(state &state);
    void destroy();
    bool is_valid() const { return m_semaphore != nullptr && mp_state != nullptr; }

private:

    state *mp_state{ nullptr };
};

class fence
{
public:

    VkFence m_fence{ nullptr };

    bool create(state &state);
    void destroy();
    bool wait(uint32_t timeout_ms);
    bool reset();
    bool is_vaild() const { return m_fence != nullptr && mp_state != nullptr; }

private:

    state *mp_state{ nullptr };
};

} // namespace vk
} // namespace stla
