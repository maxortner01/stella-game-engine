#include "vk_image.h"
#include "vk_state.h"

namespace stla
{
namespace vk
{

bool image::create(const create_info &create, state &state)
{
    VkImage image            = nullptr;
    VmaAllocation allocation = nullptr;
    VkImageView image_view   = nullptr;
    VkSampler sampler        = nullptr;
    VkFormat format;

    if (!state.find_supported_format(
        { create.format },
        create.tiling,
        create.features,
        &format)) {
        return false;
    }

    if (!state.vk_create_image(
        create.width, 
        create.height, 
        format, 
        create.tiling, 
        create.usage, 
        create.properties, 
        image, 
        allocation)) {
        return false;
    }

    if ((image_view = state.vk_create_image_view(image, format, create.aspect_flags)) == nullptr) {
        return false;
    }

    if ((sampler = state.vk_create_sampler()) == nullptr) {
        return false;
    }

    if (is_valid()) {
        destroy();
    }

    mp_state     = &state;
    m_width      = create.width;
    m_height     = create.height;
    m_format     = format;
    m_image      = image;
    m_allocation = allocation;
    m_image_view = image_view;
    m_sampler    = sampler;

    return true;
}

void image::destroy()
{
    if (mp_state == nullptr) {
        return;
    }

    if (m_sampler != nullptr) {
        mp_state->push_deletor([sampler = m_sampler](state &state) {
            vkDestroySampler(state.get_device_handle(), sampler, nullptr);
        });
    }

    if (m_image_view != nullptr) {
        mp_state->push_deletor([image_view = m_image_view](state &state) {
            vkDestroyImageView(state.get_device_handle(), image_view, nullptr);
        });
    }

    if (m_image != nullptr && m_allocation != nullptr) {
        mp_state->push_deletor([image = m_image, allocation = m_allocation](state &state) {
            vmaDestroyImage(state.m_allocator, image, allocation);
        });
    }

    m_width      = 0;
    m_height     = 0;
    m_format     = VK_FORMAT_UNDEFINED;
    mp_state     = nullptr;
    m_image      = nullptr;
    m_image_view = nullptr;
    m_sampler    = nullptr;
}

bool image::transition_layout(VkImageLayout old_layout, VkImageLayout new_layout, graphics_command_buffer &cmd_buffer, fence *p_fence) const
{
    if (p_fence) {
        p_fence->reset();
    }

    cmd_buffer.begin();
    cmd_buffer.transition_image_layout(*this, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    if (p_fence) {
        cmd_buffer.end(*p_fence);
    }
    else {
        cmd_buffer.end();
    }
    
    return mp_state->submit_command_buffer(cmd_buffer);
}
    
} // namespace vk
} // namespace stla
