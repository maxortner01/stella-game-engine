#pragma once

#include "vk_command_buffer.h"

#include "stla_list.h"

#include <vulkan/vulkan.h>

#include <stdint.h>

namespace stla
{
namespace vk
{

class state;

template<typename T>
class command_pool
{
public:

    bool create(uint32_t queue_family_index, state &state);
    void destroy();
    T acquire_command_buffer();
    bool return_command_buffer(const T &cmd_buffer);
    bool is_valid() const { return m_command_pool != nullptr && mp_state != nullptr; }

private:

    VkCommandPool m_command_pool{ nullptr };
    list_32<T>    m_command_buffers;
    state *       mp_state{ nullptr };
};

using graphics_command_pool = command_pool<graphics_command_buffer>;
using compute_command_pool  = command_pool<compute_command_buffer>;

} // namespace vk
} // namespace stla
