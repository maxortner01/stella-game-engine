#pragma once

#include "vk_image.h"
#include "stla_vector.h"

#include <vulkan/vulkan.h>

#include <stdint.h>

namespace stla
{
namespace vk
{

class renderpass;

class render_target
{
public:

    render_target() = default;
    render_target(const render_target &) = default;
    render_target(uint32_t width, uint32_t height, VkFramebuffer framebuffer, uint32_t attachment_count, image *p_attachments, state *p_state);
    
    void destroy();
    bool is_valid() const { return m_framebuffer != nullptr && mp_state != nullptr; }
    const VkFramebuffer &get_framebuffer_handle() const { return m_framebuffer; }
    uint32_t get_width() const { return m_width; }
    uint32_t get_height() const { return m_height; }
    const image &get_attachment_by_index(uint8_t index);

private:

    VkFramebuffer   m_framebuffer{ nullptr };
    uint32_t        m_width{ 0 };
    uint32_t        m_height{ 0 };
    vector_8<image> m_attachments;
    state *         mp_state{ nullptr };
};

class render_target_builder
{
public:

    render_target_builder(uint32_t width, uint32_t height, renderpass &pass, state &state) :
        m_width(width), m_height(height), m_renderpass(pass), m_state(state) { }

    render_target_builder &add_attachment(
        VkFormat format, 
        VkImageTiling tiling, 
        VkImageUsageFlags usage, 
        VkMemoryPropertyFlags props, 
        VkImageAspectFlags aspect_flags, 
        VkFormatFeatureFlags features);

    bool build(render_target &target);

private:

    uint32_t                     m_width{ 0 };
    uint32_t                     m_height{ 0 };
    renderpass &                 m_renderpass;
    state &                      m_state;
    vector_8<image::create_info> m_attachment_create_infos;
};

} // namespace vk
} // namespace stla
