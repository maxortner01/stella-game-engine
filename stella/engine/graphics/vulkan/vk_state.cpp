#include "vk_state.h"
#include "vk_common.h"
#include "vk_shader.h"
#include "vk_buffer.h"
#include "vk_renderpass.h"
#include "vk_command_buffer.h"
#include "vk_render_target.h"
#include "vk_raster_pipeline.h"
#include "extensions_vk.hpp"

#include "stla_logger.h"

#include <VkBootstrap.h>
#define VMA_IMPLEMENTATION
#include <vma/vk_mem_alloc.h>

#include <stdio.h>
#include <stdlib.h>

static const uint64_t ONE_SECOND_NS = 1000000000;

/*
    
Stella Vulkan Config File (.yaml)

instance:
    app_name: "Stella Application"
    engine_name: "Stella Vulkan Engine"
    required_api_version:
        major: 1
        minor: 3
        patch: 0
    request_validation_layers: true
    validation_features:
    - VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_EXT
    - VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT
    - VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT
    - VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT
    - VK_VALIDATION_FEATURE_ENABLE_SYNCHRONIZATION_VALIDATION_EXT
    debug_message_types:
    - VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
    - VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
    - VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT 
    - VK_DEBUG_UTILS_MESSAGE_TYPE_DEVICE_ADDRESS_BINDING_BIT_EXT

device:
    minimum_version:
        major: 1
        minor: 3
    features:
    ...
    

*/

namespace stla
{
namespace vk 
{

static VKAPI_ATTR VkBool32 VKAPI_CALL vulkan_debug_callback(
    VkDebugUtilsMessageSeverityFlagBitsEXT severity,
    VkDebugUtilsMessageTypeFlagsEXT type,
    const VkDebugUtilsMessengerCallbackDataEXT *p_callback_data,
    void*
) {
    if ((severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT) != 0) {
        STLA_CORE_LOG_ERROR(p_callback_data->pMessage);
    }
    else if ((severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) != 0) {
        STLA_CORE_LOG_WARN(p_callback_data->pMessage);
    }
    else if ((severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT) != 0) {
        STLA_CORE_LOG_INFO(p_callback_data->pMessage);
    }

    return VK_FALSE;
}

bool state::init(create_info &create)
{
    if (m_instance != nullptr) {
        return false;
    }

    vkb::InstanceBuilder builder;

    // Make the vulkan instance with basic debug features
    auto inst_ret = builder
        .set_app_name("Stella Vulkan Application")
        .request_validation_layers(true)
        .require_api_version(1, 3, 0)
        .add_validation_feature_enable(VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT)
        //.add_validation_feature_enable(VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT)
        .add_debug_messenger_type(
            VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
        )
        .add_debug_messenger_severity(
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT
        )
        //.use_default_debug_messenger()
        .set_debug_callback(vulkan_debug_callback)
        .build();

    vkb::Instance vkb_inst = inst_ret.value();

    m_instance        = vkb_inst.instance;
    m_debug_messenger = vkb_inst.debug_messenger;

    if (!create.m_create_surface(m_instance, &m_surface)) {
        return false;
    }

    // Select a GPU that can write to the surface
    vkb::PhysicalDeviceSelector selector(vkb_inst);
    VkPhysicalDeviceFeatures features = { };
    features.samplerAnisotropy = true;

    VkPhysicalDeviceAccelerationStructureFeaturesKHR rt_acceleration_features = { };
    rt_acceleration_features.sType                 = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;
    rt_acceleration_features.accelerationStructure = true;
    rt_acceleration_features.accelerationStructureCaptureReplay = true;

    VkPhysicalDeviceRayTracingPipelineFeaturesKHR rt_pipeline_features = { }; 
    rt_pipeline_features.sType              = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR;
    rt_pipeline_features.rayTracingPipeline = true;

    VkPhysicalDeviceRayQueryFeaturesKHR rt_ray_query_features = { };
    rt_ray_query_features.sType    = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR;
    rt_ray_query_features.rayQuery = true;

    VkPhysicalDeviceBufferDeviceAddressFeatures physical_device_buffer_addr_features = { };
    physical_device_buffer_addr_features.sType               = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES;
    physical_device_buffer_addr_features.bufferDeviceAddress = true;

    //todo tmp
    VkPhysicalDeviceMaintenance4Features maintenance4_features = { };
    maintenance4_features.sType        = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_4_FEATURES;
    maintenance4_features.maintenance4 = true;
    //

    VkValidationFeatureEnableEXT validation_features[] = { 
        VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT 
    };
    const uint32_t validation_feature_count = sizeof(validation_features) / sizeof(VkValidationFeatureEnableEXT);
    
    VkValidationFeaturesEXT validation_info_features = { };
    validation_info_features.sType                = VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT;
    validation_info_features.enabledValidationFeatureCount = validation_feature_count;
    validation_info_features.pEnabledValidationFeatures    = &validation_features[0];

    vkb::PhysicalDevice physical_device = selector
        .set_minimum_version(1, 3)
        .set_surface(m_surface)
        .set_required_features(features)
        .allow_any_gpu_device_type(false) // force dedicated GPU
        .prefer_gpu_device_type(vkb::PreferredDeviceType::discrete)
        .add_required_extension(VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME)
        .add_required_extension_features(rt_acceleration_features)
        .add_required_extension(VK_KHR_RAY_TRACING_PIPELINE_EXTENSION_NAME)
        .add_required_extension_features(rt_pipeline_features)
        .add_required_extension(VK_KHR_RAY_QUERY_EXTENSION_NAME)
        .add_required_extension_features(rt_ray_query_features)
        .add_required_extension(VK_KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME)
        .add_required_extension_features(physical_device_buffer_addr_features)
        .add_required_extension_features(maintenance4_features)
        .add_required_extension(VK_KHR_SHADER_NON_SEMANTIC_INFO_EXTENSION_NAME)
        .select()
        .value();

    #ifdef _WIN32
        _putenv_s("DEBUG_PRINTF_TO_STDOUT", "1");
    #else  // If not _WIN32
        static char putenvString[] = "DEBUG_PRINTF_TO_STDOUT=1";
        putenv(putenvString);
    #endif  // _WIN32

    // Create the final Vulkan Device
    vkb::DeviceBuilder device_builder(physical_device);
    vkb::Device device = device_builder.build().value();

    m_device                            = device.device;
    m_physical_device                   = physical_device.physical_device;
    m_physical_device_features          = physical_device.features;
    m_physical_device_properties        = physical_device.properties;
    m_physical_device_memory_properties = physical_device.memory_properties;
    m_graphics_queue.queue              = device.get_queue(vkb::QueueType::graphics).value();
    m_graphics_queue.queue_family       = device.get_queue_index(vkb::QueueType::graphics).value();
    m_compute_queue.queue               = device.get_queue(vkb::QueueType::compute).value();
    m_compute_queue.queue_family        = device.get_queue_index(vkb::QueueType::compute).value();
    m_transfer_queue.queue              = device.get_queue(vkb::QueueType::transfer).value();
    m_transfer_queue.queue_family       = device.get_queue_index(vkb::QueueType::transfer).value();

    VmaAllocatorCreateInfo allocator_info = { };
    allocator_info.flags          = VMA_ALLOCATOR_CREATE_BUFFER_DEVICE_ADDRESS_BIT;
    allocator_info.physicalDevice = m_physical_device;
    allocator_info.device         = m_device;
    allocator_info.instance       = m_instance;
    VK_ASSERT(vmaCreateAllocator(&allocator_info, &m_allocator));

    load_VK_EXTENSIONS(m_instance, vkGetInstanceProcAddr, m_device, vkGetDeviceProcAddr);
    
    const VkDescriptorPoolSize pool_sizes[] = {
        { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,              1000 },
        { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,             1000 },
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,             1000 },
        { VK_DESCRIPTOR_TYPE_SAMPLER,                    1000 },
        { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,     1000 },
        { VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,              1000 },
        { VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER,       1000 },
        { VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER,       1000 },
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC,     1000 },
        { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC,     1000 },
        { VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,           1000 },
        { VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR, 1000 }
    };

    m_descriptor_pool = create_descriptor_pool(std::size(pool_sizes), &pool_sizes[0], 10);
    if (!m_descriptor_pool.is_valid()) {
        return false;
    }

    m_graphics_command_pool = create_graphics_command_pool();
    m_fence = create_fence();
    return true;
}

bool state::shutdown()
{
    if (m_instance == nullptr) {
        return false;
    }

    STLA_ASSERT(m_fence.wait(1000), "fence timed out in %s", __FUNCTION__);

    vkDeviceWaitIdle(m_device);

    m_fence.destroy();
    m_graphics_command_pool.destroy();
    m_descriptor_pool.destroy();

    flush_deletor_queue();

    vmaDestroyAllocator(m_allocator);

    vkDestroyDevice(m_device, nullptr);
    
    if (m_surface != nullptr) {
        vkDestroySurfaceKHR(m_instance, m_surface, nullptr);
    }
    
    vkb::destroy_debug_utils_messenger(m_instance, m_debug_messenger);
    vkDestroyInstance(m_instance, nullptr);

    return true;
}

VkPipeline state::vk_create_pipline(const VkGraphicsPipelineCreateInfo &create_info) const 
{
    VkPipeline pipeline = nullptr;

    VK_ASSERT(vkCreateGraphicsPipelines(m_device, nullptr, 1, &create_info, nullptr, &pipeline));
    
    STLA_CORE_LOG_INFO("VkPipeline created");
    
    return pipeline;
};

VkPipelineLayout state::vk_create_pipeline_layout(const VkPipelineLayoutCreateInfo &create_info) const
{
    VkPipelineLayout layout = nullptr;

    VK_ASSERT(vkCreatePipelineLayout(m_device, &create_info, nullptr, &layout));

    STLA_CORE_LOG_INFO("VkPipelineLayout created");

    return layout;
}

VkRenderPass state::vk_create_renderpass(const VkRenderPassCreateInfo &create_info) const
{
    VkRenderPass renderpass = nullptr;

    VK_ASSERT(vkCreateRenderPass(m_device, &create_info, nullptr, &renderpass));
    
    STLA_CORE_LOG_INFO("VkRenderPass created");

    return renderpass;
}

VkShaderModule state::vk_create_shader_module(const VkShaderModuleCreateInfo& create_info) const
{
    VkShaderModule shader_module = nullptr;
    
    VK_ASSERT(vkCreateShaderModule(m_device, &create_info, nullptr, &shader_module));
    
    STLA_CORE_LOG_INFO("VkShaderModule created");

    return shader_module;
}

VkDescriptorSetLayout state::vk_create_descriptor_set_layout(const VkDescriptorSetLayoutCreateInfo &create_info) const
{
    VkDescriptorSetLayout layout = nullptr;

    VK_ASSERT(vkCreateDescriptorSetLayout(m_device, &create_info, nullptr, &layout));

    STLA_CORE_LOG_INFO("VkDescriptorSetLayout created");
    
    return layout;
}

VkDescriptorPool state::vk_create_descriptor_pool(const VkDescriptorPoolCreateInfo &create_info) const
{
    VkDescriptorPool pool = nullptr;
    
    VK_ASSERT(vkCreateDescriptorPool(m_device, &create_info, nullptr, &pool));
    
    STLA_CORE_LOG_INFO("VkDescriptorPool created");

    return pool;
}

VkDescriptorSet state::vk_create_descriptor_set(const VkDescriptorSetAllocateInfo &create_info) const
{
    VkDescriptorSet desc_set = nullptr;
    
    VK_ASSERT(vkAllocateDescriptorSets(m_device, &create_info, &desc_set));

    STLA_CORE_LOG_INFO("VkDescriptorSet created");

    return desc_set;
}

VkFramebuffer state::vk_create_framebuffer(const VkFramebufferCreateInfo &create_info) const
{
    VkFramebuffer framebuffer = nullptr;

    VK_ASSERT(vkCreateFramebuffer(m_device, &create_info, nullptr, &framebuffer));

    STLA_CORE_LOG_INFO("VkFramebuffer created");

    return framebuffer;
}

VkSemaphore state::vk_create_semaphore(const VkSemaphoreCreateInfo &create_info) const
{
    VkSemaphore semaphore = nullptr;
    
    VK_ASSERT(vkCreateSemaphore(m_device, &create_info, nullptr, &semaphore));

    STLA_CORE_LOG_INFO("VkSemaphore created");

    return semaphore;
}

VkFence state::vk_create_fence(const VkFenceCreateInfo &create_info) const
{
    VkFence fence = nullptr;
    
    VK_ASSERT(vkCreateFence(m_device, &create_info, nullptr, &fence));

    STLA_CORE_LOG_INFO("VkFence created");

    return fence;
}

VkCommandPool state::vk_create_command_pool(const VkCommandPoolCreateInfo &create_info) const
{
    VkCommandPool cmd_pool = nullptr;

    VK_ASSERT(vkCreateCommandPool(m_device, &create_info, nullptr, &cmd_pool));

    STLA_CORE_LOG_INFO("VkCommandPool created");
    
    return cmd_pool;
}

VkCommandBuffer state::vk_alloc_command_buffer(const VkCommandBufferAllocateInfo &alloc_info) const
{
    VkCommandBuffer cmd_buffer = nullptr;

    VK_ASSERT(vkAllocateCommandBuffers(m_device, &alloc_info, &cmd_buffer));
    
    STLA_CORE_LOG_INFO("VkCommandBuffers allocated (count={0})", alloc_info.commandBufferCount);

    return cmd_buffer;
}

VkImageView state::vk_create_image_view(VkImage image, VkFormat format, VkImageAspectFlags aspect_flags) const
{    
    VkImageView image_view = nullptr;

    VkImageViewCreateInfo createInfo = { };
    createInfo.sType    = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    createInfo.image    = image;
    createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D; // 1D textures, 2D textures, 3D textures and cube maps
    createInfo.format   = format;
    // Allows for swizzling the color channels around
    createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    // Describe what the image's purpose is and which part of the image should be accessed. 
    // Will be used as color targets without any mipmapping levels or multiple layers.
    createInfo.subresourceRange.aspectMask     = aspect_flags; //VK_IMAGE_ASPECT_COLOR_BIT;
    createInfo.subresourceRange.baseMipLevel   = 0;
    createInfo.subresourceRange.levelCount     = 1;
    createInfo.subresourceRange.baseArrayLayer = 0;
    createInfo.subresourceRange.layerCount     = 1;

    VK_ASSERT(vkCreateImageView(m_device, &createInfo, nullptr, &image_view));

    STLA_CORE_LOG_INFO("VkImageView created (format={0}) (aspect_flags={1:#b})", format, aspect_flags);

    return image_view;
}

VkSampler state::vk_create_sampler() const
{
    VkSampler sampler = nullptr; 

    VkSamplerCreateInfo sampler_info = { };
    sampler_info.sType            = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    sampler_info.magFilter        = VK_FILTER_LINEAR;
    sampler_info.minFilter        = VK_FILTER_LINEAR;
    sampler_info.addressModeU     = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    sampler_info.addressModeV     = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    sampler_info.addressModeW     = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    sampler_info.anisotropyEnable = VK_TRUE;

    VkPhysicalDeviceProperties properties = { };
    vkGetPhysicalDeviceProperties(m_physical_device, &properties);

    sampler_info.maxAnisotropy           = properties.limits.maxSamplerAnisotropy;
    sampler_info.borderColor             = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    sampler_info.unnormalizedCoordinates = VK_FALSE;
    sampler_info.compareEnable           = VK_FALSE;
    sampler_info.compareOp               = VK_COMPARE_OP_ALWAYS;
    sampler_info.mipmapMode              = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    sampler_info.mipLodBias              = 0.0f;
    sampler_info.minLod                  = 0.0f;
    sampler_info.maxLod                  = 0.0f;

    VK_ASSERT(vkCreateSampler(m_device, &sampler_info, nullptr, &sampler));

    STLA_CORE_LOG_INFO("VkSampler created");

    return sampler;
}

VkDeviceAddress state::vk_get_buffer_device_address(const VkBufferDeviceAddressInfo &info) const
{
    return vkGetBufferDeviceAddress(m_device, &info);
}

VkDeviceAddress state::vk_get_buffer_device_address(const buffer &buffer) const
{
    VkBufferDeviceAddressInfo info = { };
    info.sType  = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    info.pNext  = nullptr;
    info.buffer = buffer.get_buffer_handle();  
    return vkGetBufferDeviceAddress(m_device, &info); 
}

bool state::vk_create_image(
    uint32_t width, 
    uint32_t height,
    VkFormat format, 
    VkImageTiling tiling, 
    VkImageUsageFlags usage, 
    VkMemoryPropertyFlags properties, 
    VkImage &image, 
    VmaAllocation &allocation) const
{

    VkImageCreateInfo image_info = { };
    image_info.sType         = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    image_info.imageType     = VK_IMAGE_TYPE_2D;
    image_info.extent.width  = width;
    image_info.extent.height = height;
    image_info.extent.depth  = 1;
    image_info.mipLevels     = 1;
    image_info.arrayLayers   = 1;
    image_info.format        = format;
    image_info.tiling        = tiling;
    image_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    image_info.usage         = usage | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    image_info.samples       = VK_SAMPLE_COUNT_1_BIT;
    image_info.sharingMode   = VK_SHARING_MODE_EXCLUSIVE;
    image_info.queueFamilyIndexCount = 1;
    image_info.pQueueFamilyIndices   = &m_graphics_queue.queue_family;

    VmaAllocationCreateInfo alloc_info = { };
    alloc_info.usage = VMA_MEMORY_USAGE_AUTO;

    VkResult result = VK_SUCCESS;

    VK_ASSERT(result = vmaCreateImage(m_allocator, &image_info, &alloc_info, &image, &allocation, nullptr));

    STLA_CORE_LOG_INFO("VkImage & VmaAllocation created (image_usage={0:#b}) (mem_props={1:#b})", usage, properties);

    return result == VK_SUCCESS;
}


bool state::vk_create_buffer(
    uint32_t size, 
    const VkBufferCreateInfo &buffer_create_info, 
    const VmaAllocationCreateInfo &allocation_create_info, 
    VkBuffer &buffer, 
    VmaAllocation &allocation) const
{
    VkResult result = VK_SUCCESS;
    
    VK_ASSERT(result = vmaCreateBuffer(m_allocator, &buffer_create_info, &allocation_create_info, &buffer, &allocation, nullptr));

    STLA_CORE_LOG_INFO("VkBuffer & VmaAllocation created (buffer_usage={0:#b}) (mem_props={1:#b})", 
        buffer_create_info.usage, allocation_create_info.requiredFlags);

    return result == VK_SUCCESS;
}

VkAccelerationStructureKHR state::vk_create_acceleration_structure(const VkAccelerationStructureCreateInfoKHR &create_info) const
{
    VkAccelerationStructureKHR structure = nullptr;

    VK_ASSERT(vkCreateAccelerationStructureKHR(m_device, &create_info, nullptr, &structure));

    STLA_CORE_LOG_INFO("VkAccelerationStructureKHR created");

    return structure;
}

VkDeviceAddress state::vk_get_acceleration_structure_device_address(const VkAccelerationStructureDeviceAddressInfoKHR &address_info) const
{
    return vkGetAccelerationStructureDeviceAddressKHR(m_device, &address_info);
}

VkAccelerationStructureBuildSizesInfoKHR state::vk_get_acceleration_structure_build_sizes(
        const VkAccelerationStructureBuildGeometryInfoKHR &build_info, uint32_t max_primitive_count)
{
    VkAccelerationStructureBuildSizesInfoKHR size_info = { };
    size_info.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
    
    vkGetAccelerationStructureBuildSizesKHR(
        m_device, VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
        &build_info, &max_primitive_count, &size_info);

    return size_info;
}

renderpass_builder state::create_renderpass_builder() 
{
    STLA_CORE_LOG_INFO("Renderpass Builder created");
    return renderpass_builder(*this);
}

raster_pipeline_builder state::create_raster_pipeline_builder() 
{
    STLA_CORE_LOG_INFO("Raster Pipeline Builder created");
    return raster_pipeline_builder(*this);
}

render_target_builder state::create_render_target_builder(uint32_t width, uint32_t height, renderpass& pass)
{
    STLA_CORE_LOG_INFO("Render Target Builder created");
    return render_target_builder(width, height, pass, *this);
}

shader state::create_shader(shader_type type, uint32_t src_len, const char *p_src) 
{
    shader s;
    
    STLA_ASSERT(s.create(type, src_len, p_src, *this), "unable to create shader");

    STLA_CORE_LOG_INFO("shader created (type={0})", (uint32_t)type);

    return s;
}

shader state::create_shader(shader_type type, const shader_source_code &src) 
{
    shader s;
    
    STLA_ASSERT(s.create(type, src, *this), "unable to create shader");

    STLA_CORE_LOG_INFO("shader created (type={0})", (uint32_t)type);

    return s;
}

shader state::create_shader(shader_type type, const spirv_byte_code &spirv) 
{
    shader s;
    
    STLA_ASSERT(s.create(type, spirv, *this), "unable to create shader");

    STLA_CORE_LOG_INFO("shader created (type={0})", (uint32_t)type);

    return s;
}

descriptor_pool state::create_descriptor_pool(uint32_t pool_sizes_length, const VkDescriptorPoolSize *p_pool_sizes, uint32_t max_sets)
{
    descriptor_pool pool;
    
    STLA_ASSERT(pool.create(pool_sizes_length, p_pool_sizes, max_sets, *this), "unable to create descriptor_pool");

    STLA_CORE_LOG_INFO("descriptor_pool created (pool_sizes_length={0}) (max_sets={1})", pool_sizes_length, max_sets);

    return pool;
}

buffer state::create_buffer(uint32_t size, VkBufferUsageFlags usage, VkMemoryPropertyFlags memory_properties, const void *p_data)
{
    buffer b;

    STLA_ASSERT(b.alloc(size, usage | s_default_buffer_usage_flags, memory_properties, *this), "unable to create buffer");
    
    STLA_CORE_LOG_INFO("buffer alloc (size={0}) (buffer_usage={1:#b}) (memory_usage={2:#x})", size, usage, memory_properties);
    
    if (p_data != nullptr) {
        STLA_ASSERT(b.copy_from(size, p_data), "unable to initialize buffer data");
    }

    return b;
}

buffer state::create_buffer(uint32_t size, const void *p_data)
{
    return create_buffer(size, s_default_buffer_usage_flags, s_default_memory_property_flags, p_data);
}

buffer state::create_buffer(const descriptor_set_binding &binding, const void *p_data)
{
    STLA_ASSERT_RELEASE(
        binding.type == VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER || binding.type == VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        "invalid descriptor type in %s (binding=%d)", __FUNCTION__, binding.type);

    return create_buffer(
        binding.size, binding.usage_flags, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, p_data);
}

buffer state::create_buffer(uint32_t size, const descriptor_set_binding &binding, const void *p_data)
{
    descriptor_set_binding updated_binding = binding;
    updated_binding.size = size;
    return create_buffer(updated_binding, p_data);
}

buffer state::create_vertex_buffer(uint32_t size, const void *p_data)
{
    return create_buffer(size, s_default_buffer_usage_flags | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, s_default_memory_property_flags, p_data);
}

buffer state::create_index_buffer(uint32_t size, const void *p_data)
{
    return create_buffer(size, s_default_buffer_usage_flags | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, s_default_memory_property_flags, p_data);
}

descriptor_set_layout state::create_descriptor_set_layout(const descriptor_set_binding_array &bindings)
{
    return create_descriptor_set_layout(bindings.size(), bindings.data());
}

descriptor_set_layout state::create_descriptor_set_layout(uint32_t num_bindings, const descriptor_set_binding *p_bindings)
{
    descriptor_set_layout layout;
    
    STLA_ASSERT(layout.create(num_bindings, p_bindings, *this), "unable to create descriptor_set_layout");
    
    STLA_CORE_LOG_INFO("descriptor_set_layout created (num_bindings={0})", num_bindings);
    
    return layout;
}

fence state::create_fence()
{
    fence f;

    STLA_ASSERT(f.create(*this), "unable to create fence");
    
    STLA_CORE_LOG_INFO("fence created");
    
    return f;
}

semaphore state::create_semaphore()
{
    semaphore s;
    
    STLA_ASSERT(s.create(*this), "unable to create semaphore");

    STLA_CORE_LOG_INFO("semaphore created");
    
    return s;
}

graphics_command_pool state::create_graphics_command_pool()
{
    graphics_command_pool cmd_pool;

    STLA_ASSERT(cmd_pool.create(m_graphics_queue.queue_family, *this), "unable to create graphics_command_pool");

    STLA_CORE_LOG_INFO("graphics_command_pool created");

    return cmd_pool;
}

compute_command_pool state::create_compute_command_pool()
{
    compute_command_pool cmd_pool;

    STLA_ASSERT(cmd_pool.create(m_compute_queue.queue_family, *this), "unable to create compute_command_pool");

    STLA_CORE_LOG_INFO("compute_command_pool created");

    return cmd_pool;
}

raster_pipeline_context state::create_raster_pipeline_context(renderpass &renderpass, const char *p_shader_path, raster_pipeline_context::config &config)
{
    raster_pipeline_context context;

    STLA_ASSERT(context.create(renderpass, p_shader_path, config, *this), "unable to create raster_pipeline_context");

    STLA_CORE_LOG_INFO("raster_pipeline_context created");

    return context;
}

compute_pipeline_context state::create_compute_pipeline_context_from_file(const char *p_shader_path)
{
    compute_pipeline_context context;

    STLA_ASSERT(context.create_from_file(p_shader_path, *this), "unable to create compute_pipeline_context");

    STLA_CORE_LOG_INFO("compute_pipeline_context created");

    return context;
}

compute_pipeline_context state::create_compute_pipeline_context_from_src(const shader_source_code &src)
{
    compute_pipeline_context context;

    STLA_ASSERT(context.create_from_src(src, *this), "unable to create compute_pipeline_context");

    STLA_CORE_LOG_INFO("compute_pipeline_context created");

    return context;
}

image state::create_image(const image::create_info &create_info)
{
    image img;

    STLA_ASSERT(img.create(create_info, *this), "unable to create image");

    STLA_CORE_LOG_INFO("image created");

    return img;
}

descriptor_set state::alloc_descriptor_set(const descriptor_set_layout &layout) const
{
    return m_descriptor_pool.alloc_descriptor_set(layout);
}

bool state::free_descriptor_set(descriptor_set set) const
{
    if (!set.is_valid()) {
        return false;
    }

    return vkFreeDescriptorSets(m_device, m_descriptor_pool.get_handle(), 1, &set.get_handle());
}

void state::update_descriptor_set(const VkWriteDescriptorSet &set_write)
{
    vkUpdateDescriptorSets(m_device, 1, &set_write, 0, nullptr);
}

/*
bool state::submit_command_buffer(queue_type type, command_buffer &cmd_buffer)
{
    VkPipelineStageFlags wait_stage_flags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    
    switch (type)
    {
        case queue_type::graphics:
        {
            VkSubmitInfo submit = { };
            submit.sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
            submit.pNext                = nullptr;
            //submit.pWaitDstStageMask    = &wait_stage_flags;
            submit.commandBufferCount   = 1;
            submit.pCommandBuffers      = &cmd_buffer.m_command_buffer;

            VK_ASSERT(vkQueueSubmit(m_graphics_queue.m_queue, 1, &submit, nullptr));
            return true;
        }
        default: STLA_ASSERT(false, "Submit for queue type (%d) not implemented", type);
    }

    return false;
}*/

bool state::submit_command_buffer(graphics_command_buffer &cmd_buffer)
{
    vector_8<VkSemaphore> vk_wait_semaphores   = cmd_buffer.get_vk_wait_semaphores();
    vector_8<VkSemaphore> vk_signal_semaphores = cmd_buffer.get_vk_signal_semaphores();

    return vk_submit_buffer(
        m_graphics_queue.queue, 
        cmd_buffer.get_handle(),
        cmd_buffer.get_fence() != nullptr ? cmd_buffer.get_fence()->m_fence : nullptr,
        vk_signal_semaphores.size(), vk_signal_semaphores.data(), 
        vk_wait_semaphores.size(), vk_wait_semaphores.data());   
}

bool state::submit_command_buffer(compute_command_buffer &cmd_buffer)
{
    vector_8<VkSemaphore> vk_wait_semaphores   = cmd_buffer.get_vk_wait_semaphores();
    vector_8<VkSemaphore> vk_signal_semaphores = cmd_buffer.get_vk_signal_semaphores();

    return vk_submit_buffer(
        m_compute_queue.queue, 
        cmd_buffer.get_handle(),
        cmd_buffer.get_fence() != nullptr ? cmd_buffer.get_fence()->m_fence : nullptr,
        vk_signal_semaphores.size(), vk_signal_semaphores.data(), 
        vk_wait_semaphores.size(), vk_wait_semaphores.data()); 
}

bool state::execute_graphics_commands(std::function<void(graphics_command_buffer &)> record_cmds_callback, uint32_t timeout_ms)
{
    graphics_command_buffer cmd_buffer = m_graphics_command_pool.acquire_command_buffer();
    if (!cmd_buffer.is_valid()) {
        return false;
    }

    if (!cmd_buffer.begin()) {
        return false;
    }

    record_cmds_callback(cmd_buffer);
    
    m_fence.reset();
    if (!cmd_buffer.end(m_fence)) {
        return false;
    }
    
    if (!submit_command_buffer(cmd_buffer)) {
        return false;
    }

    bool wait_result = m_fence.wait(timeout_ms);
    STLA_ASSERT_RELEASE(wait_result, "fence timed out in %s", __FUNCTION__);
    if (!wait_result) {
        return false;
    }

    if (!m_graphics_command_pool.return_command_buffer(cmd_buffer)) {
        return false;
    }

    return true;
}

bool state::vk_submit_buffer(
    VkQueue queue, 
    VkCommandBuffer cmd_buffer,
    VkFence fence, 
    uint16_t signal_sem_count, 
    VkSemaphore *p_signal_sems, 
    uint16_t wait_sem_count, 
    VkSemaphore *p_wait_sems)
{
    std::lock_guard lg(m_mutex);

    //todo put flags in semaphore
    VkPipelineStageFlags wait_flags = VK_PIPELINE_STAGE_VERTEX_INPUT_BIT;

    VkSubmitInfo submit = { };
    submit.sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit.pNext                = nullptr;
    submit.waitSemaphoreCount   = wait_sem_count;
    submit.pWaitSemaphores      = wait_sem_count ? p_wait_sems : nullptr;
    submit.signalSemaphoreCount = signal_sem_count;
    submit.pSignalSemaphores    = signal_sem_count ? p_signal_sems : nullptr;
    submit.commandBufferCount   = 1;
    submit.pCommandBuffers      = &cmd_buffer;
    submit.pWaitDstStageMask    = &wait_flags;
    
    VkResult result = vkQueueSubmit(queue, 1, &submit, fence);
    VK_ASSERT(result);
    return result == VK_SUCCESS;
}

void state::queue_wait_idle(queue_type type)
{
    switch (type)
    {
        case queue_type::graphics:
        {
            VK_ASSERT(vkQueueWaitIdle(m_graphics_queue.queue));
            break;
        }
        case queue_type::compute:
        {
            VK_ASSERT(vkQueueWaitIdle(m_compute_queue.queue));
            break;
        }
        case queue_type::transfer:
        {
            VK_ASSERT(vkQueueWaitIdle(m_transfer_queue.queue));
            break;
        }
        default: STLA_ASSERT(false, "Submit for queue type (%d) not implemented", type);
    }
}

void state::device_wait_idle()
{
    vkDeviceWaitIdle(m_device);
}

void state::push_deletor(delete_object_callback &&fn)
{
    m_deletors.push_back(fn);
}

void state::flush_deletor_queue()
{
    for (auto d : m_deletors) {
        d(*this);
    }

    m_deletors.clear();
}

bool state::find_supported_format(const std::vector<VkFormat> &candidates, VkImageTiling tiling, VkFormatFeatureFlags features, VkFormat *p_format)
{
    if (p_format == nullptr) {
        return false;
    }

    for (VkFormat format : candidates) {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(m_physical_device, format, &props);

        if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
            *p_format = format;
            return true;
        }
        else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
            *p_format = format;
            return true;
        }
    }
    
    return false;
}

}
}