#pragma once

#include "vk_command_pool.h"
#include "vk_raster_pipeline.h"
#include "vk_renderpass.h"
#include "vk_descriptors.h"
#include "vk_shader.h"
#include "vk_render_target.h"
#include "vk_buffer.h"
#include "vk_sync.h"
#include "vk_pipeline_context.h"
#include "vk_image.h"

#include "stla_assert.h"

#include <vulkan/vulkan.h>
#include <vma/vk_mem_alloc.h>

#include <stdint.h>

#include <deque>
#include <mutex>

namespace stla
{

class imgui_context;

namespace vk 
{

using create_surface_fn = std::function<bool(VkInstance, VkSurfaceKHR *)>;

enum class queue_type
{
    graphics, compute, transfer
};

class state
{
public:

    struct create_info
    {
        const char *      mp_name{ nullptr };
        create_surface_fn m_create_surface;
    };

    state()  = default;
    ~state() = default;

    bool init(create_info &create);
    bool shutdown();
    bool initialized() const { return m_device != nullptr; }

    const VkDevice &get_device_handle() const { return m_device; }
    const VmaAllocator &get_allocator_handle() const { return m_allocator; }

    // Vulkan Primitives
    VkPipeline            vk_create_pipline(const VkGraphicsPipelineCreateInfo &create_info) const;
    VkPipelineLayout      vk_create_pipeline_layout(const VkPipelineLayoutCreateInfo &create_info) const;
    VkRenderPass          vk_create_renderpass(const VkRenderPassCreateInfo &create_info) const;
    VkShaderModule        vk_create_shader_module(const VkShaderModuleCreateInfo& create_info) const;
    VkDescriptorSetLayout vk_create_descriptor_set_layout(const VkDescriptorSetLayoutCreateInfo &create_info) const;
    VkDescriptorPool      vk_create_descriptor_pool(const VkDescriptorPoolCreateInfo &create_info) const;
    VkDescriptorSet       vk_create_descriptor_set(const VkDescriptorSetAllocateInfo &create_info) const;
    VkFramebuffer         vk_create_framebuffer(const VkFramebufferCreateInfo &create_info) const;
    VkSemaphore           vk_create_semaphore(const VkSemaphoreCreateInfo &create_info) const;
    VkFence               vk_create_fence(const VkFenceCreateInfo &create_info) const;
    VkCommandPool         vk_create_command_pool(const VkCommandPoolCreateInfo &create_info) const;
    VkCommandBuffer       vk_alloc_command_buffer(const VkCommandBufferAllocateInfo &alloc_info) const;
    VkImageView           vk_create_image_view(VkImage image, VkFormat format, VkImageAspectFlags aspect_flags) const;
    VkSampler             vk_create_sampler() const;
    VkDeviceAddress       vk_get_buffer_device_address(const VkBufferDeviceAddressInfo &info) const;
    VkDeviceAddress       vk_get_buffer_device_address(const buffer &buffer) const;
    bool                  vk_create_image(
                            uint32_t width, 
                            uint32_t height,
                            VkFormat format, 
                            VkImageTiling tiling, 
                            VkImageUsageFlags usage, 
                            VkMemoryPropertyFlags properties, 
                            VkImage &image, 
                            VmaAllocation &allocation) const;
    bool                  vk_create_buffer(
                            uint32_t size, 
                            const VkBufferCreateInfo &buffer_create_info, 
                            const VmaAllocationCreateInfo &allocation_create_info, 
                            VkBuffer &buffer, 
                            VmaAllocation &allocation) const;
    VkAccelerationStructureKHR               vk_create_acceleration_structure(const VkAccelerationStructureCreateInfoKHR &create_info) const;
    VkDeviceAddress                          vk_get_acceleration_structure_device_address(const VkAccelerationStructureDeviceAddressInfoKHR &address_info) const;
    VkAccelerationStructureBuildSizesInfoKHR vk_get_acceleration_structure_build_sizes(
        const VkAccelerationStructureBuildGeometryInfoKHR &build_info, uint32_t max_primitive_count);

    // Builders
    renderpass_builder      create_renderpass_builder();
    raster_pipeline_builder create_raster_pipeline_builder();
    render_target_builder   create_render_target_builder(uint32_t width, uint32_t height, renderpass &pass);

    // Stella Vulkan Wrappers
    shader                   create_shader(shader_type type, uint32_t src_len, const char *p_src);
    shader                   create_shader(shader_type type, const shader_source_code &src);
    shader                   create_shader(shader_type type, const spirv_byte_code &spirv);
    descriptor_pool          create_descriptor_pool(uint32_t pool_sizes_length, const VkDescriptorPoolSize *p_pool_sizes, uint32_t max_sets);
    buffer                   create_buffer(uint32_t size, VkBufferUsageFlags usage, VkMemoryPropertyFlags memory_properties, const void *p_data=nullptr);
    buffer                   create_buffer(uint32_t size, const void *p_data=nullptr);
    buffer                   create_buffer(const descriptor_set_binding &binding, const void *p_data=nullptr);
    buffer                   create_buffer(uint32_t size, const descriptor_set_binding &binding, const void *p_data=nullptr);
    buffer                   create_vertex_buffer(uint32_t size, const void *p_data);
    buffer                   create_index_buffer(uint32_t size, const void *p_data);
    descriptor_set_layout    create_descriptor_set_layout(const descriptor_set_binding_array &bindings);
    descriptor_set_layout    create_descriptor_set_layout(uint32_t num_bindings, const descriptor_set_binding *p_bindings); 
    fence                    create_fence();
    semaphore                create_semaphore();
    graphics_command_pool    create_graphics_command_pool();
    compute_command_pool     create_compute_command_pool();
    raster_pipeline_context  create_raster_pipeline_context(renderpass &renderpass, const char *p_shader_path, raster_pipeline_context::config &config);
    compute_pipeline_context create_compute_pipeline_context_from_file(const char *p_shader_path);
    compute_pipeline_context create_compute_pipeline_context_from_src(const shader_source_code &src);
    image                    create_image(const image::create_info &create_info);

    descriptor_set alloc_descriptor_set(const descriptor_set_layout &layout) const;
    bool free_descriptor_set(descriptor_set set) const;
    void update_descriptor_set(const VkWriteDescriptorSet &set_write);

    bool submit_command_buffer(graphics_command_buffer &cmd_buffer);
    bool submit_command_buffer(compute_command_buffer &cmd_buffer);

    bool execute_graphics_commands(std::function<void(graphics_command_buffer &)> record_cmds_callback, uint32_t timeout_ms=1000);

    void queue_wait_idle(queue_type type);
    void device_wait_idle();

    VkPhysicalDeviceFeatures         get_physical_device_features() const          { return m_physical_device_features; }
    VkPhysicalDeviceProperties       get_physical_device_properties() const        { return m_physical_device_properties; }
    VkPhysicalDeviceMemoryProperties get_physical_device_memory_properties() const { return m_physical_device_memory_properties; }

    using delete_object_callback = std::function<void(state &)>;
    void push_deletor(delete_object_callback &&fn);
    void flush_deletor_queue();
    bool find_supported_format(const std::vector<VkFormat> &candidates, VkImageTiling tiling, VkFormatFeatureFlags features, VkFormat *p_format);

private:

    bool init_command_resources();
    bool vk_submit_buffer(
        VkQueue queue, 
        VkCommandBuffer cmd_buffer,
        VkFence fence, 
        uint16_t signal_sem_count, 
        VkSemaphore *p_signal_sems, 
        uint16_t wait_sem_count, 
        VkSemaphore *p_wait_sems
    );

    static inline const VkBufferUsageFlags s_default_buffer_usage_flags = (
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT |
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT |
        VK_BUFFER_USAGE_TRANSFER_DST_BIT |
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR);

    static inline const VkMemoryPropertyFlags s_default_memory_property_flags = (
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT |
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | 
        VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

private:

    std::mutex                         m_mutex;
    fence                              m_fence;
    uint32_t                           m_frame_number{ 0 };
    VkExtent2D                         m_resolution{ 0 };
    VkInstance                         m_instance{ nullptr };
    VkDebugUtilsMessengerEXT           m_debug_messenger{ nullptr };
    VkPhysicalDevice                   m_physical_device{ nullptr };        
    VkPhysicalDeviceFeatures           m_physical_device_features{ 0 };
    VkPhysicalDeviceProperties         m_physical_device_properties{ 0 };
    VkPhysicalDeviceMemoryProperties   m_physical_device_memory_properties{ 0 };
    VkDevice                           m_device{ nullptr };
    VkSurfaceKHR                       m_surface{ nullptr };
    VmaAllocator                       m_allocator;
    descriptor_pool                    m_descriptor_pool;
    std::deque<delete_object_callback> m_deletors;

    struct command_queue
    {
        uint32_t queue_family;
        VkQueue  queue;
    };

    command_queue m_graphics_queue;
    command_queue m_compute_queue;
    command_queue m_transfer_queue;

    graphics_command_pool m_graphics_command_pool;

    friend class imgui_context;
    friend class image;
    friend class command_buffer;
};

}
}