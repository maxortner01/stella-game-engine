#include "vk_shader.h"
#include "vk_state.h"

#include "stla_logger.h"
#include "stla_vector.h"
#include "stla_assert.h"

#include <shaderc/shaderc.hpp>

#include <stdint.h>

namespace stla
{
namespace vk
{

static shaderc_shader_kind shaderc_kind_to_stla_type(shader_type type) 
{
    switch (type)
    {
        case shader_type::vertex:       return shaderc_vertex_shader; 
        case shader_type::fragment:     return shaderc_fragment_shader; 
        case shader_type::raygen:       return shaderc_raygen_shader;
        case shader_type::anyhit:       return shaderc_anyhit_shader; 
        case shader_type::closesthit:   return shaderc_closesthit_shader; 
        case shader_type::miss:         return shaderc_miss_shader; 
        case shader_type::intersection: return shaderc_intersection_shader; 
        case shader_type::callable:     return shaderc_callable_shader;
        case shader_type::compute:      return shaderc_compute_shader;
        default: return shaderc_glsl_infer_from_source;
    }    
}

bool shader::create(shader_type type, uint32_t src_len, const char *p_src, state &state)
{
    if (src_len == 0 || p_src == nullptr) {
        return false;
    }

    spirv_byte_code spirv;
    if (!glsl_to_spirv(type, src_len, p_src, spirv)) {
        return false;
    }

    return create(type, spirv, state);
}

bool shader::create(shader_type type, const shader_source_code &src, state &state)
{
    return create(type, src.size(), src.data(), state);
}

bool shader::create(shader_type type, const spirv_byte_code &spirv, state &state)
{
    // Create modules
    VkShaderModule shader_module{ nullptr };

    VkShaderModuleCreateInfo create_info = { };
    create_info.sType    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    create_info.codeSize = spirv.size() * 4;
    create_info.pCode    = (const uint32_t *)spirv.data();

    if ((shader_module = state.vk_create_shader_module(create_info)) == nullptr) {
        return false;
    }

    if (!is_valid()) {
        destroy();
    }

    mp_state     = &state;
    m_shader_module = shader_module;
    m_type          = type;

    return true;
}

void shader::destroy()
{
    if (mp_state == nullptr) {
        return;
    }

    if (m_shader_module != nullptr) {
        mp_state->push_deletor([shader_module = m_shader_module](state &state) {
            vkDestroyShaderModule(state.get_device_handle(), shader_module, nullptr);
        });
    }

    mp_state     = nullptr;
    m_shader_module = nullptr;
}

bool shader::glsl_to_spirv(shader_type type, uint32_t src_len, const char *p_src, spirv_byte_code &spirv)
{
    if (src_len == 0 || p_src == nullptr) {
        return false;
    }

    spirv.clear();

    shaderc::Compiler compiler;
    shaderc::CompileOptions options;

    options.SetTargetEnvironment(shaderc_target_env_vulkan, shaderc_env_version_vulkan_1_3);
    options.SetTargetSpirv(shaderc_spirv_version_1_6);

    shaderc::SpvCompilationResult module_result = compiler.CompileGlslToSpv(
        p_src, shaderc_kind_to_stla_type(type), "file_name", options);

    if (module_result.GetCompilationStatus() != shaderc_compilation_status_success) {
        STLA_CORE_LOG_ERROR("Shader compile error:\n{0}", module_result.GetErrorMessage().c_str());
        return false;
    }
    
    uint32_t spirv_size = (uint32_t)((uint64_t)module_result.end()) - ((uint64_t)module_result.begin());
    //STLA_ASSERT(spirv_size <= spirv.capacity(), "not enough space for shader source");
    
    spirv = { module_result.cbegin(), module_result.cend() };
    return true;
}

VkShaderStageFlagBits shader::shader_type_to_vk_shader_bit(shader_type type)
{
    switch (type)
    {
        case shader_type::vertex:       return VK_SHADER_STAGE_VERTEX_BIT;
        case shader_type::fragment:     return VK_SHADER_STAGE_FRAGMENT_BIT;
        case shader_type::raygen:       return VK_SHADER_STAGE_RAYGEN_BIT_KHR;
        case shader_type::anyhit:       return VK_SHADER_STAGE_ANY_HIT_BIT_KHR;
        case shader_type::closesthit:   return VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
        case shader_type::miss:         return VK_SHADER_STAGE_MISS_BIT_KHR;
        case shader_type::intersection: return VK_SHADER_STAGE_INTERSECTION_BIT_KHR;
        case shader_type::callable:     return VK_SHADER_STAGE_CALLABLE_BIT_KHR;
        case shader_type::compute:      return VK_SHADER_STAGE_COMPUTE_BIT;
        default: return VK_SHADER_STAGE_ALL;
    }
}

} // namespace vk
} // namespace stla
