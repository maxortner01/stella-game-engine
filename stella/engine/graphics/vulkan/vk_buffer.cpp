#include "vk_buffer.h"
#include "vk_state.h"
#include "vk_common.h"

#include <string.h>

namespace stla
{
namespace vk
{

bool buffer::alloc(uint32_t size, VkBufferUsageFlags buffer_usage, VkMemoryPropertyFlags memory_properties, state &state)
{
    VkBufferCreateInfo buffer_info = { };
    buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    buffer_info.size  = size;
    buffer_info.usage = buffer_usage; 
    
    VmaAllocationCreateInfo alloc_info = { };
    alloc_info.usage         = VMA_MEMORY_USAGE_UNKNOWN;
    alloc_info.requiredFlags = memory_properties;

    VkBuffer buffer          = nullptr;
    VmaAllocation allocation = nullptr;

    if (!state.vk_create_buffer(size, buffer_info, alloc_info, buffer, allocation)) {
        return false;
    }

    if (mp_state != nullptr) {
        destroy();
    }

    m_buffer            = buffer;
    m_allocation        = allocation;
    mp_state            = &state;
    m_size              = size;
    m_buffer_usage      = buffer_usage;
    m_memory_properties = memory_properties;

    return true;
}

void buffer::destroy()
{
    if (mp_state == nullptr) {
        return;
    }

    if (m_buffer != nullptr) {
        mp_state->push_deletor([buffer = m_buffer, allocation = m_allocation](state &state) {
            vmaDestroyBuffer(state.get_allocator_handle(), buffer, allocation);
        });
    }

    m_size       = 0;
    mp_state     = nullptr;
    m_buffer     = nullptr;
    m_allocation = nullptr;
}

bool buffer::copy_from(uint32_t size, const void *p_src, uint32_t offset)
{
    if (mp_state == nullptr || m_buffer == nullptr || m_allocation == nullptr) {
        return false;
    }

    if (p_src == nullptr) {
        return false;
    }

    if ((offset + size) > m_size) {
        return false;
    }

    void *p_mapped_ptr{ nullptr };
    
    VK_ASSERT(vmaMapMemory(mp_state->get_allocator_handle(), m_allocation, &p_mapped_ptr));
    p_mapped_ptr = ((uint8_t *)p_mapped_ptr) + offset;
    
    ::memcpy(p_mapped_ptr, p_src, size);
    vmaUnmapMemory(mp_state->get_allocator_handle(), m_allocation);
    
    VK_ASSERT(vmaFlushAllocation(mp_state->get_allocator_handle(), m_allocation, 0, size));
    VK_ASSERT(vmaInvalidateAllocation(mp_state->get_allocator_handle(), m_allocation, 0, size));

    return true;
}

bool buffer::copy_to(uint32_t size, void *p_dest, uint32_t offset) const
{
    if (p_dest == nullptr || size == 0) {
        return false;
    }

    if ((offset + size) > m_size) {
        return false;
    }

    void *p_mapped_ptr{ nullptr };
    
    VK_ASSERT(vmaMapMemory(mp_state->get_allocator_handle(), m_allocation, &p_mapped_ptr));
    p_mapped_ptr = ((uint8_t *)p_mapped_ptr) + offset;
    
    ::memcpy(p_dest, p_mapped_ptr, size);
    vmaUnmapMemory(mp_state->get_allocator_handle(), m_allocation);
    
    VK_ASSERT(vmaFlushAllocation(mp_state->get_allocator_handle(), m_allocation, 0, size));
    VK_ASSERT(vmaInvalidateAllocation(mp_state->get_allocator_handle(), m_allocation, 0, size));

    return true;
}

bool buffer::map(void **pp_ptr)
{
    if (!is_valid()) {
        return false;
    }

    if (pp_ptr == nullptr) {
        return false;
    }
    
    void *p_mapped_ptr{ nullptr };
    
    VK_ASSERT(vmaMapMemory(mp_state->get_allocator_handle(), m_allocation, &p_mapped_ptr));   

    *pp_ptr = p_mapped_ptr;

    return true;
}

void buffer::unmap()
{
    vmaUnmapMemory(mp_state->get_allocator_handle(), m_allocation);
}

bool buffer::memset(uint8_t val)
{
    uint8_t *p_ptr = nullptr;
    if (!map((void **)&p_ptr)) {
        return false;
    }

    ::memset(p_ptr, val, m_size);

    unmap();
}

} // namespace vk
} // namespace stla