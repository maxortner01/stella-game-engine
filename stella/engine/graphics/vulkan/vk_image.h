#pragma once

#include <vulkan/vulkan.h>
#include <vma/vk_mem_alloc.h>

#include <stdint.h>

namespace stla
{
namespace vk
{
    
class state;
class graphics_command_buffer;
class fence;

class image
{
public:

    struct create_info
    {
        uint32_t              width;
        uint32_t              height;
        VkFormat              format;
        VkImageTiling         tiling;
        VkImageUsageFlags     usage;
        VkMemoryPropertyFlags properties;
        VkImageAspectFlags    aspect_flags;
        VkFormatFeatureFlags  features;
    };

    bool create(const create_info &create, state &state);
    void destroy();
    bool transition_layout(VkImageLayout old_layout, VkImageLayout new_layout, graphics_command_buffer& cmd_buffer, fence* p_fence = nullptr) const;
    bool is_valid() const { return m_image != nullptr && m_allocation != nullptr && m_image_view != nullptr && m_sampler != nullptr && mp_state != nullptr; }
    const VkImage &get_handle() const { return m_image; }
    const VmaAllocation &get_allocation_handle() const { return m_allocation; }
    const VkImageView &get_view_handle() const { return m_image_view; }
    const VkSampler &get_sampler_handle() const { return m_sampler; }
    uint32_t get_width() const { return m_width; }
    uint32_t get_height() const { return m_height; }
    VkFormat get_format() const { return m_format; }


private:

    VkImage       m_image{ nullptr };
    VmaAllocation m_allocation{ };
    VkImageView   m_image_view{ nullptr };
    VkSampler     m_sampler{ nullptr };
    uint32_t      m_width{ 0 };
    uint32_t      m_height{ 0 };
    VkFormat      m_format{ VK_FORMAT_UNDEFINED };
    state *       mp_state{ nullptr };
};

} // namespace vk
} // namespace stla
