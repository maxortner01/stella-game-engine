#pragma once

#include "vk_raster_pipeline.h"
#include "vk_compute_pipeline.h"
#include "vk_shader_reflection_mgr.h"
#include "vk_shader.h"
#include "vk_descriptors.h"

#include "stla_shader_parser.h"

namespace stla
{
namespace vk
{
    
class state;
class renderpass;

class raster_pipeline_context
{
public:

    struct config
    {
        uint32_t              width{ 0 };
        uint32_t              height{ 0 };
        VkPrimitiveTopology   topology{ VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST };
        bool                  enable_depth_clamp{ false };
        bool                  enable_rasterizer_discard{ false };
        VkPolygonMode         polygon_mode{ VK_POLYGON_MODE_FILL };
        VkCullModeFlags       cull_flags{ VK_CULL_MODE_NONE };
        VkFrontFace           front_face{ VK_FRONT_FACE_CLOCKWISE };
        float                 line_width{ 1.0f };
        VkSampleCountFlagBits sample_count_flags{ VK_SAMPLE_COUNT_1_BIT };
        bool                  enable_sample_shading{ false };
        float                 min_sample_shading{ 1.0f };
        bool                  enable_alpha_to_coverage{ false };
        bool                  enable_alpha_to_one{ false };
        bool                  enable_depth_test{ true };
        bool                  enable_depth_write{ true };
        VkCompareOp           depth_compare_op{ VK_COMPARE_OP_LESS_OR_EQUAL };
        float                 depth_bounds_min{ 0.0f };
        float                 depth_bounds_max{ 1.0f };
        bool                  enable_stencil_test{ false };
        
        struct {
            bool  enable{ false };
            float constant_factor{ 0.0f };
            float clamp { 0.0f };
            float slope_factor{ 0.0f };
        } depth_bias;

        struct {
            bool                  enable{ true };
            VkBlendFactor         src_color_factor{ VK_BLEND_FACTOR_SRC_ALPHA };
            VkBlendFactor         dest_color_factor{ VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA };
            VkBlendOp             color_blend_op{ VK_BLEND_OP_ADD };
            VkBlendFactor         src_alpha_factor{ VK_BLEND_FACTOR_ZERO };
            VkBlendFactor         dest_alpha_factor{ VK_BLEND_FACTOR_ONE }; 
            VkBlendOp             alpha_blend_op{ VK_BLEND_OP_ADD };
            VkColorComponentFlags color_write_mask{ VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT };
        } color_blend_attachment;
    };

    bool create(renderpass &renderpass, const char *p_shader_path, config &config, state &state);
    void destroy();
    const shader &get_shader(shader_type type) const { 
        switch (type)
        {
            case shader_type::vertex: return m_vertex_shader;
            case shader_type::fragment: return m_fragment_shader;
            default:
            {
                STLA_ASSERT_RELEASE(0, "raster pipeline context doesn't have shader (type=%d)", (int)type);
                return m_vertex_shader;
            }
        }
    }
    //const descriptor_set_layout &get_descriptor_set_layout_by_loc(descriptor_set_loc loc) const { 
    //    return m_descriptor_set_layout_container.get_layout_by_loc(loc); }

    const raster_pipeline &get_pipeline() const { return m_pipeline; }
    const descriptor_set_layout_container& get_descriptor_set_layout_container() const { return m_descriptor_set_layout_container; }

private:

    shader                m_vertex_shader;
    shader                m_fragment_shader;
    raster_pipeline       m_pipeline;
    shader_parser         m_shader_parser;
    shader_reflection_mgr m_reflection_mgr;
    descriptor_set_layout_container m_descriptor_set_layout_container;
};

class compute_pipeline_context
{
public:

    bool create_from_file(const char *p_shader_path, state &state);
    bool create_from_src(const shader_source_code &src, state &state);
    void destroy();
    const shader &get_shader() const { return m_compute_shader; }
    const compute_pipeline &get_pipeline() const { return m_pipeline; }
    const descriptor_set_layout_container &get_descriptor_set_layout_container() const { return m_descriptor_set_layout_container; }

private:

    shader                m_compute_shader;
    compute_pipeline      m_pipeline;
    shader_parser         m_shader_parser;
    shader_reflection_mgr m_reflection_mgr;
    descriptor_set_layout_container m_descriptor_set_layout_container;
};

} // namespace vk
} // namespace stla
