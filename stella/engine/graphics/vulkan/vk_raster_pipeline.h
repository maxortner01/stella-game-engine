#pragma once

#include "stla_vertex_buffer_layout.h"

#include <vulkan/vulkan.h>

#include <stdint.h>

#include <vector>

namespace stla
{
namespace vk
{

class state;
class shader;

class raster_pipeline
{
public:

	raster_pipeline() = default;
	raster_pipeline(const raster_pipeline &) = default;
	raster_pipeline(VkPipeline pipeline, VkPipelineLayout layout, const vertex_buffer_layout &vertex_layout, state *p_state) :
		m_pipeline(pipeline), m_pipeline_layout(layout), m_vertex_layout(vertex_layout), mp_state(p_state) { }
	
	void destroy();

	const VkPipeline &get_handle() const { return m_pipeline; }
	const VkPipelineLayout &get_layout_handle() const { return m_pipeline_layout; }
	vertex_buffer_layout get_vertex_layout() const { return m_vertex_layout; }

private:

	VkPipeline           m_pipeline{ nullptr };
	VkPipelineLayout     m_pipeline_layout{ nullptr };
	vertex_buffer_layout m_vertex_layout;
	state *              mp_state{ nullptr };
};

class raster_pipeline_builder
{
public:

	raster_pipeline_builder(state &state);

	// Shader Stage Create Info 
	raster_pipeline_builder &add_shader_stage(shader &stage);

	// Vertex Input State Create info
	raster_pipeline_builder &set_vertex_buffer_layout(const vertex_buffer_layout &layout);

	// Input Assembly State Create Info
	raster_pipeline_builder &set_primitive_topology(VkPrimitiveTopology topology);
	
	// Viewport State Create Info
	raster_pipeline_builder &add_viewport(float x, float y, float width, float height, float min_depth, float max_depth);
	raster_pipeline_builder &add_scissor(uint32_t offset_x, uint32_t offset_y, uint32_t extent_width, uint32_t extent_height);

	// Rasterization State Create Info
	raster_pipeline_builder &enable_depth_clamp(bool enable);
	raster_pipeline_builder &enable_rasterizer_discard(bool enable);
	raster_pipeline_builder &set_polygon_mode(VkPolygonMode mode);
	raster_pipeline_builder &set_cull_flags(VkCullModeFlags flags);
	raster_pipeline_builder &set_front_face(VkFrontFace front);
	raster_pipeline_builder &configure_depth_bias(bool enable, float constant_factor, float clamp, float slope_factor);
	raster_pipeline_builder &set_line_width(float width);

	// Multisample State Create Info
	raster_pipeline_builder &set_sample_count_flag(VkSampleCountFlagBits flag);
	raster_pipeline_builder &enable_sample_shading(bool enable);
	raster_pipeline_builder &set_min_sample_shading(float min);
	raster_pipeline_builder &set_sample_mask(VkSampleMask mask);
	raster_pipeline_builder &enable_alpha_to_coverage(bool enable);
	raster_pipeline_builder &enable_alpha_to_one(bool enable);

	// Depth Stencil State Create Info
	raster_pipeline_builder &enable_depth_test(bool enable);
	raster_pipeline_builder &enable_depth_write(bool enable);
	raster_pipeline_builder &set_depth_compare_op(VkCompareOp op);
	raster_pipeline_builder &set_depth_bounds(float min, float max);
	raster_pipeline_builder &enable_stencil_test(bool enable);

	// Color Blend State Create Info
	raster_pipeline_builder &add_color_blend_attachment(
		bool enable, VkBlendFactor src_color_factor, VkBlendFactor dest_color_factor, 
		VkBlendOp color_blend_op, VkBlendFactor src_alpha_factor, VkBlendFactor dest_alpha_factor,
		VkBlendOp alpha_blend_op, VkColorComponentFlags color_write_mask);

	// Dynamic State Create Info
	raster_pipeline_builder &add_dynamic_state(VkDynamicState state);

	// Pipeline Layout Create Info
	raster_pipeline_builder &add_descriptor_set_layout(VkDescriptorSetLayout layout);

	// Renderpass
	raster_pipeline_builder &set_renderpass(VkRenderPass renderpass);

	bool build(raster_pipeline &new_pipeline);

private:

	state *                                          mp_state{ nullptr };
	VkRenderPass                                     m_renderpass{ nullptr };
	std::vector<shader>                              m_shader_stages;
	vertex_buffer_layout                             m_vertex_buffer_layout{ };
	VkPrimitiveTopology                              m_primitive_topology{ VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST };
	std::vector<VkViewport>                          m_viewports;
	std::vector<VkRect2D>                            m_scissors;
	VkPipelineRasterizationStateCreateInfo           m_rasterization_state;
	VkSampleMask                                     m_sample_mask{ 0 };
	VkPipelineMultisampleStateCreateInfo             m_multisample_state;
	VkPipelineDepthStencilStateCreateInfo            m_depth_stencil{ };
	std::vector<VkPipelineColorBlendAttachmentState> m_color_blend_attachments;
	std::vector<VkDynamicState>                      m_dynamic_state;
	std::vector<VkDescriptorSetLayout>               m_descriptor_set_layouts; //
};

} // namespace vk
} // namespace stla
