#include "vk_pipeline_context.h"
#include "vk_state.h"

#include "stla_logger.h"

namespace stla
{
namespace vk
{

bool raster_pipeline_context::create(renderpass &renderpass, const char *p_shader_path, config &config, state &state)
{
    STLA_CORE_LOG_INFO("Loading raster_pipeline_context from {0}", p_shader_path);

    if (config.width == 0 || config.height == 0) {
        STLA_CORE_LOG_INFO("invalid width or heigh (width={0}) (height={1})", config.width, config.height);
        return false;
    }

    if (!m_shader_parser.parse(p_shader_path)) {
        return false;
    }

    if (!m_reflection_mgr.parse(m_shader_parser)) {
        return false;
    }

    descriptor_set_layout_container desc_set_layout_container;
    shader vertex_shader;
    shader fragment_shader;
    raster_pipeline pipeline;

    shader_reflection_info combined_reflection_info = m_reflection_mgr.get_info(shader_type::vertex);
    combined_reflection_info.combine(m_reflection_mgr.get_info(shader_type::fragment));

    vertex_shader   = state.create_shader(shader_type::vertex, m_shader_parser.get_source(shader_type::vertex));
    fragment_shader = state.create_shader(shader_type::fragment, m_shader_parser.get_source(shader_type::fragment));

    if (!vertex_shader.is_valid() || !fragment_shader.is_valid()) {
        vertex_shader.destroy();
        fragment_shader.destroy();
        return false;
    }

    if (!desc_set_layout_container.create(state)) {
        vertex_shader.destroy();
        fragment_shader.destroy();
        return false;
    }

    vector_4<descriptor_set_loc> locs = combined_reflection_info.get_descriptor_set_locs();

    for (descriptor_set_loc &loc : locs) {
        if (!desc_set_layout_container.create_layout(loc, combined_reflection_info.get_binding_array(loc))) {
            vertex_shader.destroy();
            fragment_shader.destroy();
            desc_set_layout_container.destroy();
            return false;
        }
    }

    raster_pipeline_builder builder = state.create_raster_pipeline_builder()
        // Shader Stage Create Info 
        .add_shader_stage(vertex_shader)
        .add_shader_stage(fragment_shader)
        // Vertex Input State Create info
        .set_vertex_buffer_layout(combined_reflection_info.get_vertex_buffer_layout())
        // Input Assembly State Create Info
        .set_primitive_topology(config.topology)
        // Viewport State Create Info
        .add_viewport(0.0f, 0.0f, config.width, config.height, 0.0f, 1.0f)
        .add_scissor(0, 0, config.width, config.height)
        // Rasterization State Create Info
        .enable_depth_clamp(config.enable_depth_clamp)
        .enable_rasterizer_discard(config.enable_rasterizer_discard)
	    .set_polygon_mode(config.polygon_mode)
	    .set_cull_flags(config.cull_flags)
	    .set_front_face(config.front_face)
	    .configure_depth_bias(config.depth_bias.enable, config.depth_bias.constant_factor, config.depth_bias.clamp, config.depth_bias.slope_factor)
	    .set_line_width(1.0f)
        // Multisample State Create Info
        .set_sample_count_flag(config.sample_count_flags)
        .enable_sample_shading(config.enable_sample_shading)
        .set_min_sample_shading(config.min_sample_shading)
        .enable_alpha_to_coverage(config.enable_alpha_to_coverage)
        .enable_alpha_to_one(config.enable_alpha_to_one)
        // Depth Stencil State Create Info
        .enable_depth_test(config.enable_depth_test)
        .enable_depth_write(config.enable_depth_write)
        .set_depth_compare_op(config.depth_compare_op)
        .set_depth_bounds(config.depth_bounds_min, config.depth_bounds_max)
        .enable_stencil_test(config.enable_stencil_test)
        // Color Blend State Create Info
        .add_color_blend_attachment(
            config.color_blend_attachment.enable, 
            config.color_blend_attachment.src_color_factor, config.color_blend_attachment.dest_color_factor, config.color_blend_attachment.color_blend_op, 
            config.color_blend_attachment.src_alpha_factor, config.color_blend_attachment.dest_alpha_factor, config.color_blend_attachment.alpha_blend_op,
            config.color_blend_attachment.color_write_mask)
	    // Renderpass   
        .set_renderpass(renderpass.get_handle());
        
    for (descriptor_set_loc &loc : locs) {
        if (desc_set_layout_container.has_layout(loc)) {
            builder.add_descriptor_set_layout(desc_set_layout_container.get_layout(loc).get_handle());
        }
    }

    if (!builder.build(pipeline)) {
        vertex_shader.destroy();
        fragment_shader.destroy();
        desc_set_layout_container.destroy();
        return false;
    }

    destroy();

    m_vertex_shader                   = vertex_shader;
    m_fragment_shader                 = fragment_shader;
    m_descriptor_set_layout_container = desc_set_layout_container;
    m_pipeline                        = pipeline;
    return true;
}

void raster_pipeline_context::destroy()
{
    m_vertex_shader.destroy();
    m_fragment_shader.destroy();
    m_descriptor_set_layout_container.destroy();
    m_pipeline.destroy();
}

bool compute_pipeline_context::create_from_file(const char *p_shader_path, state &state)
{
    STLA_CORE_LOG_INFO("Loading compute_pipeline_context from {0}", p_shader_path);

    if (!m_shader_parser.parse(p_shader_path)) {
        return false;
    }

    if (!m_reflection_mgr.parse(m_shader_parser)) {
        return false;
    }

    descriptor_set_layout_container desc_set_layout_container;
    compute_pipeline compute_pipeline;
    shader compute_shader = state.create_shader(shader_type::compute, m_shader_parser.get_source(shader_type::compute));
    
    if (!compute_shader.is_valid()) {
        return false;
    }

    if (!desc_set_layout_container.create(state)) {
        compute_shader.destroy();
        return false;
    }

    shader_reflection_info compute_reflection_info = m_reflection_mgr.get_info(shader_type::compute);

    for (descriptor_set_loc &loc : compute_reflection_info.get_descriptor_set_locs()) {
        descriptor_set_binding_array bindings = compute_reflection_info.get_binding_array(loc);
        STLA_CORE_LOG_INFO(" Descriptor Set ({0})", (int)loc);
        for (const descriptor_set_binding &binding : bindings) {
            STLA_CORE_LOG_INFO("  ({0}) (size={1}) (type={2}) (usage={3:#b}) (shader_stages={4:#b})", 
                binding.binding, binding.size, binding.type, binding.usage_flags, binding.shader_stage_flags);
        }

        if (!desc_set_layout_container.create_layout(loc, bindings)) {
            compute_shader.destroy();
            desc_set_layout_container.destroy();
            return false;
        }
    }

    if (!compute_pipeline.create(desc_set_layout_container, compute_shader, state)) {
        desc_set_layout_container.destroy();
        compute_shader.destroy();
        return false;
    }

    destroy();

    m_descriptor_set_layout_container = desc_set_layout_container;
    m_compute_shader                  = compute_shader;
    m_pipeline                        = compute_pipeline;
    return true;
}

bool compute_pipeline_context::create_from_src(const shader_source_code &src, state &state)
{
    STLA_CORE_LOG_INFO("Loading compute_pipeline_context from source");

    if (!m_reflection_mgr.parse(shader_type::compute, src)) {
        return false;
    }

    descriptor_set_layout_container desc_set_layout_container;
    compute_pipeline compute_pipeline;
    shader compute_shader = state.create_shader(shader_type::compute, src);
    
    if (!compute_shader.is_valid()) {
        return false;
    }

    if (!desc_set_layout_container.create(state)) {
        compute_shader.destroy();
        return false;
    }

    shader_reflection_info compute_reflection_info = m_reflection_mgr.get_info(shader_type::compute);

    for (descriptor_set_loc &loc : compute_reflection_info.get_descriptor_set_locs()) {
        descriptor_set_binding_array bindings = compute_reflection_info.get_binding_array(loc);
        STLA_CORE_LOG_INFO(" Descriptor Set ({0})", (int)loc);
        for (const descriptor_set_binding &binding : bindings) {
            STLA_CORE_LOG_INFO("  ({0}) (size={1}) (type={2}) (usage={3:#b}) (shader_stages={4:#b})", 
                binding.binding, binding.size, binding.type, binding.usage_flags, binding.shader_stage_flags);
        }

        if (!desc_set_layout_container.create_layout(loc, bindings)) {
            compute_shader.destroy();
            desc_set_layout_container.destroy();
            return false;
        }
    }

    if (!compute_pipeline.create(desc_set_layout_container, compute_shader, state)) {
        desc_set_layout_container.destroy();
        compute_shader.destroy();
        return false;
    }

    destroy();

    m_descriptor_set_layout_container = desc_set_layout_container;
    m_compute_shader                  = compute_shader;
    m_pipeline                        = compute_pipeline;
    return true;
}

void compute_pipeline_context::destroy()
{
    m_compute_shader.destroy();
    m_descriptor_set_layout_container.destroy();
    m_pipeline.destroy();
}
    
} // namespace vk
} // namespace stla
