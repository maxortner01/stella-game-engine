#pragma once

#include "vk_descriptors.h"
#include "vk_shader.h"

#include "stla_vector.h"
#include "stla_unordered_map.h"
#include "stla_vertex_buffer_layout.h"

#include <stdint.h>

namespace stla
{
namespace vk
{

class shader_reflection_info
{
public:

    shader_reflection_info() { m_descriptor_sets.resize(m_descriptor_sets.capacity()); }
    shader_reflection_info(const shader_reflection_info &) = default;

    bool load_from_spirv(spirv_byte_code &spirv);
    void clear();
    bool combine(const shader_reflection_info &new_info);
    descriptor_set_binding_array get_binding_array(descriptor_set_loc loc) const;
    vector_4<descriptor_set_loc> get_descriptor_set_locs() const;
    vertex_buffer_layout get_vertex_buffer_layout() const { return m_vertex_layout; }

private:

    vertex_buffer_layout                 m_vertex_layout;
    vector_4<descriptor_set_binding_map> m_descriptor_sets;
};

//TODO PARSE VERTEX LAYOUT INFO
class shader_reflection_mgr
{
public:

    bool parse(const char *p_path);
    bool parse(const shader_parser &parser);
    bool parse(shader_type type, const shader_source_code &src);
    void clear();

    shader_reflection_info get_info(shader_type type) const;

private:

    unordered_map_16<shader_type, shader_reflection_info> m_reflection_infos;

};

} // namespace vk
} // namespace stla
