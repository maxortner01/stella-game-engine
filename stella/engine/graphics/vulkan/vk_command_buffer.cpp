#include "vk_command_buffer.h"
#include "vk_state.h"
#include "vk_buffer.h"
#include "vk_raster_pipeline.h"
#include "vk_compute_pipeline.h"
#include "vk_common.h"
#include "vk_renderpass.h"
#include "vk_render_target.h"
#include "vk_descriptors.h"
#include "vk_sync.h"
#include "vk_image.h"

namespace stla
{
namespace vk
{

void icommand_buffer::destroy()
{
    m_wait_semaphores.clear();
    m_signal_semaphores.clear();
    m_command_buffer       = nullptr;
    mp_command_pool_handle = nullptr;
    mp_fence               = nullptr;
}

void icommand_buffer::push_wait_semaphore(semaphore &sem)
{
    STLA_ASSERT_RELEASE(is_valid(), "icommand_buffer invalid");
    STLA_ASSERT_RELEASE(m_wait_semaphores.available(), "m_wait_semaphores full");
    m_wait_semaphores.push_back(&sem);
}

void icommand_buffer::push_signal_semaphore(semaphore &sem)
{
    STLA_ASSERT_RELEASE(is_valid(), "icommand_buffer invalid");
    STLA_ASSERT_RELEASE(m_signal_semaphores.available(), "m_signal_semaphores full");
    m_signal_semaphores.push_back(&sem);
}

vector_8<VkSemaphore> icommand_buffer::get_vk_wait_semaphores() const
{
    vector_8<VkSemaphore> vk_semaphores;
    for (const semaphore *p_semaphore : m_wait_semaphores) {
        STLA_ASSERT_RELEASE(p_semaphore != nullptr, "p_semaphore is null");
        vk_semaphores.push_back(p_semaphore->m_semaphore);
    }
    return vk_semaphores;
}

vector_8<VkSemaphore> icommand_buffer::get_vk_signal_semaphores() const
{
    vector_8<VkSemaphore> vk_semaphores;
    for (const semaphore *p_semaphore : m_signal_semaphores) {
        STLA_ASSERT_RELEASE(p_semaphore != nullptr, "p_semaphore is null");
        vk_semaphores.push_back(p_semaphore->m_semaphore);
    }
    return vk_semaphores;
}

bool icommand_buffer::begin()
{
    STLA_ASSERT_RELEASE(is_valid(), "icommand_buffer invalid");

    m_wait_semaphores.clear();
    m_signal_semaphores.clear();
    mp_fence = nullptr;

    VkResult result = VK_SUCCESS;

    VK_ASSERT(result = vkResetCommandBuffer(m_command_buffer, 0));

    if (result != VK_SUCCESS) {
        return false;
    }

    VkCommandBufferBeginInfo cmd_buffer_begin_info = { };
    cmd_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    cmd_buffer_begin_info.pNext = nullptr;
    cmd_buffer_begin_info.pInheritanceInfo = nullptr;
    cmd_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    VK_ASSERT(result = vkBeginCommandBuffer(m_command_buffer, &cmd_buffer_begin_info));

    return result == VK_SUCCESS;
}

bool icommand_buffer::end(fence &fence)
{
    mp_fence = &fence;

    return end();
}

bool icommand_buffer::end(fence *p_fence)
{
    mp_fence = p_fence;

    return end();
}

bool icommand_buffer::end()
{
    STLA_ASSERT_RELEASE(m_command_buffer != nullptr, "icommand_buffer is null");

    VkResult result = VK_SUCCESS;

    VK_ASSERT(result = vkEndCommandBuffer(m_command_buffer))

    return result == VK_SUCCESS;
}

void graphics_command_buffer::begin_renderpass(const render_target &target, const renderpass &pass, const glm::vec4 &clear_color)
{
    VkClearValue clear_value = { 0 };
    clear_value.color = { { clear_color.r, clear_color.g, clear_color.b, clear_color.a } };
    
    // Clear depth at 1
    VkClearValue depth_clear = { };
    depth_clear.depthStencil.depth = 1.0f;

    VkClearValue clear_values[] = { clear_value, depth_clear };

    VkExtent2D resolution;
    resolution.width  = target.get_width();
    resolution.height = target.get_height();

    VkRenderPassBeginInfo render_pass_info = { };
    render_pass_info.sType               = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    render_pass_info.pNext               = nullptr;
    render_pass_info.renderPass          = pass.get_handle();
    render_pass_info.renderArea.offset.x = 0;
    render_pass_info.renderArea.offset.y = 0;
    render_pass_info.renderArea.extent   = resolution;
    render_pass_info.framebuffer         = target.get_framebuffer_handle();
    render_pass_info.clearValueCount     = sizeof(clear_values) / sizeof(VkClearValue);
    render_pass_info.pClearValues        = &clear_values[0];
    vkCmdBeginRenderPass(m_command_buffer, &render_pass_info, VK_SUBPASS_CONTENTS_INLINE);
}

void graphics_command_buffer::end_renderpass()
{
    vkCmdEndRenderPass(m_command_buffer);
}

void graphics_command_buffer::draw(uint32_t vertex_count, uint32_t instance_count)
{
    vkCmdDraw(m_command_buffer, vertex_count, instance_count, 0, 0);
}

void graphics_command_buffer::draw_indexed(uint32_t index_count)
{
    vkCmdDrawIndexed(m_command_buffer, index_count, 1, 0, 0, 0);
}

void graphics_command_buffer::bind_pipeline(const raster_pipeline &pipeline)
{
    vkCmdBindPipeline(m_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.get_handle());
}

void graphics_command_buffer::bind_vertex_buffer(const buffer &buf)
{
    VkDeviceSize offset = 0;
    vkCmdBindVertexBuffers(m_command_buffer, 0, 1, (const VkBuffer *)&buf.get_buffer_handle(), &offset);
}

void graphics_command_buffer::bind_index_buffer(const buffer &buf)
{
    vkCmdBindIndexBuffer(m_command_buffer, buf.get_buffer_handle(), 0, VK_INDEX_TYPE_UINT32);
}

void graphics_command_buffer::transition_image_layout(const image &img, VkImageLayout old_layout, VkImageLayout new_layout)
{
    static auto get_access_mask = [](VkImageLayout layout) {
        switch (layout)
        {
            case VK_IMAGE_LAYOUT_UNDEFINED:                return VK_ACCESS_NONE;
            case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL: return VK_ACCESS_SHADER_READ_BIT;
            case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:     return VK_ACCESS_TRANSFER_WRITE_BIT;
            default: STLA_ASSERT_RELEASE(0, "unhandled image layout in %s", __FUNCTION__); return VK_ACCESS_NONE;
        }
    };

    static auto get_stage = [](VkImageLayout layout) {
        switch (layout)
        {
            case VK_IMAGE_LAYOUT_UNDEFINED:                return VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL: return VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
            case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:     return VK_PIPELINE_STAGE_TRANSFER_BIT;
            default: STLA_ASSERT_RELEASE(0, "unhandled image layout in %s", __FUNCTION__); return VK_PIPELINE_STAGE_NONE;
        }
    };

    VkImageMemoryBarrier barrier = { };
    barrier.sType                           = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout                       = old_layout;
    barrier.newLayout                       = new_layout;
    barrier.image                           = img.get_handle();
    barrier.subresourceRange.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT; // todo
    barrier.subresourceRange.baseMipLevel   = 0;
    barrier.subresourceRange.levelCount     = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount     = 1;
    barrier.srcAccessMask                   = get_access_mask(old_layout);
    barrier.dstAccessMask                   = get_access_mask(new_layout);

    vkCmdPipelineBarrier(
        m_command_buffer,
        get_stage(old_layout), 
        get_stage(new_layout),
        0,
        0, nullptr,
        0, nullptr,
        1, &barrier
    );
}

void graphics_command_buffer::copy_buffer_to_image(const image &img, const buffer &buffer)
{
    VkBufferImageCopy region = { };
    region.bufferOffset      = 0;
    region.bufferRowLength   = img.get_width();
    region.bufferImageHeight = img.get_height();
    region.imageOffset       = { 0, 0, 0 };
    region.imageExtent       = { img.get_width(), img.get_height(), 1 };
    region.imageSubresource  = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1 };

    vkCmdCopyBufferToImage(
        m_command_buffer, 
        buffer.get_buffer_handle(),
        img.get_handle(),
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
}

void graphics_command_buffer::build_acceleration_structure(
    const VkAccelerationStructureBuildGeometryInfoKHR &build_geometry_info, 
    const VkAccelerationStructureBuildRangeInfoKHR &build_range_info)
{
    const VkAccelerationStructureBuildRangeInfoKHR *p_build_range_info = &build_range_info;
    vkCmdBuildAccelerationStructuresKHR(
        m_command_buffer, 1, &build_geometry_info, &p_build_range_info);
}

void compute_command_buffer::bind_pipeline(const compute_pipeline &pipeline)
{
    vkCmdBindPipeline(m_command_buffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline.get_handle());
}

void compute_command_buffer::bind_descriptor_set(const compute_pipeline &pipeline, const descriptor_set &set)
{
    vkCmdBindDescriptorSets(
        m_command_buffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline.get_layout_handle(), 0, 1, &set.get_handle(), 0, nullptr);
}

void compute_command_buffer::dispatch(uint32_t group_count_x, uint32_t group_count_y, uint32_t group_count_z)
{
    vkCmdDispatch(m_command_buffer, group_count_x, group_count_y, group_count_z);
}

} // namespace vk
} // namespace stla