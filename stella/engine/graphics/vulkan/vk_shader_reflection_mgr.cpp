#include "vk_shader_reflection_mgr.h"
#include "vk_shader.h"

#include "stla_assert.h"
#include "stla_shader_parser.h"
#include "stla_logger.h"

#include <spirv_reflect.h>
#include <etl/algorithm.h>

#include <functional>

namespace stla
{
namespace vk
{

static VkBufferUsageFlags get_buffer_usage_flags_from_descriptor_type(SpvReflectDescriptorType type)
{
    switch (type)
    {
        case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_BUFFER: return VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
        case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_BUFFER: return VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_IMAGE:  return VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        case SPV_REFLECT_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR: return VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR;
        default: 
        {
            STLA_ASSERT_RELEASE(0, "unhandled reflection descriptor type: %d", type);
            return (VkBufferUsageFlags)0;
        }
    }
}

bool shader_reflection_info::load_from_spirv(spirv_byte_code &spirv)
{
    clear();
    
    if (spirv.size() == 0) {
        return true;
    }

    spv_reflect::ShaderModule shader_module(spirv.size() * 4, spirv.data());

    STLA_ASSERT_RELEASE(shader_module.GetEntryPointCount() == 1, "%d entry point(s) unsupported", shader_module.GetEntryPointCount());

    SpvReflectResult result = SPV_REFLECT_RESULT_SUCCESS;

    // INTERFACE
    uint32_t itf_var_count = 0;
    vector_16<SpvReflectInterfaceVariable *> reflect_itf_vars;

    if ((result = shader_module.EnumerateInterfaceVariables(&itf_var_count, nullptr)) != SPV_REFLECT_RESULT_SUCCESS) {
        return false;
    }

    STLA_ASSERT_RELEASE(itf_var_count <= reflect_itf_vars.capacity(), "too many interface variables");

    reflect_itf_vars.resize(itf_var_count);
    if ((result = shader_module.EnumerateInterfaceVariables(&itf_var_count, reflect_itf_vars.data())) != SPV_REFLECT_RESULT_SUCCESS) {
        return false;
    }

    for (SpvReflectInterfaceVariable *p_itf_var : reflect_itf_vars) {
        if (p_itf_var->storage_class == SpvStorageClassInput && 
            (shader_module.GetShaderStage() & SPV_REFLECT_SHADER_STAGE_VERTEX_BIT) != 0) {
            vertex_buffer_attribute_type type;
            if ((p_itf_var->type_description->type_flags & SPV_REFLECT_TYPE_FLAG_VECTOR) != 0) {
            
                if (p_itf_var->numeric.vector.component_count == 2) {
                    type = vertex_buffer_attribute_type::v2;
                }
                else if (p_itf_var->numeric.vector.component_count == 3) {
                    type = vertex_buffer_attribute_type::v3;
                }
                else if (p_itf_var->numeric.vector.component_count == 4) {
                    type = vertex_buffer_attribute_type::v4;
                }
                else {
                    STLA_ASSERT_RELEASE(0, "unsupported interface dims count: %d", p_itf_var->numeric.vector.component_count);
                }
            }
            else {
                STLA_ASSERT_RELEASE(0, "unsupported interface type: %d", p_itf_var->type_description->type_flags);
            }
            
            m_vertex_layout.add_attribute(type, p_itf_var->location, false);
        }
    }

    // DESCRIPTOR SETS
    uint32_t desc_set_count = 0;
    vector_4<SpvReflectDescriptorSet *> reflect_desc_sets;
    
    if ((result = shader_module.EnumerateDescriptorSets(&desc_set_count, nullptr)) != SPV_REFLECT_RESULT_SUCCESS) {
        return false;
    }

    STLA_ASSERT_RELEASE(desc_set_count <= reflect_desc_sets.capacity(), "too many descriptor sets");

    reflect_desc_sets.resize(desc_set_count);
    if ((result = shader_module.EnumerateDescriptorSets(&desc_set_count, reflect_desc_sets.data())) != SPV_REFLECT_RESULT_SUCCESS) {
        return false;
    }

    for (SpvReflectDescriptorSet *p_desc_set_info : reflect_desc_sets) {
        STLA_ASSERT_RELEASE(p_desc_set_info->set < MAX_DESCRIPTOR_SETS, "invalid descriptor set: %d", p_desc_set_info->set);
        STLA_ASSERT_RELEASE(p_desc_set_info->binding_count < MAX_DESCRIPTOR_SET_BINDINGS, "too many descriptor set bindings");

        descriptor_set_binding_map desc_set_bindings;
        for (int i = 0; i < p_desc_set_info->binding_count; i++) {
            descriptor_set_binding new_binding;

            SpvReflectDescriptorBinding *p_binding   = p_desc_set_info->bindings[i];
            //SpvReflectTypeDescription   *p_type_desc = p_binding->type_description;

            new_binding.binding            = p_binding->binding;
            new_binding.type               = (VkDescriptorType)p_binding->descriptor_type;
            new_binding.usage_flags        = get_buffer_usage_flags_from_descriptor_type(p_binding->descriptor_type);
            new_binding.shader_stage_flags = VK_SHADER_STAGE_ALL;
            new_binding.size               = 0;

            std::function<void(SpvReflectTypeDescription *, uint32_t &)> calc_size_of_members;
            calc_size_of_members = [&](SpvReflectTypeDescription *p_description, uint32_t &size) {
                if (p_description == nullptr) {
                    return;
                }

                // Parse members
                for (int m = 0; m < p_description->member_count; m++) {   
                    SpvReflectTypeDescription *p_member_desc = &p_description->members[m];
                    SpvReflectNumericTraits numeric_traits   = p_member_desc->traits.numeric;
                    SpvReflectArrayTraits array_traits       = p_member_desc->traits.array;
                    uint32_t new_size = 0;

                    if ((p_member_desc->type_flags & SPV_REFLECT_TYPE_FLAG_MATRIX) != 0) {
                        new_size += numeric_traits.matrix.column_count * numeric_traits.matrix.row_count * (numeric_traits.scalar.width / 8);
                    }
                    else if ((p_member_desc->type_flags & SPV_REFLECT_TYPE_FLAG_VECTOR) != 0) {
                        new_size += numeric_traits.vector.component_count * (numeric_traits.scalar.width / 8);
                    }
                    else if ((p_member_desc->type_flags & SPV_REFLECT_TYPE_FLAG_FLOAT) != 0 ||
                        (p_member_desc->type_flags & SPV_REFLECT_TYPE_FLAG_INT) != 0) {
                        new_size += numeric_traits.scalar.width / 8;
                    }
                    else if ((p_member_desc->type_flags & SPV_REFLECT_TYPE_FLAG_STRUCT) != 0) {
                        calc_size_of_members(p_member_desc, new_size);
                    }
                    else {
                        STLA_ASSERT_RELEASE(0, "unhandled type flags: 0x%08X", p_member_desc->type_flags);
                    }

                    if ((p_member_desc->type_flags & SPV_REFLECT_TYPE_FLAG_ARRAY) != 0) {
                        STLA_ASSERT_RELEASE(array_traits.dims_count == 1, "reflection doesn't support multi-dimensional arrays (%s)", p_member_desc->struct_member_name);
                        //STLA_ASSERT_RELEASE(array_traits.dims[0] != 0, "reflection requires array sizes to be explicit in the shader (%s)", p_member_desc->struct_member_name);
                        if (array_traits.dims[0] != 0) {
                            new_size *= array_traits.dims[0];// *array_traits.stride;
                        }
                    }

                    size += new_size;
                }
            };

            calc_size_of_members(p_binding->type_description, new_binding.size);

            desc_set_bindings[new_binding.binding] = new_binding;
        }

        m_descriptor_sets[p_desc_set_info->set] = desc_set_bindings;
    }

    return true;
}

void shader_reflection_info::clear()
{
    for (auto &desc_set_bindings : m_descriptor_sets) {
        desc_set_bindings.clear();
    }
    m_descriptor_sets.clear();
    m_descriptor_sets.resize(m_descriptor_sets.capacity());
    m_vertex_layout.clear();
}

bool shader_reflection_info::combine(const shader_reflection_info &new_info)
{
    for (int i = 0; i < MAX_DESCRIPTOR_SETS; i++) {
        for (const auto &[binding_num, binding] : new_info.m_descriptor_sets[i]) {
            if (m_descriptor_sets[i].find(binding_num) == m_descriptor_sets[i].end()) {
                m_descriptor_sets[i][binding_num] = binding;
            }
            
            STLA_ASSERT(m_descriptor_sets[i][binding_num] == binding, "binding mismatch");
        }
    }

    return true;
}

descriptor_set_binding_array shader_reflection_info::get_binding_array(descriptor_set_loc loc) const
{
    descriptor_set_binding_array binding_array;

    if ((uint32_t)loc >= MAX_DESCRIPTOR_SETS) {
        STLA_ASSERT_RELEASE(0, "invalid descriptor set loc: %d", loc);
        return binding_array;
    }

    const descriptor_set_binding_map *p_desc_binding_map = &m_descriptor_sets[(int)loc];

    for (const auto &[binding_num, binding] : *p_desc_binding_map) {
        binding_array.push_back(binding);
    }

    etl::sort(binding_array.begin(), binding_array.end(), [](descriptor_set_binding const &lhs, descriptor_set_binding const &rhs) {
        return lhs.binding < rhs.binding;
    });

    return binding_array;
}

vector_4<descriptor_set_loc> shader_reflection_info::get_descriptor_set_locs() const
{
    vector_4<descriptor_set_loc> locs;

    for (int i = 0; i < m_descriptor_sets.size(); i++) {
        if (m_descriptor_sets[i].size() != 0) {
            locs.push_back((descriptor_set_loc)i);
        }
    }

    etl::sort(locs.begin(), locs.end());
    return locs;
}

bool shader_reflection_mgr::parse(const char *p_path)
{
    shader_parser parser;
    if (!parser.parse(p_path)) {
        return false;
    }

    STLA_ASSERT(parser.get_source(shader_type::raygen).size() == 0, "shader reflection not implemented for: ray_gen");
    STLA_ASSERT(parser.get_source(shader_type::anyhit).size() == 0, "shader reflection not implemented for: any_hit");
    STLA_ASSERT(parser.get_source(shader_type::closesthit).size() == 0, "shader reflection not implemented for: closest_hit");
    STLA_ASSERT(parser.get_source(shader_type::miss).size() == 0, "shader reflection not implemented for: miss");
    STLA_ASSERT(parser.get_source(shader_type::intersection).size() == 0, "shader reflection not implemented for: intersection");
    STLA_ASSERT(parser.get_source(shader_type::callable).size() == 0, "shader reflection not implemented for: callable");

    return parse(parser);
}

bool shader_reflection_mgr::parse(const shader_parser &parser)
{
    clear();
    
    spirv_byte_code vertex_spirv;
    spirv_byte_code fragment_spirv;
    spirv_byte_code compute_spirv;

    auto vertex_source   = parser.get_source(shader_type::vertex);
    auto fragment_source = parser.get_source(shader_type::fragment);
    auto compute_source  = parser.get_source(shader_type::compute);

    shader::glsl_to_spirv(shader_type::vertex,   vertex_source.size(),   vertex_source.data(),   vertex_spirv);
    shader::glsl_to_spirv(shader_type::fragment, fragment_source.size(), fragment_source.data(), fragment_spirv);
    shader::glsl_to_spirv(shader_type::compute,  compute_source.size(),  compute_source.data(),  compute_spirv);

    if (!m_reflection_infos[shader_type::vertex].load_from_spirv(vertex_spirv)) {
        return false;
    }

    if (!m_reflection_infos[shader_type::fragment].load_from_spirv(fragment_spirv)) {
        return false;
    }

    if (!m_reflection_infos[shader_type::compute].load_from_spirv(compute_spirv)) {
        return false;
    }

    return true;
}

bool shader_reflection_mgr::parse(shader_type type, const shader_source_code &src)
{
    clear();

    spirv_byte_code spirv;

    shader::glsl_to_spirv(type, src.size(), src.data(), spirv);

    return m_reflection_infos[type].load_from_spirv(spirv);
}

void shader_reflection_mgr::clear()
{
    for (auto &[type, info] : m_reflection_infos) {
        info.clear();
    }
}


shader_reflection_info shader_reflection_mgr::get_info(shader_type type) const
{
    switch (type)
    {
        case shader_type::vertex:   
        case shader_type::fragment: 
        case shader_type::compute:  return m_reflection_infos.at(type);
        default: 
        {
            STLA_ASSERT_RELEASE(0, "reflection not implemented for shader type %d", (uint32_t)type);
            return m_reflection_infos.at(type);
        }
    }
}


} // namespace vk
} // namespace stla
