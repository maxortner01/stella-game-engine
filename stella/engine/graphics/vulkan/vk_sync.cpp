#include "vk_sync.h"
#include "vk_state.h"
#include "vk_common.h"

namespace stla
{
namespace vk
{
    
bool semaphore::create(state &state)
{
	VkSemaphoreCreateInfo create_info = {};
	create_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	create_info.pNext = nullptr;
	create_info.flags = 0;

    VkSemaphore semaphore = nullptr;
	if ((semaphore = state.vk_create_semaphore(create_info)) == nullptr) {
        return false;
    }

    m_semaphore = semaphore;
    mp_state = &state;
    return true;
}

void semaphore::destroy()
{
    if (mp_state == nullptr) {
        return;
    }

    if (m_semaphore) {
        mp_state->push_deletor([sem = m_semaphore](state &state) {
            vkDestroySemaphore(state.get_device_handle(), sem, nullptr);
        });
    }

    m_semaphore = nullptr;
    mp_state = nullptr;
}

bool fence::create(state &state)
{
    VkFenceCreateInfo fence_info{};
    fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    VkFence fence = nullptr;
    if ((fence = state.vk_create_fence(fence_info)) == nullptr) {
        return false;
    }

    m_fence     = fence;
    mp_state = &state;
    return true;
}

void fence::destroy()
{
    if (mp_state == nullptr) {
        return;
    }

    if (m_fence) {
        mp_state->push_deletor([fence = m_fence](state &state) {
            vkDestroyFence(state.get_device_handle(), fence, nullptr);
        });
    }

    m_fence     = nullptr;
    mp_state = nullptr;
}

bool fence::wait(uint32_t timeout_ms)
{
    if (!is_vaild()) {
        return false;
    }
    
    VkResult result = vkWaitForFences(mp_state->get_device_handle(), 1, &m_fence, VK_TRUE, timeout_ms * 1000000);
    VK_ASSERT(result);
    return result == VK_SUCCESS;
}

bool fence::reset()
{
    if (!is_vaild()) {
        return false;
    }

    VkResult result = vkResetFences(mp_state->get_device_handle(), 1, &m_fence);
    VK_ASSERT(result);
    return result == VK_SUCCESS;
}

} // namespace vk
} // namespace stla
