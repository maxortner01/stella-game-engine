#include "vk_compute_pipeline.h"
#include "vk_shader.h"
#include "vk_state.h"
#include "vk_descriptors.h"
#include "vk_common.h"

#include "stla_assert.h"

namespace stla
{
namespace vk
{
    
bool compute_pipeline::create(const descriptor_set_layout_container &desc_set_layout_container, const shader &compute_shader, state &state)
{
    STLA_ASSERT_RELEASE(compute_shader.get_type() == shader_type::compute, "compute pipeline requires a compute shader, got type %d", 
        (int)compute_shader.get_type());

    if (!compute_shader.is_valid()) {
        return false;
    }

    vector_4<VkDescriptorSetLayout> vk_desc_set_layouts;

    descriptor_set_loc locs[] = {
        descriptor_set_loc::zero, descriptor_set_loc::one, descriptor_set_loc::two, descriptor_set_loc::three
    };

    for (descriptor_set_loc &loc : locs) {
        if (desc_set_layout_container.has_layout(loc)) {
            vk_desc_set_layouts.push_back(desc_set_layout_container.get_layout(loc).get_handle());
        }
    }

    VkPipeline pipeline = nullptr;
    VkPipelineLayout pipeline_layout = nullptr;

    VkPipelineShaderStageCreateInfo compute_shader_stage_info{};
    compute_shader_stage_info.sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    compute_shader_stage_info.stage  = VK_SHADER_STAGE_COMPUTE_BIT;
    compute_shader_stage_info.module = compute_shader.get_handle();
    compute_shader_stage_info.pName  = "main";

    VkPipelineLayoutCreateInfo pipeline_layout_info{};
    pipeline_layout_info.sType          = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipeline_layout_info.setLayoutCount = vk_desc_set_layouts.size();
    pipeline_layout_info.pSetLayouts    = vk_desc_set_layouts.size() ? vk_desc_set_layouts.data() : nullptr;

    VK_ASSERT(vkCreatePipelineLayout(state.get_device_handle(), &pipeline_layout_info, nullptr, &pipeline_layout));

    VkComputePipelineCreateInfo pipeline_info{};
    pipeline_info.sType  = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
    pipeline_info.layout = pipeline_layout;
    pipeline_info.stage  = compute_shader_stage_info;

    VK_ASSERT(vkCreateComputePipelines(state.get_device_handle(), VK_NULL_HANDLE, 1, &pipeline_info, nullptr, &pipeline));

    if (is_valid()) {
        destroy();
    }

    m_pipeline        = pipeline;
    m_pipeline_layout = pipeline_layout;
    mp_state          = &state;

    return true;
}

void compute_pipeline::destroy()
{
    if (mp_state == nullptr) {
        return;
    }

    if (m_pipeline != nullptr) {
        mp_state->push_deletor([pipeline = m_pipeline](state &state) {
            vkDestroyPipeline(state.get_device_handle(), pipeline, nullptr);
        });
    }

    if (m_pipeline_layout != nullptr) {
        mp_state->push_deletor([pipeline_layout = m_pipeline_layout](state &state) {
            vkDestroyPipelineLayout(state.get_device_handle(), pipeline_layout, nullptr);
        });
    }

    m_pipeline        = nullptr;
    m_pipeline_layout = nullptr;
    mp_state          = nullptr;
}

} // namespace vk
} // namespace stla
