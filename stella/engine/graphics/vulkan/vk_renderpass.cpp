#include "vk_renderpass.h"
#include "vk_state.h"
#include "vk_common.h"
#include "vk_buffer.h"
#include "vk_shader.h"
#include "vk_render_target.h"

#include <vulkan/vulkan.h>

namespace stla
{
namespace vk
{

void renderpass::destroy()
{
    if (mp_state == nullptr) {
        return;
    }

    mp_state->push_deletor([renderpass = m_renderpass](state &state) {
        vkDestroyRenderPass(state.get_device_handle(), renderpass, nullptr);
    });

    mp_state  = nullptr;
    m_renderpass = nullptr;
}

renderpass_builder::renderpass_builder(state &state) :
    m_state(state)
{ }

renderpass_builder &renderpass_builder::add_attachment(
    attachment_type type,
    VkFormat format,
    VkSampleCountFlagBits samples,
    VkAttachmentLoadOp load_op,
    VkAttachmentStoreOp store_op,
    VkAttachmentLoadOp stencil_load_op,
    VkAttachmentStoreOp stencil_store_op,
    VkImageLayout initial_layout,
    VkImageLayout final_layout)
{

    VkAttachmentDescription attachment = {};
    attachment.format         = format;
    attachment.samples        = samples;
    attachment.loadOp         = load_op;
    attachment.storeOp        = store_op;
    attachment.stencilLoadOp  = stencil_load_op;
    attachment.stencilStoreOp = stencil_store_op;
    attachment.initialLayout  = initial_layout;
    attachment.finalLayout    = final_layout;

    if (type == attachment_type::depth) {
        m_depth_attachment = attachment;
        m_has_depth_attachment = true;
    }
    else {
        m_color_attachments.push_back(attachment);
    }

    return *this;
}

bool renderpass_builder::build(renderpass &pass)
{
    VkAttachmentReference depth_attachment_ref = { };
    etl::vector<VkAttachmentReference, MAX_COLOR_ATTACHMENTS> attachment_refs;
    etl::vector<VkAttachmentDescription, MAX_COLOR_ATTACHMENTS + 1> attachments;

    uint32_t attachment = 0;
    for (VkAttachmentDescription &desc : m_color_attachments) {
        VkAttachmentReference color_attachment_ref = { };
        color_attachment_ref.attachment = attachment; // attachment number will index into the pAttachments array in the parent renderpass itself
        color_attachment_ref.layout     = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        attachment_refs.push_back(color_attachment_ref);
        attachments.push_back(desc);
        attachment++;
    }

    VkSubpassDescription subpass    = { };
    subpass.pipelineBindPoint       = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount    = attachment_refs.size();
    subpass.pColorAttachments       = attachment_refs.data();
    if (m_has_depth_attachment) {
        depth_attachment_ref.attachment = attachment;
        depth_attachment_ref.layout     = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        attachments.push_back(m_depth_attachment);

        subpass.pDepthStencilAttachment = &depth_attachment_ref;
    }

    VkRenderPassCreateInfo renderpass_info = { };
    renderpass_info.sType           = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderpass_info.attachmentCount = attachments.size();
    renderpass_info.pAttachments    = attachments.data();
    renderpass_info.subpassCount    = 1; // connect the subpass to the info
    renderpass_info.pSubpasses      = &subpass;

    VkRenderPass vk_renderpass = nullptr;

    if ((vk_renderpass = m_state.vk_create_renderpass(renderpass_info)) == nullptr) {
        return false;
    }

    if (pass.is_valid()) {
        pass.destroy();
    }

    pass = renderpass(vk_renderpass, m_state);
    return true;
}

} // namespace vk
} // namespace stla
