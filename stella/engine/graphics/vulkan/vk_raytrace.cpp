#include "vk_raytrace.h"
#include "vk_buffer.h"
#include "vk_state.h"

#include "stla_geometry.h"
#include "stla_assert.h"
#include "stla_vertex_buffer_layout.h"
#include "stla_logger.h"

namespace stla
{
namespace vk
{
    
bool blas::create_from_triangles(const buffer &vertex_buffer, const buffer &index_buffer, const vertex_buffer_layout &layout, state &state)
{
    if (!vertex_buffer.is_valid() || !index_buffer.is_valid()) {
        return false;
    }

    VkDeviceAddress vertex_buffer_address = state.vk_get_buffer_device_address(vertex_buffer);
    VkDeviceAddress index_buffer_address  = state.vk_get_buffer_device_address(index_buffer);    

    // Specifies where AS builder can find the vertices and optional indices for triangle data, including formats and lengths
    VkAccelerationStructureGeometryTrianglesDataKHR triangles_data = { };
    triangles_data.sType                    = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR;
    triangles_data.vertexFormat             = VK_FORMAT_R32G32B32_SFLOAT; //ignore other attributes, only position
    triangles_data.vertexData.deviceAddress = vertex_buffer_address;
    triangles_data.vertexStride             = layout.get_stride(); 
    triangles_data.indexType                = VK_INDEX_TYPE_UINT32;
    triangles_data.indexData.deviceAddress  = index_buffer_address;
    triangles_data.maxVertex                = (vertex_buffer.get_size() / layout.get_stride()) - 1;
    triangles_data.transformData            = { 0 };

    printf("stride %d, max vertex %d\n", triangles_data.vertexStride, triangles_data.maxVertex);

    // NOTE: Triangle can be marked as inactive by setting the X-component of each vertex to a floating point NaN
    VkAccelerationStructureGeometryKHR as_geometry = { };
    as_geometry.sType              = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
    as_geometry.geometryType       = VK_GEOMETRY_TYPE_TRIANGLES_KHR;
    as_geometry.geometry.triangles = triangles_data;
    as_geometry.flags              = VK_GEOMETRY_OPAQUE_BIT_KHR;

    VkAccelerationStructureBuildRangeInfoKHR range_info = { };
    range_info.firstVertex     = 0;
    range_info.primitiveCount  = (index_buffer.get_size() / 4) / 3; // / sizeof(uint32_t) / num_indices_per_primitive
    range_info.primitiveOffset = 0;
    range_info.transformOffset = 0;

    return create(as_geometry, range_info, state);
}

bool blas::create_from_aabbs(const buffer &aabb_buffer, uint32_t primitive_count, state &state, uint32_t primitive_offset)
{
    buffer blas_scratch_buffer;
    buffer blas_buffer;
    VkAccelerationStructureKHR blas;

    VkDeviceAddress aabb_buffer_address = state.vk_get_buffer_device_address(aabb_buffer);

    VkAccelerationStructureGeometryAabbsDataKHR aabb_data = { };
    aabb_data.sType              = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_AABBS_DATA_KHR;
    aabb_data.data.deviceAddress = aabb_buffer_address;
    aabb_data.stride             = sizeof(aabb_3d);

    VkAccelerationStructureGeometryKHR as_geometry = { };
    as_geometry.sType          = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
    as_geometry.geometryType   = VK_GEOMETRY_TYPE_AABBS_KHR;
    as_geometry.geometry.aabbs = aabb_data;
    as_geometry.flags          = VK_GEOMETRY_OPAQUE_BIT_KHR;

    VkAccelerationStructureBuildRangeInfoKHR range_info = { };
    range_info.firstVertex     = 0;
    range_info.primitiveCount  = primitive_count; //aabb_buffer.get_size() / sizeof(aabb_3d);
    range_info.primitiveOffset = primitive_offset;
    range_info.transformOffset = 0;

    return create(as_geometry, range_info, state);
}

bool blas::create(const VkAccelerationStructureGeometryKHR &geometry, const VkAccelerationStructureBuildRangeInfoKHR &range_info, state &state)
{
    VkAccelerationStructureKHR blas; 
    buffer blas_buffer;
    buffer blas_scratch_buffer;

    VkAccelerationStructureBuildSizesInfoKHR size_info = { };

    {
        VkAccelerationStructureBuildGeometryInfoKHR build_info = { };
        build_info.sType                    = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
        build_info.flags                    = VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR | VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
        build_info.geometryCount            = 1;
        build_info.pGeometries              = &geometry;
        build_info.mode                     = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
        build_info.type                     = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
        build_info.srcAccelerationStructure = VK_NULL_HANDLE;

        size_info = state.vk_get_acceleration_structure_build_sizes(build_info, range_info.primitiveCount);
    }

    blas_buffer = state.create_buffer(
        size_info.accelerationStructureSize, 
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR |
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | 
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    if (!blas_buffer.is_valid()) {
        return false;
    }

    VkAccelerationStructureCreateInfoKHR as_create_info = { };
    as_create_info.sType         = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
    as_create_info.type          = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
    as_create_info.size          = size_info.accelerationStructureSize;
    as_create_info.buffer        = blas_buffer.get_buffer_handle();
    as_create_info.offset        = 0;
    as_create_info.createFlags   = VK_ACCELERATION_STRUCTURE_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT_KHR;
    as_create_info.deviceAddress = state.vk_get_buffer_device_address(blas_buffer);

    // Create empty acceleration structure
    if ((blas = state.vk_create_acceleration_structure(as_create_info)) == nullptr) {
        return false;
    }

    // Allocate scratch buffer
    blas_scratch_buffer = state.create_buffer(
        size_info.buildScratchSize,
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    bool cmd_result = true;

    {
        VkBufferDeviceAddressInfo buffer_info = { };
        buffer_info.sType  = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
        buffer_info.pNext  = nullptr;
        buffer_info.buffer = blas_scratch_buffer.get_buffer_handle();  

        VkAccelerationStructureBuildGeometryInfoKHR build_info = { };
        build_info.sType                     = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
        build_info.flags                     = VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR | VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
        build_info.geometryCount             = 1;
        build_info.pGeometries               = &geometry;
        build_info.mode                      = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
        build_info.type                      = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
        build_info.srcAccelerationStructure  = VK_NULL_HANDLE;
        build_info.dstAccelerationStructure  = blas;
        build_info.scratchData.deviceAddress = state.vk_get_buffer_device_address(buffer_info);
    
        cmd_result = state.execute_graphics_commands([&](graphics_command_buffer &cmd_buffer) {
            cmd_buffer.build_acceleration_structure(build_info, range_info);
        });

    }

    if (cmd_result) {
        auto get_geometry_type_string = [](VkGeometryTypeKHR type) {
            switch (type) {
                case VK_GEOMETRY_TYPE_TRIANGLES_KHR: return "triangles";
                case VK_GEOMETRY_TYPE_AABBS_KHR:     return "aabbs";
                case VK_GEOMETRY_TYPE_INSTANCES_KHR: return "instances";
                default: return "unknown";
            }
        };

        STLA_CORE_LOG_INFO("blas created (type={0})", get_geometry_type_string(geometry.geometryType));
        STLA_CORE_LOG_INFO(" Primitive Count : {0}", range_info.primitiveCount);
        STLA_CORE_LOG_INFO(" Acceleration Structure Build Sizes");
        STLA_CORE_LOG_INFO("  Structure      : {0}", size_info.accelerationStructureSize);
        STLA_CORE_LOG_INFO("  Update Scratch : {0}", size_info.updateScratchSize);
        STLA_CORE_LOG_INFO("  Build Scratch  : {0}", size_info.buildScratchSize);

        destroy();

        mp_state      = &state;
        m_blas_buffer = blas_buffer;
        m_blas        = blas;
    }

    blas_scratch_buffer.destroy();
    return cmd_result;
}

void blas::destroy()
{
    if (!is_valid()) {
        return;
    }

    m_blas_buffer.destroy();

    if (m_blas != nullptr) {
        mp_state->push_deletor([as = m_blas](state &state) {
            vkDestroyAccelerationStructureKHR(state.get_device_handle(), as, nullptr);
        });
    }
    
    mp_state = nullptr;
}

VkDeviceAddress blas::get_device_address() const
{
    if (!is_valid()) {
        return (VkDeviceAddress)0;
    }

    VkAccelerationStructureDeviceAddressInfoKHR address_info = { };
    address_info.sType                 = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
    address_info.accelerationStructure = m_blas;

    return mp_state->vk_get_acceleration_structure_device_address(address_info);
}

bool tlas::build(state &state)
{
    buffer device_instance_buffer;
    buffer tlas_buffer;
    buffer scratch_buffer;
    VkAccelerationStructureKHR tlas = nullptr;

    if (m_instance_count == 0) {
        return false;
    }

    device_instance_buffer = state.create_buffer(
        sizeof(VkAccelerationStructureInstanceKHR) * m_instance_count,
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    if (!device_instance_buffer.is_valid()) {
        return false;
    }

    if (!device_instance_buffer.copy_from(sizeof(VkAccelerationStructureInstanceKHR) * m_instance_count, m_vk_instances.data())) {
        return false;
    }

    VkAccelerationStructureGeometryInstancesDataKHR geometry_instance_data = { };
    geometry_instance_data.sType              = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR;
    geometry_instance_data.arrayOfPointers    = VK_FALSE;
    geometry_instance_data.data.deviceAddress = state.vk_get_buffer_device_address(device_instance_buffer);

    VkAccelerationStructureGeometryKHR geometry_instance = { };
    geometry_instance.sType              = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
    geometry_instance.geometryType       = VK_GEOMETRY_TYPE_INSTANCES_KHR;
    geometry_instance.geometry.instances = geometry_instance_data;

    VkAccelerationStructureBuildRangeInfoKHR range_info;
    range_info.primitiveOffset = 0;
    range_info.primitiveCount  = m_instance_count;
    range_info.firstVertex     = 0;
    range_info.transformOffset = 0;

    // Create the build info: in this case , pointing to only one geometry object.
    VkAccelerationStructureBuildSizesInfoKHR size_info = { };
    {
        VkAccelerationStructureBuildGeometryInfoKHR build_info = { };
        build_info.sType                    = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
        build_info.flags                    = VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR | VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
        build_info.geometryCount            = 1;
        build_info.pGeometries              = &geometry_instance;
        build_info.mode                     = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
        build_info.type                     = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
        build_info.srcAccelerationStructure = VK_NULL_HANDLE;

        // Call vkGetAccelerationStructureBuildSizesKHR to get the memory size requirements to perform a build.
        size_info = state.vk_get_acceleration_structure_build_sizes(build_info, range_info.primitiveCount);
    }

    // Allocate buffers of sufficient size to hold the acceleration structure (VkAccelerationStructureBuildSizesKHR::accelerationStructureSize) 
    // and build scratch buffer (VkAccelerationStructureBuildSizesKHR::buildScratchSize)
    tlas_buffer = state.create_buffer(
        size_info.accelerationStructureSize,
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR | 
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT |
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    if (!tlas_buffer.is_valid()) {
        device_instance_buffer.destroy();
        return false;
    }

    scratch_buffer = state.create_buffer(
        size_info.buildScratchSize,
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | 
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    if (!scratch_buffer.is_valid()) {
        device_instance_buffer.destroy();
        tlas_buffer.destroy();
        return false;
    }

    // Call vkCreateAccelerationStructureKHR to create an acceleration structure at a specified location within a buffer
    VkAccelerationStructureCreateInfoKHR tlas_create_info = { };
    tlas_create_info.sType         = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
    tlas_create_info.type          = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
    tlas_create_info.size          = size_info.accelerationStructureSize;
    tlas_create_info.buffer        = tlas_buffer.get_buffer_handle();
    tlas_create_info.offset        = 0;
    tlas_create_info.createFlags   = VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR | VK_ACCELERATION_STRUCTURE_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT_KHR;
    tlas_create_info.deviceAddress = state.vk_get_buffer_device_address(tlas_buffer);

    if ((tlas = state.vk_create_acceleration_structure(tlas_create_info)) == nullptr) {
        device_instance_buffer.destroy();
        tlas_buffer.destroy();
        scratch_buffer.destroy();
        return false;
    }

    // Call vkCmdBuildAccelerationStructuresKHR to build the acceleration structure. 
    // The previously populated VkAccelerationStructureBuildGeometryInfoKHR should be used as a parameter here, 
    // along with the destination acceleration structure object, build scratch buffer, and geometry data pointers (for vertices, indices and transforms)
    {
        VkAccelerationStructureBuildGeometryInfoKHR build_info = { };
        build_info.sType                     = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
        build_info.flags                     = VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR | VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
        build_info.geometryCount             = 1;
        build_info.pGeometries               = &geometry_instance;
        build_info.mode                      = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
        build_info.type                      = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
        build_info.srcAccelerationStructure  = VK_NULL_HANDLE;
        build_info.dstAccelerationStructure  = tlas;
        build_info.scratchData.deviceAddress = state.vk_get_buffer_device_address(scratch_buffer);

        bool cmd_result = state.execute_graphics_commands([&](graphics_command_buffer &cmd_buffer) {
            cmd_buffer.build_acceleration_structure(build_info, range_info);
        });

        if (!cmd_result) {
            return false;
        }
    }
    
    STLA_CORE_LOG_INFO("tlas built");
    STLA_CORE_LOG_INFO(" Instances       : {0}", m_instance_count);
    STLA_CORE_LOG_INFO(" Acceleration Structure Build Sizes");
    STLA_CORE_LOG_INFO("  Structure      : {0}", size_info.accelerationStructureSize);
    STLA_CORE_LOG_INFO("  Update Scratch : {0}", size_info.updateScratchSize);
    STLA_CORE_LOG_INFO("  Build Scratch  : {0}", size_info.buildScratchSize);

    if (is_valid()) {
        destroy();
    }

    m_device_instance_buffer = device_instance_buffer;
    m_scratch_buffer         = scratch_buffer;
    m_tlas_buffer            = tlas_buffer;
    m_tlas_handle            = tlas;
    mp_state                 = &state;

    return true;
}

bool tlas::update()
{
    if (!is_valid()) {
        return false;
    }

    if (!m_needs_update) {
        return true;
    }

    /*
    m_vk_instances.clear();
    for (auto &[id, blas_instance] : m_host_instances) {
        VkAccelerationStructureInstanceKHR instance_info = { };
        blas_instance.transform = glm::transpose(blas_instance.transform);//todo why need this?
        
        instance_info.transform.matrix[0][0] = blas_instance.transform[0][0];
        instance_info.transform.matrix[0][1] = blas_instance.transform[0][1];
        instance_info.transform.matrix[0][2] = blas_instance.transform[0][2];
        instance_info.transform.matrix[0][3] = blas_instance.transform[0][3];

        instance_info.transform.matrix[1][0] = blas_instance.transform[1][0];
        instance_info.transform.matrix[1][1] = blas_instance.transform[1][1];
        instance_info.transform.matrix[1][2] = blas_instance.transform[1][2];
        instance_info.transform.matrix[1][3] = blas_instance.transform[1][3];

        instance_info.transform.matrix[2][0] = blas_instance.transform[2][0];
        instance_info.transform.matrix[2][1] = blas_instance.transform[2][1];
        instance_info.transform.matrix[2][2] = blas_instance.transform[2][2];
        instance_info.transform.matrix[2][3] = blas_instance.transform[2][3];

        instance_info.instanceCustomIndex                    = 0;
        instance_info.mask                                   = 0xFF;
        instance_info.instanceShaderBindingTableRecordOffset = 0;
        instance_info.flags                                  = VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR ;
        instance_info.accelerationStructureReference         = blas_instance.blas_address;

        STLA_ASSERT_RELEASE(m_vk_instances.available(), "m_vk_instances out of space");
        m_vk_instances.push_back(instance_info);
    }
    printf("SIZE: %d\n", m_vk_instances.size());
    */

    if (!m_device_instance_buffer.copy_from(sizeof(VkAccelerationStructureInstanceKHR) * m_instance_count, m_vk_instances.data())) {
        return false;
    }

    VkAccelerationStructureBuildRangeInfoKHR range_info;
    range_info.primitiveOffset = 0;
    range_info.primitiveCount  = m_instance_count;
    range_info.firstVertex     = 0;
    range_info.transformOffset = 0;

    VkAccelerationStructureGeometryInstancesDataKHR geometry_instance_data = { };
    geometry_instance_data.sType              = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR;
    geometry_instance_data.arrayOfPointers    = VK_FALSE;
    geometry_instance_data.data.deviceAddress = mp_state->vk_get_buffer_device_address(m_device_instance_buffer);

    VkAccelerationStructureGeometryKHR geometry_instance = { };
    geometry_instance.sType              = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
    geometry_instance.geometryType       = VK_GEOMETRY_TYPE_INSTANCES_KHR;
    geometry_instance.geometry.instances = geometry_instance_data;

    VkAccelerationStructureBuildGeometryInfoKHR build_info = { };
    build_info.sType                     = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    build_info.flags                     = VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR | VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
    build_info.geometryCount             = 1;
    build_info.pGeometries               = &geometry_instance;
    build_info.mode                      = VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR;
    build_info.type                      = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
    build_info.srcAccelerationStructure  = m_tlas_handle;
    build_info.dstAccelerationStructure  = m_tlas_handle;
    build_info.scratchData.deviceAddress = mp_state->vk_get_buffer_device_address(m_scratch_buffer);

    bool cmd_result = mp_state->execute_graphics_commands([&](graphics_command_buffer &cmd_buffer) {
        cmd_buffer.build_acceleration_structure(build_info, range_info);
    });

    if (!cmd_result) {
        return false;
    }

    m_needs_update = false;
    return true;
}

void tlas::destroy()
{
    if (!is_valid()) {
        return;
    }

    m_device_instance_buffer.destroy();
    m_scratch_buffer.destroy();
    m_tlas_buffer.destroy();

    mp_state->push_deletor([tlas = m_tlas_handle](state &state) {
        vkDestroyAccelerationStructureKHR(state.get_device_handle(), tlas, nullptr);
    });

    m_tlas_handle = nullptr;
    mp_state      = nullptr;

    //m_host_instances.clear();    
}

tlas::instance_id tlas::push_instance(const blas &blas, const glm::mat4 &transform)
{
    if (m_free_list.size() == 0) {
        return invalid_instance_id;
    }

    int instance_id = m_free_list.back();
    m_free_list.pop_back();

    //m_next_instance_id++;
    //STLA_ASSERT_RELEASE(m_host_instances.find(m_next_instance_id) == m_host_instances.end(), "tlas already contains instance id {0}", (uint32_t)m_next_instance_id);
    //m_host_instances[m_next_instance_id] = obj_instance(blas.get_device_address(), transform);
    
    m_instance_in_use[instance_id] = true;

    VkAccelerationStructureInstanceKHR instance_info = { };
    glm::mat4 transform_t = glm::transpose(transform);//todo why need this?
    
    instance_info.transform.matrix[0][0] = transform_t[0][0];
    instance_info.transform.matrix[0][1] = transform_t[0][1];
    instance_info.transform.matrix[0][2] = transform_t[0][2];
    instance_info.transform.matrix[0][3] = transform_t[0][3];

    instance_info.transform.matrix[1][0] = transform_t[1][0];
    instance_info.transform.matrix[1][1] = transform_t[1][1];
    instance_info.transform.matrix[1][2] = transform_t[1][2];
    instance_info.transform.matrix[1][3] = transform_t[1][3];

    instance_info.transform.matrix[2][0] = transform_t[2][0];
    instance_info.transform.matrix[2][1] = transform_t[2][1];
    instance_info.transform.matrix[2][2] = transform_t[2][2];
    instance_info.transform.matrix[2][3] = transform_t[2][3];
    
    instance_info.instanceCustomIndex                    = 0;
    instance_info.mask                                   = 0xFF;
    instance_info.instanceShaderBindingTableRecordOffset = 0;
    instance_info.flags                                  = VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR ;
    instance_info.accelerationStructureReference         = blas.get_device_address();

    m_vk_instances[instance_id] = instance_info;
    
    m_instance_count++;
    return instance_id;
}

bool tlas::set_instance_transform(tlas::instance_id id, const glm::mat4 &transform)
{
    //STLA_ASSERT(m_host_instances.available(), "out of memory for host m_vk_instances");
    //STLA_ASSERT(m_host_instances.find(id) != m_host_instances.end(), "tlas doesn't contain instance (id=%d)", id);
    STLA_ASSERT_RELEASE(id != tlas::invalid_instance_id, "invalid instance id %d", (int)id);
    STLA_ASSERT_RELEASE(m_instance_in_use[id], "invalid instance id %d", (int)id);
    STLA_ASSERT_RELEASE(id < MAX_INSTANCES, "invalid instance id %d", (int)id);

    /*
    if (m_host_instances.find(id) == m_host_instances.end()) {
        return false;
    }
    m_host_instances[id].transform = transform;
    */

    glm::mat4 transform_t = glm::transpose(transform);//todo why need this?
    
    m_vk_instances[id].transform.matrix[0][0] = transform_t[0][0];
    m_vk_instances[id].transform.matrix[0][1] = transform_t[0][1];
    m_vk_instances[id].transform.matrix[0][2] = transform_t[0][2];
    m_vk_instances[id].transform.matrix[0][3] = transform_t[0][3];

    m_vk_instances[id].transform.matrix[1][0] = transform_t[1][0];
    m_vk_instances[id].transform.matrix[1][1] = transform_t[1][1];
    m_vk_instances[id].transform.matrix[1][2] = transform_t[1][2];
    m_vk_instances[id].transform.matrix[1][3] = transform_t[1][3];

    m_vk_instances[id].transform.matrix[2][0] = transform_t[2][0];
    m_vk_instances[id].transform.matrix[2][1] = transform_t[2][1];
    m_vk_instances[id].transform.matrix[2][2] = transform_t[2][2];
    m_vk_instances[id].transform.matrix[2][3] = transform_t[2][3];

    m_needs_update = true;
    return true;
}

void tlas::clear() 
{ 
    m_instance_in_use.fill(false);
    m_instance_count = 0;
    
    m_free_list.clear();
    for (int i = 0; i < MAX_INSTANCES; i++) {
        m_free_list.push_back((MAX_INSTANCES - 1) - i);
    }
}

} // namespace vk
} // namespace stla
