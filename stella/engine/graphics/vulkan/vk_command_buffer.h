#pragma once

#include "vk_descriptors.h"

#include "stla_vector.h"

#include <vulkan/vulkan.h>
#include <glm/glm.hpp>

#include <stdint.h>

namespace stla
{
namespace vk
{

class render_target;
class renderpass;
class raster_pipeline;
class compute_pipeline;
class buffer;
class fence;
class image;
class semaphore;

class icommand_buffer
{
public:

    icommand_buffer() = default;
    icommand_buffer(VkCommandBuffer cmd_buffer) : 
        m_command_buffer(cmd_buffer) { }
    
    void destroy();
    void push_wait_semaphore(semaphore &sem);
    void push_signal_semaphore(semaphore &sem);
    bool is_valid() const { return m_command_buffer != nullptr && mp_command_pool_handle != nullptr; }

    const VkCommandBuffer &get_handle() const { return m_command_buffer; } 
    vector_8<VkSemaphore> get_vk_wait_semaphores() const;
    vector_8<VkSemaphore> get_vk_signal_semaphores() const;
    const fence *get_fence() const { return mp_fence; }
    const void *get_pool_handle() const { return mp_command_pool_handle; }
    void set_pool_handle(const void *p_pool) { mp_command_pool_handle = p_pool; }

    // Commands
    bool begin(); //todo option to begin with another command buffer as argument for inheritance info
    bool end(fence &fence);
    bool end(fence *p_fence);
    bool end();

protected:

    VkCommandBuffer       m_command_buffer{ nullptr };
    vector_8<semaphore *> m_wait_semaphores;
    vector_8<semaphore *> m_signal_semaphores;
    const void *          mp_command_pool_handle{ nullptr };
    fence *               mp_fence{ nullptr };
};

class graphics_command_buffer : public icommand_buffer
{
public:

    void begin_renderpass(const render_target &target, const renderpass &pass, const glm::vec4 &clear_color=glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
    void end_renderpass();
    void draw(uint32_t vertex_count, uint32_t instance_count);
    void draw_indexed(uint32_t index_count);
    void bind_pipeline(const raster_pipeline &pipeline);
    void bind_vertex_buffer(const buffer &buf);
    void bind_index_buffer(const buffer &buf);
    void transition_image_layout(const image &img, VkImageLayout old_layout, VkImageLayout new_layout);
    void copy_buffer_to_image(const image &img, const buffer &buffer);
    void build_acceleration_structure(
	    const VkAccelerationStructureBuildGeometryInfoKHR &build_geometry_info, 
	    const VkAccelerationStructureBuildRangeInfoKHR &build_range_info);

private:
};

class compute_command_buffer : public icommand_buffer
{
public:

    void bind_pipeline(const compute_pipeline &pipeline);
    void bind_descriptor_set(const compute_pipeline &pipeline, const descriptor_set &set);
    void dispatch(uint32_t group_count_x, uint32_t group_count_y, uint32_t group_count_z);

private:
};

} // namespace vk
} // namespace stla
