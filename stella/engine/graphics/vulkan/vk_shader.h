#pragma once

#include "stla_shader_parser.h"

#include <vulkan/vulkan.h>

#include <stdint.h>
#include <vector>

#define STLA_DECLARE_SHADER_SRC(_src) "#version 460\n#extension GL_EXT_ray_query : require\n#extension GL_EXT_debug_printf : require\n" #_src

namespace stla
{
namespace vk
{
    
class state;

using spirv_byte_code = std::vector<uint32_t>;

class shader
{
public:

    //todo configure these base on hardware?
    static inline const uint32_t COMPUTE_WORKGROUP_WIDTH   = 16;
    static inline const uint32_t COMPUTE_WORKGROUP_HEIGHT  = 8;
    static inline const uint32_t COMPUTE_WORKGROUP_DEPTH   = 1;

    bool create(shader_type type, uint32_t src_len, const char *p_src, state &state);
    bool create(shader_type type, const shader_source_code &src, state &state);
    bool create(shader_type type, const spirv_byte_code &spirv, state &state);
    void destroy();
    shader_type get_type() const { return m_type; }
    bool is_valid() const { return m_shader_module != nullptr && mp_state != nullptr; }
    const VkShaderModule &get_handle() const { return m_shader_module; }

    static bool glsl_to_spirv(shader_type type, uint32_t src_len, const char *p_src, spirv_byte_code &spirv);
    static VkShaderStageFlagBits shader_type_to_vk_shader_bit(shader_type type);

private:

    VkShaderModule m_shader_module{ nullptr };
    shader_type    m_type{ };
    state *        mp_state{ nullptr };
};

} // namespace vk
} // namespace stla
