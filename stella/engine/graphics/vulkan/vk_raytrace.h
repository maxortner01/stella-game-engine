#pragma once

#include "vk_buffer.h"

#include "stla_unordered_map.h"
#include "stla_vector.h"

#include <vulkan/vulkan.h>
#include <glm/glm.hpp>

#include <stdint.h>

namespace stla
{

class vertex_buffer_layout; 

namespace vk
{
 
class state;
class buffer;

class blas
{
public:

    bool create_from_triangles(const buffer &vertex_buffer, const buffer &index_buffer, const vertex_buffer_layout &layout, state &state);
    bool create_from_aabbs(const buffer &aabb_buffer, uint32_t primitive_count, state &state, uint32_t primitive_offset=0);
    void destroy();
    VkDeviceAddress get_device_address() const;
    bool is_valid() const { return mp_state != nullptr && m_blas != nullptr && m_blas_buffer.is_valid(); }

private:

    bool create(const VkAccelerationStructureGeometryKHR &geometry, const VkAccelerationStructureBuildRangeInfoKHR &range_info, state &state);

    state *                    mp_state{ nullptr };
    VkAccelerationStructureKHR m_blas{ nullptr };
    buffer                     m_blas_buffer;
};

class tlas
{
public:

    static const inline int32_t invalid_instance_id = -1;
    using instance_id = int32_t;

    tlas() { 
        m_instance_in_use.resize(MAX_INSTANCES);
        m_vk_instances.resize(MAX_INSTANCES);
        clear();     
    }

    bool build(state &state);
    bool update();
    void destroy();
    bool set_instance_transform(instance_id id, const glm::mat4 &transform);
    instance_id push_instance(const blas &blas, const glm::mat4 &transform);
    const VkAccelerationStructureKHR &get_handle() const { return m_tlas_handle; }
    void clear();
    bool is_valid() const { return mp_state != nullptr && m_device_instance_buffer.is_valid() && m_tlas_buffer.is_valid() && m_tlas_handle != nullptr; }

private:
    static inline const uint32_t MAX_INSTANCES = 20000000;

    uint32_t                                                  m_instance_count{ 0 };
    bool                                                      m_needs_update{ false };
    vector<instance_id, MAX_INSTANCES>                        m_free_list;
    vector<bool, MAX_INSTANCES>                               m_instance_in_use;
    vector<VkAccelerationStructureInstanceKHR, MAX_INSTANCES> m_vk_instances;
    buffer                                                    m_device_instance_buffer;
    buffer                                                    m_scratch_buffer;
    buffer                                                    m_tlas_buffer;
    VkAccelerationStructureKHR                                m_tlas_handle{ nullptr };
    state *                                                   mp_state{ nullptr };
};

} // namespace vk
} // namespace stla
