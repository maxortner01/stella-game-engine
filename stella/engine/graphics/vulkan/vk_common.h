#pragma once

#include "stla_assert.h"

#include <vulkan/vulkan.h>

#include <stdio.h>
#include <stdlib.h>

#define VK_ASSERT(vk_result) \
    { \
        VkResult _res = (vk_result); \
        STLA_ASSERT(_res == VK_SUCCESS, "Vulkan error: %d", _res); \
    }

