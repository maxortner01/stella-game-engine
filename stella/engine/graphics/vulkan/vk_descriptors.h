#pragma once

#include "stla_vector.h"
#include "stla_unordered_map.h"
#include "stla_assert.h"

#include <vulkan/vulkan.h>

#include <stdint.h>

namespace stla
{
namespace vk
{

class state;
class buffer;
class tlas;

enum class descriptor_set_loc
{
    zero, one, two, three, max
};

struct descriptor_set_binding
{
    uint32_t           binding{ 0 };
    uint32_t           size{ 0 };
    VkDescriptorType   type{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER };
    VkBufferUsageFlags usage_flags{ VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT };
    VkShaderStageFlags shader_stage_flags{ VK_SHADER_STAGE_ALL };

    descriptor_set_binding() = default;
    descriptor_set_binding(const descriptor_set_binding &) = default;
    descriptor_set_binding(uint32_t size, uint32_t binding, VkDescriptorType type, VkShaderStageFlags shader_stage_flags);

    bool operator==(const descriptor_set_binding& rhs) {
        return binding == rhs.binding && 
               size == rhs.size && 
               type == rhs.type && 
               usage_flags == rhs.usage_flags &&
               shader_stage_flags == rhs.shader_stage_flags;
    }
};

static inline const uint32_t MAX_DESCRIPTOR_SETS         = ((uint32_t)descriptor_set_loc::three) + 1;
static inline const uint32_t MAX_DESCRIPTOR_SET_BINDINGS = 32;

using descriptor_set_binding_array = vector<descriptor_set_binding, MAX_DESCRIPTOR_SET_BINDINGS>;
using descriptor_set_binding_map   = unordered_map<uint32_t, descriptor_set_binding, MAX_DESCRIPTOR_SET_BINDINGS>;

class descriptor_set_layout
{
public:
    
    bool create(const descriptor_set_binding_array &bindings, state &state);
    bool create(uint32_t num_bindings, const descriptor_set_binding *p_bindings, state &state);
    void destroy();
    descriptor_set_binding get_binding(uint32_t binding) const;
    const VkDescriptorSetLayout &get_handle() const { return m_layout; }
    const descriptor_set_binding *begin() const { return m_bindings.begin(); }
    const descriptor_set_binding *end() const { return m_bindings.end(); }
    bool is_valid() const { return m_layout != nullptr && mp_state != nullptr; }

private:

    VkDescriptorSetLayout        m_layout{ nullptr };
    descriptor_set_binding_array m_bindings;
    state *                      mp_state{ nullptr };
};

class descriptor_set
{
public:

    descriptor_set() = default;
    descriptor_set(const descriptor_set &) = default;
    descriptor_set(VkDescriptorSet descriptor_set, state &state) : m_descriptor_set(descriptor_set), mp_state(&state) { }
    const VkDescriptorSet &get_handle() const { return m_descriptor_set; }
    bool is_valid() const { return m_descriptor_set != nullptr && mp_state != nullptr; }

    bool bind(const descriptor_set_binding &binding, const buffer &buffer);
    bool bind(const descriptor_set_binding &binding, const tlas &tlas);

private:

    state *         mp_state{ nullptr };
    VkDescriptorSet m_descriptor_set{ nullptr };
};

class descriptor_pool
{
public:

    bool create(uint32_t pool_sizes_length, const VkDescriptorPoolSize *p_pool_sizes, uint32_t max_sets, state &state);
    bool destroy();
    descriptor_set alloc_descriptor_set(const descriptor_set_layout &layout) const;
    bool is_valid() const { return m_pool != nullptr && mp_state != nullptr; }
    const VkDescriptorPool &get_handle() const { return m_pool; }

private:

    VkDescriptorPool m_pool{ nullptr };
    state *          mp_state{ nullptr }; 
};

class descriptor_set_layout_container
{
public:

    bool create(state &state);
    void destroy();
    bool create_layout(descriptor_set_loc loc, const descriptor_set_binding_array &bindings);
    const descriptor_set_layout &get_layout(descriptor_set_loc loc) const;
    bool has_layout(descriptor_set_loc loc) const;
    descriptor_set_binding get_binding(descriptor_set_loc loc, uint32_t binding) const { return get_layout(loc).get_binding(binding); }

private:

    unordered_map_4<descriptor_set_loc, descriptor_set_layout> m_layouts;
    state *mp_state{ nullptr };
};

} // namespace vk
} // namespace stla
