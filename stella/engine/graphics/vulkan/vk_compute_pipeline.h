#pragma once

#include "stla_vector.h"

#include <vulkan/vulkan.h>

namespace stla
{
namespace vk
{

class state;
class shader;
class descriptor_set_layout_container;

class compute_pipeline
{
public:

	compute_pipeline() = default;
	compute_pipeline(const compute_pipeline &) = default;

    bool create(const descriptor_set_layout_container &desc_set_layout_container, const shader &compute_shader, state &state);
	void destroy();
    bool is_valid() const { return m_pipeline != nullptr && m_pipeline_layout != nullptr && mp_state != nullptr; }
	const VkPipeline &get_handle() const { return m_pipeline; }
	const VkPipelineLayout &get_layout_handle() const { return m_pipeline_layout; }

private:

	VkPipeline       m_pipeline{ nullptr };
	VkPipelineLayout m_pipeline_layout{ nullptr };
	state *          mp_state{ nullptr };
};
    
} // namespace vk
} // namespace stla
