#include "vk_raster_pipeline.h"
#include "vk_common.h"
#include "vk_state.h"
#include "vk_renderpass.h"
#include "vk_descriptors.h"
#include "vk_shader.h"

#include "stla_logger.h"

#include <vector>
#include <etl/vector.h>

namespace stla
{
namespace vk
{

static VkFormat vertex_buffer_attribute_type_to_vk_format(vertex_buffer_attribute_type type)
{
	switch (type)
	{
		case vertex_buffer_attribute_type::f: return VK_FORMAT_R32_SFLOAT; 
		case vertex_buffer_attribute_type::v2: return VK_FORMAT_R32G32_SFLOAT; 
		case vertex_buffer_attribute_type::v3: return VK_FORMAT_R32G32B32_SFLOAT;
		case vertex_buffer_attribute_type::v4: return VK_FORMAT_R32G32B32A32_SFLOAT;
		//vertex_buffer_attribute_type::m4: return VK_FORMAT_R32_SFLOAT;
		default:
		{
			STLA_ASSERT_RELEASE(0, "vertex format not supported: %d", (uint32_t)type);
			return VK_FORMAT_R32_SFLOAT;
		} 
	}
}
    
void raster_pipeline::destroy()
{
	if (mp_state == nullptr) {
		return;
	}

	if (m_pipeline != nullptr) {
		mp_state->push_deletor([pipeline = m_pipeline](state &state) {
			vkDestroyPipeline(state.get_device_handle(), pipeline, nullptr);
		});
	}

	if (m_pipeline_layout != nullptr) {
		mp_state->push_deletor([pipeline_layout = m_pipeline_layout](state &state) {
			vkDestroyPipelineLayout(state.get_device_handle(), pipeline_layout, nullptr);
		});
	}

	m_pipeline        = nullptr;
	m_pipeline_layout = nullptr;
	mp_state          = nullptr;
}

// PIPELINE BUILDER //

raster_pipeline_builder::raster_pipeline_builder(state &state) :
	mp_state(&state)
{
	// Raster State Default
	m_rasterization_state.sType                   = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    m_rasterization_state.pNext                   = nullptr;
    m_rasterization_state.depthClampEnable        = VK_FALSE;
    m_rasterization_state.rasterizerDiscardEnable = VK_FALSE;
    m_rasterization_state.polygonMode             = VK_POLYGON_MODE_FILL;
    m_rasterization_state.lineWidth               = 1.0f;
    m_rasterization_state.cullMode                = VK_CULL_MODE_NONE;
    m_rasterization_state.frontFace               = VK_FRONT_FACE_CLOCKWISE;
    m_rasterization_state.depthBiasEnable         = VK_FALSE;
    m_rasterization_state.depthBiasConstantFactor = 0.0f;
    m_rasterization_state.depthBiasClamp          = 0.0f;
    m_rasterization_state.depthBiasSlopeFactor    = 0.0f;

	// Multisample State Default
    m_multisample_state.sType                 = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    m_multisample_state.pNext                 = nullptr;
    m_multisample_state.sampleShadingEnable   = VK_FALSE;
    m_multisample_state.rasterizationSamples  = VK_SAMPLE_COUNT_1_BIT;
    m_multisample_state.minSampleShading      = 1.0f;
    m_multisample_state.pSampleMask           = nullptr;
    m_multisample_state.alphaToCoverageEnable = VK_FALSE;
    m_multisample_state.alphaToOneEnable      = VK_FALSE;
	m_multisample_state.flags                 = 0;

	// Depth Stencil Default
	m_depth_stencil.sType                 = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    m_depth_stencil.pNext                 = nullptr;
    m_depth_stencil.depthTestEnable       = VK_TRUE;
    m_depth_stencil.depthWriteEnable      = VK_TRUE;
    m_depth_stencil.depthCompareOp        = VK_COMPARE_OP_ALWAYS;
    m_depth_stencil.depthBoundsTestEnable = VK_FALSE;
    m_depth_stencil.minDepthBounds        = 0.0f; // Optional
    m_depth_stencil.maxDepthBounds        = 1.0f; // Optional
    m_depth_stencil.stencilTestEnable     = VK_FALSE;
}

raster_pipeline_builder &raster_pipeline_builder::add_shader_stage(shader &stage)
{
	STLA_ASSERT_RELEASE(stage.is_valid(), "shader is invalid");
	m_shader_stages.push_back(stage);
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::set_vertex_buffer_layout(const vertex_buffer_layout &layout)
{
	m_vertex_buffer_layout = layout;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::set_primitive_topology(VkPrimitiveTopology topology)
{
	m_primitive_topology = topology;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::add_viewport(float x, float y, float width, float height, float min_depth, float max_depth)
{
	VkViewport viewport = { };
	viewport.x        = x;
	viewport.y        = y;
	viewport.width    = width;
	viewport.height   = height;
	viewport.minDepth = min_depth;
	viewport.maxDepth = max_depth;
	m_viewports.push_back(viewport);
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::add_scissor(uint32_t offset_x, uint32_t offset_y, uint32_t extent_width, uint32_t extent_height)
{
	VkRect2D scissor = { };
	scissor.offset.x      = offset_x;
	scissor.offset.y      = offset_y;
	scissor.extent.width  = extent_width;
	scissor.extent.height = extent_height;
	m_scissors.push_back(scissor);
	return *this;
}


raster_pipeline_builder &raster_pipeline_builder::enable_depth_clamp(bool enable)
{
	m_rasterization_state.depthClampEnable = enable;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::enable_rasterizer_discard(bool enable)
{
	m_rasterization_state.rasterizerDiscardEnable = enable;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::set_polygon_mode(VkPolygonMode mode)
{
	m_rasterization_state.polygonMode = mode;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::set_cull_flags(VkCullModeFlags flags)
{
	m_rasterization_state.cullMode |= flags;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::set_front_face(VkFrontFace front)
{
	m_rasterization_state.frontFace = front;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::configure_depth_bias(bool enable, float constant_factor, float clamp, float slope_factor)
{
	m_rasterization_state.depthBiasEnable         = enable;
	m_rasterization_state.depthBiasConstantFactor = constant_factor;
	m_rasterization_state.depthBiasClamp          = clamp;
	m_rasterization_state.depthBiasSlopeFactor    = slope_factor;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::set_line_width(float width)
{
	m_rasterization_state.lineWidth = width;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::set_sample_count_flag(VkSampleCountFlagBits flag)
{
	m_multisample_state.rasterizationSamples = flag;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::enable_sample_shading(bool enable)
{
	m_multisample_state.sampleShadingEnable = enable;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::set_min_sample_shading(float min)
{
	m_multisample_state.minSampleShading = min;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::set_sample_mask(VkSampleMask mask)
{
	m_sample_mask = mask;
	m_multisample_state.pSampleMask = &m_sample_mask;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::enable_alpha_to_coverage(bool enable)
{
	m_multisample_state.alphaToCoverageEnable = enable;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::enable_alpha_to_one(bool enable)
{
	m_multisample_state.alphaToOneEnable = enable;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::enable_depth_test(bool enable)
{
	m_depth_stencil.depthTestEnable = enable;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::enable_depth_write(bool enable)
{
	m_depth_stencil.depthWriteEnable = enable;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::set_depth_compare_op(VkCompareOp op)
{
	m_depth_stencil.depthCompareOp = op;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::set_depth_bounds(float min, float max)
{
	m_depth_stencil.minDepthBounds = min;
	m_depth_stencil.maxDepthBounds = max;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::enable_stencil_test(bool enable)
{
	m_depth_stencil.stencilTestEnable = enable;
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::add_color_blend_attachment(
	bool enable, VkBlendFactor src_color_factor, VkBlendFactor dest_color_factor, 
	VkBlendOp color_blend_op, VkBlendFactor src_alpha_factor, VkBlendFactor dest_alpha_factor,
	VkBlendOp alpha_blend_op, VkColorComponentFlags color_write_mask)
{
	VkPipelineColorBlendAttachmentState attachment = { };
	attachment.blendEnable         = enable;
	attachment.srcColorBlendFactor = src_color_factor;
	attachment.dstColorBlendFactor = dest_color_factor;
	attachment.colorBlendOp        = color_blend_op;
	attachment.srcAlphaBlendFactor = src_alpha_factor;
	attachment.dstAlphaBlendFactor = dest_alpha_factor;
	attachment.alphaBlendOp        = alpha_blend_op;
	attachment.colorWriteMask      = color_write_mask;
	m_color_blend_attachments.push_back(attachment);
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::add_dynamic_state(VkDynamicState state)
{
	m_dynamic_state.push_back(state);
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::add_descriptor_set_layout(VkDescriptorSetLayout layout)
{
	if (layout != nullptr) {
		m_descriptor_set_layouts.push_back(layout);
	}
	return *this;
}

raster_pipeline_builder &raster_pipeline_builder::set_renderpass(VkRenderPass renderpass)
{
	m_renderpass = renderpass;
	return *this;
}

bool raster_pipeline_builder::build(raster_pipeline& new_pipeline)
{
	static const uint32_t max_array_size = 12;

	if (mp_state == nullptr) {
		return false;
	}

	if (m_renderpass == nullptr) {
		return false;
	}

	STLA_CORE_LOG_INFO("Building Raster Pipeline");

	VkPipeline       pipeline = nullptr;
	VkPipelineLayout pipeline_layout = nullptr;

	// ---- Pipeline Layout ---- //
	VkPipelineLayoutCreateInfo pipeline_layout_info = { };
	pipeline_layout_info.sType          = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipeline_layout_info.setLayoutCount = m_descriptor_set_layouts.size();
	pipeline_layout_info.pSetLayouts    = m_descriptor_set_layouts.data();
	
	if ((pipeline_layout = mp_state->vk_create_pipeline_layout(pipeline_layout_info)) == nullptr) {
		return false;
	}

	// ---- Shader Stages ---- //
	STLA_CORE_LOG_INFO(" Shader Stages");
	etl::vector<VkPipelineShaderStageCreateInfo, max_array_size> shader_stages;
	for (auto& s : m_shader_stages) {
		if (!shader_stages.available()) {
			//todo destroy pipeline layout
			return false;
		}

		VkPipelineShaderStageCreateInfo shader_stage_create_info = { };
		shader_stage_create_info.sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		shader_stage_create_info.pNext  = nullptr;
		shader_stage_create_info.stage  = shader::shader_type_to_vk_shader_bit(s.get_type());
		shader_stage_create_info.module = s.get_handle();
		shader_stage_create_info.pName  = "main";
		shader_stages.push_back(shader_stage_create_info);
	
		STLA_CORE_LOG_INFO("  (handle={0:#x}) (stage={1})", (uint64_t)s.get_handle(), (uint32_t)s.get_type());
	}

	// ---- Vertex Input ---- //
	etl::vector<VkVertexInputBindingDescription, max_array_size>   vertex_bindings;
	etl::vector<VkVertexInputAttributeDescription, max_array_size> vertex_attributes;

	//todo only supports one interleaved vertex buffer
	{
		VkVertexInputBindingDescription vertex_binding = { };
		vertex_binding.binding   = 0;
		vertex_binding.stride    = m_vertex_buffer_layout.get_stride();
		vertex_binding.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
		vertex_bindings.push_back(vertex_binding);
	}

	STLA_CORE_LOG_INFO(" Vertex Input (stride={0})", m_vertex_buffer_layout.get_stride());
	uint32_t offset = 0;
	for (vertex_buffer_attribute &attrib : m_vertex_buffer_layout.get_attribute_array()) {
		VkVertexInputAttributeDescription input_attribute = { };
		input_attribute.binding  = 0;
		input_attribute.location = attrib.m_index;
		input_attribute.format   = vertex_buffer_attribute_type_to_vk_format(attrib.m_type); //VK_FORMAT_R32G32B32_SFLOAT; //todo get format from attrib
		input_attribute.offset   = offset;
		vertex_attributes.push_back(input_attribute);

		STLA_CORE_LOG_INFO("  ({0}) (loc={1}) (format={2}) (offset={3})", 
			input_attribute.binding, input_attribute.location, input_attribute.format, input_attribute.offset);		

		offset += attrib.get_size();
	}

	VkPipelineVertexInputStateCreateInfo vertex_input_create_info = { };
	vertex_input_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertex_input_create_info.vertexBindingDescriptionCount   = vertex_bindings.size();
	vertex_input_create_info.pVertexBindingDescriptions      = vertex_bindings.data();
	vertex_input_create_info.vertexAttributeDescriptionCount = vertex_attributes.size();
	vertex_input_create_info.pVertexAttributeDescriptions    = vertex_attributes.data();

	// ---- Input Assembly ---- //
	VkPipelineInputAssemblyStateCreateInfo input_assembly_create_info = { };
	input_assembly_create_info.sType    = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    input_assembly_create_info.topology = m_primitive_topology;

	// ---- Viewport ---- //
    VkPipelineViewportStateCreateInfo viewport_create_info = { };
	viewport_create_info.sType         = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewport_create_info.viewportCount = m_viewports.size();
	viewport_create_info.pViewports    = m_viewports.size() == 0 ? nullptr : m_viewports.data();
	viewport_create_info.scissorCount  = m_scissors.size();
	viewport_create_info.pScissors     = m_scissors.size() == 0 ? nullptr : m_scissors.data();

	// ---- Rasterization ---- //
	VkPipelineRasterizationStateCreateInfo raster_create_info = m_rasterization_state;

	// ---- Multisample ---- //
	VkPipelineMultisampleStateCreateInfo multisample_create_info = m_multisample_state;

	// ---- Depth Stencil ---- //
	VkPipelineDepthStencilStateCreateInfo depth_stencil_create_info = m_depth_stencil;

	// ---- Color Blend ---- //
	VkPipelineColorBlendStateCreateInfo color_blend_create_info = { };
	color_blend_create_info.sType             = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	color_blend_create_info.logicOpEnable     = VK_FALSE;
	color_blend_create_info.logicOp           = VK_LOGIC_OP_COPY;
	color_blend_create_info.attachmentCount   = m_color_blend_attachments.size();
	color_blend_create_info.pAttachments      = m_color_blend_attachments.size() == 0 ? nullptr : m_color_blend_attachments.data();
	color_blend_create_info.blendConstants[0] = 0.0f;
	color_blend_create_info.blendConstants[1] = 0.0f;
	color_blend_create_info.blendConstants[2] = 0.0f;
	color_blend_create_info.blendConstants[3] = 0.0f;

	// ---- Dynamic State ---- //
	VkPipelineDynamicStateCreateInfo dynamic_create_info = { };
	dynamic_create_info.sType             = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamic_create_info.dynamicStateCount = m_dynamic_state.size();
	dynamic_create_info.pDynamicStates    = m_dynamic_state.size() == 0 ? nullptr : m_dynamic_state.data();

	// ---- Build Pipeline ---- //
	VkGraphicsPipelineCreateInfo pipeline_info = { };
	pipeline_info.sType               = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipeline_info.pNext               = nullptr;
	pipeline_info.stageCount          = shader_stages.size();
	pipeline_info.pStages             = shader_stages.size() == 0 ? nullptr : shader_stages.data();;
	pipeline_info.pVertexInputState   = &vertex_input_create_info;
	pipeline_info.pInputAssemblyState = &input_assembly_create_info;
	pipeline_info.pViewportState      = &viewport_create_info;
	pipeline_info.pRasterizationState = &raster_create_info;
	pipeline_info.pMultisampleState   = &multisample_create_info;
	pipeline_info.pColorBlendState    = &color_blend_create_info;
	pipeline_info.pDepthStencilState  = &depth_stencil_create_info;
	pipeline_info.layout              = pipeline_layout;
	pipeline_info.renderPass          = m_renderpass;
	pipeline_info.subpass             = 0;
	pipeline_info.basePipelineHandle  = VK_NULL_HANDLE;

	if ((pipeline = mp_state->vk_create_pipline(pipeline_info)) == nullptr) {
		return false;
	}

	new_pipeline.destroy();
	new_pipeline = raster_pipeline(pipeline, pipeline_layout, m_vertex_buffer_layout, mp_state);
	return true;
}

} // namespace vk
} // namespace stla
