#pragma once

#include "stla_utils.h"

#include <stdio.h>
#include <assert.h>

//todo remove body in release build
#define STLA_ASSERT(condition, message, ...)                  \
    do                                                       \
    {                                                        \
        if (!(condition))                                    \
        {                                                    \
            printf("[STLA ASSERT] File: %s Line: %d Function: %s -- " message "\n",   \
               __FILENAME__,                                 \
               __LINE__,                                     \
               __FUNCTION__,                                 \
               ##__VA_ARGS__);                               \
            assert(0);                                       \
        }                                                    \
    } while(0);

#define STLA_ASSERT_RELEASE(condition, message, ...)          \
    do                                                       \
    {                                                        \
        if (!(condition))                                    \
        {                                                    \
            printf("[STLA ASSERT] File: %s Line: %d Function: %s -- " message "\n",   \
               __FILENAME__,                                 \
               __LINE__,                                     \
               __FUNCTION__,                                 \
               ##__VA_ARGS__);                               \
            assert(0);                                       \
        }                                                    \
    } while(0);
