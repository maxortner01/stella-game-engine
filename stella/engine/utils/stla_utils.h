#pragma once

#include <string.h>
#include <stdint.h>

#ifdef _WIN32
#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#else
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif

#ifdef __GNUC__
#define STLA_PACK( __Declaration__ ) __Declaration__ __attribute__((__packed__))
#endif

#ifdef _MSC_VER
#define STLA_PACK( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop))
#endif

#define ONE_GB (1000000000)
#define ONE_MB (1000000)
#define ONE_KB (1000)

#define STLA_ARRAYSIZE(_ARR) ((int)(sizeof(_ARR) / sizeof(*(_ARR))))     // Size of a static C-style array. Don't use on pointers!

namespace stla
{
    static inline uint32_t flatten(uint32_t x, uint32_t y, uint32_t z, uint32_t w, uint32_t d) { return d * (y * w + x) + z; }
}