#pragma once

#include "stla_ieditor_panel.h"

#include <stella.h>
#include <etl/delegate.h>
#include <etl/singleton.h>
#include <taskflow/taskflow.hpp>

#include <stdint.h>

#include <typeindex>

namespace stla
{
    
class editor
{
public:

    editor();
    ~editor();
    void run();

    template<typename T>
    editor &register_panel(bool show);

    vk::state &get_vulkan_state() { return m_vulkan_state; }
    voxel_renderer &get_voxel_renderer() { return m_voxel_renderer; }

private:

    void on_update(float dt);
    void on_render();
    void on_render_imgui();

    void on_render_main_menu();
    
private:

    bool           m_running{ false };
    SDL_Window *   mp_window{ nullptr };
    delta_time     m_dt;
    vk::state      m_vulkan_state;
    imgui_context  m_imgui;
    tf::Executor   m_executor;
    voxel_renderer m_voxel_renderer;

    using create_panel_callback = etl::delegate<ieditor_panel *()>;

    struct panel_record
    {
        bool                  m_show{ false };
        const char *          mp_name{ nullptr };
        create_panel_callback m_create_panel_callback;
        ieditor_panel *       mp_panel{ nullptr };
    };

    using panel_type_hash_code = size_t;
    unordered_map_32<panel_type_hash_code, panel_record> m_panel_table;
};

template<typename T>
editor &editor::register_panel(bool show)
{
    STLA_ASSERT_RELEASE(m_panel_table.available(), "panel table full");
    STLA_ASSERT_RELEASE((std::is_base_of<ieditor_panel, T>::value), "Editor Panel must be derived from ieditor_panel");
    
    panel_type_hash_code id = typeid(T).hash_code();
    STLA_ASSERT_RELEASE(m_panel_table.find(id) == m_panel_table.end(), "panel type already registered");

    m_panel_table[id] = { 
        show, 
        T::get_name(), 
        []() { return (ieditor_panel *)(new T()); }, 
        nullptr 
    };
    return *this;
}

class application : public etl::singleton<editor>
{
public:

    static vk::state &vulkan_state() { return instance().get_vulkan_state(); }
    static voxel_renderer &voxel_renderer() { return instance().get_voxel_renderer(); }
    
    // Default render objects
    static vk::renderpass    create_default_renderpass();
    static vk::render_target create_default_render_target(uint32_t width, uint32_t height, vk::renderpass &renderpass);
};

} // namespace stla
