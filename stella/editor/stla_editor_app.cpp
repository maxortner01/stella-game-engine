#include <stla_editor_app.h>

#include <stella.h>
#include <SDL3/SDL_vulkan.h>
#include <etl/string.h>
#include <etl/to_string.h>

#define FONT_REGULAR_PATH ASSETS_DIR "/fonts/Montserrat/static/Montserrat-Regular.ttf"
#define FONT_BOLD_PATH    ASSETS_DIR "/fonts/Montserrat/static/Montserrat-Regular.ttf"

namespace stla
{

editor::editor()
{
    stla::logger::init();

    // SDL/Window
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        STLA_ASSERT_RELEASE(0, "Unable to initialize SDL");
    }

    SDL_SetHint(SDL_HINT_IME_SHOW_UI, "1");

    uint32_t window_flags = (
        SDL_WINDOW_RESIZABLE |
        SDL_WINDOW_VULKAN |
        SDL_WINDOW_HIGH_PIXEL_DENSITY
    );

    const SDL_DisplayMode *p_display_mode = SDL_GetDesktopDisplayMode(1);
    STLA_ASSERT_RELEASE(p_display_mode != nullptr, "unable to get SDL display mode: %s", SDL_GetError());

    float window_aspect_ratio = 16.0f / 9.0f;
    float window_scale        = 0.75f;
    uint32_t window_width     = p_display_mode->w * window_scale;
    mp_window = SDL_CreateWindow("Stella Editor", window_width, (int)(window_width / window_aspect_ratio), window_flags);

    if (mp_window == nullptr) {
        STLA_ASSERT_RELEASE(0, "Unable to initialize SDL Window: %s", SDL_GetError());
    }

    SDL_GL_SetSwapInterval(1);

    // Vulkan Renderer
    stla::vk::state::create_info create_renderer = {
        "Stella Vulkan Application",
        [&](VkInstance instance, VkSurfaceKHR* p_surface) {
            if (p_surface == nullptr) {
                return false;
            }

            return (bool)SDL_Vulkan_CreateSurface(mp_window, instance, p_surface);
        }
    };

    if (!m_vulkan_state.init(create_renderer)) {
        STLA_ASSERT_RELEASE(0, "Unable to initialize Stella Vulkan Engine");
    }

    STLA_ASSERT_RELEASE(m_voxel_renderer.create(m_vulkan_state, glm::uvec2(1600, 900)), "unable to create voxel_renderer");

    // ImGui
    if (!m_imgui.init(mp_window, &m_vulkan_state)) {
        STLA_ASSERT_RELEASE(0, "Unable to initialize ImGui");
    }

    m_imgui.set_font(FONT_REGULAR_PATH, (window_width / window_aspect_ratio) * 0.02f);

    // Log Engine Settings
    VkPhysicalDeviceProperties engine_props = m_vulkan_state.get_physical_device_properties();
    STLA_CORE_LOG_INFO("Stella Editor Initialized");
    STLA_CORE_LOG_INFO("  API Version");
    STLA_CORE_LOG_INFO("   Major         : {0}", VK_API_VERSION_MAJOR(engine_props.apiVersion));
    STLA_CORE_LOG_INFO("   Minor         : {0}", VK_API_VERSION_MINOR(engine_props.apiVersion));
    STLA_CORE_LOG_INFO("   Variant       : {0}", VK_API_VERSION_VARIANT(engine_props.apiVersion));
    STLA_CORE_LOG_INFO("   Patch         : {0}", VK_API_VERSION_PATCH(engine_props.apiVersion));
    STLA_CORE_LOG_INFO("  Driver Version : {0:#x}", engine_props.driverVersion);
    STLA_CORE_LOG_INFO("  Vendor ID      : {0:#x}", engine_props.vendorID);
    STLA_CORE_LOG_INFO("  Device ID      : {0:#x}", engine_props.deviceID);
    STLA_CORE_LOG_INFO("  Device Type    : {0:#x}", engine_props.deviceType);
    STLA_CORE_LOG_INFO("  Device Name    : {0}\n", engine_props.deviceName);
}

editor::~editor()
{
    if (mp_window) {
        m_vulkan_state.queue_wait_idle(vk::queue_type::graphics);

        // Delete panels
        for (auto &[key, record] : m_panel_table) {
            if (record.mp_panel != nullptr) {
                delete record.mp_panel;
            }
        }

        m_imgui.shutdown();
        m_voxel_renderer.destroy();
        m_vulkan_state.shutdown();

        SDL_DestroyWindow(mp_window);
        SDL_Quit();
        mp_window = nullptr;
    }
}

void editor::run()
{
    m_running = true;

    while (m_running) {
        
        m_dt.tick();
        etl::string<128> tick_string;
        etl::to_string(1000.0f / (float)m_dt, tick_string);
        SDL_SetWindowTitle(mp_window, tick_string.c_str());
        
    // Update
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            m_imgui.process_event(event);
            switch (event.type) 
            {
                case SDL_EVENT_QUIT: { m_running = false; break; }
            }
        }

        on_update(m_dt);

        m_imgui.on_update();

        on_render();

        m_imgui.start_frame();
        on_render_imgui();
        m_imgui.end_frame();

        m_vulkan_state.flush_deletor_queue();
    }   
}

void editor::on_update(float dt)
{
    for (auto &[key, record] : m_panel_table) {
        if (record.m_show && record.mp_panel == nullptr) {
            record.mp_panel = record.m_create_panel_callback();
        }
    }

    tf::Taskflow task_flow;
    for (auto &[key, record] : m_panel_table) {
        if (record.mp_panel) {
            task_flow.emplace([dt = dt, p_panel = record.mp_panel]() { 
                p_panel->on_update(dt);
            });
        }
    }

    m_executor.run(task_flow).wait();
}

void editor::on_render()
{
    thread_safe_queue_32<vk::graphics_command_buffer> cmd_buffers;

    //https://taskflow.github.io/taskflow/index.html
    tf::Taskflow task_flow;
    for (auto &[key, record] : m_panel_table) {
        if (record.mp_panel) {
            task_flow.emplace([
                p_cmd_queue = &cmd_buffers,
                p_panel = record.mp_panel
            ]() { 
                vk::graphics_command_buffer cmd_buffer = p_panel->on_render();
                if (cmd_buffer.is_valid()) {
                    p_cmd_queue->push(cmd_buffer);
                }
            });
        }
    }

    m_executor.run(task_flow).wait();
    
    vk::graphics_command_buffer cmd_buffer;
    while (cmd_buffers.pop(cmd_buffer)) {
        m_vulkan_state.submit_command_buffer(cmd_buffer);
    }

    m_vulkan_state.queue_wait_idle(vk::queue_type::graphics);
}

void editor::on_render_imgui()
{
    on_render_main_menu();

    for (auto &[key, panel_entry] : m_panel_table) {
        if (panel_entry.mp_panel) {
            panel_entry.mp_panel->on_render_imgui();
        }
    }
}

void editor::on_render_main_menu()
{
    if (ImGui::BeginMainMenuBar()) {
        if (ImGui::BeginMenu("File")) {
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("View")) {

            //for (int i = 0; i < ARNET_MAX_PANELS; i++) {
            for (auto &[key, panel_entry] : m_panel_table) {

                if (ImGui::Checkbox(panel_entry.mp_name, &panel_entry.m_show)) {
                    if (panel_entry.mp_panel != nullptr) {
                        delete panel_entry.mp_panel;
                        panel_entry.mp_panel = nullptr;
                    }
                }
            }

            ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }
}

vk::renderpass application::create_default_renderpass()
{
    vk::renderpass renderpass;
    
    application::vulkan_state().create_renderpass_builder()
        .add_attachment(
            vk::renderpass_builder::attachment_type::color, VK_FORMAT_R32G32B32A32_SFLOAT, VK_SAMPLE_COUNT_1_BIT, 
            VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE,
            VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
        .add_attachment
            (vk::renderpass_builder::attachment_type::depth, VK_FORMAT_D32_SFLOAT, VK_SAMPLE_COUNT_1_BIT,
            VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_DONT_CARE,
            VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
        .build(renderpass);

    return renderpass;
}

vk::render_target application::create_default_render_target(uint32_t width, uint32_t height, vk::renderpass &renderpass)
{
    vk::render_target target;

    application::vulkan_state().create_render_target_builder(width, height, renderpass)
        .add_attachment(
            VK_FORMAT_R32G32B32A32_SFLOAT, 
            VK_IMAGE_TILING_OPTIMAL, 
            VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_STORAGE_BIT, 
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            VK_IMAGE_ASPECT_COLOR_BIT,
            VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BIT
        )
        .add_attachment(
            VK_FORMAT_D32_SFLOAT,
            VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
            VK_IMAGE_ASPECT_DEPTH_BIT,
            VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
        )
        .build(target);

    return target;
}

} // namespace stla
