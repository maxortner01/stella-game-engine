#pragma once

#include <stella.h>

#include <glm/glm.hpp>
#include <etl/string.h>

namespace stla
{

struct uniform_data
{
    glm::mat4 m_view_projection;
    glm::mat4 m_transform;
};

struct uniform_data_1
{
    float m_red;
};

class editor;

class ieditor_panel
{
public:

    ieditor_panel() { }
    virtual ~ieditor_panel() = default;
    virtual void on_update(float dt) = 0;
    virtual vk::graphics_command_buffer on_render() = 0;
    virtual void on_render_imgui() = 0;
};

} // namespace stla
