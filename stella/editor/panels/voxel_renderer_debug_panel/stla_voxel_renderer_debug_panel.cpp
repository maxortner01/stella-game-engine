#pragma once

#include "stla_editor_app.h"
#include "stla_voxel_renderer_debug_panel.h"

#include <stella.h>

#include <string.h>

namespace stla
{

voxel_renderer_debug_panel::voxel_renderer_debug_panel()
{

}

voxel_renderer_debug_panel::~voxel_renderer_debug_panel()
{

}

void voxel_renderer_debug_panel::on_update(float dt)
{
    m_last_dt = dt;
}

vk::graphics_command_buffer voxel_renderer_debug_panel::on_render()
{
    return vk::graphics_command_buffer();
}

void voxel_renderer_debug_panel::on_render_imgui()
{
    voxel_renderer &renderer = application::voxel_renderer();

    ImGui::Begin("Voxel Renderer Debug Panel");

    // ImGui::ShowDemoWindow();

    if (ImGui::CollapsingHeader("Scene Data")) {
        ImGui::Indent();
        ImGui::InputFloat3("Camera Position", &renderer.m_scene_data.camera_position[0]);
        ImGui::InputFloat4("Debug Background Color", &renderer.m_scene_data.debug_background_color[0]);
        ImGui::Text("Resolution (%d %d)", renderer.m_scene_data.resolution.x, renderer.m_scene_data.resolution.y);
        ImGui::Combo("Render Type", (int *)&renderer.m_scene_data.render_type, voxel_renderer::s_render_type_string, IM_ARRAYSIZE(voxel_renderer::s_render_type_string));
        ImGui::InputInt("Samples", &renderer.m_scene_data.samples);
        ImGui::InputInt("Max Ray Depth", &renderer.m_scene_data.max_ray_depth);
        ImGui::InputFloat("T Max", &renderer.m_scene_data.t_max, 100.0f);
        ImGui::InputFloat("T Min", &renderer.m_scene_data.t_min, 0.01f);
        ImGui::InputInt("Debug Grid Id", &renderer.m_scene_data.grid_id);
        ImGui::InputFloat("Color Divisor", &renderer.m_scene_data.color_divisor);
        if (ImGui::TreeNode("Animate")) {
            ImGui::Indent();
            static bool do_animation = true;
            static int num_frames    = 8;
            static int start_frame   = 0;
            static int ms_per_frame  = 175;
            static int current_frame = 0;
            static int current_frame_time = 0;

            ImGui::InputInt("Num Frames", &num_frames);
            ImGui::InputInt("Ms Per Frame", &ms_per_frame);
            ImGui::InputInt("Start Frame", &start_frame);
            ImGui::Text("Current Frame: %d", current_frame);

            current_frame_time += m_last_dt; 
            if (current_frame_time >= ms_per_frame) {
                current_frame = ((current_frame + 1) % num_frames) + start_frame;
                current_frame_time = 0;
            }

            renderer.m_scene_data.grid_id = current_frame;

            ImGui::Unindent();
            ImGui::TreePop();
        }
        ImGui::Unindent();
    }
    
    if (ImGui::CollapsingHeader("Device Buffers")) {
        ImGui::Indent();
        if (ImGui::BeginTable("Device Buffer Sizes Table", 2)) {
            struct {
                const char *p_col0_text;
                const char *p_col1_fmt;
                float size;
            } row_data[] = {
                { "Scene Data",                 "%.0f bytes", (float)renderer.m_scene_data_device.get_size() },
                { "Pixel Buffer",               "%.3f (Mb)",  (float)renderer.m_pixel_buffer_device.get_size() / ONE_MB },
                { "Structure Grid Descriptors", "%.3f (Kb)",  (float)renderer.m_voxel_structure_grid_descriptors_device.get_size() / ONE_KB },
                { "Structure Data",             "%.3f (Mb)",  (float)renderer.m_voxel_structure_data_device.get_size() / ONE_MB },
                { "Material Data",              "%.3f (Mb)",  (float)renderer.m_voxel_material_data_device.get_size() / ONE_MB },
                { "AABB Buffer",                "%.3f (Kb)",  (float)renderer.m_aabb_buffer_device.get_size() / ONE_KB }
            };

            for (auto row : row_data) {
                ImGui::TableNextRow();
                ImGui::TableSetColumnIndex(0);
                ImGui::Text(row.p_col0_text);
                ImGui::TableSetColumnIndex(1);
                ImGui::Text(row.p_col1_fmt, row.size);
            }

            ImGui::EndTable();
        }
        ImGui::Unindent();
    }

    // Voxel Structure Descriptors
    if (ImGui::CollapsingHeader("Grid Structure Descriptors")) {
        ImGui::Indent();

        ImGui::Text("Next allocation offset : %d", renderer.m_next_structure_allocation_offset);
        ImGui::Text("Max Descriptors: %d", renderer.m_voxel_structure_grid_descriptors.size());

        static int display_count = 10;
        static bool show_active_descriptors = false;
        ImGui::InputInt("Show desc number", &display_count);
        ImGui::Checkbox("Show active descriptors", &show_active_descriptors);

        uint32_t descriptors_displayed = 0;
        uint32_t descriptor_num = 0;
        for (auto descriptor : renderer.m_voxel_structure_grid_descriptors) {
            descriptor_num++;

            if (descriptors_displayed >= display_count) {
                break;
            }

            if (show_active_descriptors && !descriptor.in_use) {
                break;
            }

            char header_text[128] = { 0 };
            snprintf(&header_text[0], sizeof(header_text), "descriptor (%d) (in_use=%s)", descriptor_num - 1, descriptor.in_use ? "true" : "false");
            if (ImGui::TreeNode(header_text)) {
                ImGui::Indent();
                ImGui::Text("Dimensions (%d %d %d)", descriptor.dimensions.x, descriptor.dimensions.y, descriptor.dimensions.z);
                ImGui::Text("Padding (%d %d %d)", voxel_renderer::VOXEL_DIM_PADDING, voxel_renderer::VOXEL_DIM_PADDING, voxel_renderer::VOXEL_DIM_PADDING);
                ImGui::Text("Structure Offset (%d)", descriptor.structure_offset);
                ImGui::Text("Material Offset (%d)", descriptor.material_offset);
                ImGui::Text("Voxel Size (%f)", descriptor.voxel_size);   
                ImGui::Text("BLAS (%d)", descriptor.blas_id);
                ImGui::Unindent();
                ImGui::TreePop();
            }

            descriptors_displayed++;        
        }
        ImGui::Unindent();
    }

    // AABBs
    if (ImGui::CollapsingHeader("AABBs")) {
        ImGui::Indent();

        static int aabb_max_display_count = 10;
        ImGui::InputInt("Show aabb number", &aabb_max_display_count);

        uint32_t aabb_num        = 0;
        uint32_t aabbs_displayed = 0;
        for (auto &aabb : renderer.m_aabbs) {
            aabb_num++;

            if (aabbs_displayed >= aabb_max_display_count) {
                break;
            }

            char aabb_header_text[128] = { 0 };
            snprintf(&aabb_header_text[0], sizeof(aabb_header_text), "aabb (%d)", aabb_num - 1);
            if (ImGui::TreeNode(aabb_header_text)) {
                ImGui::Indent();
                ImGui::Text("Min (%f %f %f)", aabb.min_x, aabb.min_y, aabb.min_z);
                ImGui::Text("Max (%f %f %f)", aabb.max_x, aabb.max_y, aabb.max_z);
                ImGui::Unindent();
                ImGui::TreePop();
            }

            aabbs_displayed++;
        }

        ImGui::Unindent();
    }

    // Dump Device Buffer
    if (ImGui::CollapsingHeader("Dump Device Buffer")) {
        ImGui::Indent();

        static int buffer_offset      = 0;
        static int selected_buffer    = 0;
        static int element_count      = 16;
        static int selected_data_type = 0;
        static const char * buffer_options[] = {
            "m_scene_data_device",
            "m_pixel_buffer_device",
            "m_voxel_structure_grid_descriptors_device",
            "m_voxel_structure_data_device",
            "m_aabb_buffer_device"
        };
        static const char * data_types[] = {
            "uint8_t", "uint16_t", "uint32_t", "float"
        };

        ImGui::Combo("Device Buffer", &selected_buffer, &buffer_options[0], IM_ARRAYSIZE(buffer_options));
        ImGui::Combo("Data Type", &selected_data_type, &data_types[0], IM_ARRAYSIZE(data_types));
        ImGui::InputInt("Offset", &buffer_offset);
        ImGui::InputInt("Element Count", &element_count);

        vk::buffer *p_buffer   = nullptr;
        uint8_t *p_mapped_ptr  = nullptr;
        uint8_t data_type_size = 0;

        switch (selected_data_type)
        {
            case 0: data_type_size = sizeof(uint8_t);  break;
            case 1: data_type_size = sizeof(uint16_t); break;
            case 2: data_type_size = sizeof(uint32_t); break;
            case 3: data_type_size = sizeof(float);    break;
        }

        switch (selected_buffer)
        {  
            case 0: p_buffer = &renderer.m_scene_data_device; break; 
            case 1: p_buffer = &renderer.m_pixel_buffer_device; break;
            case 2: p_buffer = &renderer.m_voxel_structure_grid_descriptors_device; break;
            case 3: p_buffer = &renderer.m_voxel_structure_data_device; break;
            case 4: p_buffer = &renderer.m_aabb_buffer_device; break;
        }

        if (p_buffer) {
            if (!p_buffer->map((void **)&p_mapped_ptr)) {
                STLA_CORE_LOG_WARN("Unable to map device buffer pointer");
            }
            else {
                ImGui::Text("Buffer Size = %d", p_buffer->get_size());

                if (ImGui::BeginTable("Device Memory", 2)) {
                    for (int i = 0; i < element_count; i++) {
                        uint32_t element_position = buffer_offset + i;
                        if ((element_position * data_type_size) >= p_buffer->get_size()) {
                            STLA_CORE_LOG_WARN("Byte position ({0}) out of bounds", element_position);
                            break;
                        }

                        ImGui::TableNextRow();
                        ImGui::TableSetColumnIndex(0);
                        ImGui::Text("%d", element_position);
                        ImGui::TableSetColumnIndex(1);
                        switch (selected_data_type)
                        {
                            case 0: ImGui::Text("0x%08X (%d)", p_mapped_ptr[element_position], p_mapped_ptr[element_position]); break;
                            case 1: ImGui::Text("0x%08X (%d)", ((uint16_t *)p_mapped_ptr)[element_position], ((uint16_t *)p_mapped_ptr)[element_position]); break;
                            case 2: ImGui::Text("0x%08X (%d)", ((uint32_t *)p_mapped_ptr)[element_position], ((uint32_t *)p_mapped_ptr)[element_position]); break;
                            case 3: ImGui::Text("0x%08X (%f)", ((uint32_t *)p_mapped_ptr)[element_position], ((float *)p_mapped_ptr)[element_position]); break;
                        }
                    }

                    ImGui::EndTable();
                }
                p_buffer->unmap();
            }   
        }

        ImGui::Unindent();
    }

    ImGui::End();
}
    
} // namespace stla
