#pragma once

#include "stla_ieditor_panel.h"

namespace stla
{

class voxel_renderer_debug_panel : public ieditor_panel
{
public:
 
    voxel_renderer_debug_panel();
    ~voxel_renderer_debug_panel();
    virtual void on_update(float dt) override;
    virtual vk::graphics_command_buffer on_render() override;
    virtual void on_render_imgui() override;
    static const char *get_name() { return "Voxel Renderer Debug"; } 

private:

    uint32_t m_last_dt{ 0 };
};
    
} // namespace stla
