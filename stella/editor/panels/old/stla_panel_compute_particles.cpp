#include "stla_panel_compute_particles.h"
#include "stla_editor_app.h"

#include <stella.h>
#include <glm/gtc/matrix_transform.hpp>

#include <etl/vector.h>

#include <random>

const uint32_t WIDTH = 800;
const uint32_t HEIGHT = 600;
const uint32_t MAX_PARTICLES = (512 * 1000);

STLA_PACK(struct particle
{
    glm::vec2 m_position;
    glm::vec2 m_velocity;
    glm::vec4 m_color;
});

STLA_PACK(struct particle_ubo
{
    float dt;
});

STLA_PACK(struct particle_buffer
{
    particle m_particles[MAX_PARTICLES];
});

#define SHADER_DIR                   ASSETS_DIR "/shaders"
#define SHADER_PATH_BASIC_P3N3UV2    SHADER_DIR "/basic_p3n3uv2.shader"
#define SHADER_PATH_PARTICLE_P2V2C4  SHADER_DIR "/particle_p2v2c4.shader"
#define SHADER_PATH_PARTICLE_COMPUTE SHADER_DIR "/particle_compute.shader"

static particle_ubo g_particle_ubo     = { };
static particle_buffer g_particles_in  = { };
static particle_buffer g_particles_out = { };

namespace stla
{
    
compute_particles_panel::compute_particles_panel()
{
    vk::state &state = application::vulkan_state();

    m_renderpass    = application::create_default_renderpass();
    m_render_target = application::create_default_render_target(WIDTH, HEIGHT, m_renderpass);
    
    m_graphics_cmd_pool   = state.create_graphics_command_pool();
    m_compute_cmd_pool    = state.create_compute_command_pool();
    m_graphics_cmd_buffer = m_graphics_cmd_pool.acquire_command_buffer();
    m_compute_cmd_buffer  = m_compute_cmd_pool.acquire_command_buffer();
    
    vk::raster_pipeline_context::config pipeline_config;
    pipeline_config.width  = WIDTH;
    pipeline_config.height = HEIGHT;
    pipeline_config.topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
    m_raster_pipeline_context = state.create_raster_pipeline_context(m_renderpass, SHADER_PATH_PARTICLE_P2V2C4, pipeline_config);

    m_compute_pipeline_context = state.create_compute_pipeline_context(SHADER_PATH_PARTICLE_COMPUTE);
    m_uniform_set = state.create_uniform_set(
        m_compute_pipeline_context.get_descriptor_set_layout_container().get_layout(vk::descriptor_set_loc::zero));
    
    // Initialize Particles
    std::default_random_engine random_engine((unsigned)time(nullptr));
    std::uniform_real_distribution<float> random_dist(0.0f, 1.0f);
    for (auto &particle : g_particles_in.m_particles) {
        float r = 0.25f * sqrt(random_dist(random_engine));
        float theta = random_dist(random_engine) * 2 * 3.14159265358979323846;
        float x = r * cos(theta) * HEIGHT / WIDTH;
        float y = r * sin(theta);
        particle.m_position = glm::vec2(x, y);
        particle.m_velocity = glm::normalize(glm::vec2(x,y)) * 0.00025f;
        particle.m_color    = glm::vec4(random_dist(random_engine), random_dist(random_engine), random_dist(random_engine), 1.0f);
    }

    m_uniform_set.write(1, g_particles_in);
    
    m_fence = state.create_fence();
    m_compute_complete_sem = state.create_semaphore();

    m_desc_set = (VkDescriptorSet)ImGui_ImplVulkan_AddTexture(
        m_render_target.get_attachment_by_index(0).get_sampler_handle(),
        m_render_target.get_attachment_by_index(0).get_view_handle(),
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    );
}

compute_particles_panel::~compute_particles_panel()
{   
    m_compute_cmd_pool.destroy();
    m_graphics_cmd_pool.destroy();
    m_uniform_set.destroy();
    m_compute_pipeline_context.destroy();
    m_renderpass.destroy();
    m_render_target.destroy();
    m_fence.destroy();
    m_compute_complete_sem.destroy();
    m_raster_pipeline_context.destroy();
}

void compute_particles_panel::on_update(float dt) 
{
    m_fence.wait(1000);
    m_fence.reset();

    g_particle_ubo.dt = 16.0f;
    m_uniform_set.write(0, g_particle_ubo);

    m_compute_cmd_buffer.begin();
    m_compute_cmd_buffer.push_signal_semaphore(m_compute_complete_sem);

    vk::descriptor_set desc_set = m_uniform_set.get_descriptor_set();

    vkCmdBindPipeline(m_compute_cmd_buffer.get_handle(), VK_PIPELINE_BIND_POINT_COMPUTE, m_compute_pipeline_context.get_pipeline().get_handle());
    vkCmdBindDescriptorSets(
        m_compute_cmd_buffer.get_handle(), 
        VK_PIPELINE_BIND_POINT_COMPUTE, 
        m_compute_pipeline_context.get_pipeline().get_layout_handle(), 
        0, 1, &desc_set.get_handle(), 0, 0);
    vkCmdDispatch(m_compute_cmd_buffer.get_handle(), MAX_PARTICLES / 32, 1, 1);

    m_compute_cmd_buffer.end(m_fence);
    application::vulkan_state().submit_command_buffer(m_compute_cmd_buffer);
}

vk::graphics_command_buffer compute_particles_panel::on_render() 
{

    vk::buffer vk_particle_buffer = m_uniform_set.get_buffer(2);

    m_graphics_cmd_buffer.begin();
    m_graphics_cmd_buffer.push_wait_semaphore(m_compute_complete_sem);
    m_graphics_cmd_buffer.begin_renderpass(m_render_target, m_renderpass);
    m_graphics_cmd_buffer.bind_pipeline(m_raster_pipeline_context.get_pipeline());
    m_graphics_cmd_buffer.bind_vertex_buffer(vk_particle_buffer);
    m_graphics_cmd_buffer.draw(MAX_PARTICLES, 1);
    m_graphics_cmd_buffer.end_renderpass();
    m_graphics_cmd_buffer.end();

    return m_graphics_cmd_buffer;
}

void compute_particles_panel::on_render_imgui()
{
    ImGui::Begin(compute_particles_panel::get_name());
    ImGui::Text("Sandbox - Compute Shader");

    ImVec2 viewport_panel_size = ImGui::GetContentRegionAvail();
    ImGui::Image(m_desc_set, ImVec2{ viewport_panel_size.x, viewport_panel_size.y });
    
    ImGui::End();
}

} // namespace stla
