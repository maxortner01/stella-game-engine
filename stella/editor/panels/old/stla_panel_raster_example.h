#pragma once

#include "stla_ieditor_panel.h"

#include <stella.h>
#include <glm/glm.hpp>

namespace stla
{

class raster_example_panel : public ieditor_panel
{
public:

    raster_example_panel();
    ~raster_example_panel();
    virtual void on_update(float dt) override;
    virtual vk::graphics_command_buffer on_render() override;
    virtual void on_render_imgui() override;
    static const char *get_name() { return "Raster Example"; }

private:
    vk::renderpass      m_renderpass;

    vk::uniform_set     m_uniform_set;
    vk::raster_pipeline_context m_pipeline_context;

    vk::render_target   m_render_target;
    shader_parser       m_shader_parser;
    vk::shader_reflection_mgr m_shader_reflection_mgr;

    vk::graphics_command_pool   m_command_pool;
    vk::graphics_command_buffer m_command_buffer;

    tri_mesh m_tri_mesh;
    vk::buffer m_viking_vertex_buffer;
    vk::buffer m_viking_index_buffer;
    perspective_camera m_camera;
    VkDescriptorSet m_desc_set{ nullptr };

    uniform_data   m_uniform_data{ };
    uniform_data_1 m_uniform_data_1{ 0 };

    glm::vec3 m_camera_position_input;
};
    
} // namespace stla
