#pragma once

#include "stla_ieditor_panel.h"

#include <stella.h>
#include <glm/glm.hpp>

namespace stla
{

class compute_particles_panel : public ieditor_panel
{
public:
 
    compute_particles_panel();
    ~compute_particles_panel();
    virtual void on_update(float dt) override;
    virtual vk::graphics_command_buffer on_render() override;
    virtual void on_render_imgui() override;
    static const char *get_name() { return "Compute Particles"; } // This method must be included

private:
    vk::compute_command_pool     m_compute_cmd_pool;
    vk::compute_command_buffer   m_compute_cmd_buffer;
    vk::graphics_command_pool    m_graphics_cmd_pool;
    vk::graphics_command_buffer  m_graphics_cmd_buffer;
    vk::uniform_set              m_uniform_set;
    vk::compute_pipeline_context m_compute_pipeline_context;
    vk::raster_pipeline_context  m_raster_pipeline_context;

    vk::renderpass    m_renderpass;
    vk::render_target m_render_target;
    VkDescriptorSet   m_desc_set{ nullptr };

    vk::semaphore m_compute_complete_sem;
    vk::fence m_fence;
};
    
} // namespace stla
