#include "stla_panel_raster_example.h"
#include "stla_editor_app.h"

#include <stella.h>
#include <glm/gtc/matrix_transform.hpp>

#include <etl/vector.h>

#define SHADER_DIR                    ASSETS_DIR "/shaders"
#define SHADER_PATH_BASIC             SHADER_DIR "/basic.shader"
#define SHADER_PATH_BASIC_P3N3UV2     SHADER_DIR "/basic_p3n3uv2.shader"
#define SHADER_PATH_BASIC_P3          SHADER_DIR "/basic_p3.shader"
#define SHADER_PATH_BASIC_P3N3UV2_TMP SHADER_DIR "/basic_p3n3uv2_tmp.shader"
#define SHADER_PATH_BASIC_RAY_TRACE   SHADER_DIR "/rt_basic.shader"
#define SHADER_PATH_COMPUTE_RAY_TRACE SHADER_DIR "/rt_compute.shader"

#define MODEL_DIR                   ASSETS_DIR "/models"
#define MODEL_PATH_VIKING_ROOM      MODEL_DIR "/viking_room.obj"
#define MODEL_PATH_CORNELL_ORIGINAL MODEL_DIR "/CornellBox-Original.obj"

const uint32_t IMAGE_WIDTH  = 800 * 2;
const uint32_t IMAGE_HEIGHT = 600 * 2;

namespace stla
{
    
raster_example_panel::raster_example_panel() :
    m_camera(
        45.0f,
        16.0f / 9.0f,
        0.0001f, 1000.0f,
        glm::vec3(0.0f, 0.0f, 5.0f),
        glm::normalize(glm::vec3(0.0f, 0.0f, -1.0f))
    )
{
    vk::state &state = application::vulkan_state();

    m_command_pool   = state.create_graphics_command_pool();
    m_command_buffer = m_command_pool.acquire_command_buffer();

    m_renderpass    = application::create_default_renderpass();
    m_render_target = application::create_default_render_target(IMAGE_WIDTH, IMAGE_HEIGHT, m_renderpass);

    vk::raster_pipeline_context::config pipeline_config;
    pipeline_config.width  = IMAGE_WIDTH;
    pipeline_config.height = IMAGE_HEIGHT;
    m_pipeline_context.create(m_renderpass, SHADER_PATH_BASIC_P3, pipeline_config, state);
    m_uniform_set = state.create_uniform_set(m_pipeline_context.get_descriptor_set_layout_container().get_layout(vk::descriptor_set_loc::zero));

    // Load model
    m_tri_mesh.load(MODEL_PATH_CORNELL_ORIGINAL);

    STLA_ASSERT_RELEASE(m_pipeline_context.get_pipeline().get_vertex_layout() == m_tri_mesh.get_vertex_layout(),
        "mesh and pipeline have incompatible vertex layouts");

    VkBufferUsageFlags buffer_usage = (
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR
    );

    VkMemoryPropertyFlags memory_properties = (
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT |
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | 
        VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
    );

    m_viking_vertex_buffer = state.create_buffer(
        m_tri_mesh.get_vertices().size() * sizeof(float),
        buffer_usage | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, 
        memory_properties, 
        m_tri_mesh.get_vertices().data()
    );

    m_viking_index_buffer = state.create_buffer(
        m_tri_mesh.get_indices().size() * sizeof(uint32_t),
        buffer_usage | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, 
        memory_properties, 
        m_tri_mesh.get_indices().data()
    );

    m_desc_set = (VkDescriptorSet)ImGui_ImplVulkan_AddTexture(
        m_render_target.get_attachment_by_index(0).get_sampler_handle(),
        m_render_target.get_attachment_by_index(0).get_view_handle(),
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    );

    m_camera_position_input = m_camera.get_position();
}

raster_example_panel::~raster_example_panel()
{   
    m_viking_vertex_buffer.destroy();
    m_viking_index_buffer.destroy();
    m_command_pool.destroy();
    m_uniform_set.destroy();
    m_render_target.destroy();
    m_renderpass.destroy();
    m_pipeline_context.destroy();
}

void raster_example_panel::on_update(float dt) 
{

}

vk::graphics_command_buffer raster_example_panel::on_render() 
{
    glm::mat4 transform(1.0f);
    //transform = glm::translate(transform, glm::vec3(0.0f, m_translate_y, 0.0f));
    //transform = glm::rotate(transform, m_rotate_x, glm::vec3(1.0f, 0.0f, 0.0f));
    //transform = glm::rotate(transform, (float)SDL_GetTicks() / 2000.0f, glm::vec3(1.0f, 1.0f, 0.0f));
    
    m_uniform_data.m_transform       = transform;
    m_uniform_data.m_view_projection = m_camera.get_view_projection();

    m_uniform_set.write(0, m_uniform_data);
    m_uniform_set.write(1, m_uniform_data_1);

    m_command_buffer.begin();
    m_command_buffer.begin_renderpass(m_render_target, m_renderpass, glm::vec4(0.0f, 0.3f, 0.2f, 1.0f));
    m_command_buffer.bind_pipeline(m_pipeline_context.get_pipeline());
    m_command_buffer.bind_uniform_data(m_pipeline_context.get_pipeline(), m_uniform_set);
    m_command_buffer.bind_vertex_buffer(m_viking_vertex_buffer);
    m_command_buffer.bind_index_buffer(m_viking_index_buffer);
    m_command_buffer.draw_indexed(m_tri_mesh.get_indices().size());
    m_command_buffer.end_renderpass();
    m_command_buffer.end();

    return m_command_buffer;
}

void raster_example_panel::on_render_imgui()
{
    ImGui::Begin(raster_example_panel::get_name());
    ImGui::Text("Raster Example");
    if (ImGui::SliderFloat3("Camera Position", &m_camera_position_input[0], -25.0f, 25.0f)) {
        m_camera.set_position(m_camera_position_input);
    }

    ImVec2 viewport_panel_size = ImGui::GetContentRegionAvail();
    ImGui::Image(m_desc_set, ImVec2{ viewport_panel_size.x, viewport_panel_size.y }, ImVec2(1, 1), ImVec2(0, 0));
    
    ImGui::End();
}

} // namespace stla
