#include "stla_panel_sandbox.h"
#include "stla_editor_app.h"

#include <stella.h>

#include <etl/vector.h>

#include <random>
#include <stdlib.h>

const uint32_t IMAGE_WIDTH = 1600;
const uint32_t IMAGE_HEIGHT = 900;

#define SHADER_DIR                         ASSETS_DIR "/shaders"
#define SHADER_PATH_PARTICLE_P2V2C4        SHADER_DIR "/particle_p2v2c4.shader"
#define SHADER_PATH_PARTICLE_COMPUTE       SHADER_DIR "/particle_compute.shader"
#define SHADER_PATH_IMAGE_COMPUTE          SHADER_DIR "/image_compute.shader"
#define SHADER_PATH_RT_COMPUTE             SHADER_DIR "/stla_voxel_renderer_compute.shader"
#define SHADER_PATH_FULL_SCREEN_QUAD_P3UV2 SHADER_DIR "/fullscreen_quad_p3uv2.shader"
#define SHADER_PATH_BASIC_P3               SHADER_DIR "/basic_p3.shader"

#define MODEL_DIR                   ASSETS_DIR "/models"
#define MODEL_PATH_VIKING_ROOM      MODEL_DIR  "/viking_room.obj"
#define MODEL_PATH_CORNELL_ORIGINAL MODEL_DIR  "/CornellBox-Original.obj"
#define MODEL_PATH_CUBE             MODEL_DIR  "/cube.obj"

#define FLATTEN(_x, _y, _z, _w, _d) (((((_x) * (_w)) + (_y)) * (_d)) + (_z))

namespace stla
{

sandbox_panel::sandbox_panel() :
    m_voxel_renderer(application::vulkan_state(), glm::uvec2(IMAGE_WIDTH, IMAGE_HEIGHT)),
    m_camera(
        45.0f,
        16.0f / 9.0f,
        0.0001f, 1000.0f,
        glm::vec3(3.0f, 3.0f, 15.0f),
        glm::normalize(glm::vec3(0.0f, 0.0f, -1.0f))
    )
{
    srand((unsigned)time(NULL));

    vk::state &state = application::vulkan_state();

    glm::ivec3 voxel_dims(DIM);
    
    glm::vec3 radius = glm::vec3(voxel_dims - 2) / 2.0f;
    for (int x = 1; x < voxel_dims.x - 1; x++) {
        for (int y = 1; y < voxel_dims.y - 1; y++) {
            for (int z = 1; z < voxel_dims.z - 1; z++) {
                float r_squared = glm::pow((x - radius.x), 2) + glm::pow((y - radius.y), 2) + glm::pow((z - radius.z), 2);
                float r = glm::sqrt(r_squared);
                
                bool is_active = 0;
                
                if (glm::any(glm::lessThanEqual(glm::vec3(r), radius))) {
                //float random_number = (float)rand() / (float)RAND_MAX;
                //if (random_number >= 0.5) {
                    is_active = 1;
                }

                m_voxel_grid.voxels[FLATTEN(x, y, z, voxel_dims.x, voxel_dims.z)] = (int)is_active;
            }
        }
    }

    m_renderpass    = application::create_default_renderpass();
    m_render_target = application::create_default_render_target(IMAGE_WIDTH, IMAGE_HEIGHT, m_renderpass);
    
    m_graphics_cmd_pool   = state.create_graphics_command_pool();
    m_compute_cmd_pool    = state.create_compute_command_pool();
    m_graphics_cmd_buffer = m_graphics_cmd_pool.acquire_command_buffer();
    m_compute_cmd_buffer  = m_compute_cmd_pool.acquire_command_buffer();
    
    m_compute_pipeline_context = state.create_compute_pipeline_context(SHADER_PATH_RT_COMPUTE);
    const vk::descriptor_set_layout_container &desc_set_layout_container = m_compute_pipeline_context.get_descriptor_set_layout_container();

    m_compute_desc_set = state.alloc_descriptor_set(desc_set_layout_container.get_layout(vk::descriptor_set_loc::zero));
    vk::descriptor_set_binding pixels_binding                 = desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 0);
    //vk::descriptor_set_binding scene_data_binding             = desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 1);
    //vk::descriptor_set_binding tlas_binding                   = desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 2);
    //vk::descriptor_set_binding aabb_buffer_binding            = desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 3);
    //vk::descriptor_set_binding voxel_data_binding             = desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 4);
    //vk::descriptor_set_binding voxel_grid_desc_buffer_binding = desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 5);
    //vk::descriptor_set_binding instance_desc_buffer_binding   = desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 6);

    STLA_ASSERT_RELEASE(m_tri_mesh.load(MODEL_PATH_CUBE), "unable to load obj file");

    const float voxel_size   = (0.03125f);
    const glm::vec3 aabb_max = (glm::vec3(voxel_dims) * voxel_size) / 2.0f;
    const glm::vec3 aabb_min = -aabb_max;
    aabb_3d aabb(aabb_min.x, aabb_min.y, aabb_min.z, aabb_max.x, aabb_max.y, aabb_max.z);

    //m_aabb_buffer = state.create_buffer(sizeof(aabb_3d), (const void*)&aabb);
    //STLA_ASSERT_RELEASE(m_blas.create_from_aabbs(m_aabb_buffer, state), "unable to create blas");
    //STLA_ASSERT_RELEASE(m_blas.create_from_triangles(m_vertex_buffer, m_index_buffer, m_tri_mesh.get_vertex_layout(), state), "unable to create blas");

    for (int i = 0; i < m_blas_instances.capacity(); i++) {
        vk::tlas::instance_id instance_id = m_tlas.push_instance(m_blas, glm::mat4(1.0f));
        STLA_ASSERT_RELEASE(instance_id != vk::tlas::invalid_instance_id, "unable to push tlas instance");
        m_blas_instances.push_back(instance_id);
    }

    STLA_ASSERT_RELEASE(m_tlas.build(application::vulkan_state()), "unable to create tlas");
    
    m_pixel_buffer = state.create_buffer(IMAGE_WIDTH * IMAGE_HEIGHT * sizeof(glm::vec4), pixels_binding);
    m_compute_desc_set.bind(pixels_binding, m_pixel_buffer);

    //m_scene_data_buffer = state.create_buffer(scene_data_binding);
   // m_compute_desc_set.bind(scene_data_binding, m_scene_data_buffer);

   // m_voxel_buffer = state.create_buffer(sizeof(voxel_grid), voxel_data_binding, &m_voxel_grid.voxels[0]);
   // m_compute_desc_set.bind(voxel_data_binding, m_voxel_buffer);

    //m_compute_desc_set.bind(tlas_binding, m_tlas);
    //m_compute_desc_set.bind(aabb_buffer_binding, m_aabb_buffer);

    //m_instance_descriptor_buffer = state.create_buffer(instance_desc_buffer_binding, )

    m_fence = state.create_fence();
    m_compute_complete_sem = state.create_semaphore();

    m_desc_set = (VkDescriptorSet)ImGui_ImplVulkan_AddTexture(
        m_render_target.get_attachment_by_index(0).get_sampler_handle(),
        m_render_target.get_attachment_by_index(0).get_view_handle(),
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    );

    //m_scene_data_host.time_ms          = 0.0f;
    m_scene_data_host.camera_position  = glm::vec4(m_camera.get_position(), 0.0f);
    m_scene_data_buffer.copy_from(sizeof(m_scene_data_host), &m_scene_data_host);

    {
        m_fence.wait(1000);
        m_fence.reset();
        m_graphics_cmd_buffer.begin();
        m_graphics_cmd_buffer.transition_image_layout(
            m_render_target.get_attachment_by_index(0), VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        m_graphics_cmd_buffer.end(m_fence);
        state.submit_command_buffer(m_graphics_cmd_buffer);  
    }
}

sandbox_panel::~sandbox_panel()
{   
    STLA_ASSERT(m_fence.wait(1000), "fence timeout");

    m_compute_cmd_pool.destroy();
    m_graphics_cmd_pool.destroy();
    m_pixel_buffer.destroy();
    m_scene_data_buffer.destroy();
    m_compute_pipeline_context.destroy();
    m_renderpass.destroy();
    m_render_target.destroy();
    m_fence.destroy();
    m_compute_complete_sem.destroy();
    m_vertex_buffer.destroy();
    m_index_buffer.destroy();
    m_tlas.destroy();
    m_blas.destroy();
    m_aabb_buffer.destroy();
}

void sandbox_panel::on_update(float dt) 
{
    m_scene_data_host.random_seed = rand();
    //m_scene_data_host.time_ms += dt;
    STLA_ASSERT(m_fence.wait(1000), "fence timedout");
    m_fence.reset();

    const uint32_t columns = glm::pow(m_blas_instances.size(), 1.0f / 3.0f);

    const float gap = 2.0f;
    for (int i = 0; i < m_blas_instances.size(); i++) {
        int z = i % ROOT;
        int y = (i / ROOT) % ROOT;
        int x = i / (ROOT * ROOT); 

        glm::mat4 transform(1.0f);
        transform = glm::translate(transform, glm::vec3(x * gap, y * gap, z * gap));
        if (m_rotate_x || m_rotate_y || m_rotate_z) {
            transform = glm::rotate(transform, (float)SDL_GetTicks() / 2500.0f, glm::vec3((float)x, (float)1.0f, (float)z));
        }
        //transform = glm::rotate(transform, 45.0f, glm::vec3(1.0f, 1.0f, 1.0f));
        m_tlas.set_instance_transform(m_blas_instances[i], transform);
    }
    
    STLA_ASSERT_RELEASE(m_tlas.update(), "unable to update tlas");
    //STLA_ASSERT(m_scene_data_buffer.copy_from(sizeof(m_scene_data_host), &m_scene_data_host), "unable to copy uniform buffer");

    // Compute
    uint32_t render_width     = IMAGE_WIDTH;
    uint32_t render_height    = IMAGE_HEIGHT;
    uint32_t workgroup_width  = 16;
    uint32_t workgroup_height = 8;

    m_compute_cmd_buffer.begin();
    m_compute_cmd_buffer.push_signal_semaphore(m_compute_complete_sem);
    m_compute_cmd_buffer.bind_pipeline(m_compute_pipeline_context.get_pipeline());
    m_compute_cmd_buffer.bind_descriptor_set(m_compute_pipeline_context.get_pipeline(), m_compute_desc_set);
    m_compute_cmd_buffer.dispatch(
        (uint32_t(render_width) + workgroup_width - 1) / workgroup_width,
        (uint32_t(render_height) + workgroup_height - 1) / workgroup_height, 1);
    m_compute_cmd_buffer.end();

    application::vulkan_state().submit_command_buffer(m_compute_cmd_buffer);
}

vk::graphics_command_buffer sandbox_panel::on_render() 
{
    m_graphics_cmd_buffer.begin();
    m_graphics_cmd_buffer.push_wait_semaphore(m_compute_complete_sem);
    
    m_graphics_cmd_buffer.transition_image_layout(
        m_render_target.get_attachment_by_index(0), 
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    
    m_graphics_cmd_buffer.copy_buffer_to_image(
        m_render_target.get_attachment_by_index(0), m_pixel_buffer); //m_uniform_set.get_buffer(0));

    m_graphics_cmd_buffer.transition_image_layout(
        m_render_target.get_attachment_by_index(0),
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    m_graphics_cmd_buffer.end(m_fence);
    return m_graphics_cmd_buffer;
}

void sandbox_panel::on_render_imgui()
{
    ImGui::Begin(sandbox_panel::get_name());
    ImGui::Text("Sandbox - Compute Shader");

    ImGui::InputFloat3("Camera Position", &m_scene_data_host.camera_position[0]);
    ImGui::Checkbox("Rotate X", &m_rotate_x);
    ImGui::Checkbox("Rotate Y", &m_rotate_y);
    ImGui::Checkbox("Rotate Z", &m_rotate_z);

    ImVec2 viewport_panel_size = ImGui::GetContentRegionAvail();
    ImGui::Image(m_desc_set, ImVec2{ viewport_panel_size.x, viewport_panel_size.y });
    
    ImGui::End();

    //TLA_ASSERT(m_scene_data_buffer.copy_from(sizeof(m_scene_data_host), &m_scene_data_host), "unable to copy uniform buffer");
}

} // namespace stla
