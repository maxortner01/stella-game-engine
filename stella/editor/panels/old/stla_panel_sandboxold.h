#pragma once

#include "stla_ieditor_panel.h"

#include <stella.h>
#include <glm/glm.hpp>

namespace stla
{

STLA_PACK(struct voxel_volume_descriptor
{

});
#define DIM ((32 + 2))
STLA_PACK(struct voxel_grid 
{
    int voxels[DIM * DIM * DIM];
});

STLA_PACK(struct voxel_instance_descriptor
{
    int grid_descriptor_id;
});

STLA_PACK(struct voxel_grid_descriptor
{
    int voxel_data_start;
    glm::ivec3 dims;
});


STLA_PACK(struct scene_data
    {
        glm::vec4  camera_position = glm::vec4(0.0f);
        glm::uvec2 resolution = glm::uvec2(0);
        int        random_seed{ 0 };
    });

class sandbox_panel : public ieditor_panel
{
public:
 
    sandbox_panel();
    ~sandbox_panel();
    virtual void on_update(float dt) override;
    virtual vk::graphics_command_buffer on_render() override;
    virtual void on_render_imgui() override;
    static const char *get_name() { return "Sandbox"; } // This method must be included

private:
    voxel_renderer m_voxel_renderer;

    vk::compute_command_pool     m_compute_cmd_pool;
    vk::compute_command_buffer   m_compute_cmd_buffer;
    vk::graphics_command_pool    m_graphics_cmd_pool;
    vk::graphics_command_buffer  m_graphics_cmd_buffer;
    //vk::uniform_set              m_uniform_set;
    vk::compute_pipeline_context m_compute_pipeline_context;
    vk::blas                     m_blas;
    
    tri_mesh                     m_tri_mesh;
    vk::buffer                   m_vertex_buffer;
    vk::buffer                   m_index_buffer;

    vk::descriptor_set m_compute_desc_set;

    // Compute Shader Buffers
    vk::buffer m_pixel_buffer;
    vk::buffer m_scene_data_buffer;
    vk::tlas   m_tlas;
    vk::buffer m_aabb_buffer;
    vk::buffer m_voxel_buffer;
    vk::buffer m_grid_descriptor_buffer;
    vk::buffer m_instance_descriptor_buffer;

    vk::renderpass    m_renderpass;
    vk::render_target m_render_target;
    VkDescriptorSet   m_desc_set{ nullptr };

    vk::semaphore m_compute_complete_sem;
    vk::fence m_fence;

    scene_data m_scene_data_host{ };
    
    bool m_rotate_x{ true };
    bool m_rotate_y{ true };
    bool m_rotate_z{ true };
    perspective_camera m_camera;

    #define ROOT (10)
    stla::vector<vk::tlas::instance_id, ROOT * ROOT * ROOT> m_blas_instances;
    voxel_grid m_voxel_grid{ 0 };
};
    
} // namespace stla
