#pragma once

#include "stla_ieditor_panel.h"

#include <stella.h>
#include <glm/glm.hpp>

namespace stla
{

class sandbox_panel : public ieditor_panel
{
public:
 
    sandbox_panel();
    ~sandbox_panel();
    virtual void on_update(float dt) override;
    virtual vk::graphics_command_buffer on_render() override;
    virtual void on_render_imgui() override;
    static const char *get_name() { return "Sandbox"; } // This method must be included

private:
    vk::graphics_command_pool   m_graphics_cmd_pool;
    vk::graphics_command_buffer m_graphics_cmd_buffer;
    vk::renderpass              m_renderpass;
    vk::render_target           m_render_target;
    vk::semaphore               m_voxel_render_complete_sem;
    vk::fence                   m_fence;
    VkDescriptorSet             m_desc_set{ nullptr };
    voxel_structure_grid_handle m_voxel_structure_grid_handles[32]{ };
    bool                        m_panel_resized{ false };
    ImVec2                      m_panel_size;
    bool                        m_render_target_updated{ true };
    uint32_t                    m_num_frames{ 0 };
    tri_mesh                    m_tri_mesh;
};
    
} // namespace stla
