#include "stla_sandbox_panel.h"
#include "stla_editor_app.h"

#include <stella.h>

#include <etl/vector.h>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <stdlib.h>

#include <random>
#include <filesystem>
#include <string>
#include <iostream>
#include <vector>
#include <unordered_map>
#include <list>
#include <cstddef>
#include <array>
#include <utility>
#include <type_traits>
#include <memory_resource>

const uint32_t IMAGE_WIDTH = 1600;
const uint32_t IMAGE_HEIGHT = 900;

#define SHADER_DIR                         ASSETS_DIR "/shaders"
#define SHADER_PATH_PARTICLE_P2V2C4        SHADER_DIR "/particle_p2v2c4.shader"
#define SHADER_PATH_PARTICLE_COMPUTE       SHADER_DIR "/particle_compute.shader"
#define SHADER_PATH_IMAGE_COMPUTE          SHADER_DIR "/image_compute.shader"
#define SHADER_PATH_RT_COMPUTE             SHADER_DIR "/rt_compute.shader"
#define SHADER_PATH_FULL_SCREEN_QUAD_P3UV2 SHADER_DIR "/fullscreen_quad_p3uv2.shader"
#define SHADER_PATH_BASIC_P3               SHADER_DIR "/basic_p3.shader"

#define MODEL_DIR                   ASSETS_DIR "/models"
#define MODEL_PATH_VIKING_ROOM      MODEL_DIR  "/viking_room.obj"
#define MODEL_PATH_CORNELL_ORIGINAL MODEL_DIR  "/CornellBox-Original.obj"
#define MODEL_PATH_CUBE             MODEL_DIR  "/cube.obj"
#define MODEL_PATH_DRAGON           MODEL_DIR  "/dragon.obj"

#define TEXTURE_DIR              ASSETS_DIR "/textures"
#define TEXTURE_PATH_PLAYER_WALK TEXTURE_DIR "/2d-pixel-art-character-asset-pack/v3/PlayerWalk/PlayerWalk 48x48.png"
#define TEXTURE_PATH_KNIGHT_RUN  TEXTURE_DIR "/Knight/noBKG_KnightRun_strip.png"

namespace stla
{

static void create_structure_grid_from_pixels(
    voxel_structure_grid_writer &writer, uint8_t *p_pixels, const glm::uvec3 &dims, const glm::uvec2 &sub_image_start, const glm::uvec2 &sub_image_dims)
{
    if (p_pixels == nullptr) {
        return;
    }

    STLA_ASSERT_RELEASE(dims.z == 4, "Image must have 4 channels");
    STLA_ASSERT_RELEASE(glm::all(glm::lessThanEqual(sub_image_start + sub_image_dims, glm::uvec2(dims))), "Subimage dims are out of bounds");

    uint32_t grid_i, grid_j;
    grid_i = grid_j = 0;
    for (int i = sub_image_start.x; i < sub_image_start.x + sub_image_dims.x; i++) {
        grid_j = 0;
        for (int j = sub_image_start.y; j < sub_image_start.y + sub_image_dims.y; j++) {
            uint8_t r = p_pixels[flatten(i, j, 0, dims.x, dims.z)]; 
            uint8_t g = p_pixels[flatten(i, j, 1, dims.x, dims.z)]; 
            uint8_t b = p_pixels[flatten(i, j, 2, dims.x, dims.z)]; 
            uint8_t a = p_pixels[flatten(i, j, 3, dims.x, dims.z)]; 

            if (a == 0) {
                writer.set_inactive(grid_i, (sub_image_dims.y - 1) - grid_j, 0);
            }
            else {
                writer.set_active(grid_i, (sub_image_dims.y - 1) - grid_j, 0);
                writer.set_color(grid_i, (sub_image_dims.y - 1) - grid_j, 0, glm::vec4(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f));
            }
            grid_j++;
        }
        grid_i++;
    }

    writer.commit();
}

static voxel_structure_grid_handle create_structure_grid_from_image(const char *p_path, float voxel_size)
{
    if (!std::filesystem::exists(p_path)) {
        STLA_CORE_LOG_ERROR("Texture path doesn't exist: {0}", p_path);
        return nullptr;
    }

    int width    = 0;
    int height   = 0;
    int channels = 0;
    uint8_t *p_pixels = (uint8_t *)stbi_load(p_path, &width, &height, &channels, STBI_rgb_alpha);
    if (p_pixels == nullptr) {
        STLA_CORE_LOG_ERROR("Unable to load texture from path: {0}", p_path);
        return nullptr;
    }

    STLA_CORE_LOG_INFO("Texture loaded from: {0}", p_path);
    STLA_CORE_LOG_INFO("  Width    : {0}", width);
    STLA_CORE_LOG_INFO("  Height   : {0}", height);
    STLA_CORE_LOG_INFO("  Channels : {0}", channels);

    voxel_structure_grid_handle grid_handle = application::voxel_renderer().create_structure_grid(glm::uvec3(width, height, 1), voxel_size);
    voxel_structure_grid_writer writer(grid_handle, application::voxel_renderer());

    create_structure_grid_from_pixels(writer, p_pixels, glm::uvec3(width, height, channels), glm::uvec2(0.0f, 0.0f), glm::uvec2(width, height));

    stbi_image_free(p_pixels);
    return grid_handle;
}

static uint32_t create_structure_grid_from_sprite_sheet(
    const char *p_path, float voxel_size, const glm::uvec2 &sprite_size, uint32_t length, voxel_structure_grid_handle *p_handles)
{
    if (p_handles == nullptr) {
        return 0;
    }

    if (!std::filesystem::exists(p_path)) {
        STLA_CORE_LOG_ERROR("Sprite sheet path doesn't exist: {0}", p_path);
        return 0;
    }

    glm::uvec3 dims;
    uint8_t *p_pixels = (uint8_t *)stbi_load(p_path, (int *)&dims.x, (int *)&dims.y, (int *)&dims.z, STBI_rgb_alpha);
    if (p_pixels == nullptr) {
        STLA_CORE_LOG_ERROR("Unable to load texture from path: {0}", p_path);
        return 0;
    }

    glm::uvec2 sprite_count = glm::uvec2(dims) / sprite_size;

    if (glm::any(glm::greaterThan(sprite_size * sprite_count, glm::uvec2(dims)))) {
        STLA_CORE_LOG_ERROR("Invalid sprite size and count");
        stbi_image_free(p_pixels);
        return 0;
    }

    STLA_CORE_LOG_INFO("Sprite sheet loaded from: {0}", p_path);
    STLA_CORE_LOG_INFO("  Width       : {0}", dims.x);
    STLA_CORE_LOG_INFO("  Height      : {0}", dims.y);
    STLA_CORE_LOG_INFO("  Channels    : {0}", dims.z);
    STLA_CORE_LOG_INFO("  Srite Count : {0}, {1}", sprite_count.x, sprite_count.y);

    uint32_t total_sprites_in_sheet = sprite_count.x * sprite_count.y;
    uint32_t handles_created = 0;
    glm::uvec2 sub_image_start = glm::uvec2(0);

    uint32_t sub_image_start_x = sub_image_start.x;
    for (int i = 0; i < sprite_count.x; i++) {
        uint32_t sub_image_start_y = sub_image_start.y;

        for (int j = 0; j < sprite_count.y; j++) {
            if (handles_created >= length) {
                break;
            }

            voxel_structure_grid_handle grid_handle = application::voxel_renderer().create_structure_grid(glm::uvec3(sprite_size.x, sprite_size.y, 1), voxel_size);
            if (grid_handle == nullptr) {
                break;
            }

            voxel_structure_grid_writer writer(grid_handle, application::voxel_renderer());
            create_structure_grid_from_pixels(writer, p_pixels, dims, glm::uvec2(sub_image_start_x, sub_image_start_y), sprite_size);

            p_handles[i] = grid_handle;
            sub_image_start_y += sprite_size.y;
            handles_created++;
        }
        sub_image_start_x += sprite_size.x;
    }

    stbi_image_free(p_pixels);
    return handles_created;
}



class voxelizer
{
public:

    voxelizer(const tri_mesh &mesh, const glm::uvec3 &voxel_dims, float voxel_size, const aabb_3d &aabb, vk::state &state) :
        m_mesh(mesh), m_state(state)
    {     
        m_compute_shader_args.voxel_volume_dims     = voxel_dims;
        m_compute_shader_args.voxel_volume_aabb_min = glm::vec3(aabb.min_x, aabb.min_y, aabb.min_z);
        m_compute_shader_args.voxel_volume_aabb_max = glm::vec3(aabb.max_x, aabb.max_y, aabb.max_z);
        m_compute_shader_args.vertex_count          = mesh.get_vertex_count();
        m_compute_shader_args.vertex_elements       = mesh.get_vertex_layout().get_element_count();
        m_compute_shader_args.voxel_size            = voxel_size;

        m_compute_command_pool     = state.create_compute_command_pool();
        m_compute_command_buffer   = m_compute_command_pool.acquire_command_buffer();
        m_compute_pipeline_context = state.create_compute_pipeline_context_from_src(sp_compute_src);
        m_descriptor_set           = state.alloc_descriptor_set(
            m_compute_pipeline_context.get_descriptor_set_layout_container().get_layout(vk::descriptor_set_loc::zero));

        const vk::descriptor_set_layout_container &desc_set_layout_container = m_compute_pipeline_context.get_descriptor_set_layout_container();
        auto compute_shader_args_binding = desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 0);
        auto vertex_data_binding         = desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 1);
        auto voxel_data_binding          = desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 2);
        auto index_data_binding          = desc_set_layout_container.get_binding(vk::descriptor_set_loc::zero, 3);

        m_compute_shader_args_device = state.create_buffer(compute_shader_args_binding, (const void *)&m_compute_shader_args);
        m_vertex_data_device         = state.create_buffer(sizeof(float) * mesh.get_vertices().size(), vertex_data_binding, mesh.get_vertices().data());
        m_voxel_data_device          = state.create_buffer(sizeof(int) * voxel_dims.x * voxel_dims.y * voxel_dims.z, voxel_data_binding);
        m_index_data_device          = state.create_buffer(sizeof(int) * mesh.get_indices().size(), mesh.get_indices().data());

        m_descriptor_set.bind(compute_shader_args_binding, m_compute_shader_args_device);
        m_descriptor_set.bind(vertex_data_binding, m_vertex_data_device);
        m_descriptor_set.bind(voxel_data_binding, m_voxel_data_device);
        m_descriptor_set.bind(index_data_binding, m_index_data_device);
    
        m_fence = state.create_fence();
    }

    ~voxelizer()
    {
        m_fence.destroy();
        m_compute_command_pool.return_command_buffer(m_compute_command_buffer);
        m_compute_command_pool.destroy();
        m_compute_pipeline_context.destroy();
        m_compute_shader_args_device.destroy();
        m_vertex_data_device.destroy();
        m_voxel_data_device.destroy();
        m_index_data_device.destroy();
    }

    bool execute(std::vector<int> &voxel_data_out)
    {
        m_fence.reset();

        m_compute_command_buffer.begin();

        m_compute_command_buffer.bind_pipeline(m_compute_pipeline_context.get_pipeline());
        m_compute_command_buffer.bind_descriptor_set(m_compute_pipeline_context.get_pipeline(), m_descriptor_set);
        m_compute_command_buffer.dispatch(
            (m_compute_shader_args.voxel_volume_dims.x + vk::shader::COMPUTE_WORKGROUP_WIDTH - 1) / vk::shader::COMPUTE_WORKGROUP_WIDTH,
            (m_compute_shader_args.voxel_volume_dims.y + vk::shader::COMPUTE_WORKGROUP_HEIGHT - 1) / vk::shader::COMPUTE_WORKGROUP_HEIGHT, 
            (m_compute_shader_args.voxel_volume_dims.z + vk::shader::COMPUTE_WORKGROUP_DEPTH - 1) / vk::shader::COMPUTE_WORKGROUP_DEPTH);

        m_compute_command_buffer.end(m_fence);

        m_state.submit_command_buffer(m_compute_command_buffer);

        if (!m_fence.wait(1000 * 60)) {
            return false;
        }

        voxel_data_out.clear();
        voxel_data_out.resize(m_compute_shader_args.voxel_volume_dims.x * m_compute_shader_args.voxel_volume_dims.y * m_compute_shader_args.voxel_volume_dims.z);
        m_voxel_data_device.copy_to(m_voxel_data_device.get_size(), voxel_data_out.data());
        return true;
    }

private:

    struct compute_shader_args_data
    {
        glm::uvec3 voxel_volume_dims;     uint32_t p0;
        glm::vec3  voxel_volume_aabb_min; uint32_t p1;
        glm::vec3  voxel_volume_aabb_max; uint32_t p2;
        uint32_t   vertex_count{ 0 };
        uint32_t   vertex_elements{ 0 };
        float      voxel_size;
    };

    const char * const sp_compute_src = STLA_DECLARE_SHADER_SRC(
        layout(local_size_x = 16, local_size_y = 8, local_size_z = 1) in;

        layout (set = 0, binding = 0) uniform args_block 
        {
            uvec3 g_voxel_volume_dims;     uint p0;
            vec3  g_voxel_volume_aabb_min; uint p1;
            vec3  g_voxel_volume_aabb_max; uint p2;
            uint  g_vertex_count;
            uint  g_vertex_elements;
            float g_voxel_size;
        };

        layout (set = 0, binding = 1) buffer vertex_data_block
        {
            float g_vertex_data[];
        };

        layout (set = 0, binding = 2) buffer voxel_data_block
        {
            int g_voxel_data[];
        };

        layout (set = 0, binding = 3) buffer index_data_block
        {
            uint g_indices[];
        };

        uint flatten(uint x, uint y, uint z, uint w, uint d) { return d * (y * w + x) + z; }

        bool sep_axis_test(vec3 axis, vec3 v0, vec3 v1, vec3 v2, vec3 extents) 
        {
            vec3 u0 = vec3(1.0, 0.0, 0.0);
            vec3 u1 = vec3(0.0, 1.0, 0.0);
            vec3 u2 = vec3(0.0, 0.0, 1.0);

            float p0 = dot(v0, axis);
            float p1 = dot(v1, axis);
            float p2 = dot(v2, axis);
            // Project the AABB onto the seperating axis
            // We don't care about the end points of the prjection
            // just the length of the half-size of the AABB
            // That is, we're only casting the extents onto the 
            // seperating axis, not the AABB center. We don't
            // need to cast the center, because we know that the
            // aabb is at origin compared to the triangle!
            
            float r = extents.x * abs(dot(u0, axis)) +
                      extents.y * abs(dot(u1, axis)) + 
                      extents.z * abs(dot(u2, axis));

            //float r = e.x * glm::abs(vec3_dot(u0, axis)) +
            //        e.y * glm::abs(vec3_dot(u1, axis)) +
            //      e.z * glm::abs(vec3_dot(u2, axis));
            
            // Now do the actual test, basically see if either of
            // the most extreme of the triangle points intersects r
            // You might need to write Min & Max functions that take 3 arguments
            
            if (max(-max(max(p0, p1), p2), min(min(p0, p1), p2)) > r) {
                // This means BOTH of the points of the projected triangle
                // are outside the projected half-length of the AABB
                // Therefore the axis is seperating and we can exit
                return false;
            }
            return true;
        };

        struct triangle_t
        {
            vec3 p0;
            vec3 p1;
            vec3 p2;
        };

        
        bool intersects(triangle_t tri, vec3 aabb_min, vec3 aabb_max)
        {
        //bool intersect_aabb_triangle(AABB box, vec3 p1, vec3 p2, vec3 p3) 
            vec3 v0 = tri.p0; 
            vec3 v1 = tri.p1;
            vec3 v2 = tri.p2;

            // Convert AABB to center-extents form
            //vec3 c = box_center(box); 
            //vec3 e = box_extents(box);
            //glm::vec3 aabb_min = glm::vec3(aabb.min_x, aabb.min_y, aabb.min_z);
            //glm::vec3 aabb_max = glm::vec3(aabb.max_x, aabb.max_y, aabb.max_z);
            
            vec3 c = aabb_min + (0.5f * (aabb_max - aabb_min)); 
            //vec3 c = (aabb_max + aabb_min) * 0.5;
            vec3 e = (aabb_max - aabb_min) * 0.5;

            // Translate the triangle as conceptually moving the AABB to origin
            // This is the same as we did with the point in triangle test
            v0 = v0 - c;
            v1 = v1 - c;
            v2 = v2 - c;

            // Compute the edge vectors of the triangle  (ABC)
            // That is, get the lines between the points as vectors
            vec3 f0 = v1 - v0; // B - A
            vec3 f1 = v2 - v1; // C - B
            vec3 f2 = v0 - v2; // A - C

            // Compute the face normals of the AABB, because the AABB
            // is at center, and of course axis aligned, we know that 
            // it's normals are the X, Y and Z axis.
            vec3 u0 = vec3(1.0, 0.0, 0.0);
            vec3 u1 = vec3(0.0, 1.0, 0.0);
            vec3 u2 = vec3(0.0, 0.0, 1.0);

            // There are a total of 13 axis to test!

            // We first test against 9 axis, these axis are given by
            // cross product combinations of the edges of the triangle
            // and the edges of the AABB. You need to get an axis testing
            // each of the 3 sides of the AABB against each of the 3 sides
            // of the triangle. The result is 9 axis of seperation
            // https://awwapp.com/b/umzoc8tiv/

            // Compute the 9 axis
            vec3 axis_u0_f0 = cross(u0, f0);
            vec3 axis_u0_f1 = cross(u0, f1);
            vec3 axis_u0_f2 = cross(u0, f2);

            vec3 axis_u1_f0 = cross(u1, f0);
            vec3 axis_u1_f1 = cross(u1, f1);
            vec3 axis_u1_f2 = cross(u2, f2);

            vec3 axis_u2_f0 = cross(u2, f0);
            vec3 axis_u2_f1 = cross(u2, f1);
            vec3 axis_u2_f2 = cross(u2, f2);

            // Repeat this test for the other 8 seperating axis
            if (!sep_axis_test(axis_u0_f0, v0, v1, v2, e) || 
                !sep_axis_test(axis_u0_f1, v0, v1, v2, e) || 
                !sep_axis_test(axis_u0_f2, v0, v1, v2, e) ||
                !sep_axis_test(axis_u1_f0, v0, v1, v2, e) || 
                !sep_axis_test(axis_u1_f1, v0, v1, v2, e) || 
                !sep_axis_test(axis_u1_f2, v0, v1, v2, e) ||
                !sep_axis_test(axis_u2_f0, v0, v1, v2, e) || 
                !sep_axis_test(axis_u2_f1, v0, v1, v2, e) || 
                !sep_axis_test(axis_u2_f2, v0, v1, v2, e)) {
                return false;
            }

            // Next, we have 3 face normals from the AABB
            // for these tests we are conceptually checking if the bounding box
            // of the triangle intersects the bounding box of the AABB
            // that is to say, the seperating axis for all tests are axis aligned:
            // axis1: (1, 0, 0), axis2: (0, 1, 0), axis3 (0, 0, 1)
            // Do the SAT given the 3 primary axis of the AABB
            // You already have vectors for this: u0, u1 & u2
            if (!sep_axis_test(u0, v0, v1, v2, e) || 
                !sep_axis_test(u1, v0, v1, v2, e) || 
                !sep_axis_test(u2, v0, v1, v2, e)) {
                return false;
            }

            // Finally, we have one last axis to test, the face normal of the triangle
            // We can get the normal of the triangle by crossing the first two line segments
            vec3 tri_normal = cross(f0, f1);
            if (!sep_axis_test(tri_normal, v0, v1, v2, e)) {
                return false;
            }

            // Passed testing for all 13 seperating axis that exist!
            return true;
        }

        void main()
        {
            // debugPrintfEXT("Voxel Volume\n  Dims %d %d %d\n  AABB Min %f %f %f\n  AABB Max %f %f %f\n  Vertex Count: %d\n  Vertex Stride %d\n  Voxel Size %f\n",
                // g_voxel_volume_dims.x, g_voxel_volume_dims.y, g_voxel_volume_dims.z,
                // g_voxel_volume_aabb_min.x, g_voxel_volume_aabb_min.y, g_voxel_volume_aabb_min.z,
                // g_voxel_volume_aabb_max.x, g_voxel_volume_aabb_max.y, g_voxel_volume_aabb_max.z,
                // g_vertex_count, g_vertex_elements, g_voxel_size);

            const uvec3 voxel_index = gl_GlobalInvocationID.xyz;
            if ((voxel_index.x >= g_voxel_volume_dims.x) || (voxel_index.y >= g_voxel_volume_dims.y) || (voxel_index.z >= g_voxel_volume_dims.z)) {
                return;
            }

            uint linear_voxel_index = flatten(voxel_index.x, voxel_index.y, voxel_index.z, g_voxel_volume_dims.x, g_voxel_volume_dims.z);

            vec3 voxel_aabb_min = g_voxel_volume_aabb_min + (vec3(voxel_index) * g_voxel_size);
            vec3 voxel_aabb_max = voxel_aabb_min + g_voxel_size;
            
            g_voxel_data[linear_voxel_index] = 0;

            //debugPrintfEXT("%d %d %d - %f %f %f, %f %f %f\n", 
              //  voxel_index.x, voxel_index.y, voxel_index.z, voxel_aabb_min.x, voxel_aabb_min.y, voxel_aabb_min.z, voxel_aabb_max.x, voxel_aabb_max.y, voxel_aabb_max.z);
            
            for (int i = 0; i < g_indices.length(); i += 3) {
                uint i0 = g_indices[i + 0];
                uint i1 = g_indices[i + 1];
                uint i2 = g_indices[i + 2];

                triangle_t tri;
                tri.p0 = vec3(
                    g_vertex_data[(i0 * g_vertex_elements) + 0],
                    g_vertex_data[(i0 * g_vertex_elements) + 1],
                    g_vertex_data[(i0 * g_vertex_elements) + 2]
                );

                tri.p1 = vec3(
                    g_vertex_data[(i1 * g_vertex_elements) + 0],
                    g_vertex_data[(i1 * g_vertex_elements) + 1],
                    g_vertex_data[(i1 * g_vertex_elements) + 2]
                );

                tri.p2 = vec3(
                    g_vertex_data[(i2 * g_vertex_elements) + 0],
                    g_vertex_data[(i2 * g_vertex_elements) + 1],
                    g_vertex_data[(i2 * g_vertex_elements) + 2]
                );

                if (intersects(tri, voxel_aabb_min, voxel_aabb_max)) {
                    g_voxel_data[linear_voxel_index] = 1;
                }
            }
        }
    );

private:

    const tri_mesh &             m_mesh;
    vk::state &                  m_state;
    vk::fence                    m_fence;
    vk::compute_command_pool     m_compute_command_pool;
    vk::compute_command_buffer   m_compute_command_buffer;
    vk::compute_pipeline_context m_compute_pipeline_context;
    vk::descriptor_set           m_descriptor_set;
    compute_shader_args_data     m_compute_shader_args;
    vk::buffer                   m_compute_shader_args_device;
    vk::buffer                   m_vertex_data_device;
    vk::buffer                   m_voxel_data_device;
    vk::buffer                   m_index_data_device;
};

void create_grid(const tri_mesh &mesh, float voxel_size)
{
    STLA_CORE_LOG_INFO("Creating grid from triangle mesh");
    for (auto attribute : mesh.get_vertex_layout().get_attribute_array()) {
        STLA_CORE_LOG_INFO("  ({0}) (type={1})", attribute.m_index, get_vertex_buffer_attribute_type_string(attribute.m_type));
    }

    aabb_3d aabb = mesh.get_aabb();
    glm::vec3 bb_length = glm::vec3(aabb.max_x, aabb.max_y, aabb.max_z) - glm::vec3(aabb.min_x, aabb.min_y, aabb.min_z);
    glm::vec3 voxel_dims = glm::ceil(bb_length / voxel_size);
    printf("aabb (min=%f %f %f) (max=%f %f %f)\n", aabb.min_x, aabb.min_y, aabb.min_z, aabb.max_x, aabb.max_y, aabb.max_z);
    printf("length (%f %f %f)\n", bb_length.x, bb_length.y, bb_length.z);
    printf("dims (%f %f %f)\n", voxel_dims.x, voxel_dims.y, voxel_dims.z);

    voxel_structure_grid_handle grid_handle = application::voxel_renderer().create_structure_grid(glm::uvec3(voxel_dims), voxel_size);
    voxel_structure_grid_writer writer(grid_handle, application::voxel_renderer());

    glm::vec3 aabb_min(aabb.min_x, aabb.min_y, aabb.min_z);
    const std::vector<uint32_t> &indices = mesh.get_indices();

    voxelizer voxelizer(mesh, glm::uvec3(voxel_dims), voxel_size, aabb, application::vulkan_state());

    std::vector<int> voxel_data;

    STLA_ASSERT(voxelizer.execute(voxel_data), "Error executing voxelizer");

    for (int x = 0; x < voxel_dims.x; x++) {
        for (int y = 0; y < voxel_dims.y; y++) {
            for (int z = 0; z < voxel_dims.z; z++) {
                if (voxel_data[flatten(x, y, z, voxel_dims.x, voxel_dims.z)] == 1) {
                    writer.set_active(x, y, z);
                    writer.set_color(x, y, z, glm::vec4(1.0f));
                }
                else {
                    writer.set_inactive(x, y, z);
                }
                // writer.set_active(x, y, z);
                // writer.set_color(x, y, z, glm::vec4(x, y, z, 1.0));
                /*
                for (int i = 0; i < indices.size(); i += 3) {
                    uint32_t p0 = indices[i + 0];
                    uint32_t p1 = indices[i + 1];
                    uint32_t p2 = indices[i + 2];

                    triangle tri(
                        mesh.get_position(indices[i + 0]), 
                        mesh.get_position(indices[i + 1]),
                        mesh.get_position(indices[i + 2])
                    );

                    glm::vec3 voxel_aabb_min = aabb_min + (glm::vec3(x, y, z) * voxel_size);
                    glm::vec3 voxel_aabb_max = voxel_aabb_min + voxel_size;

                    aabb_3d voxel_aabb(voxel_aabb_min, voxel_aabb_max);

                    if (intersects(tri, voxel_aabb)) {
                        writer.set_active(x, y, z);
                        writer.set_color(x, y, z, glm::vec4(mesh.get_normal(p0), 1.0));
                    }
                }
                */
            }
        }
    }

    writer.commit();
    return;
}

//-------------------------------------------------------------

//------------------------------------------------

#define STLA_CONTAINERS_USE_MANAGED_RESOURCE
#ifdef STLA_CONTAINERS_USE_MANAGED_RESOURCE
    template<typename RscMgr=default_resource_manager>
    using stla_string = mrsc::string<RscMgr>;
#else
    using stla_string = std::string;
#endif

sandbox_panel::sandbox_panel() 
{
    vk::state &state = application::vulkan_state();

    m_renderpass                = application::create_default_renderpass();
    m_render_target             = application::create_default_render_target(IMAGE_WIDTH, IMAGE_HEIGHT, m_renderpass);
    m_fence                     = state.create_fence();
    m_voxel_render_complete_sem = state.create_semaphore();
    m_graphics_cmd_pool         = state.create_graphics_command_pool();
    m_graphics_cmd_buffer       = m_graphics_cmd_pool.acquire_command_buffer();
  
    m_render_target.get_attachment_by_index(0).transition_layout(
        VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, m_graphics_cmd_buffer, &m_fence);
    
    std::unordered_map<
        std::string,
        int,
        std::hash<std::string>,
        std::equal_to<std::string>,
        resource_allocator<std::pair<const std::string, int>, default_resource_manager>
    > map;

    map["test_0"] = 1;
    map["test_1"] = 2;
    map["test_2"] = 3;

    std::vector<int, resource_allocator<int, default_resource_manager>> vec = { 1, 2, 3, 4, 5 };

    stla_string str = "test string";

    //std::basic_string<char, std::char_traits<char>, resource_allocator<char, default_resource_manager>> str = "test string";

    

    /*
    m_num_frames = create_structure_grid_from_sprite_sheet(
        TEXTURE_PATH_PLAYER_WALK, 
        0.25f,  
        glm::uvec2(48), 
        STLA_ARRAYSIZE(m_voxel_structure_grid_handles), 
        &m_voxel_structure_grid_handles[0]
    );
    */

   //todo make a sphere
    /*glm::uvec3 dims(48);
    m_voxel_structure_grid_handles[0] = application::voxel_renderer().create_structure_grid(dims, 0.15f);
    voxel_structure_grid_writer writer(m_voxel_structure_grid_handles[0], application::voxel_renderer());

    glm::vec3 radius = glm::vec3(dims) / 2.0f;
    for (int x = 1; x < dims.x - 1; x++) {
        for (int y = 1; y < dims.y - 1; y++) {
            for (int z = 1; z < dims.z - 1; z++) {
                float r_squared = glm::pow((x - radius.x), 2) + glm::pow((y - radius.y), 2) + glm::pow((z - radius.z), 2);
                float r = glm::sqrt(r_squared);
                
                if (glm::any(glm::lessThanEqual(glm::vec3(r), radius))) {
                    writer.set_active(x, y, z);
                    writer.set_color(x, y, z, glm::vec4(1.0f));
                }
                else {
                    writer.set_inactive(x, y, z);
                }
            }
        }
    }
    writer.commit();
    */

    m_num_frames = create_structure_grid_from_sprite_sheet(
        //TEXTURE_PATH_KNIGHT_RUN, 
        TEXTURE_PATH_PLAYER_WALK,
        0.25f,  
        //glm::uvec2(96, 64),
        glm::uvec2(48),
        STLA_ARRAYSIZE(m_voxel_structure_grid_handles), 
        &m_voxel_structure_grid_handles[0]
    );

    /*
    glm::mat4 t(1.0f);
    t = glm::scale(t, glm::vec3(10.0f));
    STLA_ASSERT(m_tri_mesh.load(
        MODEL_PATH_DRAGON
        //MODEL_PATH_VIKING_ROOM
        //MODEL_PATH_CORNELL_ORIGINAL
        //MODEL_PATH_CUBE
    ), "Unable to load obj file: %s", MODEL_PATH_DRAGON);
    m_tri_mesh.transform(t);
    create_grid(m_tri_mesh, 0.10f);
    */
}   

sandbox_panel::~sandbox_panel()
{   
    m_graphics_cmd_pool.destroy();
    m_renderpass.destroy();
    m_render_target.destroy();
    m_fence.destroy();
    m_voxel_render_complete_sem.destroy();
}

void sandbox_panel::on_update(float dt) 
{
    if (m_panel_resized) {
        m_panel_resized = false;
        m_render_target.destroy();
        m_render_target = application::create_default_render_target(m_panel_size.x, m_panel_size.y, m_renderpass);
        m_render_target.get_attachment_by_index(0).transition_layout(
            VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, m_graphics_cmd_buffer, &m_fence);
        application::voxel_renderer().resize(glm::uvec2(m_panel_size.x, m_panel_size.y));
        m_render_target_updated = true;
    }
}

vk::graphics_command_buffer sandbox_panel::on_render() 
{
    m_fence.wait(1000);
    m_fence.reset();

    application::voxel_renderer().on_render(&m_voxel_render_complete_sem);

    m_graphics_cmd_buffer.begin();
    m_graphics_cmd_buffer.push_wait_semaphore(m_voxel_render_complete_sem);
    
    m_graphics_cmd_buffer.transition_image_layout(
        m_render_target.get_attachment_by_index(0), 
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    
    m_graphics_cmd_buffer.copy_buffer_to_image(
        m_render_target.get_attachment_by_index(0), application::voxel_renderer().get_pixel_buffer());

    m_graphics_cmd_buffer.transition_image_layout(
        m_render_target.get_attachment_by_index(0),
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        
    m_graphics_cmd_buffer.end(m_fence);

    return m_graphics_cmd_buffer;
}

void sandbox_panel::on_render_imgui()
{
    ImGui::Begin(sandbox_panel::get_name());
    ImGui::Text("Sandbox - Voxel Renderer");

    ImVec2 current_panel_size = ImGui::GetContentRegionAvail();

    if (current_panel_size.x != m_panel_size.x || current_panel_size.y != m_panel_size.y) {
        m_panel_size = current_panel_size;
        m_panel_resized = true;
    }

    if (m_render_target_updated) {
        ImGui_ImplVulkan_RemoveTexture(m_desc_set);
        m_desc_set = (VkDescriptorSet)ImGui_ImplVulkan_AddTexture(
            m_render_target.get_attachment_by_index(0).get_sampler_handle(),
            m_render_target.get_attachment_by_index(0).get_view_handle(),
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        );
        m_render_target_updated = false;
    }

    ImGui::Image(m_desc_set, ImVec2{ current_panel_size.x, current_panel_size.y });
    
    ImGui::End();
}

} // namespace stla
