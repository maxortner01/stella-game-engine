cmake_minimum_required(VERSION 3.7.1)
set(CMAKE_CXX_STANDARD 20)
project(sandbox)
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

# Sandbox Options
option(SANDBOX_VENDORED "Use vendored libraries" ON)

# Stella Options
option(STELLA_OPTION_ENABLE_RESULT_MESSAGES "Enable result messages" ON)

find_package(Vulkan COMPONENTS shaderc_combined)

find_library(
    Vulkan_shaderc_combined_LIBRARY 
    NAMES shaderc_combined
    HINTS "$ENV{VULKAN_SDK}/Lib"
    REQUIRED
)

find_library(
    Vulkan_shaderc_combined_debug_LIBRARY 
    NAMES shaderc_combinedd
    HINTS "$ENV{VULKAN_SDK}/Lib"
    REQUIRED
)


if(SANDBOX_VENDORED)
    # This assumes you have added SDL as a submodule in vendored/SDL
    add_subdirectory(deps/sdl EXCLUDE_FROM_ALL)
else()
    # 1. Look for a SDL3 package,
    # 2. look for the SDL3-shared component, and
    # 3. fail if the shared component cannot be found.
    find_package(SDL3 REQUIRED CONFIG REQUIRED COMPONENTS SDL3-shared)
endif()

file(
	GLOB IMGUI_SOURCES
	deps/imgui/imgui_demo.cpp
	deps/imgui/imgui_draw.cpp
	deps/imgui/imgui_tables.cpp
	deps/imgui/imgui_widgets.cpp
	deps/imgui/imgui.cpp
	deps/imgui/backends/imgui_impl_sdl3.cpp
	deps/imgui/backends/imgui_impl_vulkan.cpp
)

include_directories(
# Dependencies
    ${Vulkan_INCLUDE_DIRS}
    deps/glm
    deps/vk-bootstrap/src
    deps/imgui
    deps/imgui/backends
    deps/etl/include
    deps/tinyobjloader
    deps/spdlog/include
    deps/taskflow
    deps/SPIRV-Reflect
    deps/stb

# Sandbox
    sandbox

# Engine
    stella/engine
    stella/engine/core/delta_time
    stella/engine/core/logger
    stella/engine/core/data_types
    stella/engine/core/resource_manager
    stella/engine/utils
    stella/engine/graphics
    stella/engine/graphics/geometry
    stella/engine/graphics/shader_parser
    stella/engine/graphics/camera
    stella/engine/graphics/camera/perspective
    stella/engine/graphics/vertex_buffer_layout
    stella/engine/graphics/imgui_context
    stella/engine/graphics/vulkan
    stella/engine/graphics/tri_mesh
    stella/engine/graphics/voxel_renderer

# Editor
    stella/editor
    stella/editor/panels
    stella/editor/panels/sandbox_panel
    stella/editor/panels/voxel_renderer_debug_panel
)

file(
	GLOB STELLA_ENGINE_SOURCES
	
    stella/engine/core/delta_time/stla_delta_time.cpp

    stella/engine/graphics/vertex_buffer_layout/stla_vertex_buffer_layout.cpp
    stella/engine/graphics/imgui_context/stla_imgui_context.cpp
    stella/engine/graphics/tri_mesh/stla_tri_mesh.cpp
    stella/engine/graphics/camera/perspective/stla_perspective_camera.cpp
    stella/engine/graphics/shader_parser/stla_shader_parser.cpp
    stella/engine/graphics/geometry/stla_geometry.cpp
    stella/engine/graphics/voxel_renderer/stla_voxel_renderer.cpp

    stella/engine/graphics/vulkan/vk_sync.cpp
    stella/engine/graphics/vulkan/vk_state.cpp
    stella/engine/graphics/vulkan/vk_shader.cpp
    stella/engine/graphics/vulkan/vk_raster_pipeline.cpp
    stella/engine/graphics/vulkan/vk_shader_reflection_mgr.cpp
    stella/engine/graphics/vulkan/vk_buffer.cpp
    stella/engine/graphics/vulkan/vk_render_target.cpp
    stella/engine/graphics/vulkan/vk_image.cpp
    stella/engine/graphics/vulkan/vk_renderpass.cpp
    stella/engine/graphics/vulkan/vk_command_buffer.cpp
    stella/engine/graphics/vulkan/vk_descriptors.cpp
    stella/engine/graphics/vulkan/vk_command_pool.cpp
    stella/engine/graphics/vulkan/vk_pipeline_context.cpp
    stella/engine/graphics/vulkan/vk_compute_pipeline.cpp
    stella/engine/graphics/vulkan/vk_raytrace.cpp
    stella/engine/graphics/vulkan/extensions_vk.cpp
)

file (
    GLOB STELLA_EDITOR_SOURCES
    stella/editor/stla_editor_app.cpp
    stella/editor/panels/sandbox_panel/stla_sandbox_panel.cpp
    stella/editor/panels/voxel_renderer_debug_panel/stla_voxel_renderer_debug_panel.cpp
)

add_executable(
    ${PROJECT_NAME}
    ${IMGUI_SOURCES}
    deps/vk-bootstrap/src/VkBootstrap.cpp
    deps/SPIRV-Reflect/spirv_reflect.cpp

# Sandbox
    sandbox/main.cpp

    ${STELLA_ENGINE_SOURCES}
    ${STELLA_EDITOR_SOURCES}
)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

set(SDL3_DEBUG_DLL_PATH ${CMAKE_CURRENT_SOURCE_DIR}/build/deps/sdl/Debug/SDL3.dll)
set(SDL3_RELEASE_DLL_PATH ${CMAKE_CURRENT_SOURCE_DIR}/build/deps/sdl/Release/SDL3.dll)

add_custom_command(
        TARGET ${PROJECT_NAME} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
                ${SDL3_DEBUG_DLL_PATH}
                ${CMAKE_CURRENT_SOURCE_DIR}/build/Debug/SDL3.dll)

add_custom_command(
    TARGET ${PROJECT_NAME} POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy
            ${SDL3_RELEASE_DLL_PATH}
            ${CMAKE_CURRENT_SOURCE_DIR}/build/Release/SDL3.dll)


target_link_libraries(
    ${PROJECT_NAME} 
    PRIVATE 
    Vulkan::Vulkan 
    ${Vulkan_shaderc_combined_debug_LIBRARY} 
    SDL3::SDL3
)

target_compile_definitions(${PROJECT_NAME} PUBLIC -DASSETS_DIR="${CMAKE_CURRENT_SOURCE_DIR}/assets")

target_compile_definitions(${PROJECT_NAME} PUBLIC -DSTLA_VOXEL_RENDERER_COMPUTE_SHADER_PATH="${CMAKE_CURRENT_SOURCE_DIR}/stella/engine/graphics/voxel_renderer/stla_voxel_renderer_compute.shader")

if (STELLA_OPTION_ENABLE_RESULT_MESSAGES)
    target_compile_definitions(${PROJECT_NAME} PUBLIC -DSTELLA_OPTION_ENABLE_RESULT_MESSAGES=1)
endif()