#include "stella.h"
#include "vk_common.h"

#include <SDL3/SDL.h>
#include <SDL3/SDL_vulkan.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <etl/string.h>
#include <etl/to_string.h>

#include <stdio.h>
#include <stdint.h>

#include <string>
#include <atomic>
#include <thread>
#include <execution>
#include <vector>

#define SHADER_DIR                    ASSETS_DIR "/shaders"
#define SHADER_PATH_BASIC             SHADER_DIR "/basic.shader"
#define SHADER_PATH_BASIC_P3N3UV2     SHADER_DIR "/basic_p3n3uv2.shader"
#define SHADER_PATH_BASIC_P3N3UV2_TMP SHADER_DIR "/basic_p3n3uv2_tmp.shader"
#define SHADER_PATH_BASIC_RAY_TRACE   SHADER_DIR "/rt_basic.shader"
#define SHADER_PATH_COMPUTE_RAY_TRACE SHADER_DIR "/rt_compute.shader"

#define MODEL_DIR              ASSETS_DIR "/models"
#define MODEL_PATH_VIKING_ROOM MODEL_DIR "/viking_room.obj"

struct basic_shader_uniform_data
{
    glm::mat4 m_view_projection;
    glm::mat4 m_transform;
};

class game_state
{
public:

    game_state() : 
        m_camera(
            45.0f,
            16.0f / 9.0f,
            0.0001f, 1000.0f,
            glm::vec3(0.0f, 0.0f, -5.0f),
            glm::normalize(glm::vec3(0.0f, 0.0f, 1.0f))
        )
    { }
    bool init();
    void shutdown();
    void run();

private:

    bool                         m_running{ false };
    SDL_Window *                 mp_window{ nullptr };
    stla::delta_time             m_dt;
    stla::vk::renderer           m_renderer;
    stla::vk::render_context     m_render_context;
    stla::vk::render_target      m_render_target;
    stla::vk::command_buffer     m_command_buffer;
    stla::imgui_context          m_imgui;
    stla::perspective_camera     m_camera;
    VkDescriptorSet              m_render_target_dset{ nullptr };

// Assets

    stla::tri_mesh   m_viking_room_mesh;
    stla::vk::buffer m_viking_vertex_buffer;
    stla::vk::buffer m_viking_index_buffer;

    stla::vk::pipeline m_basic_pipeline;
    stla::vk::shader   m_basic_vertex_shader;
    stla::vk::shader   m_basic_fragment_shader;
    stla::vk::buffer   m_basic_shader_ubo;

    stla::vk::descriptor_set_layout m_basic_shader_descriptor_set_layout;
    stla::vk::descriptor_set        m_basic_shader_descriptor_set;

    stla::vk::shader m_raygen_shader;
    stla::vk::shader m_any_hit_shader;
    stla::vk::shader m_closest_hit_shader;

    stla::vk::shader m_compute_shader;
    stla::vk::descriptor_set m_compute_desc_set;
    stla::vk::buffer m_compute_image_buffer;
    stla::vk::descriptor_set_layout m_compute_desc_layout;
    stla::shader_parser<2048> m_shader_parser;
};

int main() {
    
    game_state state;
    if (state.init()) {
        state.run();
    }
    state.shutdown();
}

bool game_state::init()
{
    stla::logger::init();

    // SDL/Window
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        STLA_CORE_LOG_CRITICAL("Unable to initialize SDL");
        return false;
    }

    SDL_SetHint(SDL_HINT_IME_SHOW_UI, "1");

    uint32_t window_flags = (
        SDL_WINDOW_RESIZABLE |
        SDL_WINDOW_VULKAN |
        SDL_WINDOW_HIGH_PIXEL_DENSITY
    );

    float window_scale = 0.75f;
    uint32_t window_width = 1920 * window_scale;
    uint32_t window_height = 1080 * window_scale;
    mp_window = SDL_CreateWindow(
        "Retro Space Game",
        window_width,
        window_height,
        window_flags
    );

    if (mp_window == nullptr) {
        STLA_CORE_LOG_CRITICAL("Unable to initialize SDL Window: %s", SDL_GetError());
        return false;
    }

    SDL_GL_SetSwapInterval(1);

    // Vulkan
    float aspect_ratio = 16.0f / 9.0f;
    uint32_t resolution_x = 256;
    uint32_t resolution_y = (uint32_t)(resolution_x / aspect_ratio);

    stla::vk::renderer::create_info create_renderer = {
        "Stella Vulkan Application",
        [&](VkInstance instance, VkSurfaceKHR* p_surface) {
            if (p_surface == nullptr) {
                return false;
            }

            return (bool)SDL_Vulkan_CreateSurface(mp_window, instance, p_surface);
        }
    };

    bool renderer_success = m_renderer.init(create_renderer);
    if (!renderer_success) {
        STLA_CORE_LOG_CRITICAL("Unable to initialize Stella Vulkan Engine");
        return false;
    }

    VkPhysicalDeviceProperties engine_props = m_renderer.get_physical_device_properties();
    STLA_CORE_LOG_INFO("Stella Vulkan Engine Initialize!");
    STLA_CORE_LOG_INFO("  API Version");
    STLA_CORE_LOG_INFO("   Major         : %d", VK_API_VERSION_MAJOR(engine_props.apiVersion));
    STLA_CORE_LOG_INFO("   Minor         : %d", VK_API_VERSION_MINOR(engine_props.apiVersion));
    STLA_CORE_LOG_INFO("   Variant       : %d", VK_API_VERSION_VARIANT(engine_props.apiVersion));
    STLA_CORE_LOG_INFO("   Patch         : %d", VK_API_VERSION_PATCH(engine_props.apiVersion));
    STLA_CORE_LOG_INFO("  Driver Version : 0x%08X", engine_props.driverVersion);
    STLA_CORE_LOG_INFO("  Vendor ID      : 0x%08X", engine_props.vendorID);
    STLA_CORE_LOG_INFO("  Device ID      : 0x%08X", engine_props.deviceID);
    STLA_CORE_LOG_INFO("  Device Type    : %d", engine_props.deviceType);
    STLA_CORE_LOG_INFO("  Device Name    : %s", engine_props.deviceName);
    STLA_CORE_LOG_INFO("  Limits         ");
    STLA_CORE_LOG_INFO("    Indirect Draw Count: %d", engine_props.limits.maxDrawIndirectCount);

    stla::vk::render_context::create_info create_render_context =  {
        VK_FORMAT_R32G32B32A32_SFLOAT,
        VK_FORMAT_D32_SFLOAT,
        //{ window_width, window_height }
        { 800, 600 }
    };

    if (!m_render_context.create(create_render_context, &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to create render context");
        return false;
    }

    // ImGui
    if (!m_imgui.init(mp_window, &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to initialize ImGui");
        return false;
    }

    if (!m_render_target.create(window_width, window_height, &m_render_context)) {
        STLA_CORE_LOG_CRITICAL("Unable to initialize render target");
        return false;
    }
    
    m_render_target_dset = (VkDescriptorSet)ImGui_ImplVulkan_AddTexture(
        m_render_target.get_color_attachment().m_sampler, 
        m_render_target.get_color_attachment().m_image_view, 
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    );

    if (!m_viking_room_mesh.load(MODEL_PATH_VIKING_ROOM)) {
        STLA_CORE_LOG_CRITICAL("Unable to load Viking Room mesh");
        return false;
    }

    STLA_CORE_LOG_INFO("Viking Model Info:");
    STLA_CORE_LOG_INFO("  Vertices : %d", m_viking_room_mesh.get_vertex_count());
    STLA_CORE_LOG_INFO("  Indices  : %d", m_viking_room_mesh.m_indices.size());
    STLA_CORE_LOG_INFO("  Vertex Layout");
    for (stla::buffer_attribute& attrib : m_viking_room_mesh.m_layout.get_attribute_vector()) {
        STLA_CORE_LOG_INFO("    (%d) (type:%s) (instanced:%d)", attrib.m_index, stla::buffer_attribute_type_string(attrib.m_type), attrib.m_instanced);
    }

    if (!m_viking_vertex_buffer.alloc(
        m_viking_room_mesh.m_vertices.size() * sizeof(float),
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | 
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR,
        VMA_MEMORY_USAGE_CPU_TO_GPU,
        &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to allocate viking model vertex buffer");
        return false;
    }

    if (!m_viking_vertex_buffer.memcpy(m_viking_room_mesh.m_vertices.data(), m_viking_room_mesh.m_vertices.size() * sizeof(float))) {
        STLA_CORE_LOG_CRITICAL("Unable to copy viking vertex buffer");
        return false;
    }

    if (!m_viking_index_buffer.alloc(
        m_viking_room_mesh.m_indices.size() * sizeof(uint32_t),
        VK_BUFFER_USAGE_INDEX_BUFFER_BIT | 
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR,
        VMA_MEMORY_USAGE_CPU_TO_GPU,
        &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to allocate viking index buffer");
        return false;
    }

    if (!m_viking_index_buffer.memcpy(m_viking_room_mesh.m_indices.data(), m_viking_room_mesh.m_indices.size() * sizeof(uint32_t))) {
        STLA_CORE_LOG_CRITICAL("Unable to copy viking index buffer");
        return false;
    }

    stla::vk::descriptor_set_binding bindings[] = {
        { 0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT }
    };
    uint32_t binding_count = sizeof(bindings) / sizeof(stla::vk::descriptor_set_binding);

    if (!m_basic_shader_descriptor_set_layout.create(binding_count, &bindings[0], &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to create Descriptor Set Layout");
        return false;
    }

    

    if (!m_shader_parser.parse(SHADER_PATH_BASIC_P3N3UV2)) {
        STLA_CORE_LOG_CRITICAL("Unable to parse shaders from %s", SHADER_PATH_BASIC_P3N3UV2);
        return false;
    }

    if (!m_basic_vertex_shader.create(stla::vk::shader_type::vertex, m_shader_parser.m_vertex_source.c_str(), &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to create VERTEX shader module");
        return false;
    }
    
    if (!m_basic_fragment_shader.create(stla::vk::shader_type::fragment, m_shader_parser.m_fragment_source.c_str(), &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to create FRAGMENT shader module");
        return false;
    }

    stla::vk::shader shader_stages[] = { m_basic_vertex_shader, m_basic_fragment_shader };
    uint32_t shader_stage_count = sizeof(shader_stages) / sizeof(stla::vk::shader);
    if (!m_basic_pipeline.create(
        shader_stage_count, 
        &shader_stages[0], 
        m_viking_room_mesh.m_layout, 
        m_basic_shader_descriptor_set_layout, 
        &m_render_context)) {
        STLA_CORE_LOG_CRITICAL("Unable to create graphics pipeline");
        return false;
    }

    if (!m_basic_shader_ubo.alloc(
        sizeof(basic_shader_uniform_data),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU,
        &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to allocate uniform buffer");
        return false;
    }

    if (!m_basic_shader_descriptor_set.create(&m_basic_shader_descriptor_set_layout)) {
        STLA_CORE_LOG_CRITICAL("Unable to allocate descriptor set");
        return false;
    }

    if (!m_basic_shader_descriptor_set.bind_buffer(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, m_basic_shader_ubo)) {
        STLA_CORE_LOG_CRITICAL("Unable to bind ubo to descriptor set");
        return false;
    }

    stla::vk::descriptor_set_binding compute_bindings[] = {
        { 0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_COMPUTE_BIT }
    };
    uint32_t compute_binding_count = sizeof(compute_bindings) / sizeof(stla::vk::descriptor_set_binding);
    if (!m_compute_desc_layout.create(compute_binding_count, &compute_bindings[0], &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to create compute descriptor set");
        return false;
    }

    if (!m_compute_desc_set.create(&m_compute_desc_layout)) {
        STLA_CORE_LOG_ERROR("Unable to allocate compute descriptor set");
        return false;
    }

    if (!m_compute_image_buffer.alloc(
        800 * 600 * sizeof(float) * 3,
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VMA_MEMORY_USAGE_GPU_TO_CPU,
        &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to allocate compute image buffer");
        return false;
    }

    if (!m_compute_desc_set.bind_buffer(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, m_compute_image_buffer)) {
        STLA_CORE_LOG_CRITICAL("Unable to bind compute descriptor set buffer");
        return false;
    }

    if (!m_shader_parser.parse(SHADER_PATH_COMPUTE_RAY_TRACE)) {
        STLA_CORE_LOG_CRITICAL("Unable to parse rt compute shader");
        return false;
    }

    //if ((res = m_raygen_shader.create(stla::vk::shader_type::raygen, m_shader_parser.m_ray_gen_source.c_str(), &m_renderer)) != true) {
    if (!m_compute_shader.create(stla::vk::shader_type::compute, m_shader_parser.m_compute_source.c_str(), &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to create compute shader");
        return false;
    }

    m_running = true;
    return true;
}

void game_state::shutdown()
{
    if (mp_window) {

        //todo delete should happen when renderer isn't in the middle of rendering, destroy pushes objects to a delete queue in the renderer
        m_basic_pipeline.destroy();
        m_basic_shader_ubo.destroy();
        m_basic_shader_descriptor_set_layout.destroy();

        m_viking_vertex_buffer.destroy();
        m_viking_index_buffer.destroy();

        m_imgui.shutdown();

        m_render_target.destroy();
        m_render_context.destroy();
        m_renderer.shutdown();

        SDL_DestroyWindow(mp_window);
        SDL_Quit();
        mp_window = nullptr;
    }
}

void game_state::run()
{
    VkDeviceAddress vertex_buffer_address;
    VkDeviceAddress index_buffer_address;

    VkBufferDeviceAddressInfo buffer_info = { };
    buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    buffer_info.pNext = nullptr;
    buffer_info.buffer = m_viking_vertex_buffer.m_buffer;

    vertex_buffer_address = vkGetBufferDeviceAddress(m_renderer.m_device, &buffer_info);
    buffer_info.buffer = m_viking_index_buffer.m_buffer;
    index_buffer_address = vkGetBufferDeviceAddress(m_renderer.m_device, &buffer_info);

    STLA_CORE_LOG_INFO("Vertex Buffer Device Addr: 0x%08X", vertex_buffer_address);
    STLA_CORE_LOG_INFO("Index Buffer Device Addr: 0x%08X", index_buffer_address);

    // Specifies where AS builder can find the vertices and optional indices for triangle data, including formats and lengths
    VkAccelerationStructureGeometryTrianglesDataKHR as_geometry_data = { };
    as_geometry_data.sType                    = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR;
    as_geometry_data.vertexFormat             = VK_FORMAT_R32G32B32_SFLOAT;//todo when vertex is pos/normal/uv?
    as_geometry_data.vertexData.deviceAddress = vertex_buffer_address;
    as_geometry_data.vertexStride             = 8 * sizeof(float);
    as_geometry_data.indexType                = VK_INDEX_TYPE_UINT32;
    as_geometry_data.indexData.deviceAddress  = index_buffer_address;
    as_geometry_data.maxVertex                = (uint32_t)m_viking_room_mesh.m_vertices.size() - 1;//todo
    as_geometry_data.transformData            = { 0 };

    // Triangle can be marked as inactive by setting the X-component of each vertex to a floating point NaN
    VkAccelerationStructureGeometryKHR as_geometry = { };
    as_geometry.sType              = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
    as_geometry.geometryType       = VK_GEOMETRY_TYPE_TRIANGLES_KHR;
    as_geometry.geometry.triangles = as_geometry_data;
    as_geometry.flags              = VK_GEOMETRY_OPAQUE_BIT_KHR;

    VkAccelerationStructureBuildRangeInfoKHR range_info = { };
    range_info.firstVertex     = 0;
    range_info.primitiveCount  = (uint32_t)m_viking_room_mesh.m_indices.size() / 3;
    range_info.primitiveOffset = 0;
    range_info.transformOffset = 0;

    VkAccelerationStructureBuildGeometryInfoKHR build_info = { };
    build_info.sType         = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    build_info.flags         = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
    build_info.geometryCount = 1;
    build_info.pGeometries   = &as_geometry;
    build_info.mode          = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
    build_info.type          = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
    build_info.srcAccelerationStructure = VK_NULL_HANDLE;

    VkAccelerationStructureBuildSizesInfoKHR size_info = { };
    size_info.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
    vkGetAccelerationStructureBuildSizesKHR(
        m_renderer.m_device, VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
        &build_info, &range_info.primitiveCount, &size_info);

    STLA_CORE_LOG_INFO("Acceleration Structure Build Sizes");
    STLA_CORE_LOG_INFO("  Structure Size      : %d", size_info.accelerationStructureSize);
    STLA_CORE_LOG_INFO("  Update Scratch Size : %d", size_info.updateScratchSize);
    STLA_CORE_LOG_INFO("  Build Scratch Size  : %d", size_info.buildScratchSize);

    // Allocate a buffer for the acceleration structure
    stla::vk::buffer blas_buffer;
    if (!blas_buffer.alloc(
        size_info.accelerationStructureSize, 
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR | 
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | 
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VMA_MEMORY_USAGE_AUTO,
        &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to allocate accleration structure buffer");
    }

    VkAccelerationStructureKHR blas = { nullptr };

    // Create an emtpy acceleration structure object
    VkAccelerationStructureCreateInfoKHR as_create_info = { };
    as_create_info.sType  = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
    as_create_info.type   = build_info.type;
    as_create_info.size   = size_info.accelerationStructureSize;
    as_create_info.buffer = blas_buffer.m_buffer;
    as_create_info.offset = 0;
    VK_ASSERT(vkCreateAccelerationStructureKHR(m_renderer.m_device, &as_create_info, nullptr, &blas));

    build_info.dstAccelerationStructure = blas;

    // Allocate scratch buffer
    stla::vk::buffer blas_scratch_buffer;
    if (!blas_scratch_buffer.alloc(
        size_info.buildScratchSize,
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | 
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VMA_MEMORY_USAGE_AUTO,
        &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to allocate scratch buffer");
    }

    buffer_info.buffer = blas_scratch_buffer.m_buffer;
    build_info.scratchData.deviceAddress = vkGetBufferDeviceAddress(
        m_renderer.m_device, &buffer_info);

    stla::vk::command_buffer tmp_cmd_buffer;
    if (!m_renderer.get_graphics_pool().acquire_command_buffer(tmp_cmd_buffer)) {
        STLA_CORE_LOG_CRITICAL("Unable to acquire command buffer");
    }

    tmp_cmd_buffer.begin();
    tmp_cmd_buffer.build_acceleration_structure(1, &build_info, &range_info);
    tmp_cmd_buffer.end();
    m_renderer.submit_graphics(tmp_cmd_buffer);
    tmp_cmd_buffer.wait_fence();

    m_renderer.get_graphics_pool().return_command_buffer(tmp_cmd_buffer);

    STLA_CORE_LOG_INFO("BLAS Built Successfully!");

    VkAccelerationStructureDeviceAddressInfoKHR as_device_address_info = { };
    as_device_address_info.sType                 = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
    as_device_address_info.accelerationStructure = blas;

    VkDeviceAddress blas_address = vkGetAccelerationStructureDeviceAddressKHR(m_renderer.m_device, &as_device_address_info);

    VkAccelerationStructureInstanceKHR instance = {};
    // Set the instance transform to a 135- degree rotation around the y-axis.
    const float rcp_sqrt2 = sqrtf(0.5f);
    instance.transform.matrix [0][0]                = -rcp_sqrt2;
    instance.transform.matrix [0][2]                = rcp_sqrt2;
    instance.transform.matrix [1][1]                = 1.0f;
    instance.transform.matrix [2][0]                = -rcp_sqrt2;
    instance.transform.matrix [2][2]                = -rcp_sqrt2;
    instance.instanceCustomIndex                    = 0;
    instance.mask                                   = 0xFF;
    instance.instanceShaderBindingTableRecordOffset = 0;
    instance.flags                                  = VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR ;
    instance.accelerationStructureReference         = blas_address;

    // Create a buffer of instances
    VkAccelerationStructureInstanceKHR instance_list[] = { instance };
    const uint32_t instance_count = sizeof(instance_list) / sizeof(VkAccelerationStructureInstanceKHR);

    stla::vk::buffer blas_instance_buffer;
    if (!blas_instance_buffer.alloc(
        sizeof(instance_list),
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | 
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT |
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR,
        VMA_MEMORY_USAGE_CPU_TO_GPU,
        &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to allocate instance list buffer");
    }

    if (!blas_instance_buffer.memcpy(&instance_list[0], sizeof(instance_list))) {
        STLA_CORE_LOG_CRITICAL("Unable to copy instance list");
    }

    VkAccelerationStructureBuildRangeInfoKHR tlas_range_info;
    tlas_range_info.primitiveOffset = 0;
    tlas_range_info.primitiveCount  = instance_count; // Number of instances
    tlas_range_info.firstVertex     = 0;
    tlas_range_info.transformOffset = 0;

    VkAccelerationStructureGeometryInstancesDataKHR as_geometry_instance_data = { };
    as_geometry_instance_data.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR;
    as_geometry_instance_data.arrayOfPointers = VK_FALSE;
    
    buffer_info.buffer = blas_instance_buffer.m_buffer;
    as_geometry_instance_data.data.deviceAddress = vkGetBufferDeviceAddress(m_renderer.m_device, &buffer_info);

    // Like creating the BLAS, point the geometry (in this case, instances) in a polymorphic object
    VkAccelerationStructureGeometryKHR as_geometry_instance = { };
    as_geometry_instance.sType              = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
    as_geometry_instance.geometryType       = VK_GEOMETRY_TYPE_INSTANCES_KHR;
    as_geometry_instance.geometry.instances = as_geometry_instance_data;

    // Create the build info: in this case , pointing to only one geometry object.
    VkAccelerationStructureBuildGeometryInfoKHR tlas_build_info = { };
    tlas_build_info.sType                    = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    tlas_build_info.flags                    = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
    tlas_build_info.geometryCount            = 1;
    tlas_build_info.pGeometries              = &as_geometry_instance;
    tlas_build_info.mode                     = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
    tlas_build_info.type                     = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
    tlas_build_info.srcAccelerationStructure = VK_NULL_HANDLE;

    // Query the worst -case AS size and scratch space size based on the number of instances (in this case , 1).
    VkAccelerationStructureBuildSizesInfoKHR tlas_size_info = { };
    tlas_size_info.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
    vkGetAccelerationStructureBuildSizesKHR(
        m_renderer.m_device, VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR, &tlas_build_info,
        &tlas_range_info.primitiveCount, &tlas_size_info);

    // Allocate buffer for TLAS
    stla::vk::buffer tlas_buffer;
    if (!tlas_buffer.alloc(
        tlas_size_info.accelerationStructureSize,
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR | 
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | 
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VMA_MEMORY_USAGE_AUTO,
        &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to allocate TLAS buffer");
    }

    VkAccelerationStructureKHR tlas = nullptr;

    VkAccelerationStructureCreateInfoKHR tlas_create_info = { };
    tlas_create_info.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
    tlas_create_info.type = tlas_build_info.type;
    tlas_create_info.size = tlas_size_info.accelerationStructureSize;
    tlas_create_info.buffer = tlas_buffer.m_buffer;
    tlas_create_info.offset = 0;
    VK_ASSERT(vkCreateAccelerationStructureKHR(m_renderer.m_device, &tlas_create_info, nullptr, &tlas));

    tlas_build_info.dstAccelerationStructure = tlas;

    // Allocate scratch buffer for tlas
     // Allocate scratch buffer
    stla::vk::buffer tlas_scratch_buffer;
    if (!tlas_scratch_buffer.alloc(
        tlas_size_info.buildScratchSize,
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | 
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VMA_MEMORY_USAGE_AUTO,
        &m_renderer)) {
        STLA_CORE_LOG_CRITICAL("Unable to allocate scratch buffer for tlas");
    }

    buffer_info.buffer = tlas_scratch_buffer.m_buffer;
    tlas_build_info.scratchData.deviceAddress = vkGetBufferDeviceAddress(
        m_renderer.m_device, &buffer_info);

    stla::vk::command_buffer another_cmd_buffer;
    if (!m_renderer.get_graphics_pool().acquire_command_buffer(another_cmd_buffer)) {
        STLA_CORE_LOG_CRITICAL("Unable to acquire command buffer");
    }

    another_cmd_buffer.begin();
    another_cmd_buffer.build_acceleration_structure(1, &tlas_build_info, &tlas_range_info);
    another_cmd_buffer.end();
    m_renderer.submit_graphics(another_cmd_buffer);
    another_cmd_buffer.wait_fence();

    m_renderer.get_graphics_pool().return_command_buffer(another_cmd_buffer);

    // RT Compute Shader
    // Describes the entrypoint and the stage to use for this shader module in the pipeline
    VkPipelineShaderStageCreateInfo shader_stage_create_info = stla::vk::renderer::pipeline_shader_stage_create_info(
        VK_SHADER_STAGE_COMPUTE_BIT, m_compute_shader.get_module_handle());

    // Create emtpy pipeline layout
    VkPipelineLayout compute_pipeline_layout{ nullptr };
    VkPipelineLayoutCreateInfo compute_pipeline_layout_create_info = stla::vk::renderer::pipeline_layout_create_info();
    compute_pipeline_layout_create_info.setLayoutCount = 1;
    compute_pipeline_layout_create_info.pSetLayouts    = &m_compute_desc_layout.m_descriptor_layout;

    VK_ASSERT(vkCreatePipelineLayout(
        m_renderer.m_device, &compute_pipeline_layout_create_info, nullptr, &compute_pipeline_layout));

    VkComputePipelineCreateInfo compute_pipeline_create_info = { };
    compute_pipeline_create_info.sType  = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
    compute_pipeline_create_info.stage  = shader_stage_create_info;
    compute_pipeline_create_info.layout = compute_pipeline_layout;

    VkPipeline compute_pipeline{ nullptr };
    VK_ASSERT(vkCreateComputePipelines(
        m_renderer.m_device, nullptr, 1, &compute_pipeline_create_info, nullptr, &compute_pipeline));

    if (!m_renderer.get_compute_pool().acquire_command_buffer(another_cmd_buffer)) {
        STLA_CORE_LOG_CRITICAL("Unable to acquire command buffer");
    }

    another_cmd_buffer.begin();
    vkCmdBindPipeline(another_cmd_buffer.m_command_buffer, VK_PIPELINE_BIND_POINT_COMPUTE, compute_pipeline);
    //another_cmd_buffer.bind_descriptor_set(compute_pipeline, m_compute_desc_set);
    vkCmdBindDescriptorSets(
        another_cmd_buffer.m_command_buffer, VK_PIPELINE_BIND_POINT_COMPUTE, compute_pipeline_layout, 0, 1, &m_compute_desc_set.m_descriptor_set, 0, nullptr);
    vkCmdDispatch(another_cmd_buffer.m_command_buffer, 1, 1, 1);

    VkMemoryBarrier memory_barrier = { };
    memory_barrier.sType          = VK_STRUCTURE_TYPE_MEMORY_BARRIER;
    memory_barrier.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
    memory_barrier.dstAccessMask = VK_ACCESS_HOST_READ_BIT;

    vkCmdPipelineBarrier(
        another_cmd_buffer.m_command_buffer, 
        VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
        VK_PIPELINE_STAGE_HOST_BIT,
        0, 
        1, &memory_barrier,
        0, nullptr, 0, nullptr);

    another_cmd_buffer.end();

    m_renderer.submit_compute(another_cmd_buffer);
    //another_cmd_buffer.wait_fence();
            
    m_renderer.get_compute_pool().return_command_buffer(another_cmd_buffer);

    if (!m_renderer.get_transfer_pool().acquire_command_buffer(m_command_buffer)) {
        STLA_CORE_LOG_CRITICAL("Unable to acquire command buffer");
    }

    while (m_running) {

        m_dt.tick();
        etl::string<128> tick_string;
        etl::to_string(1000.0f / (float)m_dt, tick_string);
        SDL_SetWindowTitle(mp_window, tick_string.c_str());
        
    // Update
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            m_imgui.process_event(event);
            switch (event.type) 
            {
                case SDL_EVENT_QUIT: { m_running = false; break; }
            }
        }

        m_imgui.on_update();

    // Render
    /*
        glm::mat4 transform(1.0f);
        transform = glm::rotate(transform, 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
        transform = glm::rotate(transform, (float)SDL_GetTicks() / 2000.0f, glm::vec3(0.0f, 0.0f, 1.0f));

        basic_shader_uniform_data uniform_data = {
            m_camera.get_view_projection(),
            transform
        };

        m_basic_shader_ubo.memcpy(&uniform_data, sizeof(basic_shader_uniform_data));
        
        if (!m_render_context.get_command_pool().acquire_command_buffer(m_command_buffer)) {
            STLA_CORE_LOG_CRITICAL("Unable to acquire command buffer");
        }
        m_command_buffer.begin();
        m_command_buffer.begin_render_pass(m_render_context, m_render_target);
        m_command_buffer.bind_pipeline(m_basic_pipeline);
        m_command_buffer.bind_descriptor_set(m_basic_pipeline, m_basic_shader_descriptor_set);
        m_command_buffer.bind_vertex_buffer(m_viking_vertex_buffer);
        m_command_buffer.bind_index_buffer(m_viking_index_buffer);
        m_command_buffer.draw_indexed(m_viking_room_mesh.m_indices.size());
        m_command_buffer.end_render_pass();
        m_command_buffer.end();
        m_renderer.submit_graphics(m_command_buffer, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);
        m_command_buffer.wait_fence();
        m_render_context.get_command_pool().return_command_buffer(m_command_buffer);
    */

        /*
        VkDeviceSize                bufferOffset;
        uint32_t                    bufferRowLength;
        uint32_t                    bufferImageHeight;
        VkImageSubresourceLayers    imageSubresource;
        VkOffset3D                  imageOffset;
        VkExtent3D                  imageExtent;
        */

        m_command_buffer.begin();
        m_renderer.submit_transfer(m_command_buffer);
        VkBufferImageCopy img_copy = { };
        img_copy.bufferOffset = 0;
        img_copy.bufferRowLength = 800;
        img_copy.bufferImageHeight = 600;
        img_copy.imageExtent = { 1, m_render_target.get_color_attachment().m_width, m_render_target.get_color_attachment().m_height };

        vkCmdCopyBufferToImage(
            m_command_buffer.m_command_buffer, 
            m_compute_image_buffer.m_buffer, 
            m_render_target.get_color_attachment().m_image,
            VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
            1, &img_copy);
        m_command_buffer.end();

        vkQueueWaitIdle(m_renderer.m_transfer.m_queue);


        m_imgui.start_frame();
        
        {
            ImGui::Begin("Scene");
            ImVec2 viewport_panel_size = ImGui::GetContentRegionAvail();
            ImGui::Image(m_render_target_dset, ImVec2{ viewport_panel_size.x, viewport_panel_size.y });
            ImGui::End();
        }

        m_imgui.end_frame();

        m_renderer.flush_deletor_queue();
    }
}
