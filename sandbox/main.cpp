#include <stella.h>
#include <stella_editor.h>

int main()
{   
    stla::application::create();
    stla::editor &editor = stla::application::instance()
        .register_panel<stla::voxel_renderer_debug_panel>(true)
        .register_panel<stla::sandbox_panel>(true);
    editor.run();
    stla::application::destroy();

    return 0;
}