#type VERTEX
#version 450

layout (location = 0) in vec3 a_position;
layout (location = 1) in vec3 a_normal;
layout (location = 2) in vec2 a_uv;

layout(set = 0, binding = 0) uniform uniform_buffer {
	mat4 view_projection;
	mat4 transform;
} uniform_data;

layout (location = 0) out vec3 v_normal;

void main()
{
	v_normal    = a_normal;
	gl_Position = uniform_data.view_projection * uniform_data.transform * vec4(a_position, 1.0f);
}

#endtype
#type FRAGMENT
#version 450

// Shader input
layout (location = 0) in vec3 v_normal;

// Shader output
layout (location = 0) out vec4 out_frag_color;

void main()
{
	//return red
	out_frag_color = vec4(v_normal, 1.0f);
}
#endtype
