[shader(compute)]
#version 460
#extension GL_EXT_ray_query : require
#extension GL_EXT_debug_printf : require

// https://github.com/KhronosGroup/GLSL/blob/master/extensions/ext/GLSL_EXT_ray_query.txt

layout(local_size_x = 16, local_size_y = 8, local_size_z = 1) in;

layout(set = 0, binding = 0) buffer pixel_buffer_block
{
    vec4 pixels[];
};

layout(set = 0, binding = 1) uniform scene_data_block 
{
    vec4   background_color;
    vec3   camera_position;
};

layout(set = 0, binding = 2) uniform accelerationStructureEXT tlas;

layout(set = 0, binding = 3) buffer vertices_block
{
  float vertices[];
};

layout(set = 0, binding = 4) buffer indices_block
{
  uint indices[];
};

layout(set = 0, binding = 5) buffer voxel_block
{
    int voxels[];
};

#define FLATTEN(_x, _y, _z, _w, _d) (((((_x) * (_w)) + (_y)) * (_d)) + (_z))
//[0.0, voxel_dim]
//int voxel_structure[32 * 32 * 32];
bool voxel_is_active(ivec3 voxel_index)
{
    int vwidth = 32;
    int vdepth = 32;

    int index = FLATTEN(voxel_index.x, voxel_index.y, voxel_index.z, vwidth, vdepth);
    if (index < 0 || index >= (32 * 32 * 32)) {
        return false;
    }

    return voxels[index] == 1;
}

vec4 get_background_color(vec3 ray_origin, vec3 ray_dir) 
{
    vec3 unit_direction = normalize(ray_dir);
    float a = 0.5 * (unit_direction.y + 1.0);
    return vec4((1.0 - a) * vec3(1.0, 1.0, 1.0) + a * vec3(0.5, 0.7, 1.0), 1.0);
}

struct dda_traversal_record_t
{
    bool hit;
    vec3 color;
    vec3 world_backside_intersection;
};

vec3 ray_at(vec3 origin, vec3 direction, float t)
{
    return origin + (direction * t);
}

dda_traversal_record_t dda_traversal(rayQueryEXT ray_query)
{
    dda_traversal_record_t record;
    record.hit   = false;
    record.color = vec3(0.0);

    float intersection_t   = rayQueryGetIntersectionTEXT(ray_query, true);
    vec3 obj_ray_origin    = rayQueryGetIntersectionObjectRayOriginEXT(ray_query, true);
    vec3 obj_ray_direction = rayQueryGetIntersectionObjectRayDirectionEXT(ray_query, true);

    const vec3 aabb_intersection = ray_at(obj_ray_origin, obj_ray_direction, intersection_t);

    const int vwidth  = 32;
    const int vheight = 32;
    const int vdepth  = 32;
    const float max_t = 1.0;
    float current_t   = 0.0;

    const float _bin_size = 1.0;// / 32.0;

    // [-0.5, 0.5]
    const float threshold = 0.499;
    vec3 ray_start = aabb_intersection;
    
    if (aabb_intersection.x >= threshold) {
        ray_start = ray_at(aabb_intersection, obj_ray_direction, -0.00001);
    }
    else if (aabb_intersection.y >= threshold) {
        ray_start = ray_at(aabb_intersection, obj_ray_direction, -0.00001);
    }
    else if (aabb_intersection.z >= threshold) {
        ray_start = ray_at(aabb_intersection, obj_ray_direction, -0.00001);
    }
    else if (aabb_intersection.x <= -threshold) {
        ray_start = ray_at(aabb_intersection, obj_ray_direction, 0.00001);
    }
    else if (aabb_intersection.y <= -threshold) {
        ray_start = ray_at(aabb_intersection, obj_ray_direction, 0.00001);
    }
    else if (aabb_intersection.z <= -threshold) {
        ray_start = ray_at(aabb_intersection, obj_ray_direction, 0.00001);
    }

    //vec3 ray_end = ray_at(aabb_intersection, obj_ray_direction, 1.0);

    // [0.0, 1.0]
    ray_start += vec3(0.5);
    //ray_end   += vec3(0.5);

    if (ray_start.x > 1.0) { ray_start.x = 1.0; } 
    if (ray_start.y > 1.0) { ray_start.y = 1.0; } 
    if (ray_start.z > 1.0) { ray_start.z = 1.0; }
    if (ray_start.x < 0.0) { ray_start.x = 0.0; }
    if (ray_start.y < 0.0) { ray_start.y = 0.0; }
    if (ray_start.z < 0.0) { ray_start.z = 0.0; }

    // [0.0, volume_size]
    ray_start *= 32.0;
    //ray_end   *= 32.0;
    //if (ray_start.x > 32.0) { ray_start.x = 32.0; }
    //if (ray_start.y > 32.0) { ray_start.y = 32.0; }
    //if (ray_start.z > 32.0) { ray_start.z = 32.0; }
    //if (ray_start.x > -0.25 && ray_start.x < 0.25) { ray_start.x = 0.0; }
    //if (ray_start.y > -0.25 && ray_start.y < 0.25) { ray_start.y = 0.0; }
    //if (ray_start.z > -0.25 && ray_start.z < 0.25) { ray_start.z = 0.0; }

    vec3 rayPos = ray_start;
    vec3 rayDir = obj_ray_direction;
    const float offset = 0.001;

    ivec3 mapPos;
    
    mapPos = ivec3(floor(rayPos + 0.));
    //ivec3 mapPos = ivec3(floor(rayPos + 0.));
    //if (rayPos.x < 1.0 || rayPos.y < 1.0 || rayPos.z < 1.0) {
        //mapPos = ivec3(floor(rayPos - offset));
    //}
    //else {
    //}

    vec3 map_pos = vec3(mapPos.x, mapPos.y, mapPos.z) / 32.0;
    vec3 deltaDist = abs(vec3(length(rayDir)) / rayDir);        
    ivec3 rayStep = ivec3(sign(rayDir));
    vec3 sideDist = (sign(rayDir) * (vec3(mapPos) - rayPos) + (sign(rayDir) * 0.5) + 0.5) * deltaDist; 
    
    bvec3 mask;

#define MAX_RAY_STEPS (64)
    for (int i = 0; i < MAX_RAY_STEPS; i++) {
        if (voxel_is_active(mapPos)) { break; }
        
        if (sideDist.x < sideDist.y) {
            if (sideDist.x < sideDist.z) {
                sideDist.x += deltaDist.x;
                mapPos.x += rayStep.x;
                mask = bvec3(true, false, false);
            }
            else {
                sideDist.z += deltaDist.z;
                mapPos.z += rayStep.z;
                mask = bvec3(false, false, true);
            }
        }
        else {
            if (sideDist.y < sideDist.z) {
                sideDist.y += deltaDist.y;
                mapPos.y += rayStep.y;
                mask = bvec3(false, true, false);
            }
            else {
                sideDist.z += deltaDist.z;
                mapPos.z += rayStep.z;
                mask = bvec3(false, false, true);
            }
        }
    }

    vec3 color;
    if (mask.x) {
        color = vec3(0.5);
        //record.hit = true;
    }
    if (mask.y) {
        color = vec3(1.0);
       // record.hit = true;
    }
    if (mask.z) {
        color = vec3(0.75);
        // record.hit = true;
    }
    
    
    if (mapPos.x >= 0 && mapPos.x < 32 && 
        mapPos.y >= 0 && mapPos.y < 32 && 
        mapPos.z >= 0 && mapPos.z < 32) {
        //pixel_color = vec4(color, 1.0);
        //record.hit   = true;
        record.hit   = (mask.x || mask.y || mask.z);
        record.color = color;
    }

    if (!record.hit) {
        //record.color = vec3(1.0, 0.0, 0.0);
        //record.hit = true;
        //return record;

        float t = 0.0;

        mat4x3 object_to_world = rayQueryGetIntersectionObjectToWorldEXT(ray_query, true);
        //vec3 aabb_enter_point  = (ray_start / 32.0) - 0.5;  //ray_at(obj_ray_origin, obj_ray_direction, intersection_t + 0.00001);
        const vec3 aabb_enter_point = ray_at(aabb_intersection, obj_ray_direction, 0.00001);

        for (int i = 0; i < 3; i++) {
            if (obj_ray_direction[i] != 0.0) {
                t = (0.5 - aabb_enter_point[i]) / obj_ray_direction[i];

                if (t < 0) {
                    t = (-0.5 - aabb_enter_point[i]) / obj_ray_direction[i];
                }

                vec3 test_point = ray_at(aabb_enter_point, obj_ray_direction, t);
                //test_point = ray_at(test_point, -obj_ray_direction, 0.001);


                if (all(greaterThanEqual(test_point + vec3(0.00001), vec3(-0.5))) && all(lessThanEqual(test_point - vec3(0.00001), vec3(0.5)))) {
                    record.world_backside_intersection = vec3(object_to_world * vec4(test_point, 1.0));
                    
                    break;
                }
            }
        }

        //vec3 object_backside_intersection  = (mapPos / 32.0) - 0.5;
        //object_backside_intersection       = ray_at(object_backside_intersection, obj_ray_direction, 0.001); 
        //mat4x3 object_to_world             = rayQueryGetIntersectionObjectToWorldEXT(ray_query, true);
        //record.world_backside_intersection = vec3(object_to_world * vec4(object_backside_intersection, 1.0)); 
        //vec4(ray_at(aabb_intersection, obj_ray_direction, 1.0), 1.0));
    }

    return record;
}

void main()
{
    const uvec2 resolution = uvec2(1600, 900);
    const uvec2 pixel      = gl_GlobalInvocationID.xy;
    const uint pixel_index = resolution.x * pixel.y + pixel.x;

    // If the pixel is outside of the image, don't do anything:
    if ((pixel.x >= resolution.x) || (pixel.y >= resolution.y)) {
        return;
    }

    float aspect_ratio    = float(resolution.x) / float(resolution.y);
    uint image_width      = resolution.x;
    uint image_height     = resolution.y;
    float viewport_height = 2.0;
    float viewport_width  = viewport_height * (float(image_width) / float(image_height));
    float focal_length    = 1.0;
    vec3 camera_center    = camera_position;

    // Calculate the vectors across the horizontal and down the vertical viewport edges.
    vec3 viewport_u = vec3(viewport_width, 0, 0);
    vec3 viewport_v = vec3(0, -viewport_height, 0);

    // Calculate the horizontal and vertical delta vectors from pixel to pixel.
    vec3 pixel_delta_u = viewport_u / float(image_width);
    vec3 pixel_delta_v = viewport_v / float(image_height);

    // Calculate the location of the upper left pixel.
    vec3 viewport_upper_left = camera_center - vec3(0, 0, focal_length) - viewport_u / 2 - viewport_v / 2;
    vec3 pixel00_loc         = viewport_upper_left + 0.5 * (pixel_delta_u + pixel_delta_v);

    vec3 pixel_center  = pixel00_loc + (pixel.x * pixel_delta_u) + (pixel.y * pixel_delta_v);
    vec3 ray_direction = normalize(pixel_center - camera_center);
    vec3 ray_origin    = camera_center;

    // Trace Rays
    const float max_t    = 10000.0;
    const float min_t    = 0.1;
    const uint max_iters = 10;
    bool hit             = false;
    vec4 final_color     = vec4(0.0);

    vec3 ro = ray_origin;
    vec3 rd = ray_direction;
    for (int i = 0; i < max_iters; i++) {
        rayQueryEXT ray_query;
        rayQueryInitializeEXT(ray_query, tlas, 0, 0xFF, ro, min_t, rd, max_t);

        float closest_t = max_t;
        while (rayQueryProceedEXT(ray_query)) { }

        if (rayQueryGetIntersectionTypeEXT(ray_query, true) == gl_RayQueryCommittedIntersectionTriangleEXT) {
            const int primitive_id = rayQueryGetIntersectionPrimitiveIndexEXT(ray_query, true);

            const uint i0 = indices[3 * primitive_id + 0];
            const uint i1 = indices[3 * primitive_id + 1];
            const uint i2 = indices[3 * primitive_id + 2];

            const vec3 v0 = vec3(vertices[3 * i0 + 0], vertices[3 * i0 + 1], vertices[3 * i0 + 2]);
            const vec3 v1 = vec3(vertices[3 * i1 + 0], vertices[3 * i1 + 1], vertices[3 * i1 + 2]);
            const vec3 v2 = vec3(vertices[3 * i2 + 0], vertices[3 * i2 + 1], vertices[3 * i2 + 2]);

            mat4x3 object_to_world = rayQueryGetIntersectionObjectToWorldEXT(ray_query, true);
            mat3x3 normal_matrix   = transpose(inverse(mat3x3(object_to_world)));

            const vec3 object_normal = normalize(cross(v1 - v0, v2 - v0));
            const vec3 world_normal  = normalize(normal_matrix * object_normal);

            dda_traversal_record_t dda_record = dda_traversal(ray_query);
            if (dda_record.hit) {
                final_color = vec4(dda_record.color, 1.0);
                hit = true;
                break;
            }
            else {
                ro = dda_record.world_backside_intersection;
            }
        }
    }

    if (!hit) {
        final_color = get_background_color(ray_origin, ray_direction);
    }
    
    pixels[pixel_index] = final_color;
}