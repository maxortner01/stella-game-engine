[shader(vertex)]
#version 460

layout (location = 0) in vec2 a_position;
layout (location = 1) in vec2 a_velocity;
layout (location = 2) in vec4 a_color;

layout (location = 0) out vec3 v_color;

void main() {

    gl_PointSize = 14.0;
    gl_Position = vec4(a_position.xy, 1.0, 1.0);
    v_color = a_color.rgb;
}

[shader(fragment)]
#version 460

layout (location = 0) in vec3 v_color;

layout (location = 0) out vec4 f_color;

void main() {
    vec2 coord = gl_PointCoord - vec2(0.5);
    f_color = vec4(v_color, 0.5 - length(coord));
}
