[shader(vertex)]
#version 460

layout (location = 0) in vec3 a_position;

layout (set = 0, binding = 0) uniform uniform_buffer {
	mat4 view_projection;
	mat4 transform;
} uniform_data;

layout (set = 0, binding = 1) uniform uniform_buffer_1 {
	float red;
} uniform_data_1;

layout (location = 0) out float v_red;
layout (location = 1) out vec3 v_position;

void main()
{
	v_position  = a_position;
	v_red       = uniform_data_1.red;
	gl_Position = uniform_data.view_projection * uniform_data.transform * vec4(a_position, 1.0f);
}

[shader(fragment)]
#version 460

layout (location = 0) in float v_red;
layout (location = 1) in vec3 v_position;

layout (location = 0) out vec4 out_frag_color;

void main()
{
	//return red
	out_frag_color = vec4(vec3(v_position.x + v_red, v_position.y, v_position.z), 1.0f);
}
