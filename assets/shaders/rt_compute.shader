[shader(compute)]
#version 460
#extension GL_EXT_ray_query : require
#extension GL_EXT_debug_printf : require

#define DIM ((32 + 2))

// https://github.com/KhronosGroup/GLSL/blob/master/extensions/ext/GLSL_EXT_ray_query.txt

uint step_random(uint random_state);
float step_and_output_random_float(inout uint random_state);
float step_and_output_random_float(inout uint random_state, float min, float max)
{
    return (step_and_output_random_float(random_state) * (max - min)) + min; 
}

vec3 step_and_output_random_vec3(inout uint random_state) 
{
    return vec3(step_and_output_random_float(random_state), step_and_output_random_float(random_state), step_and_output_random_float(random_state));
}

vec3 step_and_output_random_vec3(inout uint random_state, float min, float max) 
{
    return vec3(
        step_and_output_random_float(random_state, min, max),
        step_and_output_random_float(random_state, min, max),
        step_and_output_random_float(random_state, min, max)
    );
}

vec3 step_and_output_random_in_unit_sphere(inout uint random_state) 
{
    while (true) 
    {
        vec3 p = step_and_output_random_vec3(random_state, -1, 1);
        if (dot(p, p) < 1) { 
            return p;
        }
    }
}

vec3 step_and_output_random_unit_vector(inout uint random_state) 
{
    return normalize(step_and_output_random_in_unit_sphere(random_state));
}

vec3 step_and_output_random_on_hemisphere(inout uint random_state, vec3 normal) 
{
    vec3 on_unit_sphere = step_and_output_random_unit_vector(random_state);
    if (dot(on_unit_sphere, normal) > 0.0) {
        // In the same hemisphere as the normal
        return on_unit_sphere;
    }
    else {
        return -on_unit_sphere;
    }
}

struct aabb_t
{
    vec3 min;
    vec3 max;
};

struct ray_t
{
    vec3 origin;
    vec3 direction;
};

ray_t new_ray(vec3 o, vec3 d);
vec3 ray_at(ray_t r, float t);
float ray_aabb_intersection(ray_t ray, vec3 aabb_min, vec3 aabb_max, float max_t);

struct dda_result_t
{
    bool hit;
    ivec3 position;
    vec3 normal;
};

dda_result_t new_dda_result(bool hit, ivec3 position, vec3 normal);
dda_result_t dda_query(ray_t object_ray, float intersection, aabb_t aabb, ivec3 dims);

struct voxel_ray_query_t
{
    bool hit;
    float t;
    ivec3 voxel_position;
    vec3 normal;
    vec2 uv;
};

voxel_ray_query_t new_voxel_ray_query(float max_t);
voxel_ray_query_t voxel_ray_query_hit(ray_t ray, float min_t, float max_t);

struct instance_descriptor_t
{
    int grid_desc_id;
};

struct voxel_grid_descriptor_t
{
    int voxel_data_start;
    vec3 dims;
};

aabb_t get_aabb(uint geometry_id);
voxel_grid_descriptor_t get_voxel_grid_descriptor(uint grid_id);
instance_descriptor_t get_instance_descriptor(uint instance_id);

layout (local_size_x = 16, local_size_y = 8, local_size_z = 1) in;

layout (set = 0, binding = 0) buffer pixel_buffer_block
{
    vec4 pixels[];
};

layout (set = 0, binding = 1) uniform scene_data_block 
{
    vec4 camera_position;
    float time_ms;
    int random_seed;
};

layout (set = 0, binding = 2) uniform accelerationStructureEXT tlas;

layout (set = 0, binding = 3) buffer aabb_block
{
    float g_aabb_buffer[];
};

layout (set = 0, binding = 4) buffer voxel_block
{
    int voxels[];
};

layout (set = 0, binding = 5) buffer voxel_grid_descriptor_block
{
    int g_voxel_grid_descriptor_buffer[];
};

layout (set = 0, binding = 6) buffer instance_descriptor_block
{
    int g_instance_descriptor_buffer[];
};

#define FLATTEN(_x, _y, _z, _w, _d) (((((_x) * (_w)) + (_y)) * (_d)) + (_z))

bool voxel_is_active(ivec3 voxel_index)
{
    ivec3 dims = ivec3(DIM);

    if (voxel_index.x < 0 || voxel_index.x >= dims.x) {
        return false;
    }

    if (voxel_index.y < 0 || voxel_index.y >= dims.y) {
        return false;
    }

    if (voxel_index.z < 0 || voxel_index.z >= dims.z) {
        return false;
    }

    int vwidth = dims.x;
    int vdepth = dims.z;

    int index = FLATTEN(voxel_index.x, voxel_index.y, voxel_index.z, vwidth, vdepth);
    if (index < 0 || index >= (dims.x * dims.y * dims.z)) {
        return false;
    }

    return voxels[index] == 1;
}

vec4 get_background_color(vec3 ray_origin, vec3 ray_dir) 
{
    vec3 unit_direction = normalize(ray_dir);
    float a = 0.5 * (unit_direction.y + 1.0);
    return vec4((1.0 - a) * vec3(1.0, 1.0, 1.0) + a * vec3(0.5, 0.7, 1.0), 1.0);
}

vec4 ray_color(ray_t ray, float min_t, float max_t, inout voxel_ray_query_t voxel_query) 
{       
    voxel_query = voxel_ray_query_hit(ray, min_t, max_t);

    if (voxel_query.hit) {
        //vec3 direction = step_and_output_random_on_hemisphere(random_state, voxel_query.normal);
        //return 0.5 * ray_color(new_ray(ray_at(ray, voxel_query.t), direction), min_t, max_t, max_depth - 1, random_state);
        //final_color += vec4(voxel_query.normal, 1.0);
        return vec4(voxel_query.normal, 1.0);
    }
    else {
        return get_background_color(ray.origin, ray.direction);
    }
}

void main()
{
    const uvec2 resolution = uvec2(1600, 900);
    const uvec2 pixel      = gl_GlobalInvocationID.xy;
    const uint pixel_index = resolution.x * pixel.y + pixel.x;

    // If the pixel is outside of the image, don't do anything:
    if ((pixel.x >= resolution.x) || (pixel.y >= resolution.y)) {
        return;
    }

    float aspect_ratio    = float(resolution.x) / float(resolution.y);
    uint samples          = 2;
    uint image_width      = resolution.x;
    uint image_height     = resolution.y;
    float viewport_height = 2.0;
    float viewport_width  = viewport_height * (float(image_width) / float(image_height));
    float focal_length    = 1.0;
    vec3 camera_center    = vec3(camera_position.xyz);

    // Calculate the vectors across the horizontal and down the vertical viewport edges.
    vec3 viewport_u = vec3(viewport_width, 0, 0);
    vec3 viewport_v = vec3(0, -viewport_height, 0);

    // Calculate the horizontal and vertical delta vectors from pixel to pixel.
    vec3 pixel_delta_u = viewport_u / float(image_width);
    vec3 pixel_delta_v = viewport_v / float(image_height);

    // Calculate the location of the upper left pixel.
    vec3 viewport_upper_left = camera_center - vec3(0, 0, focal_length) - viewport_u / 2 - viewport_v / 2;
    vec3 pixel00_loc         = viewport_upper_left + 0.5 * (pixel_delta_u + pixel_delta_v);

    vec3 pixel_center  = pixel00_loc + (pixel.x * pixel_delta_u) + (pixel.y * pixel_delta_v);

    // Trace Rays
    const float max_t = 10000.0;
    const float min_t = 0.0;
    uint max_depth    = 32;
    vec4 final_color  = vec4(0.0);

    uint random_state = random_seed * resolution.x * pixel.y + pixel.x;  // Initial seed

    for (int s = 0; s < samples; s++) {
        float px = -0.5 + step_and_output_random_float(random_state);
        float py = -0.5 + step_and_output_random_float(random_state);

        vec3 pixel_sample = pixel_center + vec3((px * pixel_delta_u) + (py * pixel_delta_v));
        ray_t ray = new_ray(camera_center, normalize(pixel_sample - camera_center));

        vec4 accumulated_color = vec4(1.0);
        for (int traced_segment = 0; traced_segment < max_depth; traced_segment++) {
            voxel_ray_query_t voxel_query = voxel_ray_query_hit(ray, min_t, max_t);

            if (voxel_query.hit) {
                accumulated_color *= 0.75 * vec4(voxel_query.normal, 1.0);
                const float theta = 6.2831853 * step_and_output_random_float(random_state);   // Random in [0, 2pi]
                const float u     = 2.0 * step_and_output_random_float(random_state) - 1.0;  // Random in [-1, 1]
                const float r     = sqrt(1.0 - u * u);
                
                ray = new_ray(ray_at(ray, voxel_query.t), voxel_query.normal + vec3(r * cos(theta), r * sin(theta), u));
                // ray.origin = ray_at(ray, 0.25); //todo start - intersection issues when starting inside volume

                // /if (traced_segment == 1)
                    //break;
            }
            else {
                accumulated_color *= get_background_color(ray.origin, ray.direction);;
                final_color += accumulated_color;
                break;
            }
        }
        //final_color += accumulated_color;
    }

    pixels[pixel_index] = final_color / float(samples);
}

uint step_random(uint random_state)
{
    return random_state * 747796405 + 1;
}

// Steps the RNG and returns a floating-point value between 0 and 1 inclusive.
float step_and_output_random_float(inout uint random_state)
{
    // Condensed version of pcg_output_rxs_m_xs_32_32, with simple conversion to floating-point [0,1].
    random_state  = step_random(random_state);
    uint word = ((random_state >> ((random_state >> 28) + 4)) ^ random_state) * 277803737;
    word      = (word >> 22) ^ word;
    return float(word) / 4294967295.0f;
}

ray_t new_ray(vec3 o, vec3 d)
{
    ray_t r;
    r.origin    = o;
    r.direction = normalize(d);
    return r;
}

vec3 ray_at(ray_t r, float t)
{
    return r.origin + (r.direction * t);
}

float ray_aabb_intersection(ray_t ray, aabb_t aabb, float max_t)
{
    // Inside aabb
    if (all(greaterThanEqual(ray.origin, aabb.min)) && all(lessThanEqual(ray.origin, aabb.max))) {
        return 0.0;
    }

    float tmin = -max_t;
    float tmax =  max_t;

    for (int i = 0; i < 3; i++) {
        float t1 = (aabb.max[i] - ray.origin[i]) / ray.direction[i];
        float t2 = (aabb.min[i] - ray.origin[i]) / ray.direction[i];

        tmin = max(tmin, min(t1, t2));
        tmax = min(tmax, max(t1, t2));
    }

    return tmin;
}

dda_result_t new_dda_result(bool hit, ivec3 position, vec3 normal)
{
    dda_result_t result;
    result.hit      = hit;
    result.position = position;
    result.normal   = normal;
    return result;
}

dda_result_t dda_query(ray_t object_ray, float intersection, aabb_t aabb, ivec3 dims)
{
    ivec3 voxel_position = ivec3(-1);

    const vec3 aabb_object_intersection = ray_at(object_ray, intersection);
    object_ray.origin = aabb_object_intersection;
    vec3 ray_start = object_ray.origin;

    // to -> [0.0, 1.0]
    ray_start = (ray_start - aabb.min) / (aabb.max - aabb.min);

    // [0.0, volume_size]
    ray_start *= vec3(dims);

    vec3 dda_ray_position  = ray_start;
    vec3 dda_ray_direction = object_ray.direction;
    ivec3 dda_map_position = ivec3(floor(dda_ray_position + 0.));

    vec3 dda_delta_distance = abs(vec3(length(dda_ray_direction)) / dda_ray_direction);        
    ivec3 dda_ray_step      = ivec3(sign(dda_ray_direction));
    vec3 dda_side_distance  = (sign(dda_ray_direction) * (vec3(dda_map_position) - dda_ray_position) + (sign(dda_ray_direction) * 0.5) + 0.5) * dda_delta_distance; 
    
    bvec3 dda_hit_mask = bvec3(false);

#define MAX_RAY_STEPS (256) //todo dependent on volume dims
    for (int i = 0; i < MAX_RAY_STEPS; i++) {
        if (any(greaterThanEqual(dda_map_position, dims))) {
            return new_dda_result(false, ivec3(-1.0), vec3(0.0));
        }

        if (any(lessThan(dda_map_position, vec3(0.0)))) {
            return new_dda_result(false, ivec3(-1.0), vec3(0.0));
        }
        
        if (voxel_is_active(dda_map_position)) { return new_dda_result(true, dda_map_position, clamp(vec3(dda_hit_mask), 0.0, 1.0)); }

        dda_hit_mask = lessThanEqual(dda_side_distance.xyz, min(dda_side_distance.yzx, dda_side_distance.zxy));        
        dda_side_distance += vec3(dda_hit_mask) * dda_delta_distance;
        dda_map_position += ivec3(vec3(dda_hit_mask)) * dda_ray_step;
    }

    return new_dda_result(false, ivec3(-1.0), vec3(0.0));
}

voxel_ray_query_t new_voxel_ray_query(float max_t)
{
    voxel_ray_query_t query;
    query.hit            = false;
    query.t              = max_t;
    query.voxel_position = ivec3(-1);
    query.normal         = vec3(0.0);
    query.uv             = vec2(0.0);
    return query;
}

voxel_ray_query_t voxel_ray_query_hit(ray_t ray, float min_t, float max_t)
{
    //todo pull from buffers
    const ivec3 voxel_dims = ivec3(DIM);
    const vec3 voxel_size  = vec3(0.03125);

    voxel_ray_query_t voxel_ray_query = new_voxel_ray_query(max_t);
    
    rayQueryEXT ray_query;
    rayQueryInitializeEXT(ray_query, tlas, 0, 0xFF, ray.origin, min_t, ray.direction, max_t);

    while (rayQueryProceedEXT(ray_query)) { 
        ray_t object_ray = new_ray(
            rayQueryGetIntersectionObjectRayOriginEXT(ray_query, false),
            rayQueryGetIntersectionObjectRayDirectionEXT(ray_query, false)
        );

        // instance_descriptor_t instance_desc = get_instance_descriptor(rayQueryGetIntersectionInstanceIdEXT(ray_query, false));

        int geometry_index = rayQueryGetIntersectionGeometryIndexEXT(ray_query, false);
        aabb_t aabb        = get_aabb(geometry_index);

        float aabb_intersection = ray_aabb_intersection(object_ray, aabb, max_t) + 0.0001; // push into volume a bit

        if (aabb_intersection < voxel_ray_query.t) {
            dda_result_t dda_result = dda_query(object_ray, aabb_intersection, aabb, voxel_dims);
            if (dda_result.hit) {
                aabb_t voxel_aabb;
                voxel_aabb.min  = ((vec3(dda_result.position) / vec3(voxel_dims)) * (aabb.max - aabb.min)) + aabb.min;
                voxel_aabb.max  = voxel_aabb.min + voxel_size;
                float voxel_t   = ray_aabb_intersection(object_ray, voxel_aabb, max_t);

                if (voxel_t < voxel_ray_query.t) {    
                    // calculate uv
                    vec3 voxel_hit = ray_at(object_ray, voxel_t);
                    vec3 local     = (voxel_hit - voxel_aabb.min) / (voxel_aabb.max - voxel_aabb.min);
                    vec2 uv        = vec2(0.0);
                    dda_result.normal.x *= -sign(object_ray.direction.x);
                    dda_result.normal.y *= -sign(object_ray.direction.y);
                    dda_result.normal.z *= -sign(object_ray.direction.z);

                    if (dda_result.normal.z == 1.0) {
                        uv = vec2(local.x, 1.0 - local.y);
                    }
                    else if (dda_result.normal.z == -1.0) {
                        uv = vec2(1.0 - local.x, 1.0 - local.y);
                    }
                    else if (dda_result.normal.y == 1.0) {
                        uv = vec2(local.x, local.z);
                    }
                    else if (dda_result.normal.y == -1.0) {
                        uv = vec2(local.x, local.z);
                    }
                    else if (dda_result.normal.x == 1.0) {
                        uv = vec2(1.0 - local.z, 1.0 - local.y);
                    }
                    else if (dda_result.normal.x == -1.0) {
                        uv = vec2(local.z, 1.0 - local.y);
                    }

                    mat4x3 object_to_world  = rayQueryGetIntersectionObjectToWorldEXT(ray_query, false);
                    mat3x3 normal_matrix    = transpose(inverse(mat3x3(object_to_world)));
                    dda_result.normal = normalize(normal_matrix * dda_result.normal);
                    
                    voxel_ray_query.hit            = true;
                    voxel_ray_query.t              = voxel_t;
                    voxel_ray_query.voxel_position = dda_result.position;
                    voxel_ray_query.normal         = dda_result.normal;
                    voxel_ray_query.uv             = uv;
                } 
            }
        }
    }

    return voxel_ray_query;
}


aabb_t get_aabb(uint geometry_id)
{
    const uint aabb_size  = 32;
    const uint aabb_start = geometry_id * aabb_size;

    aabb_t aabb;
    aabb.min = vec3(g_aabb_buffer[aabb_start + 0], g_aabb_buffer[aabb_start + 1], g_aabb_buffer[aabb_start + 2]);
    aabb.max = vec3(g_aabb_buffer[aabb_start + 3], g_aabb_buffer[aabb_start + 4], g_aabb_buffer[aabb_start + 5]);
    return aabb;
}

voxel_grid_descriptor_t get_voxel_grid_descriptor(uint grid_id)
{
    const uint voxel_grid_descriptor_size  = 4;
    const uint voxel_grid_descriptor_start = grid_id * voxel_grid_descriptor_size;

    voxel_grid_descriptor_t desc;
    desc.voxel_data_start = g_voxel_grid_descriptor_buffer[voxel_grid_descriptor_start + 0];
    desc.dims             = vec3(
        g_voxel_grid_descriptor_buffer[voxel_grid_descriptor_start + 1],
        g_voxel_grid_descriptor_buffer[voxel_grid_descriptor_start + 2],
        g_voxel_grid_descriptor_buffer[voxel_grid_descriptor_start + 3]
    );
    return desc;
}

instance_descriptor_t get_instance_descriptor(uint instance_id)
{
    const uint instance_descriptor_size  = 1;
    const uint instance_descriptor_start = instance_id * instance_descriptor_size;

    instance_descriptor_t desc;
    desc.grid_desc_id = g_instance_descriptor_buffer[instance_descriptor_start + 0];
    return desc;
}

/*
if (dda_side_distance.x < dda_side_distance.y) {
    if (dda_side_distance.x < dda_side_distance.z) {
        dda_side_distance.x += dda_delta_distance.x;
        dda_map_position.x += dda_ray_step.x;
        dda_hit_mask = bvec3(true, false, false);
    }
    else {
        dda_side_distance.z += dda_delta_distance.z;
        dda_map_position.z += dda_ray_step.z;
        dda_hit_mask = bvec3(false, false, true);
    }
}
else {
    if (dda_side_distance.y < dda_side_distance.z) {
        dda_side_distance.y += dda_delta_distance.y;
        dda_map_position.y += dda_ray_step.y;
        dda_hit_mask = bvec3(false, true, false);
    }
    else {
        dda_side_distance.z += dda_delta_distance.z;
        dda_map_position.z += dda_ray_step.z;
        dda_hit_mask = bvec3(false, false, true);
    }
}
*/