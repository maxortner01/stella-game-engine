[shader(compute)]
#version 460
#extension GL_EXT_ray_query : require

layout(local_size_x = 16, local_size_y = 8, local_size_z = 1) in;

layout(set = 0, binding = 0) buffer pixel_buffer_block
{
    vec4 pixels[800 * 600];
};

layout(set = 0, binding = 1) uniform scene_data_block 
{
    vec4   background_color;
    mat4x4 view_matrix;
    float  focus_distance;
    vec3   light_positions[16];
    vec3   light_colors[16];
};

layout(set = 0, binding = 2) uniform accelerationStructureEXT tlas;

void main()
{
    // The resolution of the buffer, which in this case is a hardcoded vector
    // of 2 unsigned integers:
    const uvec2 resolution = uvec2(800, 600);

    // Get the coordinates of the pixel for this invocation:
    //
    // .-------.-> x
    // |       |
    // |       |
    // '-------'
    // v
    // y
    const uvec2 pixel = gl_GlobalInvocationID.xy;

    // If the pixel is outside of the image, don't do anything:
    if ((pixel.x >= resolution.x) || (pixel.y >= resolution.y)) {
        return;
    }

    // Create a vector of 3 floats with a different color per pixel.
    const vec3 pixel_color = vec3(float(pixel.x) / resolution.x, float(pixel.y) / resolution.y, 0.0);

    // Get the index of this invocation in the buffer:
    uint idx = resolution.x * pixel.y + pixel.x;
    pixels[idx] = vec4(pixel_color, 1.0);
}