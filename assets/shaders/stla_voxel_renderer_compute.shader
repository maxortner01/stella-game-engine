[shader(compute)]
#version 460
#extension GL_EXT_ray_query : require
#extension GL_EXT_debug_printf : require

layout (local_size_x = 16, local_size_y = 8, local_size_z = 1) in;

layout (set = 0, binding = 0) buffer pixel_buffer_block
{
    vec4 g_pixels[];
};

void main()
{
    uvec2 resolution = uvec2(1600, 900);
    const uvec2 pixel      = gl_GlobalInvocationID.xy;
    const uint pixel_index = resolution.x * pixel.y + pixel.x;

    // If the pixel is outside of the image, don't do anything:
    if ((pixel.x >= resolution.x) || (pixel.y >= resolution.y)) {
        return;
    }

    const vec3 pixel_color = vec3(float(pixel.x) / resolution.x, float(pixel.y) / resolution.y, 0.0);
    g_pixels[pixel_index] = vec4(1.0, 0.0, 0.0, 1.0);
}
