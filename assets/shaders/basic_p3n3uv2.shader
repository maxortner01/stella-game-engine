[shader(vertex)]
#version 460

layout (location = 0) in vec3 a_position;
layout (location = 1) in vec3 a_normal;
layout (location = 2) in vec2 a_uv;

layout (set = 0, binding = 0) uniform uniform_buffer {
	mat4 view_projection;
	mat4 transform;
} uniform_data;

layout (set = 0, binding = 1) uniform uniform_buffer_1 {
	float red;
} uniform_data_1;

layout (location = 0) out vec3 v_normal;
layout (location = 1) out float v_red;

void main()
{
	v_red       = uniform_data_1.red;
	v_normal    = a_normal;
	gl_Position = uniform_data.view_projection * uniform_data.transform * vec4(a_position, 1.0f);
}

[shader(fragment)]
#version 460

// Shader input
layout (location = 0) in vec3 v_normal;
layout (location = 1) in float v_red;

// Shader output
layout (location = 0) out vec4 out_frag_color;

void main()
{
	//return red
	out_frag_color = vec4(vec3(v_normal.x, v_red, v_normal.z), 1.0f);
}
