[shader(compute)]
#version 460
#extension GL_EXT_ray_query : require

layout(local_size_x = 16, local_size_y = 8, local_size_z = 1) in;

layout(set = 0, binding = 0) buffer pixel_buffer_block
{
    vec4 pixels[];
};

layout(set = 0, binding = 1) uniform scene_data_block 
{
    vec4   background_color;
    vec3   camera_position;
};

layout(set = 0, binding = 2) uniform accelerationStructureEXT tlas;

layout(set = 0, binding = 3) buffer vertices_block
{
  vec3 vertices[];
};

layout(set = 0, binding = 4) buffer indices_block
{
  uint indices[];
};

void main()
{
    const uvec2 resolution = uvec2(800, 600);
    const uvec2 pixel = gl_GlobalInvocationID.xy;

    // If the pixel is outside of the image, don't do anything:
    if ((pixel.x >= resolution.x) || (pixel.y >= resolution.y)) {
        return;
    }

    // Create a vector of 3 floats with a different color per pixel.
    //const vec3 pixel_color = vec3(float(pixel.x) / resolution.x, float(pixel.y) / resolution.y, 0.0);

    // Get the index of this invocation in the buffer:
    //uint idx = resolution.x * pixel.y + pixel.x;
    //pixels[idx] = vec4(pixel_color, 1.0);

    // This scene uses a right-handed coordinate system like the OBJ file format, where the
    // +x axis points right, the +y axis points up, and the -z axis points into the screen.
    // The camera is located at (-0.001, 1, 6).
    const vec3 cameraOrigin = camera_position;// vec3(-0.001, 1.0, 6.0);
    
    // Rays always originate at the camera for now. In the future, they'll
    // bounce around the scene.
    vec3 rayOrigin = cameraOrigin;

    // Compute the direction of the ray for this pixel. To do this, we first
    // transform the screen coordinates to look like this, where a is the
    // aspect ratio (width/height) of the screen:
    //           1
    //    .------+------.
    //    |      |      |
    // -a + ---- 0 ---- + a
    //    |      |      |
    //    '------+------'
    //          -1
    const vec2 screenUV = vec2(2.0 * (float(pixel.x) + 0.5 - 0.5 * resolution.x) / resolution.y,   
                                -(2.0 * (float(pixel.y) + 0.5 - 0.5 * resolution.y) / resolution.y));  // Flip the y axis


    // Next, define the field of view by the vertical slope of the topmost rays,
    // and create a ray direction:
    const float fovVerticalSlope = 1.0 / 5.0;
    vec3        rayDirection     = vec3(fovVerticalSlope * screenUV.x, fovVerticalSlope * screenUV.y, -1.0);

    // Trace the ray and see if and where it intersects the scene!
    // First, initialize a ray query object:
    const float t_max = 10000.0;
    rayQueryEXT ray_query;
    rayQueryInitializeEXT(ray_query,                // Ray query
                            tlas,                  // Top-level acceleration structure
                            gl_RayFlagsOpaqueEXT,  // Ray flags, here saying "treat all geometry as opaque"
                            0xFF,                  // 8-bit instance mask, here saying "trace against all instances"
                            rayOrigin,             // Ray origin
                            0.0,                   // Minimum t-value
                            rayDirection,          // Ray direction
                            t_max);              // Maximum t-value

    // Start traversal, and loop over all ray-scene intersections. When this finishes,
    // ray_query stores a "committed" intersection, the closest intersection (if any).
    while (rayQueryProceedEXT(ray_query)) { }

    // Get the t-value of the intersection (if there's no intersection, this will
    // be tMax = 10000.0). "true" says "get the committed intersection."
    const float t = rayQueryGetIntersectionTEXT(ray_query, true);
    uint pixel_index = resolution.x * pixel.y + pixel.x;
    vec4 final_color = background_color;


    if (rayQueryGetIntersectionTypeEXT(ray_query, true) == gl_RayQueryCommittedIntersectionTriangleEXT) {
        // triangle index
        const int primitive_id = rayQueryGetIntersectionPrimitiveIndexEXT(ray_query, true);
        
        // The triangle used indices: 3 * primitive_id, 3 * primitive_id + 1, and 3 * primitive_id + 2
        
        // Get the index of the first vertex of the triangle
        const uint i0 = indices[3 * primitive_id];
        
        // Get the position of the first vertex
        const vec3 v0 = vertices[i0];

        final_color = vec4(vec3(0.5) + 0.25 * v0, 1.0);
        
        //= vec4(primitive_id / 10.0, primitive_id / 100.0, primitive_id / 1000.0, 1.0);
        
    }
    //else {
        // Ray hit the sky
    //    final_color = vec4(0.0, 0.0, 0.5, 1.0);
    //}

    pixels[pixel_index] = final_color;
}