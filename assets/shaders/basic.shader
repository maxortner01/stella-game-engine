#type VERTEX
#version 450

layout (location = 0) in vec3 a_position;
layout (location = 1) in vec3 a_color;

layout(set = 0, binding = 0) uniform camera_buffer {
	mat4 view_projection;
} camera_data;

void main()
{
	//output the position of each vertex
	gl_Position = camera_data.view_projection * vec4(a_position, 1.0f);
}

#endtype
#type FRAGMENT
#version 450

//output write
layout (location = 0) out vec4 outFragColor;

void main()
{
	//return red
	outFragColor = vec4(1.f,0.f,0.f,1.0f);
}
#endtype
