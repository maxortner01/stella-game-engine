[shader(compute)]
#version 460

struct particle 
{
	vec2 position;
	vec2 velocity;
    vec4 color;
};

layout (binding = 0) uniform particle_ubo_block {
    float dt;
} particle_ubo;

#define MAX_PARTICLES (512 * 1000)

layout(std140, binding = 1) buffer particle_ssbo_in_block {
   particle particles_in[MAX_PARTICLES];
};

layout(std140, binding = 2) buffer particle_ssbo_out_block {
   particle particles_out[MAX_PARTICLES];
};

layout (local_size_x = 32, local_size_y = 1, local_size_z = 1) in;

void main() 
{
    uint index = gl_GlobalInvocationID.x;  

    particle particleIn = particles_in[index];

    particles_out[index].position = particleIn.position + particleIn.velocity.xy * particle_ubo.dt;
    particles_out[index].velocity = particleIn.velocity;
    particles_out[index].color    = particleIn.color;

    // Flip movement at window border
    if ((particles_out[index].position.x <= -1.0) || (particles_out[index].position.x >= 1.0)) {
        particles_out[index].velocity.x = -particles_out[index].velocity.x;
    }

    if ((particles_out[index].position.y <= -1.0) || (particles_out[index].position.y >= 1.0)) {
        particles_out[index].velocity.y = -particles_out[index].velocity.y;
    }

    particles_in[index] = particles_out[index];
}